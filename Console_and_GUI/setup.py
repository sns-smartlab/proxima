import sys

print("Absolute path of the build directory of the Proxima library: ")
proximaPath = sys.stdin.readline().rstrip('\n')

print("Absolute path of the Eigen linear algebra library: ")
eigenPath = sys.stdin.readline().rstrip('\n')

text = "TEMPLATE = app\nCONFIG += console c++11\nCONFIG -= app_bundle\nCONFIG -= qt\nQMAKE_CXXFLAGS += -fopenmp\nLIBS += -fopenmp\nINCLUDEPATH += "+eigenPath+"\nDEPENDPATH  += "+eigenPath+"\nSOURCES += ./ProximaConsole/main.cpp \ \n ./ProximaConsole/proximainterface.cpp \nHEADERS += \ \n ./ProximaConsole/proximainterface.h \n\nwin32:CONFIG(release, debug|release): LIBS += -L"+proximaPath+"Proxima/release/ -lProxima\nelse:win32:CONFIG(debug, debug|release): LIBS += -L"+proximaPath+"Proxima/debug/ -lProxima\nelse:unix: LIBS += -L"+proximaPath+"Proxima/ -lProxima\n\nINCLUDEPATH += $$PWD/../Src\n\nDEPENDPATH += $$PWD/../Src\nwin32-g++:CONFIG(release, debug|release): PRE_TARGETDEPS += "+proximaPath+"Proxima/release/libProxima.a\nelse:win32-g++:CONFIG(debug, debug|release): PRE_TARGETDEPS += "+proximaPath+"Proxima/debug/libProxima.a\nelse:win32:!win32-g++:CONFIG(release, debug|release): PRE_TARGETDEPS += "+proximaPath+"Proxima/release/Proxima.lib\nelse:win32:!win32-g++:CONFIG(debug, debug|release): PRE_TARGETDEPS += "+proximaPath+"Proxima/debug/Proxima.lib\nelse:unix: PRE_TARGETDEPS += "+proximaPath+"Proxima/libProxima.a"

out = open("ProximaConsole_test.pro", "w")
out.write(text)
out.close()
