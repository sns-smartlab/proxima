## ProximaConsole

In order to compile the ProximaConsole application, you need to first compile the Core C++ Proxima code.
Then, you have to execute the _setup.py_ script. This script, asks for the absolute path of the build directory of the Proxima library and for the absolute path of the eigen linear algebra library required by Proxima.

This script will generate a ProximaConsole.pro file that you can open with software such as the QtCreator suite in order to compile the ProximaConsole code.


