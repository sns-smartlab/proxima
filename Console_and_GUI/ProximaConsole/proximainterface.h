#ifndef PROXIMAINTERFACE_H
#define PROXIMAINTERFACE_H

#include <Proxima/molecularsystem.h>
#include <Proxima/parser.h>

using namespace Proxima;


class ProximaInterface
{
public:
    ProximaInterface();
    void loadFile(std::string path, double total_charge=0, int spinMultiplicity=1);
    void printFile(std::string path, bool computeCycles=true, bool computeBridge=false, bool gasteiger=false, bool fq=false, bool mep=false, bool hbonds=false, bool bo=false, size_t frameNum=0, std::string format="json");


private:
    MolecularSystemSP ms;
};

typedef std::shared_ptr<ProximaInterface> ProximaInterfaceSP;
#endif // PROXIMAINTERFACE_H
