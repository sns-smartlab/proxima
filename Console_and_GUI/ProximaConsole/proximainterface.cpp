#include "proximainterface.h"

ProximaInterface::ProximaInterface()
{
    this->ms = nullptr;
}

void ProximaInterface::loadFile(std::string path, double total_charge, int spinMultiplicity)
{
    if (path=="")
        throw std::invalid_argument("Invalid input path");

    size_t found_xyz = path.find(".xyz");
    size_t found_XYZ = path.find(".XYZ");

    size_t found_pdb = path.find(".pdb");
    size_t found_PDB = path.find(".PDB");

    size_t found_mol = path.find(".mol");
    size_t found_MOL = path.find(".MOL");

    //If it is an xyz file...
    if ((found_xyz!=std::string::npos || found_XYZ!=std::string::npos) &&
            !(found_PDB!=std::string::npos || found_pdb!=std::string::npos) &&
              !(found_mol!=std::string::npos || found_MOL!=std::string::npos))
    {
        this->ms = Parser::readXYZ(path, "MolecularSystem",total_charge);
        this->ms->setSpinMultiplicity(spinMultiplicity);
    }

    //If it is a pdb file
    if (!(found_xyz!=std::string::npos || found_XYZ!=std::string::npos) &&
            (found_PDB!=std::string::npos || found_pdb!=std::string::npos) &&
                !(found_mol!=std::string::npos || found_MOL!=std::string::npos))
    {
        this->ms = Parser::readPDB(path, "MolecularSystem",total_charge);
        this->ms->setSpinMultiplicity(spinMultiplicity);
    }

    //If it is a mol file
    if (!(found_xyz!=std::string::npos || found_XYZ!=std::string::npos) &&
            !(found_PDB!=std::string::npos || found_pdb!=std::string::npos) &&
                (found_mol!=std::string::npos || found_MOL!=std::string::npos))
    {
        this->ms = Parser::readMOL(path, "MolecularSystem",total_charge);
        this->ms->setSpinMultiplicity(spinMultiplicity);
    }
}

void ProximaInterface::printFile(std::string path, bool computeCycles, bool computeBridge, bool gasteiger, bool fq, bool mep, bool hbonds,  bool bo, size_t frameNum, std::string format)
{
    if (this->ms==nullptr)
        throw std::invalid_argument("Invalid input molecular system");

    this->ms->computeBonds_Fast();

    if (computeCycles)
        this->ms->computeHortonCycles(frameNum);

    if (computeBridge)
        this->ms->computeHortonCycles(frameNum);

    if (bo)
    {

        this->ms->computeBondOrders(frameNum);
        //this->ms->computeNormalizedBondOrders(frameNum);
        this->ms->normalizeBO(frameNum);
    }

    if ((gasteiger && fq) || (mep && !gasteiger && !fq))
    {
        std::cerr<<"Invalid input parameters. Both gasteiger and fq charge methods selected"<<std::endl;
    }

    else {

        if (gasteiger)
            this->ms->computeGasteigerCharges(frameNum);

        if (fq)
            this->ms->computeFQCharges(frameNum);

        if (mep)
        {
            Proxima::CubeGridSP v = this->ms->computeElectrostaticPotential(0.8,0.8,0.8,frameNum);
            v->printCubeFile(path+"_pot.cube","Electrostatic Potential");
        }

        if (hbonds)
            this->ms->computeHBondsInFrame_Fast(0);
    }

    if (format=="mol3000")
    {
        std::cout<<"L'ho convertito in mol"<<std::endl;
        //this->ms->toMOL_V3000(path + ".mol", frameNum);
        this->ms->toMol2(path + ".mol", frameNum);
    }
    else if (format=="mol2000")
        this->ms->toMOL_V2000(path + ".mol",frameNum);
    else if (format=="json")
        this->ms->toJSON(path + ".json",frameNum,computeBridge);

}
