#include "proximainterface.h"
#include <omp.h>

using namespace std;


int main(int argc, char *argv[])
{
    std::string inFile = "";
    std::string outFile = "";
    double totalCharge=0;
    int spinMultiplicity=1;
    bool computeCycles=false;
    bool gasteiger=false;
    bool fq = false;
    bool mep = false;
    bool hbonds = false;
    bool bo=false;
    ProximaInterfaceSP proxima(new ProximaInterface);


    std::set<std::string> arguments{"-i", "-o", "-cycles", "-mep", "-gasteiger", "-fq", "-mep", "-hbonds", "-charge", "-spin", "-bo"};

    //Lettura dei parametri in input
    for (int i = 0; i < argc; ++i)
    {
        std::string currentArg = argv[i];


        if (currentArg=="-i")
        {

            if (i==argc-1)
                break;

            std::string nextArg = argv[i+1];



            if (arguments.find(nextArg)==arguments.end())
            {
                inFile = argv[i+1];
                i++;
            }

        }
        else if (currentArg=="-o")
        {
            if (i==argc-1)
                break;

            std::string nextArg = argv[i+1];



            if (arguments.find(nextArg)==arguments.end())
            {
                outFile = argv[i+1];
                i++;
            }
        }

        else if (currentArg=="-charge")
        {
            if (i==argc-1)
                break;

            std::string nextArg = argv[i+1];



            if (arguments.find(nextArg)==arguments.end())
            {
                totalCharge = std::stod(argv[i+1]);
                i++;
            }
        }


        else if (currentArg=="-spin")
        {
            if (i==argc-1)
                break;

            std::string nextArg = argv[i+1];



            if (arguments.find(nextArg)==arguments.end())
            {
                spinMultiplicity = std::stoi(argv[i+1]);
                i++;
            }
        }


        else if (currentArg=="-cycles")
        {
            computeCycles=true;
        }

        else if (currentArg=="-bo")
        {
            bo=true;
        }

        else if (currentArg=="-mep")
        {
            //if (fq==false && gasteiger==false)
            //    std::cerr << "Error! Cannot compute MEP without charges" <<std::endl;
            mep=true;
        }

        else if (currentArg=="-hbonds")
        {
            hbonds=true;
        }

        else if (currentArg=="-gasteiger")
        {
            if (fq==true)
                std::cerr<<"Error! Only one method for charge computation must be selected"<<std::endl;
            else
                gasteiger=true;
        }

        else if (currentArg=="-fq")
        {
            if (gasteiger==true)
                std::cerr<<"Error! Only one method for charge computation must be selected"<<std::endl;
            else
                fq=true;
        }

    }

    //Loading the input file
    proxima->loadFile(inFile,totalCharge,spinMultiplicity);

    //Outputting the file
    proxima->printFile(outFile,false,false,false,false,false,false,false,0,"mol3000");

    proxima->printFile(outFile,computeCycles,false,gasteiger,fq,mep,hbonds,bo,0,"json");

    return 0;
}


