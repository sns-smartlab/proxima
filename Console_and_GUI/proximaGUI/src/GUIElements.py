#Importing Modules



from abc import ABC, abstractmethod
import Perception


import os
import time
import math
import threading
from copy import deepcopy 
import ntpath
from sys import version_info
import socket
import json
from shutil import copyfile
from shutil import *
from tkinter import *
from tkinter import filedialog
from tkinter.ttk import *
from tkinter import ttk
from tkinter import messagebox
from pathlib import Path
import numpy as np



#LIST - GESTISCE LE LISTE DI ELEMENTI PRESENTI NELL'INTERFACCIA GRAFICA
#------------------------------
def send_vmscommand(script, HOST='127.0.0.1', PORT=80):
    with socket.socket(socket.AF_INET, socket.SOCK_STREAM) as jmol:
        jmol.connect((HOST, PORT))
        script = (script).encode("UTF-8")
        jmol.send(script)
        data = jmol.recv(1024)
    return repr(data)

def script_to_jmol(script, HOST='127.0.0.1', PORT=80):
    return send_vmscommand("JMOL " + script, HOST, PORT)



class ListGUI:

    def __init__(self, parent, master, name, selectMode):

        self.currentSelected = -1
        self.parent = parent
        self.name = name

        #Inizializzo l'oggetto Listbox
        self.scrollbar = Scrollbar(master)
        self.listobj = Listbox(master, name = self.name, selectmode = selectMode, yscrollcommand=self.scrollbar.set, exportselection=False)
        self.listobj.configure(background='white')
        self.listobj.bind('<<ListboxSelect>>', self.selectedSys)
        self.scrollbar.config(command=self.listobj.yview)

        self.listEl = []


    def selectedSys(self,evt):
        if (not self.parent.isEditor and self.currentSelected!=-1):
            try:
                w = evt.widget
                index = int(w.curselection()[0])
                self.hideCurrentSelection()
                self.currentSelected = index
                self.showCurrentSelection()
            except:
                print("Ho ricevuto un'eccezione")

    #Astratto perché ogni lista deve fare cose diverse quando si seleziona il suo elemento
    @abstractmethod
    def showCurrentSelection(self):
        pass

    #Astratto perché ogni lista deve fare cose diverse quando si nasconde un suo elemento
    @abstractmethod
    def hideCurrentSelection(self):
        pass


    #Gli elementi da aggiungere devono avere name come proprietà interna
    def addElement(self, el):
        if (self.currentSelected!=-1):
            self.hideCurrentSelection()

        self.listEl.append(el)
        self.listobj.insert(len(self.listEl)-1, el.name)
        self.currentSelected = len(self.listEl)-1
        self.showCurrentSelection()


    #Elimina l'elemento selezionato
    def clearElement(self):

        if (self.currentSelected==-1):
            return

        self.hideCurrentSelection()

        # 1) È uno dei tanti sistemi molecolari nella lista (ma non l'ultimo)
        if ((self.currentSelected!=0 and self.currentSelected!=len(self.listEl)-1)  or (self.currentSelected == 0 and len(self.listEl)>1)):
            self.hideCurrentSelection()
            self.listEl.pop(self.currentSelected)
            self.listobj.delete(self.currentSelected)
            self.showCurrentSelection()

        # 2) È l'ultimo sistema molecolare nella lista (ma non è l'unico sistema rimasto)
        elif (self.currentSelected == len(self.listEl)-1 and self.currentSelected !=0):
            self.hideCurrentSelection()
            self.listEl.pop(self.currentSelected)
            self.listobj.delete(self.currentSelected)
            self.currentSelected = self.currentSelected - 1
            self.showCurrentSelection()

        # 3) C'è rimasto solo lui nella lista di sistemi!!!
        else:
            self.hideCurrentSelection()
            self.listEl.pop(self.currentSelected)
            self.listobj.delete(self.currentSelected)
            self.currentSelected = -1

    def clearAll(self):
        while (self.currentSelected!=-1):
            self.clearElement()

    def packAll(self, _side, _fill):
        self.listobj.pack(side=_side, fill=_fill)
        self.scrollbar.pack(side=RIGHT, fill=Y)

    def unpackAll(self):
        self.listobj.pack_forget()
        self.scrollbar.pack_forget()

    def getCurrentElement(self):
        if (self.currentSelected==-1):
            return -1
        return self.listEl[self.currentSelected]


#CLASSI WRAPPER PER LA LIBRERIA PERCEPTION
#TODO:oggetto perception da metterci dentro
#--------------------------------------------
class CycleGUI:
    def __init__(self, atoms, name):
        self.atoms = atoms
        self.name = name

class BondGUI:
    def __init__(self, bond, order, name):
        self.bond=bond
        self.order=order
        self.name = name

class HBondGUI:
    def __init__(self, atoms, name):
        self.atoms=atoms
        self.name=name
#--------------------------------------------
        
        

class ListCycle(ListGUI):

    def showCurrentSelection(self):
        script_to_jmol("select all; halo off; color none")
        cycl = self.getCurrentElement()
        for atom in cycl.atoms:
            script_to_jmol("select atomno=" + str(atom) + "; halo; color atoms forestgreen; select none")


    def hideCurrentSelection(self):
        script_to_jmol("select all; halo off; color none")




class ListBond(ListGUI):

    def hideCurrentSelection(self):
        script_to_jmol("select all; halo off; color none")

            
    def showCurrentSelection(self):
        script_to_jmol("select all; halo off; color none")
        bond = self.getCurrentElement()
        #print("select atomno="+str(bond.bond[0])+"; halo; color atoms red; select atomno="+str(bond.bond[1])+"; halo; color atoms red; select none")
        script_to_jmol("select atomno="+str(bond.bond[0])+"; halo; color atoms red; select atomno="+str(bond.bond[1])+"; halo; color atoms red; select none")



class ListHBond(ListGUI):


    def hideCurrentSelection(self):
        script_to_jmol("select all; halo off; color none")

            
    def showCurrentSelection(self):
        script_to_jmol("select all; halo off; color none")
        hb = self.getCurrentElement()
        for atom in hb.atoms:
            script_to_jmol("select atomno=" + str(atom) + "; halo; color atoms aqua; select none")





#SISTEMA MOLECOLARE - GESTISCE LE SUE INFORMAZIONI NELL'AMBITO DELLA GUI
#-----------------------------------------------------------------------
class MolGUI:

    def __init__(self, path, parentGUI):

        self.parentGUI = parentGUI

        self.isCharges=False
        self.isRings=False
        self.isHBonds=False
        #Metterlo a false per disattivare
        self.isBO=True
        self.isComputed=False

        self.PATH=path
        self.name=self.extractName(path)


        self.dipole=[]

        #INIZIALIZZO I CONTENITORI DI LISTE
        #----------------------------------------------------------------
        self.frags = ListCycle(self.parentGUI, self.parentGUI.fragFrame, "cycles "+str(self.name), SINGLE)
        self.bonds = ListBond(self.parentGUI, self.parentGUI.bondFrame, "bonds "+str(self.name), SINGLE)
        self.bridgebonds = ListBond(self.parentGUI, self.parentGUI.bondFrame, "bridgebonds "+str(self.name), SINGLE)
        self.hbonds = ListHBond(self.parentGUI, self.parentGUI.hbondFrame, "hbonds "+str(self.name), SINGLE)

        command = os.path.join(self.parentGUI.current_folder,"ProximaConsole") + " -i " + self.PATH  + " -o " + os.path.join(self.parentGUI.vms_folder,self.name)
        print("command: ", command)
        os.system(command)


        #try:
        self.ms = Perception.MolecularSystem(os.path.join(self.parentGUI.vms_folder,self.name)+".json")
        
        
        idx=0

            #self.parentGUI.bonds.clearAll()
        
#            for b in self.ms.bondSet:
#                
#                atom1 = b.minor
#                atom2 = b.higher
#                order = b.order
#                
#                newBond = [atom1,atom2]
#                print("Sto trattando il legame ", atom1, ", ", atom2, ", ", order)
#
#                #newBondGUI = BondGUI(newBond, order, str(order))
#                #self.parentGUI.bonds.addElement(newBondGUI)
#
#                #self.parentGUI.isBO = True


        for bndd in self.ms.bondSet:
            atom1 = int(bndd.minor) + 1
            atom2 = int(bndd.higher) + 1
            #TODO: bnd.order
            order = bndd.order
            
            print("Atom1: ", atom1)
            print("Atom2: ", atom2)
            
            bnd = [atom1, atom2]
            #bnd = Perception.Bond(idx,atom1,atom2)
            
            newBnddGUI = BondGUI(bnd, order, str(order))
            self.bonds.addElement(newBnddGUI)
            
            idx=idx+1
            
        self.packAll()

        #except:
            #print("Advanced perception features not available on this system")

    def extractName(self,path):
        head, tail = ntpath.split(path)
        name = tail or ntpath.basename(head)
        return name.split(".")[0]


    def printSymmetricRings(self):

        self.ms.computeSymmetry()
        
        out = []
        
        #TODO: Leggi i frammenti dal JSON
        cicl = self.ms.loadCycles(os.path.join(self.parentGUI.vms_folder,self.name)+".json")
        
        #TODO: Calolca i frammenti equivalenti
        symmc = self.ms.findAllSymmetricFrags(cicl)
        
        #TODO: Mostra i frammenti equivalenti in una lista separata
        i=1
        for c in symmc:
            
            atomSet = set()
            
            for bnd in c:
                atomSet.add(bnd.minor+1)
                atomSet.add(bnd.higher+1)
                
            newCycle = CycleGUI(atomSet, "Cycle "+ str(i) + " - "+ str(len(c)))
            out.append(newCycle)
            i=i+1
            
            
        return out
        

    def __del__(self):
        self.frags.clearAll()
        self.frags.unpackAll()
        del self.frags

        self.hbonds.clearAll()
        self.hbonds.unpackAll()
        del self.hbonds

        self.bonds.clearAll()
        self.bonds.unpackAll()
        del self.bonds


    def packAll(self):
        if (self.isComputed==False):
            self.parentGUI.computeButton.pack()

        if (self.isCharges==True):
            self.parentGUI.chargeFrame.pack()
            self.parentGUI.chargeSec.pack()
            self.parentGUI.computeChargesButton.pack(side=LEFT)
            self.parentGUI.showDipoleButton.pack(side=LEFT)
    
        if (self.isRings==True):
            self.parentGUI.fragFrame.pack()
            self.parentGUI.fragSec.pack()
            self.parentGUI.hideFrags.pack(side=RIGHT)

            self.parentGUI.symmFrags.pack(side=RIGHT)

            self.frags.packAll(RIGHT,BOTH)

        if (self.isBO==True):
            
            self.parentGUI.bondSec.pack()
            self.parentGUI.bondFrame.pack()
            self.parentGUI.hideBonds.pack(side=RIGHT)
            self.bonds.packAll(RIGHT,BOTH)

        if (self.isHBonds==True):
            self.parentGUI.hbondSec.pack()
            self.parentGUI.hbondFrame.pack()
            self.parentGUI.hideHBonds.pack(side=RIGHT)
            self.hbonds.packAll(RIGHT,BOTH)


    def unpackAll(self):

        if (self.isComputed==False):
            self.parentGUI.computeButton.pack_forget()

        if (self.isCharges==True):
            self.parentGUI.computeChargesButton.pack_forget()
            self.parentGUI.showDipoleButton.pack_forget()
            self.parentGUI.chargeSec.pack_forget()
            self.parentGUI.chargeFrame.pack_forget()

        if (self.isRings==True):
            self.frags.unpackAll()
            self.parentGUI.hideFrags.pack_forget()

            self.parentGUI.symmFrags.pack_forget()


            self.parentGUI.fragSec.pack_forget()
            self.parentGUI.fragFrame.pack_forget()

        if (self.isBO==True):
            self.bonds.unpackAll()
            self.parentGUI.hideBonds.pack_forget()
            self.parentGUI.bondSec.pack_forget()
            self.parentGUI.bondFrame.pack_forget()

        if (self.isHBonds==True):
            self.hbonds.unpackAll()
            self.parentGUI.hideHBonds.pack_forget()
            self.parentGUI.hbondSec.pack_forget()
            self.parentGUI.hbondFrame.pack_forget()

    def loadCycles(self):
        json_file = open(os.path.join(self.parentGUI.vms_folder,self.name+".json"),'r')
        json_data = json.load(json_file)
        json_file.close()
        frags = json_data["section_run"]["section_single_configuration_calculation"]["vms_proxima"]["vms_frags"]
            
        self.frags.clearAll()

        if (len(frags) > 0):
            self.isRings=True
        for c in frags:
            name = c["fr_name"]
            indexes = c["fr_index"]
            currentCycle = CycleGUI(name, indexes)
            self.frags.addElement(currentCycle)

    def loadBonds(self):
        json_file = open(os.path.join(self.parentGUI.vms_folder,self.name+".json"),'r')
        print("The JSON file: ", json_file)
        json_data = json.load(json_file)
        json_file.close()
        bonds = json_data["section_run"]["section_single_configuration_calculation"]["vms_proxima"]["vms_bonds"]
        idx=0
        
        self.bonds.clearAll()
        if (len(bonds) > 0):
            self.isBO=True
        for b in bonds:
            atom1 = b["bond_atoms"][0]
            atom2 = b["bond_atoms"][1]
            order = b["bond_order"]
            newBond = Perception.Bond(idx,atom1,atom2)
            newBondGUI = BondGUI(newBond,order,str(order))
            idx = idx+1
            self.bonds.addElement(newBondGUI)



    def loadHBonds(self):
        json_file = open(os.path.join(self.parentGUI.vms_folder,self.name+".json"),'r')
        json_data = json.load(json_file)
        json_file.close()
        hbs = json_data["section_run"]["section_single_configuration_calculation"]["vms_proxima"]["vms_hbonds"]


        self.hbonds.clearAll()
        if (len(hbs) > 0):
            self.isHBonds=True
        for hb in hbs:
            intensity = hb["hb_force"]
            atoms = hb["hb_atoms"]
            newHBond = HBondGUI(atoms,intensity)
            self.hbonds.addElement(newHBond)
        

    def showMOL(self):
        if (self.PATH==""):
            messagebox.showinfo("Error", "Please, select an input file.")
        else:
            try:
                script_to_jmol(r'load '+ os.path.join(self.parentGUI.vms_folder,self.name+'.mol'))
            except:
                print("Connection refused")


    def computeDipole(self):
        if (self.isCharges==True):
            
            json_file = open(os.path.join(self.parentGUI.vms_folder,self.name+".json"),'r')
            json_data = json.load(json_file)
            json_file.close()

            charg = json_data["section_run"]["section_single_configuration_calculation"]["vms_proxima"]["vms_charges"]
            idx=1

            position = json_data["section_run"]["section_system"]["atom_positions"]

            dip = [0,0,0]

            for c in charg:
                dip[0] = dip[0]+c*position[idx-1][0]
                dip[1] = dip[1]+c*position[idx-1][1]
                dip[2] = dip[2]+c*position[idx-1][2]

                idx=idx+1

            dip[0] = dip[0]*4.80268770983
            dip[1] = dip[1]*4.80268770983
            dip[2] = dip[2]*4.80268770983

            self.dipole = dip


    
    #Mostra le cariche
    def showCharges(self):
        if ((not self.parentGUI.isEditor) and (self.isCharges==True)):
            if (self.parentGUI.computeChargesButton['text']=="Show Charges"):
                json_file = open(os.path.join(self.parentGUI.vms_folder,self.name+".json"),'r')
                json_data = json.load(json_file)
                json_file.close()

                charg = json_data["section_run"]["section_single_configuration_calculation"]["vms_proxima"]["vms_charges"]
                idx=1
                for c in charg:
                    command = "select none; select atomno=" + str(idx) + "; label \"" + str(c) + "\""   
                    script_to_jmol(command)
                    idx=idx+1
                self.parentGUI.computeChargesButton.config(text="Hide Charges")
            else:
                self.parentGUI.hideLabels()
                self.parentGUI.computeChargesButton.config(text="Show Charges")


    def showDipole(self):
        if ((not self.parentGUI.isEditor) and (self.isCharges==True)):
            if (self.parentGUI.showDipoleButton['text']=="Show Dipole"):
                currentDip = self.dipole
                dipoleMoment = math.sqrt(currentDip[0]*currentDip[0]+currentDip[1]*currentDip[1]+currentDip[2]*currentDip[2])
                if (currentDip==[]):
                    self.computeDipole()
                script_to_jmol("draw dipoleVec diameter 0.1 arrow {0.0, 0.0, 0.0} {"+str(currentDip[0])+","+str(currentDip[1])+","+str(currentDip[2])+"}; set echo dipoleVec; echo "+str(dipoleMoment)+" D")
                self.parentGUI.showDipoleButton.config(text="Hide Dipole")
            else:
                script_to_jmol("draw dipoleVec delete; echo")
                self.parentGUI.showDipoleButton.config(text="Show Dipole")



#DEFINISCI L'OGGETTO ListMOL !!!!
#LIST MOL - GESTISCE LA LISTA DEI SISTEMI MOLECOLARI
#------------------------------
class ListMol(ListGUI):

    def showCurrentSelection(self):
        newMol = self.getCurrentElement()
        if (newMol!=-1):
            newMol.packAll()
            newMol.showMOL()

    def hideCurrentSelection(self):
        try:
            script_to_jmol("select all; delete; select none")
        except:
            print("Connection refused")
        mol = self.getCurrentElement()
        if (mol!=-1):
            mol.unpackAll()
        









