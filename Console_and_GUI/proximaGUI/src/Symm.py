import numpy as np
import json
import sys
from abc import ABC, abstractmethod, abstractproperty
from itertools import combinations, permutations
import math

def showCycle(cycle):
    for bond in cycle:
        print("NEW BOND: ", bond.minor, ", ", bond.higher)
        
def showCycles(allCycles):
    for cycle in allCycles:
        print("------ NEW CYCLE -------")
        showCycle(cycle)
        

def isArrayEqual(array1, array2, tol=2e-1):
    if (len(array1)!=len(array2)):
        return False
    idx=0
    for el in array1:
        if (np.isclose(el, array2[idx], 2e-1)):
            idx = idx+1
        else:
            return False

    return True


def equiv_atoms(x, ian, natoms, tol=0.2):

    # Compute the distance matrix D:
    D = np.zeros((natoms,natoms))
    for i in range(natoms):
        for j in range(i):
            D[i,j] = np.linalg.norm(x[i,:]-x[j,:])
            D[j,i] = D[i,j]
    #print(' Distance matrix:')
    #for i in range(natoms):
        #print(D[i,:])

    # Initialize eqa, an integer matrix (natoms x natoms) whose off-diagonal
    # elements are equal to 1 when symmetry-equivalent atoms are considered:
    eqa = np.zeros((natoms,natoms), dtype=int) 
    # Check the columns:
    for i in range(natoms):
        for j in range(natoms):
            if ((i != j) and (ian[i] == ian[j])):
                #if np.array_equal(np.sort(D[:,i]), np.sort(D[:,j])):
                #print("array1: ", np.sort(D[:,i]))
                #print("array2: ", np.sort(D[:,j]))
                if isArrayEqual(np.sort(D[:,i]), np.sort(D[:,j]), tol):
                    eqa[i,j] = 1

    #print("EQA (before diagonal): ", eqa)

    # Each diagonal element of eqa is equal to the number of symmetry-equivalent
    # atoms:
    for i in range(natoms):
        eqa[i,i] = np.count_nonzero(eqa[i,:]) + 1
    #print('Equivalent atom matrix:')
    #for i in range(natoms):
        #print(eqa[i,:]) 
    return eqa


def equivalentBond(simm, x, bond, ms):
    output = []

    # Compute the distance matrix D:
    D = np.zeros((ms.getNumAtoms(),ms.getNumAtoms()))
    for i in range(ms.getNumAtoms()):
        for j in range(i):
            D[i,j] = distance.euclidean(x[i,:], x[j,:]) 
            D[j,i] = D[i,j]
    #print(' Distance matrix:')
    #for i in range(ms.getNumAtoms()):
        #print(D[i,:])

    #Prendo gli atomi del legame
    atom1 = ms.getAtom(bond.getMinor())
    atom2 = ms.getAtom(bond.getHigher())

    #Prendo gli atomi simmetrici
    simm1 = simm[bond.getMinor(),:]
    simm2 = simm[bond.getHigher(),:]

    other1 = 0

    #Per ogni simmetrico del primo atomo
    while (other1<ms.getNumAtoms()):
        if (simm1[other1]!=1):
            other1=other1+1
            continue

        atomA = ms.getAtom(other1+1)

        #Per ogni simmetrico del secondo atomo
        other2=0
        while (other2<ms.getNumAtoms()):
            if (simm2[other2]!=1):
                other2=other2+1
                continue

            atomB = ms.getAtom(other2+1)

            #print("----------------------------------------")

            #print("CONTROLLO LA COPPIA: ", other1+1, ", ", other2+1)

            #Controlla se c'è un legame tra loro
            if (D[other1,other2]<=1.6 and other1!=other2):
                    #print("Ok, qui c'è un legame")
                    b = pyproxima.PyBond(other1+1,other2+1,1,False)
                    output.append(b)

            other2=other2+1

        other1 = other1+1

    return output
