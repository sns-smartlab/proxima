import Symm
import numpy as np
import json
import sys
import copy
from abc import ABC, abstractmethod, abstractproperty
from itertools import combinations, permutations
import math

covRad = {1: 0.31, 6: 0.76, 7: 0.71, 8: 0.66}


atomNum = {}
atomNum["H"]=1
atomNum["HE"]=2
atomNum["LI"]=3
atomNum["BE"]=4
atomNum["B"]=5
atomNum["C"]=6
atomNum["N"]=7
atomNum["O"]=8
atomNum["F"]=9
atomNum["NE"]=10
atomNum["NA"]=11
atomNum["MG"]=12
atomNum["AL"]=13
atomNum["SI"]=14
atomNum["P"]=15
atomNum["S"]=16
atomNum["CL"]=17
atomNum["AR"]=18
atomNum["K"]=19
atomNum["CA"]=20
atomNum["SC"]=21
atomNum["TI"]=22
atomNum["V"]=23
atomNum["CR"]=24
atomNum["MN"]=25
atomNum["FE"]=26
atomNum["CO"]=27
atomNum["NI"]=28
atomNum["CU"]=29
atomNum["ZN"]=30
atomNum["GA"]=31
atomNum["GE"]=32
atomNum["AS"]=33
atomNum["SE"]=34
atomNum["BR"]=35
atomNum["KR"]=36
atomNum["RB"]=37
atomNum["SR"]=38
atomNum["Y"]=39
atomNum["ZR"]=40
atomNum["NB"]=41
atomNum["MO"]=42
atomNum["TC"]=43
atomNum["RU"]=44
atomNum["RH"]=45
atomNum["PD"]=46
atomNum["AG"]=47
atomNum["CD"]=48
atomNum["IN"]=49
atomNum["SN"]=50
atomNum["SB"]=51
atomNum["TE"]=52
atomNum["I"]=53
atomNum["XE"]=54
atomNum["CS"]=55
atomNum["BA"]=56
atomNum["LA"]=57
atomNum["CE"]=58
atomNum["PR"]=59
atomNum["ND"]=60
atomNum["PM"]=61
atomNum["SM"]=62
atomNum["EU"]=63
atomNum["GD"]=64
atomNum["TB"]=65
atomNum["DY"]=66
atomNum["HO"]=67
atomNum["ER"]=68
atomNum["TM"]=69
atomNum["YB"]=70
atomNum["LU"]=71
atomNum["HF"]=72
atomNum["TA"]=73
atomNum["W"]=74
atomNum["RE"]=75
atomNum["OS"]=76
atomNum["IR"]=77
atomNum["PT"]=78
atomNum["AU"]=79
atomNum["HG"]=80
atomNum["TL"]=81
atomNum["PB"]=82
atomNum["BI"]=83
atomNum["PO"]=84
atomNum["AT"]=85
atomNum["RN"]=86
atomNum["FR"]=87
atomNum["RA"]=88
atomNum["AC"]=89
atomNum["TH"]=90
atomNum["PA"]=91
atomNum["U"]=92
atomNum["NP"]=93
atomNum["PU"]=94
atomNum["AM"]=95
atomNum["CM"]=96
atomNum["BK"]=97
atomNum["CF"]=98
atomNum["ES"]=99
atomNum["FM"]=100
atomNum["MD"]=101
atomNum["NO"]=102
atomNum["LR"]=103
atomNum["RF"]=104
atomNum["DB"]=105
atomNum["SG"]=106
atomNum["BH"]=107
atomNum["HS"]=108
atomNum["MT"]=109
atomNum["DS"]=110
atomNum["RG"]=111
atomNum["CN"]=112
atomNum["UUT"]=113
atomNum["FL"]=114
atomNum["UUP"]=115
atomNum["LV"]=116
atomNum["UUS"]=117
atomNum["UUO"]=118


class Bond:
    def __init__(self, index, atom1, atom2):
        if (atom1==atom2):
            return
            
        elif (atom1<atom2):
            self.minor = atom1.serial
            self.minorAtom = atom1
            self.higher = atom2.serial
            self.higherAtom = atom2
            
        else:
            self.minor = atom2.serial
            self.minorAtom = atom2
            self.higher = atom1.serial
            self.higherAtom = atom1
            
        self.index = index
        atom1.addBond(self)
        atom2.addBond(self)
        
    def relativeOrientation(self, other):
        
        vec1 = self.higherAtom.pos - self.minorAtom.pos
        vec1 = vec1/(np.linalg.norm(vec1))
        
        vec2 = other.higherAtom.pos - other.minorAtom.pos
        vec2 = vec2/(np.linalg.norm(vec2))
        
        areEqVecs = True
        for i in (0,2):
            if (vec1[i]!=vec2[i]):
                areEqVecs=False
        
        if (areEqVecs):
            return 0    
        return np.arccos(np.dot(vec1,vec2))
        
    def centerOfBond(self):
        distVec = self.higherAtom.pos - self.minorAtom.pos
        return self.minorAtom.pos + distVec/2
        
    def relativeDistance(self,other):
        pos1 = self.centerOfBond()
        pos2 = other.centerOfBond()
        
        return np.linalg.norm(pos2 - pos1)
        
    def otherAtom(self, current):
        if (current==self.minorAtom):
            return self.higherAtom
        elif (current==self.higherAtom):
            return self.minorAtom
        else:
            return False
        
        
    def __gt__(self, bond2):
        if (self.higher > bond2.higher):
            return True
        elif (self.higher==bond2.higher and self.minor > bond2.minor):
            return True
        else:
            return False
        
    def __eq__(self, bond2):
        if (self.higher==bond2.higher and self.minor==bond2.minor):
            return True
        else:
            return False
            
    def __hash__(self):
        return self.index
        
        
            
            
class Atom:
    def __init__(self, serial, atomicNum, pos):
        self.serial = serial
        self.atomicNum = atomicNum
        self.pos = pos
        self.bonds = set()
        
    def __gt__(self, other):
        if (self.serial > other.serial):
            return True
        else:
            return False
            
    def __eq__(self, other):
        if (self.serial == other.serial):
            return True
        else:
            return False
        
    def findBond(self, atom2):
        for bond in self.bonds:
            if (bond.minor==atom2.serial or bond.higher==atom2.serial):
                return bond
            
    def findBond_wSerial(self, atom2):
        for bond in self.bonds:
            if (bond.minor==atom2 or bond.higher==atom2):
                return bond
                
                
    def addBond(self, bond):
        if (bond.minor!=self.serial and bond.higher!=self.serial):
            return False
        self.bonds.add(bond)
        
        
        
        
class Fragment(ABC): 
    
#Overloading
    def __init__(self, path=""):
        self.parentFrag=0
        self.bondSet=set()
        self.atomSet={}
        self.childFrags=[]
        
        #TODO: Funzione per i calcoli dei cicli simmetrici
        self.lastBond=0
        self.isLastBond=False
        
        self.isSymmetryComputed = False

        if (path!=""):
        
            json_file = open(path,'r')
            json_data = json.load(json_file)
            json_file.close()


            atms_lab = json_data["section_run"]["section_system"]["atom_labels"]
            atms_pos = json_data["section_run"]["section_system"]["atom_positions"]

            #Creating the atom
            i=0
            for atm in atms_lab:
                currentNumber = atomNum[atm]
                currentPos = np.array([atms_pos[i][0], atms_pos[i][1], atms_pos[i][2]])
                newAtom = Atom(i,currentNumber,currentPos)
                self.addAtom(newAtom)
                i=i+1

            #Creating the bonds
            bnds = json_data["section_run"]["section_single_configuration_calculation"]["vms_proxima"]["vms_bonds"]
        

            i=0
            for bnd in bnds:
                firstAtom = self.getAtom(bnd["bond_atoms"][0]-1)
                secondAtom = self.getAtom(bnd["bond_atoms"][1]-1)
                order = bnd["bond_order"]

                newBond = Bond(i,firstAtom,secondAtom)
                newBond.order = order
                if (self.isBondCompatible(newBond)):
                    self.bondSet.add(newBond)
                    self.atomSet[bnd["bond_atoms"][0]-1].addBond(newBond)
                    self.atomSet[bnd["bond_atoms"][1]-1].addBond(newBond)

                i=i+1

        
    #Devi controllare che ci sia nei padri
    @abstractmethod
    def addBonds(self, bonds):
        pass
    
    
    def loadCycles(self, path):
        if (path!=""):
        
            json_file = open(path,'r')
            json_data = json.load(json_file)
            json_file.close()
            frags = json_data["section_run"]["section_single_configuration_calculation"]["vms_proxima"]["vms_frags"]
            
            outFrags = []
            
            
            for c in frags:
                name = c["fr_name"]
                indexes = c["fr_index"]
                currentCycle = Cycle()
                
                self.addFragment(currentCycle)
                                
                bonds=set()
                for a1 in indexes:
                    
                    #Nel json gli atomi partono da 1
                    atom1 = self.getAtom(a1-1)

                    for bnd in atom1.bonds:
                        
                        otherAtomSerial = bnd.minor if bnd.higher==(a1-1) else bnd.higher
                        
                        if ((otherAtomSerial+1 in indexes)):
                            bonds.add(bnd)
                            
                outFrags.append(bonds)
                
            return outFrags
            
            
     
    
    def getAtom(self, index):
        return self.atomSet[index]
        
    def computeSymmetry(self, tol=0.2):
        if (self.isSymmetryComputed):
            return
        
        ian = np.zeros(len(self.atomSet))
        x = np.zeros((len(self.atomSet),3))
        
        for atom in self.atomSet:
            atomObject = self.atomSet[atom]
            
            ian[atom]=atomObject.atomicNum
            
            x[atom,:]=atomObject.pos
            
        
        eqa = Symm.equiv_atoms(x, ian, len(self.atomSet), tol)
        self.equivalentAtoms = eqa
        self.isSymmetryComputed = True
        
    def __areEq(self, atom1, atom2):
        if ((self.equivalentAtoms[atom1.serial, atom2.serial]==1) or (atom1.serial==atom2.serial)):
            return True
        else:
            return False
            
    def __orderedBondList(self, bond1, bond2):
        b1 = sorted(bond1)
        b2 = sorted(bond2)
        if (b1==b2):
            return True
        else:
            return False
            
    def __areEqBond(self, bond1, bond2):
        minorAtom1 = bond1.minorAtom
        minorAtom2 = bond2.minorAtom
        
        higherAtom1 = bond1.higherAtom
        higherAtom2 = bond2.higherAtom
        if ((self.areAtomSymmetric(minorAtom1,minorAtom2) and self.areAtomSymmetric(higherAtom1, higherAtom2)) or (self.areAtomSymmetric(minorAtom1,higherAtom2) and self.areAtomSymmetric(higherAtom1, minorAtom2))):
                return True
        
        else:
            return False
        
    def areAtomSymmetric(self, atom1, atom2):
        if (self.isSymmetryComputed):
            return self.__areEq(atom1,atom2)
            
        results = []
        
        for child in self.childFrags:
            if (child.isSymmetryComputed):
                results.append(child.__areEq(atom1,atom2))
                
        if (True in results):
            return True
        else:
            return False
            
    def getAllSymmetricAtom(self, atom):
        out = []
        
        if (self.isSymmetryComputed):
            for atom2 in self.atomSet:
                if (self.areAtomSymmetric(self.atomSet[atom2], atom)):
                    out.append(self.atomSet[atom2])
            
        for child in self.childFrags:
            if (child.isSymmetryComputed):
                for atom2 in child.atomSet:
                    if (child.areAtomSymmetric(child.atomSet[atom2], atom)):
                        out.append(child.atomSet[atom2])
                        
        return out
            
                
    def areBondSymmetric(self, bond1, bond2):
        
        if (self.isSymmetryComputed):
            return self.__areEqBond(bond1,bond2)
            
        results = []
        
        for child in self.childFrags:
            if (child.isSymmetryComputed):
                results.append(child.__areEqBond(bond1,bond2))
                
        if (True in results):
            return True
        else:
            return False
            
            
    def getAllSymmetricBond(self, bond):
        out = []
        
        if (self.isSymmetryComputed):
            for bond2 in self.bondSet:
                if (self.areBondSymmetric(bond2, bond) and bond2!=bond):
                    out.append(bond2)
            
        for child in self.childFrags:
            if (child.isSymmetryComputed):
                for bond2 in child.bondSet:
                    if (child.areBondSymmetric(bond2, bond) and bond2!=bond):
                        out.append(bond2)
                        
        return out
        
        
        
    def getBond(self,atom1,atom2):
        if (not self.isAtom(atom1)):
            return False
        if (not self.isAtom(atom2)):
            return False
        return self.atomSet[atom1.serial].findBond(atom2)
    
    def getBond_wSerial(self, atom1, atom2):
        return self.atomSet[atom1].findBond_wSerial(atom2)
        
    def __eq__(self, other):
        if (self.atomSet==other.atomSet and self.bondSet==other.bondSet):
            return True
        else:
            return False
        
    def isAtom(self,atom):
        if (atom.serial in self.atomSet):
            return True
        else:
            return False
        
    def isBondCompatible(self, bond):
        if (not self.isAtom(bond.minorAtom)):
            return False
        elif (not self.isAtom(bond.higherAtom)):
            return False
        else:
            return True
        
        
    #IN QUESTA SEZIONE ADESSO BUTTO TUTTI I METODI PER IL CALCOLO DEI FRAMMENTI
    #------------------------------------------------------------------------------------
    
    def areBondsSymmetryEquivalent(self, bondOriginal, newBond, originalfragment, newFragment, relatedBonds):
        
        if (not self.areBondSymmetric(bondOriginal,newBond)):
        
            return False
            
        
        areOrientedRight = True
        for checkBond in originalfragment:
            
            minorAtomCheckBond = checkBond.minor
            higherAtomCheckBond = checkBond.higher
            

            
            symmetricCheckBond = relatedBonds[checkBond.index]
            
            
            minorAtomRelatedBond = symmetricCheckBond.minor
            higherAtomRelatedBond = symmetricCheckBond.higher
            
            orientation1 = np.abs(bondOriginal.relativeOrientation(checkBond))
            orientation2 = np.abs(newBond.relativeOrientation(symmetricCheckBond))
                        
            distBond1 = bondOriginal.relativeDistance(checkBond)
            distBond2 = newBond.relativeDistance(symmetricCheckBond)
            
            da = False
            diff = np.abs(orientation1-orientation2)
            if (diff<=5):
                da = True
            else:
                da = False
                    
            if ((not da) or (not np.isclose(distBond1,distBond2, 1e-1)) ):
                areOrientedRight=False
                break
                            
        if (areOrientedRight):
            relatedBonds[bondOriginal.index] = newBond
            originalfragment.append(bondOriginal)
            newFragment.append(newBond)
        
            return True
        else:
            
            return False
        
            
            
            
    def __fillEqCycle(self, bond, originalFragment, avoid):
        newFragment = []
        originalBuilding = []
        
        relation = {}
        mainBond = None
        isMainBond=False
        
        #1) Devi trovare 1 legame equivalente nel frammento originale
        for originalBond in originalFragment:
            if self.areBondsSymmetryEquivalent(originalBond, bond, originalBuilding, newFragment, relation):
                mainBond=originalBond
                isMainBond=True
                break

        if (not isMainBond):
            return False



        for originalBond in originalFragment:
            
            if (originalBond.index==mainBond.index):
                continue
            
            atLeastOneSymm = False
            
            
            for otherBond in self.bondSet:
                
                if (otherBond in avoid):
                    continue
                
                if (otherBond.index==mainBond.index):
                    continue
                
                areSymm = self.areBondsSymmetryEquivalent(originalBond, otherBond, originalBuilding, newFragment, relation)
                if (len(newFragment)==len(originalFragment)):
                    return newFragment
                
                if (areSymm):
                    atLeastOneSymm=True                    
                    
            if (not atLeastOneSymm):
                return False

    
    
    def __findAllSymmFromBond(self, bond, originalFrag, avoidFrags):
        
        out=[]
        loop = True
        
        isMainBond=False
        mainBond = 0
        
        #Cerca un legame di riferimento tra i due
        for bnd in originalFrag:
            if (self.areBondSymmetric(bond,bnd)):
                mainBond = bnd
                isMainBond=True
                
        #Se non lo trovi esci dal ciclo
        if (not isMainBond):
            return []
                
        
        
        while (loop):
            
            newFrag = self.__fillEqCycle(bond,originalFrag, avoidFrags)
                
            if (newFrag!=[] and newFrag!=False):
                out.append(newFrag)
                for bnd in newFrag:
                    avoidFrags.append(bnd)
            else:
                loop=False
                
        return out
    
    def areFragsEq(self,f1,f2):
        f1.sort()
        f2.sort()
        
        i=0
        for b in f1:
            if (b!=f2[i]):
                return False
            i=i+1
            
        return True
    
    
    def findSymmetricFrags(self, originalFrag):
        avoid = []
        
        out = []
        
        for bnd in self.bondSet:
            #if (bnd in originalFrag):
                #continue
            
            newCycles = self.__findAllSymmFromBond(bnd,originalFrag,avoid)
            avoid.clear()

            for cycle in newCycles:
                out.append(cycle)
                    
                    
        outResult = []
        for frag in out:
            toBeAdded=True
            
            for o in outResult:
                if (self.areFragsEq(frag, o)):
                    toBeAdded=False
                    break
                
            if (toBeAdded):
                outResult.append(frag)
                    
        return outResult
    
    
    def findAllSymmetricFrags(self, cycles):
        out = []
        
        for cycle in cycles:
            symmetricFrags = self.findSymmetricFrags(cycle)
            
            for frag in symmetricFrags:
                toBeAdded=True
                
                for o in out:
                    if (self.areFragsEq(frag, o)):
                        toBeAdded=False
                        break
                    
                if (toBeAdded):
                    out.append(frag)
                    
        return out
            
    
        #--------------------------------------------------------------------------------------
                    
    def addFragment(self, other):
        for bond in other.bondSet:
            if (not self.isBondCompatible(bond)):
                return
                
        for atom in other.atomSet:
            if (not atom in self.atomSet):
                return
                
        self.childFrags.append(other)
        other.parentFrag = self
        
        
                
class MolecularSystem(Fragment):
    
    def addAtom(self, atom):
        self.atomSet[atom.serial]=atom

    def addBonds_test(self, bonds):
        #if (self.__isClosure(bonds)):
            for bond in bonds:
                self.bondSet.add(bond)
                self.atomSet[bond.minor]=bond.minorAtom
                self.atomSet[bond.higher]=bond.higherAtom
        
    def addBonds(self, bonds):
        for bond in bonds:
            if (self.isBondCompatible(bond)):
                self.bondSet.add(bond)
                self.atomSet[bond.minor].addBond(bond)
                self.atomSet[bond.higher].addBond(bond)
                
                
    def readPDB(self, file):
        f = open(file, 'r')
        
        while (f.readline()):
            line = f.readline()
            if (line[0:4]=="ATOM"):
                serial = int(line[6:11])-1
                
                pos = np.zeros(3)
                pos[0] = float(line[30:38])
                pos[1] = float(line[38:46])
                pos[2] = float(line[46:54])
                
                newAtom = Atom(serial,1,pos)
                self.addAtom(newAtom)       
        f.close()
            
    def __createBond(self, atom1, atom2):
        if (not self.isAtom(atom1)):
            return
        if (not self.isAtom(atom2)):
            return
        if (atom1 == atom2):
            return
            
        newHash = len(self.bondSet)
        newBond = Bond(newHash,atom1,atom2)
        self.addBonds([newBond])
            
    def __isBond(self, atom1, atom2):
        dist = np.linalg.norm(atom2.pos-atom1.pos)
        if (dist < covRad[atom1.atomicNum] + covRad[atom2.atomicNum] + 0.4):
            self.__createBond(atom1, atom2)
            
    def computeBonds(self):
        for atom1 in self.atomSet:
            for atom2 in self.atomSet:
                if (atom2>atom1):
                    self.__isBond(self.atomSet[atom1],self.atomSet[atom2])
                    
                    
    #TODO: CICLI E SIMMETRIA
class Cycle(Fragment):
    
    def __recursiveClosure(self, originAtom, originalBond, bonds, currentAtom):
        for bond in currentAtom.bonds:
            if (bond==originalBond):
                continue
            
            if (bond in bonds):
                other = bond.otherAtom(currentAtom)
                if (other==originAtom):
                    return True
                else:
                    return self.__recursiveClosure(originAtom, bond, bonds, other)
                    
        return False
    
    def __isClosure(self, bondSet):
        originalBond = bondSet[0]
        originAtom = originalBond.minorAtom
        current = originalBond.higherAtom
        return self.__recursiveClosure(originAtom,originalBond,bondSet,current)
        
    def findAllSymmetricRings(self):
        return self.parentFrag.findAllSymmetricFragments(self.bondSet)
        #return symFrags
        #out=[]
        #for candidate in symFrags:
            #if (self.__isClosure(candidate)):
                #out.append(candidate)
                
        #return out
    
    def addBonds(self, bonds):
        if (self.__isClosure(bonds)):
            for bond in bonds:
                self.bondSet.add(bond)
                self.atomSet[bond.minor]=bond.minorAtom
                self.atomSet[bond.higher]=bond.higherAtom
