import GUIElements


from abc import ABC, abstractmethod

import os
import time
import math
import threading
from copy import deepcopy 
import ntpath
from sys import version_info
import socket
import json
from shutil import copyfile
from shutil import *
from tkinter import *
from tkinter import filedialog
from tkinter.ttk import *
from tkinter import ttk
from tkinter import messagebox
from pathlib import Path



#COMANDI DI SISTEMA
#------------------------
def send_vmscommand(script, HOST='127.0.0.1', PORT=80):
    with socket.socket(socket.AF_INET, socket.SOCK_STREAM) as jmol:
        jmol.connect((HOST, PORT))
        script = (script).encode("UTF-8")
        jmol.send(script)
        data = jmol.recv(1024)
    return repr(data)

def script_to_jmol(script, HOST='127.0.0.1', PORT=80):
    return send_vmscommand("JMOL " + script, HOST, PORT)

def extractName(path):
    head, tail = ntpath.split(path)
    name = tail or ntpath.basename(head)
    return name.split(".")[0]


#GUI
#-----------------------------------------------------------------------

#Questa classe definisce lo "stile" di Proxima
class ProximaGUInterface:

    def __init__(self,title, dimensions):
        self.ro = Tk()
        self.ro.style = Style()
        self.ro.style.theme_use('default')

        self.ro.configure(background='white')


        #Leggo le informazioni dall'input del costruttore
        self.ro.title(title)
        self.ro.geometry(dimensions)

        #Imposto lo stile della finestra
        self.backStyleFrame = Style(self.ro)
        self.backStyleLabel = Style(self.ro)
        self.backStyleMenu = Style(self.ro)
        self.backStyleButton = Style(self.ro)
        self.backOptions = Style(self.ro)
        self.backCheckButton = Style(self.ro)
        self.backStyleFrame.configure('TFrame',background="white")
        self.backStyleLabel.configure('TLabel',background="white")
        self.backStyleMenu.configure('TMenubar', background="white")
        self.backStyleButton.configure('TButton', background="white")
        self.backOptions.configure('TOptionMenu', background="white")

        self.backCheckButton.configure('TCheckbutton', background="white")


class EditorInterface(ProximaGUInterface):

    def __init__(self, parentGUI):

        super().__init__("Editor", "200x100")

        self.parent = parentGUI
        self.parent.isEditor = True


        self.entryNameLabel = Label(self.ro, text="File Name:")
        self.entryNameLabel.pack()
        self.nameEntry = Entry(self.ro)
        self.nameEntry.pack()
        self.okButton = Button(self.ro, text="Ok", command=self.ok)
        self.okButton.pack(side=LEFT)
        self.discardButton = Button(self.ro, text="Discard", command=self.discard)
        self.discardButton.pack(side=RIGHT)
        self.ro.protocol("WM_DELETE_WINDOW", self.discard)
        
        self.ro.mainloop()
    
    def ok(self):
        
        if (self.nameEntry.get()==""):
            messagebox.showinfo("Error", "Please, select a valid name first.")
        elif (self.nameEntry.get() in self.parent.systems.listEl):
            messagebox.showinfo("Error", "Please, select a different name. You can't have multiple systems with the same name.")
        else:
            res = script_to_jmol("set modelKitMode off; write "+ os.path.join(self.parent.vms_folder,self.nameEntry.get()+".pdb"))
            timeIdx=0
            isLoaded=False
            while (timeIdx*0.5<20):
                if (os.path.isfile(os.path.join(self.parent.vms_folder,self.nameEntry.get()+".pdb"))==True):
                    timeIdx=40
                    isLoaded=True
                else:
                    timeIdx=timeIdx+1
                    time.sleep(0.5)

            if (not isLoaded):
                messagebox.showinfo("ERROR", "Error while saving file!")
                discard()

            else:
                
                ml = GUIElements.MolGUI(os.path.join(self.parent.vms_folder,self.nameEntry.get()+".pdb"), self.parent)
                self.parent.systems.addElement(ml)


                self.parent.isEditor=False
                self.ro.destroy()
    def discard(self):
        script_to_jmol("select all; delete")
        self.parent.isEditor=False
        self.ro.destroy()



class FragListGUI(ProximaGUInterface):
    def __init__(self, parentGUI, cycles):
        
        super().__init__("Symmetric Fragments", "200x245")
        
        self.parent = parentGUI
        
        self.introLabel = Label(self.ro, text="Symmetric Fragments")
        
        self.introLabel.pack()
        
        self.fragList = GUIElements.ListCycle(parentGUI,self.ro,"cycleGUInterface", SINGLE)
        self.fragList.packAll(LEFT,BOTH)
        
        for c in cycles:
            self.fragList.addElement(c)
            
        self.ro.mainloop()
        
        
class ComputeGUI(ProximaGUInterface):

    def __init__(self, parentGUI):

        super().__init__("Compute", "200x245")


        self.parent=parentGUI

    
        self.chargeSec = Frame(self.ro, borderwidth=2, relief="solid")
        self.chargeSec.pack()
        self.compCharg = Label(self.chargeSec,text="Charge Computation: ")
        self.compCharg.pack()

        self.choice = StringVar(self.chargeSec)
        self.chargeMethod = OptionMenu(self.chargeSec, self.choice, "Select: ", "None", "Gasteiger", "FQ")
        self.chargeMethod.pack()

        self.totalCharge = Label(self.chargeSec, text="Total Charge: ")
        self.totalCharge.pack()

        self.temp_charge = StringVar(self.chargeSec)
        self.temp_charge.set("0")
        self.chargeEntry = Spinbox(self.chargeSec, from_=-10, to=10, textvariable=self.temp_charge)
        self.chargeEntry.pack()
        

        self.spinMult = Label(self.chargeSec, text="Spin Multiplicity: ")
        self.spinMult.pack()

        self.spinEntry = Spinbox(self.chargeSec, from_=1, to=10)
        #self.spinEntry.set("1")
        self.spinEntry.pack()

        self.mepVar = IntVar(self.chargeSec)
        self.mepVar.set(0)
        self.mepButton = Checkbutton(self.chargeSec,text="MEP surfaces", variable=self.mepVar, onvalue=1, offvalue=0)
        self.mepButton.var = self.mepVar
        self.mepButton.pack()

        self.fragSec = Frame(self.ro)
        self.fragSec.pack()
        self.compFrag = Label(self.fragSec,text="Fragment Computation: ")
        self.compFrag.pack()
        
        self.bondVar = IntVar(self.fragSec)
        self.bondVar.set(0)
        self.bondButton = Checkbutton(self.fragSec,text="Bond Orders", variable=self.bondVar, onvalue=1, offvalue=0)
        self.bondButton.var = self.bondVar
        self.bondButton.pack()

        self.fragVar = IntVar(self.fragSec)
        self.fragVar.set(0)
        self.fragButton = Checkbutton(self.fragSec,text="Horton Rings", variable=self.fragVar, onvalue=1, offvalue=0)
        self.fragButton.var = self.fragVar
        self.fragButton.pack()

        
        
        
        self.bridgeVar = IntVar(self.fragSec)
        self.bridgeVar.set(0)
        self.bridgeButton = Checkbutton(self.fragSec, text="Bridge Bonds", variable=self.bridgeVar, onvalue=1, offvalue=0)
        self.bridgeButton.var = self.bridgeVar
        self.bridgeButton.pack()

        
        

        self.hbSec = Frame(self.ro)
        self.hbSec.pack()
        self.compHB = Label(self.hbSec,text="Hydrogen Bonds Computation: ")
        self.compHB.pack()
        self.hbVar = IntVar(self.hbSec)
        self.hbVar.set(0)
        self.hbButton = Checkbutton(self.hbSec,text="Hydrogen Bonds", variable=self.hbVar, onvalue=1, offvalue=0)
        self.hbButton.var = self.hbVar
        self.hbButton.pack()
        
        self.okButton = Button(self.ro, text="Ok!", command=self.go)
        self.okButton.pack()

        self.ro.mainloop()


    def uniteJMOLS(self):
        currentML = self.parent.systems.getCurrentElement()
        if (currentML==-1):
            messagebox.showinfo("ERROR", "Please select a system first")
            return

        path_temp = os.path.join(self.parent.vms_folder,currentML.name+"_temp.json")


        if (not os.path.isfile(path_temp)):
            messagebox.showinfo("ERROR", "Something went wrong during the computation!")
            return


        #PATH TEMPORANEO DEL MOL
        #------------------------------------------------------------------
        path_temp_mol = os.path.join(self.parent.vms_folder,currentML.name+"_temp.mol")
        #------------------------------------------------------------------
        path_original = os.path.join(self.parent.vms_folder,currentML.name+".json")
        

    
        new_json_file = open(path_temp,'r')
        old_json_file = open(path_original,'r')
        new_json_data = json.load(new_json_file)
        new_json_file.close()
        old_json_data = json.load(old_json_file)
        old_json_file.close()

        #todo: controlla modifica !=[]
        if (new_json_data["section_run"]["section_single_configuration_calculation"]["vms_proxima"]["vms_charges"]!=[]):
            old_json_data["section_run"]["section_single_configuration_calculation"]["vms_proxima"]["vms_charges"] = new_json_data["section_run"]["section_single_configuration_calculation"]["vms_proxima"]["vms_charges"]


        if (("spin_multiplicity" in new_json_data["section_run"]["section_system"])):
            old_json_data["section_run"]["section_system"]["spin_multiplicity"] = new_json_data["section_run"]["section_system"]["spin_multiplicity"]

        if (("total_charge" in new_json_data["section_run"]["section_system"])):
            old_json_data["section_run"]["section_system"]["total_charge"] = new_json_data["section_run"]["section_system"]["total_charge"]

        if (new_json_data["section_run"]["section_single_configuration_calculation"]["vms_proxima"]["vms_bonds"]!=[]):
            old_json_data["section_run"]["section_single_configuration_calculation"]["vms_proxima"]["vms_bonds"] = new_json_data["section_run"]["section_single_configuration_calculation"]["vms_proxima"]["vms_bonds"]
            
        if (new_json_data["section_run"]["section_single_configuration_calculation"]["vms_proxima"]["vms_bridgebonds"]!=[]):
            old_json_data["section_run"]["section_single_configuration_calculation"]["vms_proxima"]["vms_bridgebonds"] = new_json_data["section_run"]["section_single_configuration_calculation"]["vms_proxima"]["vms_bridgebonds"]


        if (new_json_data["section_run"]["section_single_configuration_calculation"]["vms_proxima"]["vms_frags"]!=[]):
            old_json_data["section_run"]["section_single_configuration_calculation"]["vms_proxima"]["vms_frags"] = new_json_data["section_run"]["section_single_configuration_calculation"]["vms_proxima"]["vms_frags"]

        if (new_json_data["section_run"]["section_single_configuration_calculation"]["vms_proxima"]["vms_hbonds"]!=[]):
            old_json_data["section_run"]["section_single_configuration_calculation"]["vms_proxima"]["vms_hbonds"] = new_json_data["section_run"]["section_single_configuration_calculation"]["vms_proxima"]["vms_hbonds"]

        outFile = open(path_original,'w')
        json.dump(old_json_data,outFile)
        outFile.close()

        os.remove(path_temp)
        os.remove(path_temp_mol)

    def go(self):

        currentML = self.parent.systems.getCurrentElement()

        if (currentML == -1):
            messagebox.showinfo("ERROR", "Please select a system first")
            return
        
        currentML.unpackAll()

        #Tengo da parte le informazioni attuali del mol per poi aggiornarle
        oldCharges= deepcopy(currentML.isCharges)
        oldRings = deepcopy(currentML.isRings)
        oldHBonds = deepcopy(currentML.isHBonds)
        oldBonds = deepcopy(currentML.isBO)



#os.path.join(self.parent.vms_folder,currentML.name+".mol")
        #Comincio ad assemblare il nuovo comando per ProximaConsole
        command = os.path.join(self.parent.current_folder,"ProximaConsole") + " -i " + currentML.PATH + " -o " + os.path.join(self.parent.vms_folder,currentML.name+"_temp")
        

        if (self.choice.get()=="Gasteiger"):
            command = command + " -gasteiger "
            if (self.mepVar.get()==1):
                command = command + " -mep "
            currentML.isCharges=True

        if (self.choice.get()=="FQ"):
            command = command + " -fq "
            if (self.mepVar.get()==1):
                command = command + " -mep "
            currentML.isCharges=True

        if (self.bondVar.get()==1):
            command = command + " -bo_EXP "
            
        if (self.bridgeVar.get()==1):
            command = command + " -bridge "

        if (self.fragVar.get()==1):
            command = command + " -cycles "

        if (self.hbVar.get()==1):
            command = command + " -hbonds "

        command = command + "-charge " + str(self.chargeEntry.get()) + " -spin " + str(self.spinEntry.get())

        os.system(command)
        united = self.uniteJMOLS()
        if (united==False):
            currentML.isCharges = oldCharges
            currentML.isRings = oldRings
            currentML.isHBonds = oldHBonds
            currentML.isBO = oldBonds
            return

        
        json_file = open(os.path.join(self.parent.vms_folder,currentML.name+".json"),'r')
        json_data = json.load(json_file)
        json_file.close()

        if (currentML.isCharges==True):
            currentML.computeDipole()

        currentML.unpackAll()

        if (self.fragVar.get()==1):
            outFrags = json_data["section_run"]["section_single_configuration_calculation"]["vms_proxima"]["vms_frags"]
            idx=0
            
            #currentML.frags = GUIElements.ListCycle(self.parent, self.parent.fragFrame, "cycles", SINGLE)
            currentML.frags.unpackAll()

            for c in outFrags:

                #TODO: Controlla (allocazione memoria)
                atoms=[]
                for atom in c["fr_index"]:
                    atoms.append(atom)
            
                name=str(c["fr_name"])
                newFrag = GUIElements.CycleGUI(atoms,name)
                
                currentML.frags.addElement(newFrag)
                currentML.isRings=True
                
                idx=idx+1

        if (self.bondVar.get()==1):
            bonds = json_data["section_run"]["section_single_configuration_calculation"]["vms_proxima"]["vms_bonds"]
            idx=0
            
            currentML.bonds.unpackAll()
            currentML.bonds.clearAll()
            
            
            print("Fatto")
        
            for b in bonds:
                atom1 = b["bond_atoms"][0]
                atom2 = b["bond_atoms"][1]
                order = b["bond_order"]
                
                newBond = [atom1,atom2]
                newBondGUI = GUIElements.BondGUI(newBond, order, str(order))
                currentML.bonds.addElement(newBondGUI)
                currentML.isBO = True

                idx = idx+1
                
                
                
        
        if (self.bridgeVar.get()==1):
            bonds = json_data["section_run"]["section_single_configuration_calculation"]["vms_proxima"]["vms_bridgebonds"]
            idx=0
            
            #TODO: METTI I BRIDGEBONDS NEL MOLSYS
            
            currentML.bridgebonds.unpackAll()
            currentML.bridgebonds.clearAll()
            
                
            for b in bonds:
                atom1 = b["bond_atoms"][0]
                atom2 = b["bond_atoms"][1]
                order = b["bond_order"]
                
                newBond = [atom1,atom2]
                newBondGUI = GUIElements.BondGUI(newBond, order, str(order))
                currentML.bridgebonds.addElement(newBondGUI)
                currentML.isBO = True

                idx = idx+1



        if (self.hbVar.get()==1):
            hbs = json_data["section_run"]["section_single_configuration_calculation"]["vms_proxima"]["vms_hbonds"]
            idx=0

            currentML.hbonds.clearAll()

            for hb in hbs:
                intensity = hb["hb_force"]
                atoms = hb["hb_atoms"]

                newHB = GUIElements.HBondGUI(atoms,str(intensity))
                currentML.hbonds.addElement(newHB)

                currentML.isHBonds = True
                idx=idx+1

        currentML.isComputed=True
        currentML.packAll()
        self.ro.destroy()


#Questa classe adotta lo stile di ProximaGUI e implementa il main
class MainInterface(ProximaGUInterface):

    def sCharge(self):
        out = self.systems.getCurrentElement()
        if (out!=-1):
            out.showCharges()

    def sDipole(self):
        out = self.systems.getCurrentElement()
        if (out!=-1):
            out.showDipole()

    def eraseALL(self,*args):
        rmtree(self.vms_folder)
        self.systems.clearAll()
        self.ro.destroy()

    def firstComputation(self):
        if (self.systems.currentSelected==-1):
            messagebox.showinfo("ERROR", "No Molecular System selected")

        elif (not self.isEditor):
            computePanel = ComputeGUI(self)
            self.computeButton.pack_forget()
        else:
            messagebox.showinfo("ERROR", "You are in Editor Mode! Save the file first.")


    def __init__(self):

        super().__init__("ProximaGUI", "600x700")
        self.ro.iconphoto(self.ro, PhotoImage(file="proximaLogo.png"))

        #CREO INTERFACCIA GRAFICA
        #-------------------------------------------------------------------------------------------------------
        #Imposto il titolo della finestra]
        self.Title = Label(self.ro, text="ProximaGUI", font=("Thaoma", 32), foreground="blue")
        self.Title.pack()

        self.home = str(Path.home())    
        self.vms_folder = os.path.join(os.path.join(self.home, ".VMS"),".proxima")
        if (not os.path.isdir(self.vms_folder)):
            os.makedirs(self.vms_folder)


        #Controllo di trovarmi nella cartella giusta
        self.current_folder = os.getcwd()

        #È ProximaGUI in modalità editor?
        #------------------------
        self.isEditor=False

        #Imposto i sistemi molecolari
        self.introFrame = Frame(self.ro)
        self.introFrame.pack(side=LEFT, fill = BOTH)
        self.introSys = Label(self.introFrame, text = "Molecular Systems", font=("Thaoma bold", 12) )
        self.introSys.pack()
        self.introSys.configure(background='white')

        #In questo modo tengo in memoria un oggetto contente la lista di legami
        self.systems = GUIElements.ListMol(self,self.introFrame,"systems",SINGLE)
        self.systems.packAll(LEFT,BOTH)


        #Creo il pannello di destra con le opzioni di visualizzazione
        self.optionsFrame = Frame(self.ro)
        self.optionsFrame.pack(side=RIGHT, fill=BOTH)
        self.introOptions = Label(self.optionsFrame, text = "Visualization Options", font=("Thaoma bold", 12))
        self.introOptions.pack()

        self.computeButton = Button(self.optionsFrame, text = "Compute", command=self.firstComputation)


        #L'ordine delle proprietà è fissato all'inizio
        self.chargeFrameEXT = Frame(self.optionsFrame)
        self.chargeFrameEXT.pack()
        self.fragFrameEXT = Frame(self.optionsFrame)
        self.fragFrameEXT.pack()
        self.hbondFrameEXT = Frame(self.optionsFrame)
        self.hbondFrameEXT.pack()
        self.bondFrameEXT = Frame(self.optionsFrame)
        self.bondFrameEXT.pack()

        self.chargeFrame = Frame(self.chargeFrameEXT, borderwidth=2, relief="solid")
        self.chargeSec = Label(self.chargeFrame, text = "Charges", font = ("Lato Thin", 15))
        self.computeChargesButton = Button(self.chargeFrame, text = "Show Charges", command = self.sCharge)
        self.showDipoleButton = Button(self.chargeFrame, text="Show Dipole", command=self.sDipole)

        self.hbondFrame = Frame(self.hbondFrameEXT, borderwidth=2, relief="solid")
        self.hbondSec = Label(self.hbondFrame, text = "Hydrogen Bonds", font = ("Lato Thin", 15))
        self.hideHBonds = Button(self.hbondFrame, text= "Hide All", command=self.hideAllFrags)
        
        self.fragFrame = Frame(self.fragFrameEXT, borderwidth=2, relief="solid")
        self.fragSec = Label(self.fragFrame, text = "Fragments", font = ("Lato Thin", 15))
        self.hideFrags = Button(self.fragFrame, text= "Hide All", command=self.hideAllFrags)
        self.symmFrags = Button(self.fragFrame, text= "Print Symmetric", command=self.showSymm)


        self.bondFrame = Frame(self.bondFrameEXT, borderwidth=2, relief="solid")
        self.bondSec = Label(self.bondFrame, text = "Covalent Bonds", font = ("Lato Thin", 15))
        self.hideBonds = Button(self.bondFrame, text= "Hide All", command=self.hideAllFrags)

        #Creo la barra dei menu 
        self.menubar = Menu(self.ro)
        self.menubar.config(background="white")

        #Creo il menu file
        self.filemenu = Menu(self.menubar,tearoff=0)
        self.filemenu.config(background="white")
        self.filemenu.add_command(label="New", command=self.openEditor)
        self.filemenu.add_command(label="Open", command=self.openFile)
        self.filemenu.add_command(label="Save", command=self.saveFile)
        self.filemenu.add_separator()
        self.filemenu.add_command(label="Close", command=self.clearCurrent)

        self.menubar.add_cascade(label="File",menu=self.filemenu)

        #Creo il menu modifica &stereo
        self.editmenu = Menu(self.menubar,tearoff=0)
        self.editmenu.config(background="white")
        self.editmenu.add_command(label="Compute", command=self.compute)
    
        self.stereomenu = Menu(self.editmenu,tearoff=0)
        self.stereomenu.config(background="white")
        self.stereomenu.add_command(label="No Stereo", command=self.stereoOFF)
        self.stereomenu.add_command(label="Side By Side Stereo", command=self.stereoSIDEBYSIDE)
        self.stereomenu.add_command(label="Red/Cyan Stereo", command=self.stereoREDCYAN)
        self.stereomenu.add_command(label="Red/Blue Stereo", command=self.stereoREDBLUE)

        self.editmenu.add_cascade(label="Stereo", menu=self.stereomenu)
        self.menubar.add_cascade(label="Edit", menu=self.editmenu)
        
        self.ro.config(menu=self.menubar)

        self.ro.protocol("WM_DELETE_WINDOW", self.eraseALL)
        self.ro.mainloop()

        #-------------------------------------------------------------------------------------------------------

    def showSymm(self):
        try:
            currentMol = self.systems.getCurrentElement()
            out = currentMol.printSymmetricRings()
            newWindow = FragListGUI(self,out)
        except:
            print("Advanced perception features not available on this system")

    def stereoREDBLUE(self):
        script_to_jmol("stereo REDBLUE")

    def stereoREDCYAN(self):
        script_to_jmol("stereo REDCYAN")

    def stereoSIDEBYSIDE(self):
        script_to_jmol("stereo")

    def stereoOFF(self):
        script_to_jmol("stereo OFF")


    def openEditor(self):
        if (not self.isEditor):
            script_to_jmol("select all; delete; set modelKitMode")
            editor = EditorInterface(self)
        else:
            messagebox.showinfo("Error", "You already are in Editor Mode!")


    def clearCurrent(self):
        #Se non siamo in modalità editor
        if (not self.isEditor):

            current = self.systems.getCurrentElement()
            if (current==-1):
                return
            else:
                name = current.name

                #RIMUOVO I FILE ASSOCIATI AL SEGUENTE SISTEMA MOLECOLARE.
                os.remove(os.path.join(self.vms_folder,name+".json"))
                os.remove(os.path.join(self.vms_folder,name+".mol"))
                self.systems.clearElement()

    def isMolInSys(self, name):
        for el in self.systems.listEl:
            if (name==el.name):
                return True
        return False


    def save(self,path):
        currentMl = self.systems.getCurrentElement()
        copyfile(os.path.join(self.vms_folder,currentMl.name+".json"), os.path.join(path,currentMl.name+".json"))
        copyfile(os.path.join(self.vms_folder,currentMl.name+".mol"), os.path.join(path,currentMl.name+".mol"))


    def compute(self):
        if (self.systems.currentSelected==-1):
            messagebox.showinfo("ERROR", "No Molecular System selected")

        elif (not self.isEditor):
            computePanel = ComputeGUI(self)
        else:
            messagebox.showinfo("ERROR", "You are in Editor Mode! Save the file first.")



    def saveFile(self):
        if (self.systems.getCurrentElement()==-1):
            messagebox.showinfo("ERROR", "No Molecular System selected")
        elif (not self.isEditor):
            outPath = filedialog.askdirectory(title = "Select Output Directory")
            self.save(outPath)
        else:
            messagebox.showinfo("Error", "You are in Editor Mode! Save the file first.")


    def openFile(self):
        if (not self.isEditor):
            inputFile = filedialog.askopenfilename(initialdir = "/",title = "Select input file",filetypes = (("pdb files","*.pdb"),("xyz files", "*.xyz"),("all files","*.*")))

            if (inputFile==""):
                return

            print("Apro il file")
            ms = GUIElements.MolGUI(inputFile, self)
            

            if (self.isMolInSys(ms.name)):
                messagebox.showinfo("Error", "Invalid Name! Please select a different file.")
                return
            self.systems.addElement(ms)
            self.systems.getCurrentElement()
            
        else:
            messagebox.showinfo("Error", "You are in Editor Mode! Save the file first.")


    def hideLabels(self):
        script_to_jmol("select all; label off")


    def hideAllFrags(self):
        if (not self.isEditor):
            script_to_jmol("select all; halo off; color none; select none")




if __name__=="__main__":


#Creo la cartella di riferimento
    proceed = True
    if (not os.path.isfile("RemoteJmol.jar")):
        messagebox.showinfo("ERROR", "RemoteJmol.jar missing!")
        proceed=False
    
    if (not os.path.isdir("lib")):
        messagebox.showinfo("ERROR", "lib folder missing!")
        proceed=False
    
    if (not os.path.isfile("ProximaConsole")):
        messagebox.showinfo("ERROR", "ProximaConsole missing!")
        proceed=False
    
    if (not proceed):
        sys.exit()


    platform = sys.platform
    if (platform[0:2]=='win'):
        os.system("java -jar RemoteJmol.jar &")
    elif (platform[0:5]=='linux'):
        os.system("gnome-terminal -e 'sudo java -jar RemoteJmol.jar &'")
    else:
            os.system("osascript -e 'tell application \"Terminal\" to do script \"java -jar "+os.path.join(current_folder,"RemoteJmol.jar")+"\"'")

    t = MainInterface()



