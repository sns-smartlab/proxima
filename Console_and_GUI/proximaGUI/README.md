## ProximaGUI

In order to use the ProximaGUI application, you need to first compile the ProximaConsole application.
Then, you have to execute the _setup.py_ script. This script, asks for the absolute path of the location where the ProximaConsole executable is located.

Once such operation is done, you can invoke ProximaGUI with:

'''
sudo python GUI.py
'''

>Note: This version of ProximaGUI is still in development, not all of the features of Proxima are guaranteed to work in the GUI.


