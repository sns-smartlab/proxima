# PyProxima

The Cython version of Proxima

--------------------------------------------------------------------

## Installation: 

* Install Cython (https://cython.readthedocs.io/en/latest/src/quickstart/install.html)
* Run: 
```
python setup.py build_ext --inplace
``` 
The script will ask you for the absolute path of the Eigen linear algebra library (http://eigen.tuxfamily.org), required by Proxima for compilation.
 
## Running PyProxima:

```python
import pyproxima
```

In order to use PyProxima always remember to put a "Py" before the name of the class.
Example:

```python
pyproxima.PyAtom(1,b'H',0)
```
> Note: The current version does not have default parameter values in member functions
> Note: The documentation of PyProxima is the same for the Core C++
