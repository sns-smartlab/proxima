import sys
from distutils.core import setup, Extension
from Cython.Build import cythonize


print("Absolute path of eigen: ")
eigenPath = sys.stdin.readline().rstrip("\n")


compile_args = ['-std=c++11', '-O2', '-fopenmp', '-I '+str(eigenPath)]

if sys.platform == 'darwin':
    compile_args.append('-mmacosx-version-min=10.7')


all_module = Extension('pyproxima', sources=['pyproxima.pyx', '../Src/Proxima/molecularsystem.cpp', '../Src/Proxima/utilities/utilities.cpp', '../Src/Proxima/utilities/floating_point.cpp', '../Src/Proxima/utilities/cubegrid.cpp', '../Src/Proxima/aminoresidue.cpp', '../Src/Proxima/atom.cpp','../Src/Proxima/atomicproperties.cpp','../Src/Proxima/atomsgrid.cpp','../Src/Proxima/bond.cpp','../Src/Proxima/fragment.cpp','../Src/Proxima/gasteigerparameters.cpp','../Src/Proxima/boparameters.cpp', '../Src/Proxima/hbinteraction.cpp', '../Src/Proxima/hbond.cpp', '../Src/Proxima/nucleotideresidue.cpp','../Src/Proxima/parser.cpp','../Src/Proxima/residue.cpp','../Src/Proxima/ringgenerator.cpp','../Src/Proxima/sssrgenerator.cpp','../Src/Proxima/parser/altlocstate.cpp', '../Src/Proxima/parser/firstmodelstate.cpp', '../Src/Proxima/parser/modelsstate.cpp', '../Src/Proxima/parser/molecularbuilder.cpp', '../Src/Proxima/parser/molecularbuilderstate.cpp', '../Src/Proxima/parser/residues/aminoresiduebuilderstate.cpp', '../Src/Proxima/parser/residues/genericresiduebuilderstate.cpp','../Src/Proxima/parser/residues/nucleotideresiduebuilderstate.cpp', '../Src/Proxima/parser/residues/residuebuilderstate.cpp'], include_dirs=[str(eigenPath)], extra_compile_args=compile_args,extra_link_args=['-fopenmp'],language='c++')

ext_modules = [all_module]


setup(
    name='PyProxima',
    packages=['PyProxima'],
    ext_modules=cythonize(ext_modules)
)
