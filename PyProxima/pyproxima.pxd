from libcpp cimport bool
from libcpp.string cimport string
from libcpp cimport list
from libcpp.vector cimport vector
from libcpp cimport map

from libcpp.utility cimport pair

from libcpp.set cimport set
from libcpp.memory cimport shared_ptr
from cython.operator cimport dereference as deref


cdef extern from "../Src/Proxima/utilities/utilities.h" namespace "Proxima":
    cdef struct BoundingBox:
        double minX
        double minY
        double minZ

        double maxX
        double maxY
        double maxZ



cdef extern from "../Src/Proxima/utilities/cubegrid.h" namespace "Proxima":
    cdef cppclass CubeGrid:
         CubeGrid(MolecularSystem* ms, double dx, double dy, double dz, size_t frameNum)
         void printCubeFile(string outPath, string title, string description)
         double getNumX()
         double getNumY()
         double getNumZ()
         vector[double] getOrigin()
         vector[double] getEnd()
         double getData(size_t x, size_t y, size_t z)

    ctypedef shared_ptr[CubeGrid] CubeGridSP
                               
         
cdef class PyCubeGrid:
    cdef CubeGridSP _thisptr

    cpdef printCubeFile(self, string outPath, string title, string description)
    cpdef getNumX(self)
    cpdef getNumY(self)
    cpdef getNumZ(self)
    cpdef getOrigin(self)
    cpdef getEnd(self)
    cpdef getData(self,size_t x, size_t y, size_t z)

cdef PyCubeGrid_Init(CubeGridSP mls)

cdef extern from "../Src/Proxima/bond.h" namespace "Proxima":

    cdef cppclass Bond:
        Bond(long long atom1, long long atom2, double order, bool isPlanar) except +
        double getOrder() const
        long long getMinor() const
        long long getHigher() const
        bool isBondPlanar() const

    ctypedef shared_ptr[Bond] BondSP

cdef class PyBond:
    cdef BondSP _thisptr
    
    cpdef getOrder(self)

    cpdef getMinor(self)

    cpdef getHigher(self)

    cpdef isBondPlanar(self)


cdef extern from "../Src/Proxima/hbond.h" namespace "Proxima":

    cdef cppclass HBond:
        HBond(long long H, long long donor, long long acceptor, double _force) except +
        long long getHydrogen() const
        long long getDonor() const
        long long getAcceptor() const
        double getForce() const
        bool almostEqual(const HBond& other) const
        bool isEqual(const HBond& other) const

    ctypedef shared_ptr[HBond] HBondSP

cdef class PyHBond:
    cdef HBondSP _thisptr

    cpdef getHydrogen(self)

    cpdef getDonor(self)

    cpdef getAcceptor(self)

    cpdef getForce(self)

    cpdef almostEqual(self, PyHBond other)

    cpdef isEqual(self, PyHBond other)


cdef extern from "../Src/Proxima/atom.h" namespace "Proxima":

    #The Hybridisation structure
    cdef cppclass Hybridisation:
        unsigned sCharacter
        unsigned pCharacter
        unsigned dCharacter
        Hybridisation()
        Hybridisation(unsigned _sCharacter, unsigned _pCharacter, unsigned _dCharacter)
        bool isEqual(const Hybridisation b)

    ctypedef shared_ptr[Hybridisation] HybridisationSP

    #The Atom class
    cdef cppclass Atom:
        Atom(int atomicNum, const string& name, long long serial)
        string getName() const
        long long getSerial() const
        int getAtomicNum() const
        vector[double] getPosition(size_t frameNum) except + 
        double getCharge(size_t frameNum) except +
        size_t getNumBonds(size_t frameNum) except +
        size_t getNumHBonds(size_t frameNum) except +
        BondSP findBond(size_t frameNum, long long otherAtomSerial) except +
        HBondSP findHBond(size_t frameNum, long long atom1, long long atom2) except +
        size_t getNumFrames() const
        double getOccupancy(size_t frameNum) except +
        double getTemperatureFact(size_t frameNum) except +
        Hybridisation getHybridisation(size_t frameNum) const
        BondSP getBond(size_t id, size_t frameNum) const
        HBondSP getHBond(size_t id, size_t frameNum) const


        void setPosition(size_t frameNum,const vector[double] newPos) except +
        void setCharge(size_t frameNum, double charge) except +
        void setOccupancy(size_t frameNum, double occupancy) except +
        void setTemperatureFact(size_t frameNum, double temperature) except +
        

    ctypedef shared_ptr[Atom] AtomSP

    cdef cppclass AtomSPLessFunctor:
        bool operator() (const AtomSP atom1, const AtomSP atom2) const


cdef class PyHybridisation:
    cdef HybridisationSP _thisptr

    cpdef isEqual(self, PyHybridisation other)

    cpdef get_sCharacter(self)

    cpdef get_pCharacter(self)

    cpdef get_dCharacter(self)
    

cdef class PyAtom:
    cdef AtomSP _thisptr

    cpdef getName(self)

    cpdef getSerial(self)

    cpdef getAtomicNum(self)

    cpdef getPosition(self, frameNum)

    cpdef getCharge(self, frameNum)

    cpdef getNumBonds(self, frameNum)

    cpdef getNumHBonds(self, frameNum)

    cpdef findBond(self, frameNum, otherAtomSerial)

    cpdef findHBond(self, frameNum, atom1, atom2)
 
    cpdef getNumFrames(self)

    cpdef getOccupancy(self, frameNum)

    cpdef getTemperatureFact(self, frameNum)

    cpdef getHybridisation(self, size_t frameNum)

    cpdef setPosition(self, size_t frameNum,const vector[double] newPos)

    cpdef setCharge(self, size_t frameNum, double charge)

    cpdef setOccupancy(self, size_t frameNum, double occupancy)

    cpdef setTemperatureFact(self, size_t frameNum, double temperature)

    cpdef getBond(self, size_t id, size_t frameNum)

    cpdef getHBond(self, size_t id, size_t frameNum)


cdef PyAtom_Init(AtomSP mls)

cdef extern from "../Src/Proxima/fragment.h" namespace "Proxima":
    ctypedef shared_ptr[Fragment] FragmentSP

cdef class PyFragment

cdef extern from "../Src/Proxima/molecularsystem.h" namespace "Proxima":

    #The MolecularSystem class
    cdef cppclass MolecularSystem:
        MolecularSystem(string name)
        string getName() const
        size_t getNumFrames() const

        void addFrame(string frameLabel)
        void setFrameLabel(size_t frameNum, const string& frameLabel) except +
        string getFrameLabel(size_t frameNum) except +


        void addAtom(AtomSP atom) except +
        int getNumAtoms() const
        bool containsAtom(long long serial) const
        AtomSP getAtom(long long serial) except +
        
        void addBond(size_t frameNum, BondSP newBond) except +
        int getNumBonds(size_t frameNum) except +
        bool containsBond(size_t frameNum, BondSP bond) except +

        bool addFragment(FragmentSP fragment) except +
        bool containsFragment(char chainID) except +
        vector[FragmentSP] getFragment(char chainID) except +
        size_t getNumFragments() except +
        
        void addHBond(size_t frameNum, HBondSP bond) except +
        int getNumHBonds(size_t frameNum) except +
        bool containsHBond(size_t frameNum, HBondSP bond) except +
        void removeBond(size_t frameNum, BondSP bond) except +
        void removeHBond(size_t frameNum, HBondSP bond) except +

        BondSP getBond(size_t frameNum, long idx) except +
        HBondSP getHBond(size_t frameNum, long idx) except +

        void computeBonds_Slow(double tolerance_distance)
        void computeBonds_Fast(double tolerance_distance)

        void computeHBondsInFrame_Fast(size_t frameNum)
        void computeHBondsInFrame_Slow(size_t frameNum)

        set[BondSP] getBridgeBonds(size_t frameNum) except +
        void computeBondOrders(size_t frameNum) except +
        void toMol2(string path, size_t frameNum)   except +


        void computeHybridisation(size_t frameNum, bool useCycles, double tolerance) except +

        bool computeHortonCycles(size_t frameNum) except +

        size_t getNumCycles(size_t frameNum) except +

        bool computeGasteigerCharges(size_t frameNum, size_t numIterations) except +

        vector[double] computeDipoleMoment(size_t frameNum) except +

        double computeAngle(long long centerAtom, long long atom2, long long atom3, size_t frameNum) except +

        BoundingBox computeBBox(size_t frameNum) except +
        
        vector[BondSP] getCycle(size_t frameNum, size_t idx) except +

        CubeGridSP computeElectrostaticPotential(double dx, double dy, double dz, size_t frameNum) except +

        void computeFQCharges(size_t frameNum) except +

        void toMOL_V2000(string path, size_t frameNum) except +

        void toMOL_V3000(string path, size_t frameNum) except +

        void printV2(BondSP bond, size_t numBonds, string path, size_t frameNum) except +

        void normalizeBO(size_t frameNum) except +

        void normalizeBO_noTerminal(size_t frameNum) except +

        void toMultimodelXYZ(string path, vector[string] comments) except +

        void computeBondsInFrame_Slow(size_t frameNum, double tolerance_distance) except +

        void computeBondsInFrame_Fast(size_t frameNum, double tolerance_distance) except +





        
    ctypedef shared_ptr[MolecularSystem] MolecularSystemSP


cdef class PyMolecularSystem:
    cdef MolecularSystemSP _thisptr

    cpdef getName(self)
    cpdef getNumFrames(self)
    cpdef addFrame(self, string label)
    cpdef setFrameLabel(self, size_t frameNum, string label)
    cpdef getFrameLabel(self, size_t frameNum)

    cpdef addAtom(self, PyAtom atom)
    cpdef getNumAtoms(self)
    cpdef containsAtom(self, long long serial)
    cpdef getAtom(self, long long serial)

    cpdef addFragment(self, PyFragment fragment)
    cpdef containsFragment(self, char chainID)
    cpdef getFragment(self, char chainID)
    cpdef getNumFragments(self)

    
    cpdef addBond(self, size_t frameNum, PyBond newBond)
    cpdef getNumBonds(self, size_t frameNum)
    cpdef containsBond(self, size_t frameNum, PyBond bnd)
    cpdef addHBond(self, size_t frameNum, PyHBond bnd)
    cpdef getNumHBonds(self, size_t frameNum)
    cpdef containsHBond(self, size_t frameNum, PyHBond bnd)
    cpdef removeBond(self, size_t frameNum, PyBond bnd)
    cpdef getBond(self, size_t frameNum, long idx)
    cpdef getHBond(self, size_t frameNum, long idx)
    cpdef getBridgeBonds(self, size_t frameNum)
    cpdef computeBondOrders(self, size_t frameNum)
    cpdef normalizeBO(self, size_t frameNum)
    cpdef normalizeBO_noTerminal(self, size_t frameNum)

    cpdef getNumCycles(self, size_t frameNum)
    cpdef removeHBond(self, size_t frameNum, PyHBond bnd)
    cpdef computeBonds_Fast(self, double tolerance_distance)
    cpdef computeBonds_Slow(self, double tolerance_distance)
    cpdef computeHBondsInFrame_Fast(self, size_t frameNum)
    cpdef computeHBondsInFrame_Slow(self, size_t frameNum)
    cpdef computeHybridisation(self, size_t frameNum, bool useCycles, double tolerance)
    cpdef computeHortonCycles(self, size_t frameNum)
    cpdef computeGasteigerCharges(self, size_t frameNum, size_t numIterations)
    cpdef computeDipoleMoment(self, size_t frameNum)
    cpdef computeAngle(self, long long centerAtom, long long atom2, long long atom3, size_t frameNum)
    cpdef computeBBox(self, size_t frameNum)
    cpdef getCycle(self, size_t frameNum, idx)
    cpdef computeElectrostaticPotential(self, double dx, double dy, double dz, size_t frameNum)
    cpdef computeFQCharges(self, size_t frameNum)

    cpdef toMOL_V2000(self, string path, size_t frameNum)
    cpdef toMOL_V3000(self, string path, size_t frameNum)
    cpdef toMultimodelXYZ(self, string path, vector[string] comments)

    cpdef printV2(self, PyBond bond, size_t numBonds, string path, size_t frameNum)

    cpdef toMol2(self, string path, size_t frameNum)

    cpdef computeBondsInFrame_Slow(self, size_t frameNum, double tolerance_distance)

    cpdef computeBondsInFrame_Fast(self, size_t frameNum, double tolerance_distance)
    

cdef PyMolecularSystem_Init(MolecularSystemSP mls)


cdef extern from "../Src/Proxima/residue.h" namespace "Proxima":
    
    cdef cppclass Residue:
        const string DEFAULT_NAME
        Residue(long long serial, long long sequenceNumber, char insertionCode,
            const string name) except +

        bool addAtom(long long serial, int role) except +
        bool isEqual(const Residue& res) except +
        string getName() except +
        bool contains(long long atomSerial) except +
        long long getNumAtoms() except +
        long long getSequenceNumber() except +
        long long getSerial() except +
        char getInsertionCode() except +
        Fragment* getParentFragment() except +
        MolecularSystem* getParentMolecularSystem() except +
        bool operator<(const Residue& other) except +
        
    ctypedef shared_ptr[Residue] ResidueSP


cdef class PyResidue:
    cdef ResidueSP _thisptr

    cpdef addAtom(self, long long serial, int role)
    cpdef isEqual(self, PyResidue res)
    cpdef getName(self)
    cpdef contains(self,long long atomSerial)
    cpdef getNumAtoms(self)
    cpdef getSequenceNumber(self)
    cpdef getSerial(self)
    cpdef getInsertionCode(self)
    cpdef getParentFragment(self)
    cpdef getParentMolecularSystem(self)    

cdef PyResidue_Init(ResidueSP mls)


cdef extern from "../Src/Proxima/aminoresidue.h" namespace "Proxima":
    cdef cppclass AminoResidue(Residue):
        AminoResidue(long long serial, long long sequenceNumber, char iCode, const string& name) except +
        AtomSP getAlphaCarbon() except +
        AtomSP getOxygen() except +
        AtomSP getCarbon() except +
        AtomSP getNitrogen() except +
        vector[AtomSP] getBackbone() except +
        vector[AtomSP] getSideChain() except +
        vector[AtomSP] getGenericAtoms() except +

    ctypedef shared_ptr[AminoResidue] AminoResidueSP


cdef class PyAminoResidue:
    cdef AminoResidueSP _thisptr

    cpdef addAtom(self, long long serial, int role)
    cpdef getAlphaCarbon(self)
    cpdef getOxygen(self)
    cpdef getCarbon(self)
    cpdef getNitrogen(self)
    cpdef getBackbone(self)
    cpdef getSideChain(self)
    cpdef getGenericAtoms(self)


cdef PyAminoResidue_Init(AminoResidueSP ares)




cdef extern from "../Src/Proxima/nucleotideresidue.h" namespace "Proxima":
    cdef cppclass NucleotideResidue(Residue):
        NucleotideResidue(long long serial, long long sequenceNumber, char iCode, const string& name) except +
        
        vector[pair[AtomSP,int]] getPhosphateGroup() except +
        vector[pair[AtomSP,int]] getSugar() except +
        vector[pair[AtomSP,int]] getNitrogenousBase() except +
        vector[pair[AtomSP,int]] getGenericAtoms() except +


    ctypedef shared_ptr[NucleotideResidue] NucleotideResidueSP


cdef class PyNucleotideResidue:
    cdef NucleotideResidueSP _thisptr

    cpdef addAtom(self, long long serial, int role)
    cpdef getPhosphateGroup(self)
    cpdef getSugar(self)
    cpdef getNitrogenousBase(self)
    cpdef getGenericAtoms(self)


cdef PyNucleotideResidue_Init(NucleotideResidueSP ares)








cdef extern from "../Src/Proxima/fragment.h" namespace "Proxima":

    #The Fragment class
    cdef cppclass Fragment:

        const string DEFAULT_NAME
        Fragment(char chainID, const string& name) except +
        bool isEqual(const Fragment& other) except +
        string getName() except +
        char getChainID() except +
        bool containsResidue(long long sequenceNumber, char insertionCode) except +
        ResidueSP getGenericResidue(long long sequenceNumber, char insertionCode) except +
        AminoResidueSP getAminoResidue(long long sequenceNumber, char insertionCode) except +
        #NucleotideResidueSP getNucleotideResidue(long long sequenceNumber, char insertionCode) except +
        bool addGenericResidue(ResidueSP residue) except +
        bool addAminoResidue(AminoResidueSP residue) except +
        #bool addNucleotideResidue(NucleotideResidueSP residue) except +
        int getNumberOfResidues() except +
        int getNumberOfGenericResidues() except +
        int getNumberOfAminoResidues() except +
        int getNumberOfNucleotideResidues() except +
        long long getNumberOfAtoms() except +
        MolecularSystem* getParentMolecularSystem() except +

    ctypedef shared_ptr[Fragment] FragmentSP


cdef class PyFragment:
    cdef FragmentSP _thisptr
    cpdef isEqual(self, PyFragment other)
    cpdef getName(self)
    cpdef getChainID(self)
    cpdef containsResidue(self, long long sequenceNumber, char insertionCode)
    cpdef getGenericResidue(self, long long sequenceNumber, char insertionCode)
    cpdef addGenericResidue(self, PyResidue residue)
    cpdef getNumberOfResidues(self)
    cpdef getNumberOfGenericResidues(self)
    cpdef getNumberOfAminoResidues(self)
    cpdef getNumberOfNucleotideResidues(self)
    cpdef getNumberOfAtoms(self)
    cpdef getParentMolecularSystem(self)
    cpdef getAminoResidue(self, long long sequenceNumber, char insertionCode)
    #cpdef getNucleotideResidue(self, long long sequenceNumber, char insertionCode)
    cpdef addAminoResidue(self, PyAminoResidue res)

cdef PyFragment_Init(FragmentSP mls)


cdef extern from "../Src/Proxima/parser.h" namespace "Proxima":

    cdef cppclass Parser:
        MolecularSystemSP readPDB(const string& path, const string& name) except +
        MolecularSystemSP readXYZ(const string& path, const string& name) except +


ctypedef shared_ptr[Parser] ParserSP

cdef class PyParser:
    cdef ParserSP _thisptr
    
    cpdef readPDB(self, string path, string name)

    cpdef readXYZ(self, string path, string name)


