from libcpp cimport bool
from libcpp.string cimport string
from libcpp cimport list
from libcpp.vector cimport vector
from libcpp cimport map

from libcpp.utility cimport pair


from libcpp cimport set
from libcpp.memory cimport shared_ptr
from cython.operator cimport dereference as deref


cdef class PyBond:
        
    def __cinit__(self,long long atom1,long long atom2, double order, bool isPlanar):
        self._thisptr = BondSP(new Bond(atom1,atom2,order,isPlanar))
                
    cpdef getOrder(self):
        return deref(self._thisptr).getOrder()

    cpdef getMinor(self):
        return deref(self._thisptr).getMinor()

    cpdef getHigher(self):
        return deref(self._thisptr).getHigher()

    cpdef isBondPlanar(self):
        return deref(self._thisptr).isBondPlanar()


cdef PyBond_Init(BondSP mls):
    result = PyBond(0,1,2,3)
    result._thisptr = mls
    return result






cdef class PyCubeGrid:

    def __cinit__(self, PyMolecularSystem ms, double dx, double dy, double dz, size_t frameNum):
        ptr = ms._thisptr
        self._thisptr = CubeGridSP(new CubeGrid(ptr.get(), dx, dy, dz, frameNum))

    def __cinit__(self):
        self._thisptr = CubeGridSP(NULL)

    cpdef printCubeFile(self, string outPath, string title, string description):
        return deref(self._thisptr).printCubeFile(outPath, title, description)

    cpdef getNumX(self):
        return deref(self._thisptr).getNumX()

    cpdef getNumY(self):
        return deref(self._thisptr).getNumY()

    cpdef getNumZ(self):
        return deref(self._thisptr).getNumZ()

    cpdef getOrigin(self):
        out = []
        result = deref(self._thisptr).getOrigin()
        out[0]=result[0]
        out[1]=result[1]
        out[2]=result[2]
        return out

    cpdef getEnd(self):
        out = []
        result = deref(self._thisptr).getEnd()
        out[0]=result[0]
        out[1]=result[1]
        out[2]=result[2]
        return out

    cpdef getData(self, size_t x, size_t y, size_t z):
        return deref(self._thisptr).getData(x,y,z)

cdef PyCubeGrid_Init(CubeGridSP mls):
    result = PyCubeGrid()
    result._thisptr = mls
    return result

cdef class PyHBond:
        
    def __cinit__(self,long long H, long long donor, long long acceptor, double _force):
        self._thisptr = HBondSP(new HBond(H,donor,acceptor,_force))

    cpdef getHydrogen(self):
        return deref(self._thisptr).getHydrogen()

    cpdef getDonor(self):
        return deref(self._thisptr).getDonor()

    cpdef getAcceptor(self):
        return deref(self._thisptr).getAcceptor()

    cpdef getForce(self):
        return deref(self._thisptr).getForce()

    cpdef almostEqual(self, PyHBond other):
        oth = other._thisptr
	#oth = deref(other._thisptr)
        return deref(self._thisptr).almostEqual(deref(oth))

    cpdef isEqual(self, PyHBond other):
        oth = other._thisptr
	#oth = deref(other)
        return deref(self._thisptr).isEqual(deref(oth))

cdef PyHBond_Init(HBondSP mls):
    result = PyHBond(0,1,2,3)
    result._thisptr = mls
    return result


cdef class PyHybridisation:

    def __cinit__(self,unsigned _sCharacter, unsigned _pCharacter, unsigned _dCharacter):
        self._thisptr = HybridisationSP(new Hybridisation(_sCharacter, _pCharacter, _dCharacter))

    cpdef isEqual(self, PyHybridisation other):
        hyb = other._thisptr
        return deref(self._thisptr).isEqual(deref(hyb))

    cpdef get_sCharacter(self):
        return deref(self._thisptr).sCharacter

    cpdef get_pCharacter(self):
        return deref(self._thisptr).pCharacter

    cpdef get_dCharacter(self):
        return deref(self._thisptr).dCharacter
    
   

cdef class PyAtom:
        
    def __cinit__(self,atomicNum, name, serial):
        if (atomicNum==None and name==None and serial==None):
            self._thisptr = AtomSP(NULL)
        else:
            self._thisptr = AtomSP(new Atom(<int>atomicNum,<string>name,<long long>serial))

    cpdef getName(self):
        return deref(self._thisptr).getName()

    cpdef getSerial(self):
        return deref(self._thisptr).getSerial()

    cpdef getAtomicNum(self):
        return deref(self._thisptr).getAtomicNum()

    cpdef getPosition(self, frameNum):
        return deref(self._thisptr).getPosition(frameNum)

    cpdef getCharge(self, frameNum):
        return deref(self._thisptr).getCharge(frameNum)

    cpdef getNumBonds(self, frameNum):
        return deref(self._thisptr).getNumBonds(frameNum)

    cpdef getNumHBonds(self, frameNum):
        return deref(self._thisptr).getNumHBonds(frameNum)

    cpdef findBond(self, frameNum, otherAtomSerial):
        _bnd = deref(self._thisptr).findBond(frameNum,otherAtomSerial)
        if (_bnd!=NULL):
            b = PyBond(deref(_bnd).getMinor(),deref(_bnd).getHigher(),deref(_bnd).getOrder(),deref(_bnd).isBondPlanar())
            return b
        else:
            return None

    cpdef findHBond(self, frameNum, atom1, atom2):
        _hbnd = deref(self._thisptr).findHBond(frameNum,atom1,atom2)
        if (_hbnd!=NULL):
            b = PyHBond(deref(_hbnd).getHydrogen(), deref(_hbnd).getDonor(), deref(_hbnd).getAcceptor(), deref(_hbnd).getForce())
            return b
        else:
            return None
 
    cpdef getNumFrames(self):
        return deref(self._thisptr).getNumFrames()

    cpdef getOccupancy(self, frameNum):
        return deref(self._thisptr).getOccupancy(frameNum)

    cpdef getTemperatureFact(self, frameNum):
        return deref(self._thisptr).getTemperatureFact(frameNum)

    cpdef getHybridisation(self, size_t frameNum):
        _hbr = deref(self._thisptr).getHybridisation(frameNum)
        newHbr = PyHybridisation(_hbr.sCharacter, _hbr.pCharacter, _hbr.dCharacter)
        return newHbr

    cpdef setPosition(self, size_t frameNum,const vector[double] newPos):
        deref(self._thisptr).setPosition(frameNum,newPos)

    cpdef setCharge(self, size_t frameNum, double charge):
        deref(self._thisptr).setCharge(frameNum, charge)

    cpdef setOccupancy(self, size_t frameNum, double occupancy):
        deref(self._thisptr).setOccupancy(frameNum,occupancy)

    cpdef setTemperatureFact(self, size_t frameNum, double temperature):
        deref(self._thisptr).setTemperatureFact(frameNum,temperature)

    cpdef getBond(self, size_t id, size_t frameNum):
        bnd = deref(self._thisptr).getBond(id, frameNum)
        return PyBond_Init(bnd)

    cpdef getHBond(self, size_t id, size_t frameNum):
        bnd = deref(self._thisptr).getHBond(id, frameNum)
        return PyHBond_Init(bnd)


cdef PyAtom_Init(AtomSP mls):
    result = PyAtom(None,None,None)
    result._thisptr = mls
    return result

cdef class PyMolecularSystem:
        
    def __cinit__(self, name):
        if (name==None):
            self._thisptr=MolecularSystemSP(NULL)
        else:
            self._thisptr = MolecularSystemSP(new MolecularSystem(name))

    cpdef getName(self):
        return deref(self._thisptr).getName()

    cpdef getNumFrames(self):
        return deref(self._thisptr).getNumFrames()

    cpdef addFrame(self, string label):
        deref(self._thisptr).addFrame(label)

    cpdef setFrameLabel(self, size_t frameNum, string label):
        deref(self._thisptr).setFrameLabel(frameNum, label)

    cpdef getFrameLabel(self, size_t frameNum):
        return deref(self._thisptr).getFrameLabel(frameNum)

    cpdef addAtom(self, PyAtom atm):
        _atomSP = atm._thisptr
        deref(self._thisptr).addAtom(_atomSP)

    cpdef computeBBox(self, size_t frameNum):
        box = deref(self._thisptr).computeBBox(frameNum)
        out = (box.minX, box.minY, box.minZ, box.maxX, box.maxY, box.maxZ)
        return out          

    cpdef getNumAtoms(self):
        return deref(self._thisptr).getNumAtoms()

    cpdef containsAtom(self, long long serial):
        return deref(self._thisptr).containsAtom(serial)

    cpdef  getAtom(self, long long serial):
        atm = deref(self._thisptr).getAtom(serial)
        _atom = PyAtom_Init(atm)
        return _atom

    cpdef addFragment(self, PyFragment fragment):
        fragSP = fragment._thisptr
        return deref(self._thisptr).addFragment(fragSP)
    
    cpdef containsFragment(self, char chainID):
        return deref(self._thisptr).containsFragment(chainID)
    
    cpdef getFragment(self, char chainID):
        fragVecSP = deref(self._thisptr).getFragment(chainID)
        out = []
        for fr in fragVecSP:
            frag = PyFragment_Init(fr)
            out.append(frag)
        return out
        
    cpdef getNumFragments(self):
        return deref(self._thisptr).getNumFragments()

    cpdef addBond(self, size_t frameNum, PyBond newBond):
        _bondSP = newBond._thisptr
        deref(self._thisptr).addBond(frameNum,_bondSP)

    cpdef getNumBonds(self, size_t frameNum):
        return deref(self._thisptr).getNumBonds(frameNum)

    cpdef containsBond(self, size_t frameNum, PyBond bnd):
         _bondSP = bnd._thisptr
         return deref(self._thisptr).containsBond(frameNum, _bondSP)

    cpdef addHBond(self, size_t frameNum, PyHBond bnd):
        _hbondSP = bnd._thisptr
        deref(self._thisptr).addHBond(frameNum, _hbondSP)

    cpdef getNumHBonds(self, size_t frameNum):
        return deref(self._thisptr).getNumHBonds(frameNum)

    cpdef containsHBond(self, size_t frameNum, PyHBond bnd):
        _hbondSP = bnd._thisptr
        deref(self._thisptr).containsHBond(frameNum, _hbondSP)

    cpdef removeBond(self, size_t frameNum, PyBond bnd):
        _bondSP = bnd._thisptr
        return deref(self._thisptr).removeBond(frameNum,_bondSP)

    cpdef removeHBond(self,size_t frameNum, PyHBond bnd):
        _bondSP = bnd._thisptr
        return deref(self._thisptr).removeHBond(frameNum,_bondSP)

    cpdef getBond(self, size_t frameNum, long idx):
        bnd = deref(self._thisptr).getBond(frameNum,idx)
        bond = PyBond_Init(bnd)
        return bond

    cpdef getHBond(self, size_t frameNum, long idx):
        bnd = deref(self._thisptr).getHBond(frameNum,idx)
        bond = PyHBond_Init(bnd)
        return bond

    cpdef getBridgeBonds(self, size_t frameNum):
        cycle = deref(self._thisptr).getBridgeBonds(frameNum)
        out = []
        for b in cycle:
            bnd = PyBond_Init(b)
            out.append(bnd)
        return out

    cpdef getNumCycles(self, size_t frameNum):
        return deref(self._thisptr).getNumCycles(frameNum)

    cpdef getCycle(self, size_t frameNum, idx):
        cycle = deref(self._thisptr).getCycle(frameNum,idx)
        out = []
        for b in cycle:
            bnd = PyBond_Init(b)
            out.append(bnd)
        return out

    cpdef computeBonds_Fast(self, double tolerance_distance):
        deref(self._thisptr).computeBonds_Fast(tolerance_distance)

    cpdef computeBonds_Slow(self, double tolerance_distance):
        deref(self._thisptr).computeBonds_Slow(tolerance_distance)

    cpdef computeHBondsInFrame_Fast(self, size_t frameNum):
        deref(self._thisptr).computeHBondsInFrame_Fast(frameNum)

    cpdef computeHBondsInFrame_Slow(self, size_t frameNum):
        deref(self._thisptr).computeHBondsInFrame_Slow(frameNum)

    cpdef computeHybridisation(self, size_t frameNum, bool useCycles, double tolerance):
        deref(self._thisptr).computeHybridisation(frameNum,useCycles,tolerance)

    cpdef computeHortonCycles(self, size_t frameNum):
        deref(self._thisptr).computeHortonCycles(frameNum)

    cpdef computeGasteigerCharges(self, size_t frameNum, size_t numIterations):
        deref(self._thisptr).computeGasteigerCharges(frameNum, numIterations)

    cpdef computeDipoleMoment(self, size_t frameNum):
        return deref(self._thisptr).computeDipoleMoment(frameNum)

    cpdef computeAngle(self, long long centerAtom, long long atom2, long long atom3, size_t frameNum):
        return deref(self._thisptr).computeAngle(centerAtom,atom2,atom3,frameNum)

    cpdef computeElectrostaticPotential(self, double dx, double dy, double dz, size_t frameNum):
        out = deref(self._thisptr).computeElectrostaticPotential(dx,dy,dz,frameNum)
        return PyCubeGrid_Init(out)

    cpdef computeFQCharges(self, size_t frameNum):
        return deref(self._thisptr).computeFQCharges(frameNum)

    cpdef toMOL_V2000(self, string path, size_t frameNum):
        return deref(self._thisptr).toMOL_V2000(path,frameNum)

    cpdef toMOL_V3000(self, string path, size_t frameNum):
        return deref(self._thisptr).toMOL_V3000(path,frameNum)

    cpdef printV2(self, PyBond bond, size_t numBonds, string path, size_t frameNum):
        return deref(self._thisptr).printV2(bond._thisptr, numBonds, path, frameNum)

    cpdef computeBondOrders(self, size_t frameNum):
        return deref(self._thisptr).computeBondOrders(frameNum)

    cpdef normalizeBO(self, size_t frameNum):
        return deref(self._thisptr).normalizeBO(frameNum)

    cpdef normalizeBO_noTerminal(self, size_t frameNum):
        return deref(self._thisptr).normalizeBO_noTerminal(frameNum)


    cpdef toMultimodelXYZ(self, string path, vector[string] comments):
        return deref(self._thisptr).toMultimodelXYZ(path, comments)

    cpdef toMol2(self, string path, size_t frameNum):
        return deref(self._thisptr).toMol2(path, frameNum)

    cpdef computeBondsInFrame_Slow(self, size_t frameNum, double tolerance_distance):
        return deref(self._thisptr).computeBondsInFrame_Slow(frameNum, tolerance_distance)

    cpdef computeBondsInFrame_Fast(self, size_t frameNum, double tolerance_distance):
        return deref(self._thisptr).computeBondsInFrame_Fast(frameNum, tolerance_distance)

    
cdef PyMolecularSystem_Init(MolecularSystemSP mls):
    result = PyMolecularSystem(None)
    result._thisptr = mls
    return result


cdef class PyResidue:
    
    def __cinit__(self, serial, sequenceNumber, insertionCode, name):
        if ((serial == None) and (name == None) and (insertionCode == None) and (sequenceNumber == None)):
            self._thisptr = ResidueSP(NULL)
        else:
            self._thisptr = ResidueSP(new Residue(serial, sequenceNumber, insertionCode, name))

    cpdef addAtom(self, long long serial, int role):
        return deref(self._thisptr).addAtom(serial,role)
    
    cpdef isEqual(self, PyResidue res):
        _resOther = res._thisptr
        return deref(self._thisptr).isEqual(deref(_resOther))
    
    cpdef getName(self):
        return deref(self._thisptr).getName()
    
    cpdef contains(self,long long atomSerial):
        return deref(self._thisptr).contains(atomSerial)
    
    cpdef getNumAtoms(self):
        return deref(self._thisptr).getNumAtoms()
    
    cpdef getSequenceNumber(self):
        return deref(self._thisptr).getSequenceNumber()

    cpdef getSerial(self):
        return deref(self._thisptr).getSerial()
    
    cpdef getInsertionCode(self):
        return deref(self._thisptr).getInsertionCode()

    #DEVI CONVERTIRLO A MANO IN OGGETTI PYTHON
    
    cpdef getParentFragment(self):
        _ptr = deref(self._thisptr).getParentFragment()
        outSP = <FragmentSP>(_ptr)
        out = PyFragment_Init(outSP)
        return out
    
    cpdef getParentMolecularSystem(self):
        _ptr = deref(self._thisptr).getParentMolecularSystem()
        outSP = <MolecularSystemSP>(_ptr)
        out = PyMolecularSystem_Init(outSP)
        return out

cdef PyResidue_Init(ResidueSP mls):
    result = PyResidue(None,None,None)
    result._thisptr = mls
    return result

cdef class PyAminoResidue:
    
    def __cinit__(self, serial, sequenceNumber,iCode,name):
        if (serial==None and name==None and iCode==None and sequenceNumber==None):
            self._thisptr = AminoResidueSP(NULL)
        else:
            self._thisptr = AminoResidueSP(new AminoResidue(serial, sequenceNumber, iCode, name))

            
    cpdef addAtom(self, long long serial, int role):
        return deref(self._thisptr).addAtom(serial,role)

    cpdef getAlphaCarbon(self):
        atmSP = deref(self._thisptr).getAlphaCarbon()
        atm = PyAtom_Init(atmSP)
        return atm

    cpdef getOxygen(self):
        atmSP = deref(self._thisptr).getOxygen()
        atm = PyAtom_Init(atmSP)
        return atm

    cpdef getCarbon(self):
        atmSP = deref(self._thisptr).getCarbon()
        atm = PyAtom_Init(atmSP)
        return atm

    cpdef getNitrogen(self):
        atmSP = deref(self._thisptr).getNitrogen()
        atm = PyAtom_Init(atmSP)
        return atm

    cpdef getBackbone(self):
        atmVecSP = deref(self._thisptr).getBackbone()
        list = []
        for el in atmVecSP:
            list.append(PyAtom_Init(el))
        return list

    cpdef getSideChain(self):
        atmVecSP = deref(self._thisptr).getSideChain()
        list = []
        for el in atmVecSP:
            list.append(PyAtom_Init(el))
        return list

    cpdef getGenericAtoms(self):
        atmVecSP = deref(self._thisptr).getGenericAtoms()
        list = []
        for el in atmVecSP:
            list.append(PyAtom_Init(el))
        return list

cdef PyAminoResidue_Init(AminoResidueSP ares):
    out = PyAminoResidue(None,None,None,None)
    out._thisptr = ares
    return out
    






cdef class PyNucleotideResidue:

    def __cinit__(self, serial, sequenceNumber,iCode,name):
        if (serial==None and name==None and iCode==None and sequenceNumber==None):
            self._thisptr = NucleotideResidueSP(NULL)
        else:
            self._thisptr = NucleotideResidueSP(new NucleotideResidue(serial, sequenceNumber, iCode, name))


    cpdef addAtom(self, long long serial, int role):
        return deref(self._thisptr).addAtom(serial,role)

    cpdef getPhosphateGroup(self):
        atms = deref(self._thisptr).getPhosphateGroup()
        out = []
        for atm in atms:
           currentAtm = PyAtom_Init(atm.first)
           out.append(currentAtm)
        return out

    cpdef getSugar(self):
        atms = deref(self._thisptr).getSugar()
        out = []
        for atm in atms:
           currentAtm = PyAtom_Init(atm.first)
           out.append(currentAtm)
        return out


    cpdef getNitrogenousBase(self):
        atms = deref(self._thisptr).getNitrogenousBase()
        out = []
        for atm in atms:
           currentAtm = PyAtom_Init(atm.first)
           out.append(currentAtm)
        return out

    cpdef getGenericAtoms(self):
        atms = deref(self._thisptr).getGenericAtoms()
        out = []
        for atm in atms:
           currentAtm = PyAtom_Init(atm.first)
           out.append(currentAtm)
        return out


cdef PyNucleotideResidue_Init(NucleotideResidueSP ares):
    out = PyNucleotideResidue(None,None,None,None)
    out._thisptr = ares
    return out
















cdef class PyFragment:

    def __cinit__(self, chainID, name):
        if (name==None and chainID==None):
            self._thisptr=FragmentSP(NULL)
        else:
            self._thisptr = FragmentSP(new Fragment(chainID,name))

    cpdef isEqual(self, PyFragment other):
        _othr = other._thisptr
        return deref(self._thisptr).isEqual(deref(_othr))
        

    cpdef getName(self):
        return deref(self._thisptr).getName()
    

    cpdef getChainID(self):
        return deref(self._thisptr).getChainID()
    

    cpdef containsResidue(self, long long sequenceNumber, char insertionCode):
        return deref(self._thisptr).containsResidue(sequenceNumber,insertionCode)
    

    cpdef getGenericResidue(self, long long sequenceNumber, char insertionCode):
        outSP = deref(self._thisptr).getGenericResidue(sequenceNumber, insertionCode)
        out = PyResidue_Init(outSP)
        return out

    cpdef getAminoResidue(self, long long sequenceNumber, char insertionCode):
        outSP = deref(self._thisptr).getAminoResidue(sequenceNumber,insertionCode)
        out = PyAminoResidue_Init(outSP)
        return out
    
    cpdef addGenericResidue(self, PyResidue residue):
        return deref(self._thisptr).addGenericResidue(residue._thisptr)

    cpdef addAminoResidue(self, PyAminoResidue res):
        return deref(self._thisptr).addAminoResidue(res._thisptr)

    cpdef getNumberOfResidues(self):
        return deref(self._thisptr).getNumberOfResidues()

    cpdef getNumberOfGenericResidues(self):
        return deref(self._thisptr).getNumberOfGenericResidues()

    cpdef getNumberOfAminoResidues(self):
        return deref(self._thisptr).getNumberOfAminoResidues()

    cpdef getNumberOfNucleotideResidues(self):
        return deref(self._thisptr).getNumberOfNucleotideResidues()

    cpdef getNumberOfAtoms(self):
        return deref(self._thisptr).getNumberOfAtoms()

    cpdef getParentMolecularSystem(self):
        mlsys = deref(self._thisptr).getParentMolecularSystem()
        mlsysSP = <MolecularSystemSP>(mlsys)
        out = PyMolecularSystem_Init(mlsysSP)
        return out

    
cdef PyFragment_Init(FragmentSP mls):
    result = PyFragment(None,None)
    result._thisptr = mls
    return result

cdef class PyParser:
        
    def __cinit__(self):
        self._thisptr = ParserSP(new Parser())
                
    cpdef readPDB(self,string path, string name):
        _molsys = deref(self._thisptr).readPDB(path,name)
        mlsys = PyMolecularSystem_Init(_molsys)
        return mlsys

    cpdef readXYZ(self, string path, string name):
        _molsys = deref(self._thisptr).readXYZ(path,name)
        mlsys = PyMolecularSystem_Init(_molsys)
        return mlsys
