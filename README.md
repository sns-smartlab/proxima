# Proxima

The SMART Molecular Perception library

----------------------------------------

Proxima is a C++ library meant to be used for Molecular Perception tasks.
It can efficiently read PDB files, compute covalent and hydrogen bonds, 
Gasteiger charges, molecular rings (using a variation of Horton's SSSR)
and atomic hybridisation.

Proxima also includes a Cython wrapper module for using Proxima in Python.

###Src

Contains the Core C++ code for compiling the library.

###PyProxima

Contains the PyProxima library, that is the Cython version of the Core C++ library.

###Console_and_GUI

Contains both the ProximaConsole application and the ProximaGUI
