#ifndef PROXIMA_GASTEIGERPARAMETERS_H
#define PROXIMA_GASTEIGERPARAMETERS_H

#include <map>
#include "atom.h"

/**
 * @author Federico Lazzari
 */
namespace Proxima
{

    /**
     * @brief This class contains several atomic parameters
     *        for the computation of Gasteiger charges for each element.
     *
     * Johann Gasteiger, Mario Marsili,
     * Iterative partial equalization of orbital electronegativity—a rapid access to atomic charges,
     * Tetrahedron,
     * Volume 36, Issue 22,
     * 1980,
     * Pages 3219-3228,
     * ISSN 0040-4020,
     * https://doi.org/10.1016/0040-4020(80)80168-2.
     * (http://www.sciencedirect.com/science/article/pii/0040402080801682)
     */
    class GasteigerParameters
    {
    public:

        /**
         * @brief This method returns the electronegativity of an atom
         *        with the Gasteiger method.
         *
         *        This method computes the electronegativity of an atom
         *        as a taylor series on the charge of the atom.
         *
         * @param atomicNum The atomic number of the atom.
         *
         * @param charge    The charge of the atom.
         *
         * @param hyb       A string representing the hybridisation of the atom.
         *                  (sp3, sp2, sp).
         */
        static double getElectronegativity(int atomicNum, double charge, std::string hyb);

        /**
         * @brief This method returns the parameter used within the Gasteiger method
         *        for the computation of charges within molecular systems, that is the electronegativity
         *        of the atom in the +1 state.
         */
        static double getElectronegativity_plusState(int atomicNum, std::string hyb);


    private:
        struct Parameters
        {
            Parameters(double a, double b, double c);

            double a;
            double b;
            double c;
        };

        /**
         * @brief This associates each set of Gasteiger parameters to each atom in a specific hybridisation state.
         */
        static std::map<std::pair<int,std::string>, Parameters> atomicParameters_withHyb;

        /**
         * @brief This associates each set of Gasteiger parameters to those atom whose parameters are independent of the hybridisation state.
         */
        static std::map<int,Parameters> atomicParameters;

        /**
         * @brief A boolean value determining whether the atomic parameters have been correctly initialized.
         */
        static bool atomicParametersInitialized;

        /**
         * @brief This method initiate the gasteiger parameters for each atom.
         */
        static void initAtomicParameters();
    };

}

#endif // GASTEIGERPARAMETERS_H
