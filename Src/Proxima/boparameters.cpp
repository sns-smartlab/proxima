#include "boparameters.h"

#include <cmath>
#include "./utilities/utilities.h"

Proxima::BOParameters::BOParameters()
{
    this->supportedTypes.insert(5);
    this->supportedTypes.insert(6);
    this->supportedTypes.insert(7);
    this->supportedTypes.insert(8);
    this->supportedTypes.insert(9);

    this->supportedTypes.insert(14);
    this->supportedTypes.insert(15);
    this->supportedTypes.insert(16);
    this->supportedTypes.insert(17);

    this->supportedTypes.insert(32);
    this->supportedTypes.insert(33);
    this->supportedTypes.insert(34);
    this->supportedTypes.insert(35);

    this->supportedTypes.insert(50);
    this->supportedTypes.insert(51);
    this->supportedTypes.insert(52);
    this->supportedTypes.insert(53);

    this->supportedTypes.insert(75);


    //Single bond parameters
    this->peterSingleBondParameters.insert(std::pair<int,double>(5,86));
    this->peterSingleBondParameters.insert(std::pair<int,double>(6,77));
    this->peterSingleBondParameters.insert(std::pair<int,double>(7,73));
    this->peterSingleBondParameters.insert(std::pair<int,double>(8,74));
    this->peterSingleBondParameters.insert(std::pair<int,double>(9,71));

    this->peterSingleBondParameters.insert(std::pair<int,double>(14,117));
    this->peterSingleBondParameters.insert(std::pair<int,double>(15,110));
    this->peterSingleBondParameters.insert(std::pair<int,double>(16,103));
    this->peterSingleBondParameters.insert(std::pair<int,double>(17,99));

    this->peterSingleBondParameters.insert(std::pair<int,double>(32,121));
    this->peterSingleBondParameters.insert(std::pair<int,double>(33,122));
    this->peterSingleBondParameters.insert(std::pair<int,double>(34,117));
    this->peterSingleBondParameters.insert(std::pair<int,double>(35,114));

    this->peterSingleBondParameters.insert(std::pair<int,double>(50,141));
    this->peterSingleBondParameters.insert(std::pair<int,double>(51,142));
    this->peterSingleBondParameters.insert(std::pair<int,double>(52,136));
    this->peterSingleBondParameters.insert(std::pair<int,double>(53,133));

    this->peterSingleBondParameters.insert(std::pair<int,double>(75,137));


    //Multiple bond parameters
    this->peterMultipleBondParameters.insert(std::pair<int,double>(5,40));
    this->peterMultipleBondParameters.insert(std::pair<int,double>(6,35));
    this->peterMultipleBondParameters.insert(std::pair<int,double>(7,38));
    this->peterMultipleBondParameters.insert(std::pair<int,double>(8,45));
    this->peterMultipleBondParameters.insert(std::pair<int,double>(9,43));

    this->peterMultipleBondParameters.insert(std::pair<int,double>(14,31));
    this->peterMultipleBondParameters.insert(std::pair<int,double>(15,32));
    this->peterMultipleBondParameters.insert(std::pair<int,double>(16,29));
    this->peterMultipleBondParameters.insert(std::pair<int,double>(17,28));

    this->peterMultipleBondParameters.insert(std::pair<int,double>(32,22));
    this->peterMultipleBondParameters.insert(std::pair<int,double>(33,35));
    this->peterMultipleBondParameters.insert(std::pair<int,double>(34,28));
    this->peterMultipleBondParameters.insert(std::pair<int,double>(35,27));

    this->peterMultipleBondParameters.insert(std::pair<int,double>(50,20));
    this->peterMultipleBondParameters.insert(std::pair<int,double>(51,31));
    this->peterMultipleBondParameters.insert(std::pair<int,double>(52,27));
    this->peterMultipleBondParameters.insert(std::pair<int,double>(53,26));

    this->peterMultipleBondParameters.insert(std::pair<int,double>(75,45));


    //Electronegativity
    this->peterElectronegativities.insert(std::pair<int,double>(5,2.01));
    this->peterElectronegativities.insert(std::pair<int,double>(6,2.50));
    this->peterElectronegativities.insert(std::pair<int,double>(7,3.07));
    this->peterElectronegativities.insert(std::pair<int,double>(8,3.50));
    this->peterElectronegativities.insert(std::pair<int,double>(9,4.10));

    this->peterElectronegativities.insert(std::pair<int,double>(14,1.74));
    this->peterElectronegativities.insert(std::pair<int,double>(15,2.06));
    this->peterElectronegativities.insert(std::pair<int,double>(16,2.44));
    this->peterElectronegativities.insert(std::pair<int,double>(17,2.83));

    this->peterElectronegativities.insert(std::pair<int,double>(32,2.02));
    this->peterElectronegativities.insert(std::pair<int,double>(33,2.20));
    this->peterElectronegativities.insert(std::pair<int,double>(34,2.48));
    this->peterElectronegativities.insert(std::pair<int,double>(35,2.74));

    this->peterElectronegativities.insert(std::pair<int,double>(50,1.72));
    this->peterElectronegativities.insert(std::pair<int,double>(51,1.82));
    this->peterElectronegativities.insert(std::pair<int,double>(52,2.01));
    this->peterElectronegativities.insert(std::pair<int,double>(53,2.21));

    this->peterElectronegativities.insert(std::pair<int,double>(75,1.46));

}

double Proxima::BOParameters::computeBO_peter(int atom1, int atom2, std::vector<double> atom1Pos, std::vector<double> atom2Pos)
{
    double dist = Utilities::calculateDistance(atom1Pos,atom2Pos);
    double exponent = this->computeExponent(atom1, atom2,dist);
    return std::pow(10.0,exponent);
}



double Proxima::BOParameters::computeExponent(int atom1, int atom2, double dist)
{
    if (atom1==1 || atom2==1)
        return 0;

    if (this->supportedTypes.find(atom1)==this->supportedTypes.end() ||
            this->supportedTypes.find(atom2)==this->supportedTypes.end())
        throw std::invalid_argument("BOParameters::computeExponent - Unsupported atom types!");

    double ra = this->peterSingleBondParameters.at(atom1);
    double rb = this->peterSingleBondParameters.at(atom2);

    double ca = this->peterMultipleBondParameters.at(atom1);
    double cb = this->peterMultipleBondParameters.at(atom2);

    double deltaX = std::abs(this->peterElectronegativities.at(atom2)-this->peterElectronegativities.at(atom1));

    return (-1.0) * ( ((dist*100)-ra-rb+10.0*deltaX) / (ca+cb-17.0*deltaX) );
}
