#ifndef PROXIMA_SSSRGENERATOR_H
#define PROXIMA_SSSRGENERATOR_H

#include "./utilities/WarningsOff.h"

#include <map>
#include <set>
#include <unordered_set>
#include <bitset>
#include <algorithm>

#include "./utilities/WarningsOn.h"

#include "atom.h"

/**
 * @author Federico Lazzari
 */
namespace Proxima {

//Forward declaration
class MolecularSystem;

/**
 * @brief The SSSRGenerator class handles different methods
 *        for the perception of SSSRs.
 */
class SSSRGenerator
{
public:
    /**
     * @brief    The Constructor of the class.
     * @param ms The reference Molecular System
     */
    SSSRGenerator(MolecularSystem* ms);


    /** @brief Destructor. */
    virtual ~SSSRGenerator() = default;

    /**
     * @brief atom_iterator
     *        An iterator to atoms.
     */
    typedef std::vector<long long>::iterator atom_iterator;

    /**
     * @brief It adds a new bond (that is part of the Molecular System)
     *        to the internal bond space of the SSSRGenerator.
     *
     * @note  Cycles are computed within the internal space of bonds.
     */
    virtual bool addBond(BondSP bond);

    /**
     * @brief It sets the internal bond space of the SSSRGenerator
     * @param A vector containing all the bonds to be setted
     */
    virtual bool setBonds(std::vector<BondSP> bonds);

    /**
     * @brief It sets the internal bond space of the SSSRGenerator
     * @param bonds A set containing all the bonds to be setted
     */
    virtual bool setBonds(std::set<BondSP> bonds);

    /**
     * @brief It returns the total number of bonds in the current bond space
     */
    virtual size_t getNumBonds() const;

    /**
     * @brief It returns the total number of atoms in the current atom space
     */
    virtual size_t getNumAtoms() const;

    /**
     * @brief It returns a begin atom:iterator to the current atom space
     */
    virtual atom_iterator atomBegin();

    /**
     * @brief It returns a end atom:iterator to the current atom space
     */
    virtual atom_iterator atomEnd();

    /**
     * @brief  It computes the Smallest Set of Smallest Rings on the internal set of bonds.
     * @return A vector of cycles (each cycle is a vector of bonds)
     * @note  Horton J., "A polynomial-time algorithm to find the shortest cycle basis of a graph.", SIAM J Comput., 1987.
     */
    virtual std::vector<std::vector<BondSP> > findSSSR_Horton(size_t numCycles=0);



protected:

    /**
     * @brief This method associates each bond to an index within the current bond space.
     */
    virtual size_t bond_to_index(BondSP bond);

    /**
     * @brief This method associates each index within the current bond space to a bond.
     */
    virtual BondSP index_to_bond(size_t index);

    /**
     * @brief This method associates each atom to an index within the current atom space.
     */
    virtual size_t atom_to_index(long long atom);

    /**
     * @brief This method associates each index within the current atom space to an atom.
     */
    virtual long long index_to_atom(size_t index);

    //Internal comparator for bitvectors
    struct bitvector_compare {
        bool operator() (const std::vector<bool>& a, const std::vector<bool>& b) const {
            if (a==b) return false;
            if (std::count(a.begin(),a.end(),1)==std::count(b.begin(),b.end(),1)) return (a<b);
            return (std::count(a.begin(),a.end(),1)<std::count(b.begin(),b.end(),1));
        }
    };

    /**
     * @brief Function that returns all of the cycles within the internal set of bonds
     */
    virtual std::set<std::vector<bool>, bitvector_compare> getAllCycles(size_t frameNum);

    /**
     * @brief Function that builds the shortest path matrix
     */
    virtual std::vector<std::vector<std::vector<bool>>> pathBuilder(size_t frameNum);

    /**
     * @brief Function that finds the shortest paths from a source atom
     */
    virtual void pathBuilder_fromSource(long long source, size_t frameNum, std::vector<std::vector<std::vector<bool>>>& paths);

    /**
     * @brief Breadth-First search throw the molecular graph
     */
    virtual std::map<long long, long long> BFS(long long source, size_t frameNum);

    /**
     * @brief This method performs the gaussian elimination.
     */
    virtual std::vector<std::vector<bool>> gaussElimination(std::set<std::vector<bool>, bitvector_compare>& cycles, size_t numCycles);

    /**
     * @brief This method checks the linear independence of the vectors.
     */
    virtual bool testIndependence(std::vector<std::vector<bool>> &cycles);


private:

    /**
     * @brief The internal space of the SSSRGenerator
     *        described by the given bonds.
     */
    std::vector<BondSP> bondSpace;

    /**
     * @brief The atoms of the molecular space
     *        that are part of, at least, one of the
     *        internal  bonds.
     */
    std::vector<long long> atomSpace;

    /**
     * @brief A map associating each bitvector index to a bond
     */
    std::map<size_t,BondSP>  bondSpace_fromIndexToBond;

    /**
     * @brief Map associating each bond to a bitvector index
     */
    std::map<std::pair<long long,long long>, size_t> bondSpace_fromBondToIndex;

    /**
     * @brief Map associating each atom index (in the internal atom space) to an atom
     */
    std::map<size_t,long long>  atomSpace_fromIndexToAtom;

    /**
     * @brief Map associating each atom to an atom index (in the internal atom space)
     */
    std::map<long long, size_t> atomSpace_fromAtomToIndex;

    /**
     * @brief The reference molecular system
     */
    MolecularSystem* molecularSystem;
};




}

#endif // SSSRGENERATOR_H
