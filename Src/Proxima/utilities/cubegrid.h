#ifndef PROXIMA_CUBEGRID_H
#define PROXIMA_CUBEGRID_H

#include <vector>
#include <string>
#include <memory>


/**
 * @author Federico Lazzari
 */
namespace Proxima {

class MolecularSystem;

/**
 * @brief The CubeGrid class represents scalar fields such as the electrostatic potential, and
 *        provides methods for easily access data and for printing data in an output cube file.
 */
class CubeGrid
{

public:

    /**
     * @brief The constructor automatically builds a Bounding Box for the given molecular system and
     *        partitions the bounding box in small units.
     * @param ms The molecular system.
     * @param dx The unit dimension along the x axis.
     * @param dy The unit dimension along the y axis.
     * @param dz The unit dimension along the z axis.
     * @param frameNum The frame number.
     */
    CubeGrid(MolecularSystem* ms, double dx, double dy, double dz, size_t frameNum = 0);

    /** @brief Destructor. */
    virtual ~CubeGrid() = default;

    /**
     * @brief It prints the current data in an output cube file.
     * @param outPath     The path of the output cube file.
     * @param title       The title of current data. It is written in the first row of the cube file.
     * @param description The description of current data. It is written in the second row of the cube file.
     */
    virtual void printCubeFile(std::string outPath, std::string title = "", std::string description = "");

    /**
     * @brief It returns the number of voxels along the x axis.
     */
    virtual int getNumX();

    /**
     * @brief It returns the number of voxels along the y axis.
     */
    virtual int getNumY();

    /**
     * @brief It returns the number of voxels along the z axis.
     */
    virtual int getNumZ();

    /**
     * @brief It returns the origin point of the bounding box.
     */
    virtual std::vector<double> getOrigin();

    /**
     * @brief It returns the end point of the bounding box.
     */
    virtual std::vector<double> getEnd();

    /**
     * @brief It returns the value associated to the given voxel.
     * @param x The xth voxel along x axis
     * @param y The yth voxel along y axis
     * @param z The zth voxel along z axis
     */
    virtual double getData(size_t x, size_t y, size_t z);

protected:
    friend class MolecularSystem;

    //The raw numerical data points
    std::vector<std::vector<std::vector<double>>> data;

    //The number of voxels along x axis
    int numX;

    //The number of voxels along y axis
    int numY;

    //The number of voxels along z axis
    int numZ;

    //The size of voxels along x axis
    double dx;

    //The size of voxels along y axis
    double dy;

    //The size of voxels along z axis
    double dz;

    //The Molecular System
    MolecularSystem* ms;

    //The origin point of the bounding box
    std::vector<double> origin;

    //The end point of the bounding box
    std::vector<double> end;

    //The frame number
    size_t frameNum;

};

/** @brief CubeGridSP is a smart pointer to a CubeGrid object. */
typedef std::shared_ptr< Proxima::CubeGrid > CubeGridSP;

}


#endif // CUBEGRID_H
