#include "floating_point.h"
//#include "utilities/WarningsOff.h"
#include <cmath>
//#include "utilities/WarningsOn.h"

bool Proxima::fuzzyEqual(double x, double y, double epsilon)
{
	return	((fabs(x - y)/fabs(x == 0.0 ? 1.0 : x)) <= epsilon) ||
			((fabs(x - y)/fabs(y == 0.0 ? 1.0 : y)) <= epsilon);
}

bool Proxima::fuzzyEqual(float x, float y, float epsilon)
{
	return	((fabs(x - y)/fabs(x == 0.0f ? 1.0f : x)) <= epsilon) ||
			((fabs(x - y)/fabs(y == 0.0f ? 1.0f : y)) <= epsilon);
}

bool Proxima::fuzzyEqual(const double& x, const double& y)
{
	return fuzzyEqual(x,y,1e-4);
}

bool Proxima::fuzzyEqual(const float& x, const float& y)
{
	return fuzzyEqual(x,y,1e-4f);
}


bool Proxima::fuzzyLessThen(double x, double y, double epsilon)
{
	return (x < y) && (!fuzzyEqual(x, y, epsilon));
}

bool Proxima::fuzzyLessThen(float x, float y, float epsilon)
{
	return (x < y) && (!fuzzyEqual(x, y, epsilon));
}

bool Proxima::fuzzyLessThen(const double& x, const double& y)
{
	return fuzzyLessThen(x,y,1e-4);
}

bool Proxima::fuzzyLessThen(const float& x, const float& y)
{
	return fuzzyLessThen(x,y,1e-4f);
}


bool Proxima::fuzzyEqualOrLessThen(double x, double y, double epsilon)
{
	return (x < y) || fuzzyEqual(x, y, epsilon);
}

bool Proxima::fuzzyEqualOrLessThen(float x, float y, float epsilon)
{
	return (x < y) || fuzzyEqual(x, y, epsilon);
}

bool Proxima::fuzzyEqualOrLessThen(const double& x, const double& y)
{
	return fuzzyEqualOrLessThen(x,y,1e-4);
}

bool Proxima::fuzzyEqualOrLessThen(const float& x, const float& y)
{
	return fuzzyEqualOrLessThen(x,y,1e-4f);
}


bool Proxima::fuzzyGreaterThan(double x, double y, double epsilon)
{
	return (x > y) && (!fuzzyEqual(x, y, epsilon));
}

bool Proxima::fuzzyGreaterThan(float x, float y, float epsilon)
{
	return (x > y) && (!fuzzyEqual(x, y, epsilon));
}

bool Proxima::fuzzyGreaterThan(const double& x, const double& y)
{
	return fuzzyGreaterThan(x,y,1e-4);
}

bool Proxima::fuzzyGreaterThan(const float& x, const float& y)
{
	return fuzzyGreaterThan(x,y,1e-4f);
}


bool Proxima::fuzzyEqualOrGreaterThan(double x, double y, double epsilon)
{
	return (x > y) || fuzzyEqual(x, y, epsilon);
}

bool Proxima::fuzzyEqualOrGreaterThan(float x, float y, float epsilon)
{
	return (x > y) || fuzzyEqual(x, y, epsilon);
}

bool Proxima::fuzzyEqualOrGreaterThan(const double& x, const double& y)
{
	return fuzzyEqualOrGreaterThan(x,y,1e-4);
}

bool Proxima::fuzzyEqualOrGreaterThan(const float& x, const float& y)
{
	return fuzzyEqualOrGreaterThan(x,y,1e-4f);
}

//bool Proxima::fuzzyEqual(const QVector3D& v1,
//                                      const QVector3D& v2,
//                                      float epsilon)
//{
//    return	fuzzyEqual(v1.x(), v2.x(), epsilon) &&
//            fuzzyEqual(v1.y(), v2.y(), epsilon) &&
//            fuzzyEqual(v1.z(), v2.z(), epsilon);
//}

//bool Proxima::fuzzyEqual(const QVector3D& v1, const QVector3D& v2)
//{
//    return	fuzzyEqual(v1.x(), v2.x(), 1e-4f) &&
//            fuzzyEqual(v1.y(), v2.y(), 1e-4f) &&
//            fuzzyEqual(v1.z(), v2.z(), 1e-4f);
//}

//bool Proxima::fuzzyEqual(const QVector4D& v1,
//                                const QVector4D& v2,
//                                float epsilon)
//{
//    return	fuzzyEqual(v1.x(), v2.x(), epsilon) &&
//            fuzzyEqual(v1.y(), v2.y(), epsilon) &&
//            fuzzyEqual(v1.z(), v2.z(), epsilon) &&
//            fuzzyEqual(v1.w(), v2.w(), epsilon);
//}

//bool Proxima::fuzzyEqual(const QVector4D& v1, const QVector4D& v2)
//{
//    return	fuzzyEqual(v1.x(), v2.x(), 1e-4f) &&
//            fuzzyEqual(v1.y(), v2.y(), 1e-4f) &&
//            fuzzyEqual(v1.z(), v2.z(), 1e-4f) &&
//            fuzzyEqual(v1.w(), v2.w(), 1e-4f);
//}


//bool Proxima::fuzzyEqual(const QMatrix4x4& m1,
//                                const QMatrix4x4& m2,
//                                float epsilon)
//{
//    bool equals = true;
//    for(int i = 0; (i < 4) && equals; ++i)
//        for(int j = 0; (j < 4) && equals; ++j)
//            equals = equals &&
//                     Proxima::fuzzyEqual(m1(i,j), m2(i,j), epsilon);

//    return equals;
//}

//bool Proxima::fuzzyEqual(const QMatrix4x4& m1, const QMatrix4x4& m2)
//{
//    return Proxima::fuzzyEqual(m1, m2, 1e-4f);
//}
