#ifndef PROXIMA_UTILITIES_H
#define PROXIMA_UTILITIES_H

#include "WarningsOff.h"
#include <chrono>
#include <string>
#include <vector>
#include <limits>
#include <memory>
#include <iostream>

#include "floating_point.h"

#include "WarningsOn.h"

/**
 * @author Federico Lazzari
 */
namespace Proxima
{

/**
 * @brief The Utilities class is a static class containing
 *        support methods (e.g. for computing dihedrals,
 *        distances, etc.)
 */
class Utilities
{

public:

	/** @brief String trim. */
	static std::string trim(const std::string& str);

	/** @brief Checks whether str is a white-space string. */
	static bool whiteSpacesOnly(const std::string& str);

	/** @brief Checks if str is decimal digit.  */
	static bool containsDigit(const std::string& str);

	/** @brief Makes a string upper case. */
	static void upperCase(std::string& str);

	/** @brief Calculates the distance between two vectors. */
    static double calculateDistance(const std::vector<double>& vector1,
                                        const std::vector<double>& vector2);

    /** @brief Brings the current angle value in the [0,180] interval
     *         Angles are in degrees
     */
    static double angleInterval(const double angle);

    //TODO: Controlla cosa succede a string to double/string to long long quando passi una stringa vuota
	/**
	 * @brief	Converts a string in a double.
	 *			The output parameter "outOk" is set to true if the convertion
	 *			succeeded and the result is a finite number, and is set to false otherwise.
	 */
	static double stringToDouble(const std::string& s, bool& outOk);

	/**
	 * @brief	Converts a string in a double.
	 *			Returns NaN if the conversion fails or if the result is note a finite number.
	 */
	static long long stringToLongLong(const std::string& s, bool& outOk);

    /**
     * @brief It outputs the normalized version of the vector given as input.
     */
    static std::vector<double> normalize(std::vector<double>& vec);

    /**
     * @brief It computes the cross product between the two vectors given as input.
     */
    static std::vector<double> cross(std::vector<double>& vec1, std::vector<double>& vec2);

    /**
     * @brief It computes the dot product between the vectors given as input.
     */
    static double dot(std::vector<double>& vec1, std::vector<double>& vec2);

    /**
     * @brief It computes the sum of the vectors given as input.
     */
    static std::vector<double> sum(std::vector<double>& vec1, std::vector<double>& vec2);

    /**
     * @brief It computes the difference between the vectors given as input.
     */
    static std::vector<double> minus(std::vector<double>& vec1, std::vector<double>& vec2);

    /**
     * @brief It computes the product between the vector and the scalar number given as input.
     */
    static std::vector<double> product(std::vector<double>& vec, double number);

    /** @brief Computes the angle beetween two vectors.
     *  @note angle is expressed in degrees.
    */
    static double calculateAngle(std::vector<double> &vector1, std::vector<double> &vector2);

    /**
     * @brief Computes the dihedral angle between the three vectors.
     * @note  angle is expressed in degrees.
     */
    static double calculateDihedral(std::vector<double> vector1, std::vector<double> vector2, std::vector<double> vector3);

    /**
     * @brief It returns the positive result of a second order equation.
     */
    static double solveSecondOrderPositive(double a, double b, double c);

    /**
     * @brief It returns the negative result of a second order equation.
     */
    static double solveSecondOrderNegative(double a, double b, double c);

    /**
     * @brief       It returns the rotation matrix along an axis given a specific angle
     * @param axis  The axis along wich the rotation occurs
     * @param angle The rotation angle
     */
    static std::vector<std::vector<double>> getRotationMatrix(std::vector<double> axis, double angle);

    /**
     * @brief           It computes the matrix multiplication.
     * @param matrix    The matrix.
     * @param vector    The vector to multiply with the matrix.
     */
    static std::vector<double> matrixMultiplication(std::vector<std::vector<double> >& matrix, std::vector<double>& vector);

    /**
     * @brief Compares two vectors.
     * @param vec1 vector to be compared with vec2.
     * @param vec2 vector.
     * @param differences a vector containing vec1 elements which are not in vec2.
     * @note  Not valid for floating point numbers.
     * @note  Eventual old values in differences are erased.
     */
    template <typename T>
    static void compareVector(const std::vector<T>& vec1,
                              const std::vector<T>& vec2,
                              std::vector<T>& differences);

    /**
     * @brief It detects whether two vector are fuzzy equal
     */
    static bool areVectorEqual(const std::vector<double>& vec1,
                                   const std::vector<double>& vec2);

private:
    friend class MolecularSystem;


    static std::string outStrPos_molParser(double x);

};




/**
 * @brief	Three-dimensional box bounding the
 *			region of space occupied by an object.
 */
struct BoundingBox
{
	double minX;
	double minY;
	double minZ;

	double maxX;
	double maxY;
	double maxZ;

	BoundingBox()
	{
		minX=std::numeric_limits<double>::max();
		minY=std::numeric_limits<double>::max();
		minZ=std::numeric_limits<double>::max();

		maxX=std::numeric_limits<double>::lowest();
		maxY=std::numeric_limits<double>::lowest();
		maxZ=std::numeric_limits<double>::lowest();
	}
};

}


#endif // UTILITIES_H
