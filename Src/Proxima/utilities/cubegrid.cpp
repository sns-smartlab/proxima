#include "cubegrid.h"
#include  "../molecularsystem.h"
#include <iostream>
#include <fstream>


Proxima::CubeGrid::CubeGrid(Proxima::MolecularSystem *ms, double dx, double dy, double dz, size_t frameNum)
{
    BoundingBox box = ms->computeBBox(frameNum);

    std::vector<double> origin{box.minX, box.minY, box.minZ};
    std::vector<double> end{box.maxX, box.maxY, box.maxZ};

    this->origin = origin;
    this->end = end;

    int numX = int((end[0]-origin[0])/dx);
    int numY = int((end[1]-origin[1])/dy);
    int numZ = int((end[2]-origin[2])/dz);

    this->dx=dx;
    this->dy=dy;
    this->dz=dz;

    //Initializing data
    this->data.resize(size_t(numX));
    for (size_t idx = 0; idx != size_t(numX); idx++)
    {
        this->data[idx].resize(size_t(numY));

        for (size_t idx2 = 0; idx2 != size_t(numY); idx2++)
        {
            this->data[idx][idx2].resize(size_t(numZ));
        }
    }

    this->numX = numX;
    this->numY = numY;
    this->numZ = numZ;

    this->ms = ms;
    this->frameNum = frameNum;
}

void Proxima::CubeGrid::printCubeFile(std::string outPath, std::string title, std::string description)
{
    std::ofstream outFile;
    outFile.open(outPath); //TODO: Try/Catch

    outFile << " " << title << "\n";
    outFile << " " << description << "\n";

    outFile  << "   " << this->ms->getNumAtoms() << "   " << this->origin[0] << "   " << this->origin[1] << "   " << this->origin[2] << "   1\n";

    //Volume data (axis vectors)
    outFile << "   " << this->numX << "   "  << dx << "   0.000000   0.000000\n";
    outFile << "   " << this->numY << "   0.000000   " << dy << "   0.000000\n";
    outFile << "   " << this->numZ << "   0.000000   0.000000   "<< dz <<"\n";



    //Prima parte: geometria
    for(auto it=this->ms->atomsBegin(); it != this->ms->atomsEnd(); it++)
    {
        AtomSP current = it->second;

        std::vector<double> pos = current->getPosition(this->frameNum);

        outFile << current->getAtomicNum()<< "    " << double(current->getAtomicNum()) << ".000000   " << pos[0] << "    " << pos[1] << "    " << pos[2] << "\n";

    }

    int numPoints=0;

    //Seconda parte: Voxels
    for (size_t idX= 0; idX != size_t(numX); idX++)
    {
        for (size_t idY=0; idY != size_t(numY); idY++)
        {
            for (size_t idZ=0; idZ != size_t(numZ); idZ++)
            {
                double value = this->data[idX][idY][idZ];
                numPoints++;

                if (numPoints<=6)
                {
                    outFile << " " << value;
                }

                else
                {
                    outFile << "\n";
                    outFile << value;
                    numPoints = 1;
                }

            }
        }
    }



    outFile.close();
}

int Proxima::CubeGrid::getNumX()
{
    return this->numX;
}

int Proxima::CubeGrid::getNumY()
{
    return this->numY;
}

int Proxima::CubeGrid::getNumZ()
{
    return this->numZ;
}

std::vector<double> Proxima::CubeGrid::getOrigin()
{
    return this->origin;
}

std::vector<double> Proxima::CubeGrid::getEnd()
{
    return this->end;
}

double Proxima::CubeGrid::getData(size_t x, size_t y, size_t z)
{
    if (x>size_t(this->numX))
            std::cerr<<"CubeGrid::getData - wrong x value"<<std::endl;

    if (y>size_t(this->numY))
            std::cerr<<"CubeGrid::getData - wrong y value"<<std::endl;

    if (z>size_t(this->numZ))
            std::cerr<<"CubeGrid::getData - wrong z value"<<std::endl;

    return this->data[x][y][z];
}
