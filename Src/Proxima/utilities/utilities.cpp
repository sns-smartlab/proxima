//#include "utilities/WarningsOff.h"
#include <algorithm>
#include <math.h>
#include <sstream>
//#include "utilities/WarningsOn.h"
#include "utilities.h"

#ifndef M_PI
    #define M_PI 3.14159265358979323846
#endif

using namespace Proxima;

std::string Utilities::trim(const std::string& str)
{
	size_t first = str.find_first_not_of(' ');
	size_t last = str.find_last_not_of(' ');
	if( (first != std::string::npos) && (last != std::string::npos) )
		return str.substr(first, (last-first+1));
	else
		return "";
}

bool Utilities::whiteSpacesOnly(const std::string& str)
{
	return std::all_of(str.begin(), str.end(), isspace);
}

bool Utilities::containsDigit(const std::string& str)
{
	return std::any_of(str.begin(), str.end(), isdigit);
}

void Utilities::upperCase(std::string& str)
{
    for(auto & c: str) c = toupper(c);
}

double Utilities::calculateDistance(const std::vector<double> &vector1, const std::vector<double> &vector2)
{
    if (vector1.size()!=3 || vector2.size()!=3) throw std::invalid_argument("Utilities::calculateDistance - Non tridimensional vectors given");
    double output=0;
    size_t i=0;
    while (i!=3)
    {
        output+=(vector2[i]-vector1[i])*(vector2[i]-vector1[i]);
        i++;
    }

    return sqrt(output);
}

double Utilities::angleInterval(const double angle)
{
    return angle-180.0*std::floor(angle/180.0);
}

double Utilities::stringToDouble(const std::string& s, bool& outOk)
{
    if(whiteSpacesOnly(s))
    {
        outOk = false;
        return 0;
    }

	double result = 0;
	try
	{
		result = std::stod(s);
		if(std::isinf(result)) outOk = false;
		else outOk = true;
	}
	catch(...) { outOk = false; }

	return result;
}

long long Utilities::stringToLongLong(const std::string& s, bool& outOk)
{
    if(whiteSpacesOnly(s))
    {
        outOk = false;
        return 0;
    }

	long long result = 0;
	try
	{
		result = std::stoll(s);
		outOk = true;
	}
	catch(...) { outOk = false; }

    return result;
}

std::vector<double> Utilities::normalize(std::vector<double> &vec)
{
    if (vec.size()!=3) throw std::invalid_argument("Utilities::normalize - Non tridimensional vectors given");
    std::vector<double> output = vec;
    double norm = dot(vec,vec);
    norm = sqrt(norm);

    size_t i=0;
    while (i!=3)
    {
        output[i]=output[i]/norm;
        i++;
    }

    return output;
}

std::vector<double> Utilities::cross(std::vector<double> &vec1, std::vector<double> &vec2)
{
    if (vec1.size()!=3 || vec2.size()!=3) throw std::invalid_argument("Utilities::cross - Non tridimensional vectors given");
    std::vector<double> output{0.,0.,0.};

    output[0]=vec1[1]*vec2[2]-vec1[2]*vec2[1];
    output[1]=vec1[2]*vec2[0]-vec1[0]*vec2[2];
    output[2]=vec1[0]*vec2[1]-vec1[1]*vec2[0];

    return output;
}

double Utilities::dot(std::vector<double> &vec1, std::vector<double> &vec2)
{
    if (vec1.size()!=3 || vec2.size()!=3) throw std::invalid_argument("Utilities::dot - Non tridimensional vectors given");
    double output=0;
    size_t i=0;
    while (i!=3)
    {
        output+=vec1[i]*vec2[i];
        i++;
    }

    return output;
}

std::vector<double> Utilities::sum(std::vector<double> &vec1, std::vector<double> &vec2)
{
    if (vec1.size()!=3 || vec2.size()!=3) throw std::invalid_argument("Utilities::sum - Non tridimensional vectors given");

    std::vector<double> output{0.,0.,0.};

    size_t i=0;
    while (i!=3)
    {
        output[i]=vec1[i]+vec2[i];
        i++;
    }

    return output;

}

std::vector<double> Utilities::minus(std::vector<double> &vec1, std::vector<double> &vec2)
{
    if (vec1.size()!=3 || vec2.size()!=3) throw std::invalid_argument("Utilities::minus - Non tridimensional vectors given");

    std::vector<double> invertedsign = product(vec2,-1);
    return sum(vec1,invertedsign);
}

std::vector<double> Utilities::product(std::vector<double> &vec, double number)
{
    if (vec.size()!=3) throw std::invalid_argument("Utilities::product - Non tridimensional vectors given");

    std::vector<double> output{0.,0.,0.};

    size_t i=0;
    while (i!=3)
    {
        output[i]=vec[i]*number;
        i++;
    }

    return output;
}

double Utilities::calculateAngle(std::vector<double>& vector1, std::vector<double>& vector2)
{
    std::vector<double> normd1 = normalize(vector1);
    std::vector<double> normd2 = normalize(vector2);

    return acos(dot(normd1,normd2))*(180/M_PI);

}

double Utilities::calculateDihedral(std::vector<double> vector1, std::vector<double> vector2, std::vector<double> vector3)
{
    std::vector<double> cross1 = cross(vector1,vector2);
    std::vector<double> cross2 = cross(vector2,vector3);
    std::vector<double> normd2 = normalize(vector2);
    std::vector<double> crossed = cross(cross1,cross2);

    double angle = std::atan2(dot(crossed,normd2),dot(cross1,cross2))*(180/M_PI);
    if (angle>180) return 360-angle;
    else return angle;

}

double Utilities::solveSecondOrderPositive(double a, double b, double c)
{
    double delta = b*b-4*a*c;
    return (-b+sqrt(delta))/(2*a);
}

double Utilities::solveSecondOrderNegative(double a, double b, double c)
{
    double delta = b*b-4*a*c;
    return (-b-sqrt(delta))/(2*a);
}

std::vector<std::vector<double> > Utilities::getRotationMatrix(std::vector<double> axis, double angle)
{
    std::vector<std::vector<double> > out{std::vector<double>{0.,0.,0.}, std::vector<double>{0., 0., 0.}, std::vector<double>{0., 0., 0.}};

    axis = Utilities::normalize(axis);
    double a = cos(angle *(M_PI/180) / 2.0);
    double b, c, d;
    b = Utilities::product(axis,(-1.0)*sin(angle*(M_PI/180)/2.0))[0];
    c = Utilities::product(axis,(-1.0)*sin(angle*(M_PI/180)/2.0))[1];
    d = Utilities::product(axis,(-1.0)*sin(angle*(M_PI/180)/2.0))[2];

    double aa, bb, cc, dd;
    aa = a*a;
    bb = b*b;
    cc = c*c;
    dd = d*d;

    double bc, ad, ac, ab, bd, cd;
    bc = b*c;
    ad = a*d;
    ac = a*c;
    ab = a*b;
    bd = b*d;
    cd = c*d;

    out[0][0] = aa + bb - cc - dd;
    out[0][1] = 2 * (bc + ad);
    out[0][2] = 2 * (bd - ac);

    out[1][0] = 2 * (bc - ad);
    out[1][1] = aa + cc - bb - dd;
    out[1][2] = 2 * (cd + ab);

    out[2][0] = 2 * (bd + ac);
    out[2][1] = 2 * (cd - ab);
    out[2][2] = aa + dd - bb - cc;


    return out;


}

std::vector<double> Utilities::matrixMultiplication(std::vector<std::vector<double> > &matrix, std::vector<double> &vector)
{
    if (matrix.size()!=3 || vector.size()!=3) throw std::invalid_argument("Utilities::matrixMultiplication - Non tridimensional vectors given");

    std::vector<double> out{0., 0., 0.};

    for (size_t i = 0; i != 3; i++)
    {
        double currentValue = 0;

        for (size_t j = 0; j != 3; j++)
        {
            currentValue = currentValue + matrix[i][j]*vector[j];
        }

        out[i] = currentValue;
    }

    return out;
}

bool Utilities::areVectorEqual(const std::vector<double> &vec1, const std::vector<double> &vec2)
{
    if(vec1.size()!=vec2.size())
        return false;

    for(auto i=0; i!=3; i++)
    {
        if (!fuzzyEqual(vec1[i],vec2[i]))
        {
            return false;
        }
    }

    return true;
}



std::string Utilities::outStrPos_molParser(double x)
{
    //Generating the string
    std::stringstream ss;
    ss << x;
    std::string out = ss.str();
    if (out[0] == '-')
        out.erase(out.begin());

    size_t len = out.length();

    if (len<7)
    {
        size_t foundPoint = out.find('.');
        if (foundPoint == std::string::npos)
            out.append(".");
        out.resize(7,'0');
    }

    else if (len>7)
    {
        out.resize(7);
    }

    return out;
}



template <typename T>
void Utilities::compareVector(const std::vector<T> &vec1, const std::vector<T> &vec2, std::vector<T> &differences)
{
    if(vec1.size()!=vec2.size())
        std::cerr<<"Utilities::compareVector(vec1, vec2, differences) - vec1 and vec2 have different sizes"<<std::endl;

    differences.clear();
    for(size_t i=0; i!=vec1.size(); i++)
    {
        if ((i<vec2.size()-1 && (vec1[i])!=(vec2[i])) || i>vec2.size()-1)
        {
            differences.push_back(vec1[i]);
        }
    }
}
