#include "math3d.h"

#include <cmath>


bool Proxima::rayAABBIntersection(	const mathfu::Vector<double, 3>& rayOrigin,
									const mathfu::Vector<double, 3>& rayDir,
									const mathfu::Vector<double, 3>& aabbMinXYZ,
									const mathfu::Vector<double, 3>& aabbMaxXYZ,
									double& outTmin, double& outTmax)
{
	const double EPSILON = 1e-4;

	outTmin = std::numeric_limits<double>::lowest();
	outTmax = std::numeric_limits<double>::max();

	// For each pair of planes orthogonal to X, Y and Z...
	for(short i = 0; i < 3; i++)
	{
		if(std::abs(rayDir[i]) < EPSILON)
		{
			// The ray is orthogonal to the i-th axis.
			// The ray may intersect the box only if the origin lies
			// within the considered pair of planes.
			if((rayOrigin[i] < aabbMinXYZ[i]) || (rayOrigin[i] > aabbMaxXYZ[i]))
				return false;
		}
		else
		{
			// Computes the intersection "times" between the ray and the
			// pair of plains orthogonal to the i-th axis.
			double inv_dir = 1.0 / rayDir[i];
			double t_i_min =  (aabbMinXYZ[i] - rayOrigin[i]) * inv_dir;
			double t_i_max =  (aabbMaxXYZ[i] - rayOrigin[i]) * inv_dir;
			if(t_i_max < t_i_min) std::swap(t_i_min, t_i_max);

			// If needed, updates outTmin and outTmax
			outTmin = std::max(outTmin, t_i_min);
			outTmax = std::min(outTmax, t_i_max);

			// If the intersection between the 3 intervals
			// [t_i_min, t_i_max] is empty, the ray miss the box.
			if(outTmin > outTmax) return false;
		}
	}

	// The ray hits the box. The two intersection "times"
	// are stored in outTmin and outTmax.
	return true;
}


bool Proxima::rayPlaneIntersection(	const mathfu::Vector<double, 3>& rayOrigin,
									const mathfu::Vector<double, 3>& rayDir,
									const mathfu::Vector<double, 3>& planePoint,
									const mathfu::Vector<double, 3>& planeNormal,
									double& out_t)
{
	typedef mathfu::Vector<double, 3> dvec3;

	dvec3 N = planeNormal.Normalized();
	double NdotD = dvec3::DotProduct(N, rayDir);
	// If the ray is (almost) parallel to the plane, there is no intersection
	const double EPSILON = 1e-5;
	if(std::fabs(NdotD) < EPSILON) return false;

	out_t = dvec3::DotProduct(planePoint - rayOrigin, N) / NdotD;
	return true;
}
