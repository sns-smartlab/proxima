#ifndef WARN_ON
#define WARN_ON

/* To be included after WarningsOff.h to renable all the compiler's warnings. */

#if defined(_MSC_VER)	/* Visual Studio */
	#pragma warning(pop)

#elif defined(__GNUC__) || defined(__GNUG__) /* GNU GCC/G++ */
	#pragma GCC diagnostic pop

#elif defined(__clang__) /* Clang/LLVM */
	#pragma clang diagnostic pop

#endif

#endif
