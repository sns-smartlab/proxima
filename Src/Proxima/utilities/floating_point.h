#ifndef PROXIMA_FLOATING_POINT_H
#define PROXIMA_FLOATING_POINT_H


/**
 * @author Andrea Salvadori
 */
namespace Proxima
{

/**
 * @brief	Compares two floating point numbers for equality.
 *			In particular, x and y are considered equals if the
 *			relative differerence is smaller than epsilon.
 *			The default value for epsilon is 1e-4.
 *
 * @note    For details see:
 *			http://www.boost.org/doc/libs/1_59_0/libs/test/doc/html/boost_test/testing_tools/extended_comparison/floating_point/floating_points_comparison_theory.html
 */
///@{
bool fuzzyEqual(double x, double y, double epsilon);
bool fuzzyEqual(const double& x, const double& y); // const T& has been used to work with template stuff
bool fuzzyEqual(float x, float y, float epsilon);
bool fuzzyEqual(const float& x, const float& y); // const T& has been used to work with template stuff
///!@}

/**
 * @brief	These functions implement the binary comparing
 *			operators between two floating point numbers.
 *			The comparison is performed in an approximate
 *			("fuzzy") manner, by exploiting the fuzzyEqual()
 *			function.
 */
///@{

/**
 * @brief	Approximate < operator. Compares two floating point
 *			numbers according to the following expression:
 *			(x < y) && (!fuzzyEqual(x, y, epsilon))
 *			The default value for epsilon is 1e-4.
 */
///@{
bool fuzzyLessThen(double x, double y, double epsilon);
bool fuzzyLessThen(float x, float y, float epsilon);
bool fuzzyLessThen(const double& x, const double& y); // const T& has been used to work with template stuff
bool fuzzyLessThen(const float& x, const float& y); // const T& has been used to work with template stuff
///!@}

/**
 * @brief	Approximate <= operator. Compares two floating point
 *			numbers according to the following expression:
 *			(x < y) || fuzzyEqual(x, y, epsilon)
 *			The default value for epsilon is 1e-4.
 */
///@{
bool fuzzyEqualOrLessThen(double x, double y, double epsilon);
bool fuzzyEqualOrLessThen(float x, float y, float epsilon);
bool fuzzyEqualOrLessThen(const double& x, const double& y); // const T& has been used to work with template stuff
bool fuzzyEqualOrLessThen(const float& x, const float& y); // const T& has been used to work with template stuff
///!@}

/**
 * @brief	Approximate > operator. Compares two floating point
 *			numbers according to the following expression:
 *			(x > y) && (!fuzzyEqual(x, y, epsilon))
 *			The default value for epsilon is 1e-4.
 */
///@{
bool fuzzyGreaterThan(double x, double y, double epsilon);
bool fuzzyGreaterThan(float x, float y, float epsilon);
bool fuzzyGreaterThan(const double& x, const double& y); // const T& has been used to work with template stuff
bool fuzzyGreaterThan(const float& x, const float& y); // const T& has been used to work with template stuff
///!@}

/**
 * @brief	Approximate <= operator. Compares two floating point
 *			numbers according to the following expression:
 *			(x > y) || fuzzyEqual(x, y, epsilon)
 *			The default value for epsilon is 1e-4.
 */
///@{
bool fuzzyEqualOrGreaterThan(double x, double y, double epsilon);
bool fuzzyEqualOrGreaterThan(float x, float y, float epsilon);
bool fuzzyEqualOrGreaterThan(const double& x, const double& y); // const T& has been used to work with template stuff
bool fuzzyEqualOrGreaterThan(const float& x, const float& y); // const T& has been used to work with template stuff
///!@}

///!@}


 /**
 * @brief	Compares two QVectors3D or QVectors4D for equality.
 *			In particular, v1 and v2 are considered equals if the relative
 *			differerence between each pair of components is smaller than a
 *			epsilon. The default value for epsilon is 1e-4.
 *
 * @note    For details see:
 *			http://www.boost.org/doc/libs/1_59_0/libs/test/doc/html/boost_test/testing_tools/extended_comparison/floating_point/floating_points_comparison_theory.html
 */
 ///@{
//bool fuzzyEqual(const QVector3D& v1, const QVector3D& v2, float epsilon);
//bool fuzzyEqual(const QVector3D& v1, const QVector3D& v2);
//bool fuzzyEqual(const QVector4D& v1, const QVector4D& v2, float epsilon);
//bool fuzzyEqual(const QVector4D& v1, const QVector4D& v2);
 ///!@}


 /**
 * @brief	Compares two QMatrix4x4 for equality.
 *			In particular, m1 and m2 are considered equals if the relative
 *			differerence between their elements is smaller than a epsilon.
 *			The default value for epsilon is 1e-4.
 *
 * @note    For details see:
 *			http://www.boost.org/doc/libs/1_59_0/libs/test/doc/html/boost_test/testing_tools/extended_comparison/floating_point/floating_points_comparison_theory.html
 */
 ///@{
 ///
//bool fuzzyEqual(const QMatrix4x4& m1, const QMatrix4x4& m2, float epsilon);
//bool fuzzyEqual(const QMatrix4x4& m1, const QMatrix4x4& m2);
 ///!@}

}

#endif // FLOATING_POINT_H
