#ifndef WARN_OFF
#define WARN_OFF

/* Disables all the compiler's warnings. Include WarningsOn.h to re-enable them. */

#if defined(_MSC_VER)	/* Visual Studio */
	#pragma warning(push, 0)

#elif defined(__GNUC__) || defined(__GNUG__) /* GNU GCC/G++ */
	#pragma GCC diagnostic push
	#pragma GCC diagnostic ignored "-Wall"
	#pragma GCC diagnostic ignored "-Wextra"
    #pragma GCC diagnostic ignored "-Wstrict-aliasing"
    #pragma GCC diagnostic ignored "-Wunused-parameter"
    #pragma GCC diagnostic ignored "-Wsign-compare"
    #pragma GCC diagnostic ignored "-Wdeprecated-declarations"
//    #pragma GCC diagnostic ignored "-Wswitch"
//    #pragma GCC diagnostic ignored "-Wreorder"
//    #pragma GCC diagnostic ignored "-Wtype-limits"
//    #pragma GCC diagnostic ignored "-Wmaybe-uninitialized"
//    #pragma GCC diagnostic ignored "-Wdelete-non-virtual-dtor"





#elif defined(__clang__) /* Clang/LLVM */
	#pragma clang diagnostic push
	#pragma clang diagnostic ignored "-Weverything"
    #pragma clang diagnostic ignored "-Wstrict-aliasing"
    #pragma clang diagnostic ignored "-Wunused-parameter"
    #pragma clang diagnostic ignored "-Wsign-compare"
//    #pragma clang diagnostic ignored "-Wswitch"
//    #pragma clang diagnostic ignored "-Wreorder"
//    #pragma clang diagnostic ignored "-Wtype-limits"
//    #pragma clang diagnostic ignored "-Wmaybe-uninitialized"
//    #pragma clang diagnostic ignored "-Wdelete-non-virtual-dtor"


#endif

#endif
