#include<iostream>
#include<fstream>

#include "parser.h"
#include "utilities/utilities.h"
#include"parser/molecularbuilder.h"
#include"parser/altlocstate.h"
#include"parser/firstmodelstate.h"
#include"parser/modelsstate.h"


using namespace	Proxima;


MolecularSystemSP Parser::readPDB(const std::string &path, const std::string &name, double total_charge)
{
    //Opening file
    std::ifstream f(path);

    //Checking wheter the file is open
    if (!(f.is_open()))
        throw std::invalid_argument("MolecularSystem::readPDB - Cannot open file!");

    PDBMolecularBuilderSP molBuilder(new PDBMolecularBuilder(name, total_charge));

    std::string line;
    unsigned numLine=0;
    RecordData atomInfo;
    bool firstModelEncountered=false;


    //Reading lines
    while (std::getline(f, line))
    {
        numLine++;
        //Resizing the line as to satisfy the PDB standard (80 characters)
        line.resize(80,' ');

        //If the Line is a ATOM/HETATM record
        if (line.substr(0,6)=="ATOM  " || line.substr(0,6)=="HETATM")
        {

            //Parsing the ATOM record
            bool isRecordValid = molBuilder->parseAtomRecord(line,numLine,atomInfo);
            if (!isRecordValid) break;

            //Handling the ATOM record accordingly
            bool isHandledWell = molBuilder->handleAtomRecord(atomInfo);
            if (!isHandledWell) break;

        }

        //If the line is a MODEL record (and it is not the first line)
        else if (line.substr(0,5)=="MODEL")
        {
            if (firstModelEncountered)
            {
                //Handling the MODEL record accordingly
                bool isParsingAllowed = molBuilder->handleModelRecord();
                if (!isParsingAllowed) break;
            }

            else firstModelEncountered = true;

        }

        else continue;
    }

    molBuilder->handleNewResidue();

    //Adding the eventual alternate locations to the system
    molBuilder->addAltLocAtoms();

    //Closing file
    f.close();

    //Returning the molecular system
    return molBuilder->getMolecularSystem();

}

MolecularSystemSP Parser::readXYZ(const std::string &path, const std::string &name, double total_charge)
{

    //Opening file
    std::ifstream f(path);

    //Checking wheter the file is open
    if (!(f.is_open()))
        throw std::invalid_argument("MolecularSystem::readXYZ - Cannot open file!");

    //Creating Molecular System
    MolecularSystemSP ms(new MolecularSystem(name, total_charge));

    std::string line;
    unsigned numLine=0;

    //Reading lines
    while (std::getline(f, line))
    {
        numLine++;

        std::stringstream s(line); // Used for breaking words
        std::string word; //The single word

        int count=0;

        if (numLine>2)
        {
            int z;
            std::string name;
            double x_pos;
            double y_pos;
            double z_pos;
            bool isValid=true;

            while (s >> word)
            {
                if (count==0)
                {
                    //This should be the atomic symbol
                    name= word;
                    z = Element::getAtomicNumber(word);
                    count++;
                }
                else if (count==1) {
                    //This should be the x
                    try {
                        x_pos = std::stod(word);
                    }
                    catch (std::invalid_argument) {
                        std::cerr<<"Warning! - Parser::readXYZ. Invalid coordinates. The parsing of the XYZ file is not completed."<<std::endl;
                        isValid=false;
                    }
                    count++;
                }
                else if (count == 2){
                    //This should be the y
                    try {
                        y_pos = std::stod(word);
                    }
                    catch (std::invalid_argument) {
                        std::cerr<<"Warning! - Parser::readXYZ. Invalid coordinates. The parsing of the XYZ file is not completed."<<std::endl;
                        isValid=false;
                    }
                    count++;
                } else {
                    //This should be z
                    try {
                        z_pos = std::stod(word);
                    }
                    catch (std::invalid_argument) {
                        std::cerr<<"Warning! - Parser::readXYZ. Invalid coordinates. The parsing of the XYZ file is not completed."<<std::endl;
                        isValid=false;
                    }
                    count++;
                }

            }


            if (z!=0 && isValid) {
                AtomSP atom(new Atom(z,name,numLine-3));
                atom->setPosition(0, std::vector<double>{x_pos,y_pos,z_pos});
                ms->addAtom(atom);
                z=0;
            } else {
                std::cerr<<"Warning! - Parser::readXYZ. The parsing of the XYZ file is not completed."<<std::endl;
                break;
            }
        }
    }

    //Closing file
    f.close();

    return ms;

}

MolecularSystemSP Parser::readMOL(const std::string &path, const std::string &name, double total_charge)
{
    //Opening file
    std::ifstream f(path);

    //Checking wheter the file is open
    if (!(f.is_open()))
        throw std::invalid_argument("MolecularSystem::readMOL - Cannot open file!");

    //Creating Molecular System
    MolecularSystemSP ms(new MolecularSystem(name, total_charge));

    std::string line;
    unsigned numLine=0;

    bool atomData=false;
    bool bondData=false;

    //Reading lines
    while (std::getline(f, line))
    {
        numLine++;

        std::stringstream s(line); // Used for breaking words
        std::string word; //The single word

        int count=0;

        if (line.substr(0,17) == "M  V30 BEGIN ATOM")
            atomData=true;

        else if (line.substr(0,15)=="M  V30 END ATOM")
            atomData=false;

        else if (line.substr(0,17) =="M  V30 BEGIN BOND")
            bondData=true;
        else if (line.substr(0,15)== "M  V30 END BOND")
            bondData=false;

        else if (atomData)
        {
            int z=0;
            std::string name;
            long long index=0;
            double x_pos=0;
            double y_pos=0;
            double z_pos=0;
            bool isValid=true;

            while (s >> word)
            {
                if (count==2)
                {
                    //This should be the index
                    index = std::stoll(word);
                }

                else if (count==3)
                {
                    //This should be the name
                    name = word;
                    z = Element::getAtomicNumber(word);
                }


                else if (count==4) {
                    //This should be the x
                    try {
                        x_pos = std::stod(word);
                    }
                    catch (std::invalid_argument) {
                        std::cerr<<"Warning! - Parser::readMOL. Invalid coordinates. The parsing of the MOL file is not completed."<<std::endl;
                        isValid=false;
                    }
                }
                else if (count == 5){
                    //This should be the y
                    try {
                        y_pos = std::stod(word);
                    }
                    catch (std::invalid_argument) {
                        std::cerr<<"Warning! - Parser::readXYZ. Invalid coordinates. The parsing of the XYZ file is not completed."<<std::endl;
                        isValid=false;
                    }

                } else if (count==6) {
                    //This should be z
                    try {
                        z_pos = std::stod(word);
                    }
                    catch (std::invalid_argument) {
                        std::cerr<<"Warning! - Parser::readXYZ. Invalid coordinates. The parsing of the XYZ file is not completed."<<std::endl;
                        isValid=false;
                    }
                }

                count++;

            }


            if (z!=0) {
                AtomSP atom(new Atom(z,name,index));
                atom->setPosition(0, std::vector<double>{x_pos,y_pos,z_pos});
                ms->addAtom(atom);
                z=0;
            } else {
                std::cerr<<"Warning! - Parser::readXYZ. The parsing of the XYZ file is not completed."<<std::endl;
                break;
            }
        }


        else if (bondData)
        {
            long long minorAtom;
            long long higherAtom;
            int bondOrder;

            while (s>>word)
            {
                if (count==3)
                    bondOrder = std::stoi(word);


                else if (count==4)
                    minorAtom = std::stoll(word);



                else if (count==5)
                    higherAtom = std::stoll(word);

                count++;
            }

            try {
                BondSP newBond(new Bond(minorAtom,higherAtom,bondOrder));
                ms->addBond(0,newBond);
            } catch (std::invalid_argument)
                {std::cerr << "Parser::readMOL - Error while adding bond"<<std::endl;}

        }



    }

    //Closing file
    f.close();

    return ms;
}









