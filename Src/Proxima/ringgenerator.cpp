#include "ringgenerator.h"
#include "molecularsystem.h"
#include "sssrgenerator.h"
#include <queue>
#include <deque>
#include <omp.h>
#include <stdlib.h>
#include <thread>
#include <algorithm>
using namespace Proxima;

//Constructor
RingGenerator::RingGenerator(MolecularSystem *ms, size_t frameNum)
{
    //Setting the molecular system
    this->molecularSystem=ms;

    if (ms==nullptr) return;

    //Setting the frame number
    if(frameNum>=ms->getNumFrames()) return;

    this->_frameNum = frameNum;

    //Setting the atoms containers
    size_t idx=0;
    for (auto it=ms->atomsBegin(); it!=ms->atomsEnd(); it++)
    {
        this->atomSpace.insert(std::pair<long long,bool>(it->first,true));
        std::pair<long long,size_t> couple(it->first,it->second->getNumBonds(frameNum));
        this->numBonds.insert(couple);
        this->atomSpace_associatedToBlocks.insert(std::pair<long long,bool>(it->first,false));
        this->atomSpace_fromAtomToIndex.insert(std::pair<long long,size_t>(it->first,idx));
        this->atomSpace_fromIndexToAtom.insert(std::pair<size_t,long long>(idx,it->first));
        idx++;
    }

    //Setting the bonds containers
    for (auto it=ms->bondsBegin(frameNum); it!=ms->bondsEnd(frameNum); it++)
    {
        this->bondSpace.insert(std::pair<BondSP,bool>(*it,true));
    }
}

//Blocks
std::set<std::set<BondSP> > RingGenerator::getBlocks()
{

    //If this object has already been used for blocks generations,
    //don't compute these again.
    if(this->areBlocksGenerated)
        return this->_blocks;

    std::set<std::set<BondSP>> blocks;

    //1) Removing all terminal atoms
    for (auto it=this->atomSpace.begin(); it!=this->atomSpace.end(); it++)
    {
        if (!it->second) continue;

        if (this->numBonds.at(it->first)==1 || this->numBonds.at(it->first)==0)
            this->removeTerminalAtoms_fromSource(it->first);
    }

    //2) Remove acyclic bonds
    for (auto it=this->atomSpace.begin(); it!=this->atomSpace.end(); it++)
    {
        if (!it->second) continue;

        if (this->numBonds.at(it->first)!=1)
            this->removeAcyclicAtomsAndBonds_fromSource(it->first);
    }

    //2) Check for blocks
    for (auto it=this->atomSpace_associatedToBlocks.begin();
         it!= this->atomSpace_associatedToBlocks.end(); it++)
    {
        //If the current atom is cyclic AND it has not been already assigned to some block...
        if (this->atomSpace.at(it->first) && !it->second)
        {
            std::set<BondSP> newBlock = this->connectedComponents_fromSource(it->first);
            blocks.insert(newBlock);
        }
    }

    this->_blocks = blocks;
    this->areBlocksGenerated=true;
    return blocks;

}

//Returns the cycles
std::vector<std::vector<BondSP> > RingGenerator::getHortonCycles()
{
    //Computes the blocks
    std::set<std::set<BondSP>> blocks = this->getBlocks();

    //COMMENT TO ENABLE PARALLEL VERSION.
    //UNCOMMENT TO ENABLE SERIAL VERSION.
#define OMP


    typedef std::vector<std::vector<BondSP>> cycles_type;
    cycles_type cycles;

    //Number of threads
   // int num_threads = 6;


    //Alloco un vettore per un output per thread
   // std::vector<cycles_type> out_threads;
    //out_threads.resize(num_threads);

    std::vector<int> numbers;

    unsigned numThr = std::max(unsigned(1),std::thread::hardware_concurrency());

    //BUG: viene usato un solo thread!!!

    //omp_set_dynamic(0);
    omp_set_num_threads(int(numThr));
    std::cout<<omp_get_num_threads()<<" threads used."<<std::endl;

#ifdef OMP
#pragma omp parallel
#endif
    {
#ifdef OMP
#pragma omp for
#endif
        for(long step=0; step < static_cast<long>(blocks.size()); step++)
        {
            auto it = std::next(blocks.begin(),step);
            std::set<BondSP> block = *(it);

            //Invokes a SSSRGenerator object for the task
            SSSRGenerator sssrgenerator(this->molecularSystem);
            sssrgenerator.setBonds(block);

            //It computes the cyclomatic number for the current block (A single connected component)
            size_t numCycles = sssrgenerator.getNumBonds() - sssrgenerator.getNumAtoms() +1;

            if (numCycles==1)
            {
#ifdef OMP
#pragma omp critical
#endif
                {
                    std::vector<BondSP> out;
                    std::copy(block.begin(), block.end(), std::back_inserter(out));
                    cycles.push_back(out);
                }
            }

            else
            {
                std::vector<std::vector<BondSP>> _cycles = sssrgenerator.findSSSR_Horton(numCycles);
#ifdef OMP
#pragma omp critical
#endif
                {
                    for (auto it=_cycles.begin(); it!=_cycles.end(); it++)
                    {
                        std::vector<BondSP> current_cycle = *it;
                        cycles.push_back(current_cycle);
                    }
                }

            }

        }

    }

    return cycles;
}

//It removes terminal atoms for a source terminal atom
bool RingGenerator::removeTerminalAtoms_fromSource(long long source)
{
    //The atom is not contained in the Molecular System...
    if (!this->molecularSystem->containsAtom(source))
        return false;

    //Is not a terminal atom...
    if (numBonds.at(source)>1)
        return false;

    //Retrieving the Atom and initializing a null Bond
    AtomSP atomSource = this->molecularSystem->getAtom(source);
    atomSpace.at(atomSource->getSerial()) = false;
    BondSP bond = nullptr;

    //Selecting the bond to erase
    for (auto it=atomSource->getBondBegin(this->_frameNum); it!=atomSource->getBondEnd(this->_frameNum); it++)
    {
        bond = *it;
        //Selecting just the bond that is part of the current bond space
        //in which our blocks are defined
        if (bondSpace.at(bond)) break;
    }

    //If the bond is a null pointer...
    if (bond==nullptr)
        return false;

    //Looking for the other atom it is connected to...
    long long otherAtom = (bond->getMinor()==source) ? bond->getHigher() : bond->getMinor();

    //Erasing the bond
    bondSpace.at(bond)=false;
    numBonds.at(otherAtom) = numBonds.at(otherAtom)-1;
    numBonds.at(source) = 0;
    atomSpace.at(source)=false;

    if (numBonds.at(otherAtom)==0)
        atomSpace.at(otherAtom)=false;

    //If the other atom is now become a terminal atom...
    //repeat the same procedure on the other atom!
    if (numBonds.at(otherAtom)==1)
        return this->removeTerminalAtoms_fromSource(otherAtom);

    else return true;

}


//Checks whether a source atom is part of some cycle
bool RingGenerator::isPartOfCycle_DFS(long long source, long long current, BondSP lastBond, std::vector<int> &visited)
{
    //Gets the current atom
    AtomSP atom = this->molecularSystem->getAtom(current);

    //-1: Gray [Not fully visited]
    visited[this->atomSpace_fromAtomToIndex.at(current)]=-1;

    //For each son...
    for (auto it=atom->getBondBegin(this->_frameNum); it!=atom->getBondEnd(this->_frameNum); it++)
    {
        BondSP bond = *it;

        //Not going back in the graph
        if (lastBond!=nullptr && bond->isEqual(*lastBond)) continue;

        //If the bond is not part of the current graph, continue to the next one
        if (!bondSpace.at(bond)) continue;

        //Selecting the other node
        long long other = (bond->getMinor()==current) ? bond->getHigher() : bond->getMinor();

        //If the other vertex is now the source vertex we have found a cycle!
        if (other==source)
            return true;

        //If it is a not fully visited vertex, just skip it.
        if (visited[this->atomSpace_fromAtomToIndex.at(other)]==-1) continue;

        //If it is a new vertex, visit it!
        if ((visited[this->atomSpace_fromAtomToIndex.at(other)]==0) && (this->isPartOfCycle_DFS(source,other,bond,visited)))
            return true;
    }

    //At this point, the Vertex is fully visited
    visited[this->atomSpace_fromAtomToIndex.at(current)]=1;

    return false;
}


//Checks whether the given atom is a cyclic atom
bool RingGenerator::isCyclicAtom(long long source)
{
    //Initializing a vector of visited atoms
    //Every atom is not visited yet
    std::vector<int> visited;
    visited.resize(size_t(this->molecularSystem->getNumAtoms()));
    std::fill(visited.begin(), visited.end(), 0);

    //Initializing a null bond
    BondSP nullBond = nullptr;

    //Calling the recursive function
    return this->isPartOfCycle_DFS(source,source,nullBond,visited);
}

//It removes acyclic atoms from source
bool RingGenerator::removeAcyclicAtomsAndBonds_fromSource(long long current)
{
    //If the current atom is not part of the atom space...
    if (!this->atomSpace.at(current)) return false;

    //If the atom is not a terminal atom...
    if (numBonds.at(current)>=2)
    {
        //Checks whether it is a cyclic atom
        bool isCyclic = this->isCyclicAtom(current);

        //If it is not a cyclic atom...
        if (!isCyclic)
        {

            //Erasing every bond of the atom from the bond set (essentially erasing the atom)
            for (auto itBond=this->molecularSystem->getAtom(current)->getBondBegin(this->_frameNum);
                 itBond!=this->molecularSystem->getAtom(current)->getBondEnd(this->_frameNum); itBond++)
            {
                BondSP bond = *itBond;

                //If the bond is part of the system
                if (this->bondSpace.at(bond))
                {
                    //Reducing the number of bonds of the other atom it is connected to
                    long long other = (bond->getMinor()==current) ? bond->getHigher() : bond->getMinor();
                    numBonds.at(current)=numBonds.at(current)-1;
                    numBonds.at(other)=numBonds.at(other)-1;

                    //Removing the bond from the system
                    this->bondSpace.at(bond)=false;

                    //If the current atom has reach 0 bonds, erase the atom from the atom space
                    if (numBonds.at(current)==0)
                        this->atomSpace.at(current)=false;

                    //If the other atom has reach 0 bonds, erase the atom from the atom space
                    if (numBonds.at(other)==0)
                        this->atomSpace.at(other)=false;

                    //If the other atom has now become a terminal atom, erase the terminal atoms!
                    if (numBonds.at(other)==1)
                        this->removeTerminalAtoms_fromSource(other);

                } //If the bond is part of the system
            } //Erasing every bond of the atom
        } //If the atom is not cyclic
    } //If it is not a terminal atom
    return true;
}

//It finds the connected component of a given source atom
std::set<BondSP> RingGenerator::connectedComponents_fromSource(long long source)
{
    std::set<BondSP> connectedComponent;

    //Searching for connected components (With a BFS)
    std::queue<long long> queue;

    std::map<long long, bool> visited;

    std::map<long long, long long> prev;

    //Initializing vector and maps.
    for (auto it=this->atomSpace.begin(); it!=this->atomSpace.end(); it++)
    {
        long long serial = it->first;
        if ( (serial != source) && (it->second) )
        {
            prev.insert(std::pair<long long, long long>(serial,std::numeric_limits<long long>::max()));
            visited.insert(std::pair<long long, bool>(serial,false));
        }
    }
    prev.insert(std::pair<long long, long long>(source,std::numeric_limits<long long>::max()));
    visited.insert(std::pair<long long, bool>(source,false));
    queue.push(source);

    //Exploring the graph
    while (queue.size()!=0)
    {
        long long u = queue.front();
        queue.pop();

        //It doesn't visit vertices that are already been explored
        if (visited.at(u)) continue;

        //Setting the current vertex as visited (and part of a block)
        visited.at(u)=true;
        this->atomSpace_associatedToBlocks.at(u)=true;

        //For each son...
        for (auto it=this->molecularSystem->getAtom(u)->getBondBegin(this->_frameNum);
             it!=this->molecularSystem->getAtom(u)->getBondEnd(this->_frameNum); it++)
        {
            BondSP bond = *it;

            //If the bond is not part of the bond space, just skip it
            if (!bondSpace.at(bond)) continue;

            else connectedComponent.insert(bond);

            long long v = (bond->getHigher() == u) ? bond->getMinor() : bond->getHigher();

            //It cannot go back during the BFS
            if (v==prev.at(u)) continue;

            queue.push(v);

            if (prev.at(v) == std::numeric_limits<long long>::max())
            {
                prev.at(v)=u;
            }

        } //For each son...

    } //Exploring the graph

    return connectedComponent;
}
