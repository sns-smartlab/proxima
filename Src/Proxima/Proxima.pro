#-------------------------------------------------
#
# Project created by QtCreator 2016-01-18T19:07:16
#
#-------------------------------------------------

TARGET = Proxima
TEMPLATE = lib

CONFIG -= qt
CONFIG += staticlib
CONFIG += c++11
CONFIG += warn_on

# Disable the g++ compiler optimizations when compiling in debug mode
# Enables compiler optimizations when compiling in release mode
msvc {
    QMAKE_CXXFLAGS_DEBUG += /Od
    QMAKE_CXXFLAGS_RELEASE += /O2
}

*-g++*|*-clang++ {
    QMAKE_CXXFLAGS_DEBUG += -O0
    QMAKE_CXXFLAGS_RELEASE += -O2
#    QMAKE_CXXFLAGS += -march=native -mtune=native -mavx2
}

# Verificare se su mac serve l'opzione seguente e trovare il modo
macx {
    QMAKE_CXXFLAGS += -mmacosx-version-min=10.7
}


# Enables OpenMP
msvc {
  QMAKE_CXXFLAGS += /openmp
}

*-g++*|*-clang++ {
  QMAKE_CXXFLAGS += -fopenmp
  QMAKE_LFLAGS += -fopenmp
}

### DEPENDENCIES: ###

### PROXIMA SOURCES ###



include(Proxima.pri)
