#include "hbinteraction.h"

using namespace Proxima;

HBInteractions::HBParameters::HBParameters()
{
    r_e = 0;
    s_r = 0;
    theta_e = 0;
    s_theta = 0;
}

HBInteractions::HBParameters::HBParameters(double _R_e, double _s_r, double _theta_e, double _s_theta)
{
    r_e = _R_e;
    s_r = _s_r;
    theta_e = _theta_e;
    s_theta = _s_theta;
}

std::map<std::pair<int,int>,HBInteractions::HBParameters> HBInteractions::parameters;

void HBInteractions::initHBParameters()
{
    if (!HBInteractions::parameters.empty()) return;

	//Atomic numbers are used as index
    HBInteractions::parameters[std::pair<int,int>(8,8)]=HBInteractions::HBParameters(1.94807,0.127367,7.57868,5.85762);
    HBInteractions::parameters[std::pair<int,int>(7,7)]=HBInteractions::HBParameters(2.13493,0.116083,7.93518,8.17105);
    HBInteractions::parameters[std::pair<int,int>(8,7)]=HBInteractions::HBParameters(1.99192,0.130077,6.90785,4.89889);
    HBInteractions::parameters[std::pair<int,int>(7,8)]=HBInteractions::HBParameters(2.0439,0.15488,9.96002,8.28617);

}

bool Proxima::HBInteractions::canBeDonorOrAcceptor(int atom)
{
	if(atom == 7 || atom == 8) return true;
	return false;
}

double Proxima::HBInteractions::getHBParameter_r_e(int atom1, int atom2)
{
    HBInteractions::initHBParameters();
    return HBInteractions::parameters[std::pair<int,int>(atom1,atom2)].r_e;
}

double Proxima::HBInteractions::getHBParameter_s_r(int atom1, int atom2)
{
    HBInteractions::initHBParameters();
    return HBInteractions::parameters[std::pair<int,int>(atom1,atom2)].s_r;
}

double Proxima::HBInteractions::getHBParameter_theta_e(int atom1, int atom2)
{
    HBInteractions::initHBParameters();
    return HBInteractions::parameters[std::pair<int,int>(atom1,atom2)].theta_e;
}

double Proxima::HBInteractions::getHBParameter_s_theta(int atom1, int atom2)
{
    HBInteractions::initHBParameters();
    return HBInteractions::parameters[std::pair<int,int>(atom1,atom2)].s_theta;
}
