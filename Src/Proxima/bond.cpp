#include <list>
#include <algorithm>
#include <stdexcept>
#include <iostream>

#include "bond.h"
#include "./utilities/floating_point.h"

using namespace Proxima;


Bond::Bond(long long atom1, long long atom2, double order, bool isPlanar)
{
	if(atom1 == atom2)
		throw std::invalid_argument("Bond::Bond - Atoms serials are equals!");
    if(fuzzyEqual(order,0))
    {
        order=1;
        std::cerr<<"Bond::Bond WARNING! - A value of 0 for the bond order "
                   "is passed as argument."<<std::endl;
    }


	_minorAtom = std::min(atom1,atom2);
	_higherAtom = std::max(atom1,atom2);
	_order = order;
    _isPlanar = isPlanar;
}

void Bond::clone(const std::shared_ptr<Bond> &bnd)
{

    this->_minorAtom = bnd->_minorAtom;
    this->_higherAtom = bnd->_higherAtom;
    this->_order = bnd->_order;
    this->_isPlanar = bnd->_isPlanar;
}

//Bond::Bond(const Bond &bnd)
//{
//    this->harmonicForceConstant = bnd.harmonicForceConstant;
//    this->cubicForceConstant = bnd.cubicForceConstant;
//    this->quarticForceConstant = bnd.quarticForceConstant;

//    this->_minorAtom = bnd._minorAtom;
//    this->_higherAtom = bnd._higherAtom;
//    this->_order = bnd._order;
//    this->_isPlanar = bnd._isPlanar;
//}

double Bond::getOrder() const
{
	return _order;
}

long long Bond::getMinor() const
{
	return _minorAtom;
}

long long Bond::getHigher() const
{
    return _higherAtom;
}


bool Bond::isBondPlanar() const
{
    return this->_isPlanar;
}


bool Bond::isEqual(const Bond& other) const
{
	if(this->getMinor()			  == other.getMinor()          &&
	   this->getHigher()		  == other.getHigher()		   &&
       fuzzyEqual(this->getOrder(),other.getOrder())) return true;
    else return false;
}

//bool Bond::setBondOrder(double bo)
//{
//    this->_order = bo;
//    return true;
//}

bool Bond::setPlanarity()
{
    this->_isPlanar=true;
    return true;
}
