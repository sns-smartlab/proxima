#ifndef PROXIMA_ATOMSGRID_H
#define PROXIMA_ATOMSGRID_H

#include <array>
#include <map>
#include <algorithm>
//todo: controlla l'ifndef
#include <cmath>


#include "atom.h"
#include "./utilities/utilities.h"

/**
 * @author Federico Lazzari
 */
namespace Proxima
{

// Forward declaration
class MolecularSystem;

/**
 * @brief   Given a molecular system, an instances of this class partitions
 *          a region of the 3D space bounding the molecular system into a
 *          regular grid and, for each cell of the grid, stores the atoms
 *          it contains. The partitioning operated by the instances of these
 *          class is exploited to accelerate bonds computation.
 *
 *          In fact, by setting the length of the sides of the cells slightly
 *          higher then the maximum bond distance, and by providing a method
 *          that returns the atoms nearby a specified atom (getNeighbors()),
 *          this data structure allows to greatly reduce the number of atoms
 *          pair to be compared by a bonds computation procedure.
 *
 * @note	The grid extends from a vector called "origin" to a vector
 *			"origin + GRID_SIDE_LENGHT * [nx, ny, nz]" where GRID_SIDE_LENGHT is
 *			the lenght of the sides of a cell belonging to the grid and nx, ny, nz
 *			are the number of cells along the x, y, z direction respectively.
 */
class AtomsGrid
{

public:

    /**
	 * @brief  Length of the sides of a cell belonging to the grid,
	 *		   expressed in Angstrom.
     *         Must be >= to the maximum bond distance.
     */
    static const double GRID_SIDE_LENGTH;

    /**
     * @brief Constructor.
     * @param molecular_system The Molecular System for which the grid is computed.
     * @param frameNum The frame number of the trajectory described by the molecular system for which the grid is computed.
     */
    AtomsGrid(MolecularSystem* const molecular_system, size_t frameNum);

    /** @brief Destructor. */
    virtual ~AtomsGrid() = default;

    /**
     * @brief Returns the point taken as origin of the grid.
     */
    virtual std::vector<double> getOrigin() const;

    /**
     * @brief Returns the number of cells along the X axis.
     */
    virtual unsigned getNumCellsX() const;

    /**
     * @brief Returns the number of cells along the Y axis.
     */
    virtual unsigned getNumCellsY() const;

    /**
     * @brief Returns the number of cells along the Z axis.
     */
    virtual unsigned getNumCellsZ() const;

    /**
     * @brief   Returns a vector which contains the indices of the
     *          cell containing the atom passed as parameter.
     *
     * @param atom The atom whose position within the grid is required.
     */
    virtual std::array<unsigned,3> getCellIndex(AtomSP atom) const;

    /**
     * @brief   Returns the list of atoms lying in the specified cell.
     */
    virtual std::vector<AtomSP> getAtomsInCell(unsigned x,unsigned y,unsigned z) const;

    /**
     * @brief Returns the list of atoms lying in the specified cell and the nearest neighbors.
     */
    virtual std::vector<AtomSP> getAtomsInNeighborsCells(unsigned x, unsigned y, unsigned z) const;


    /**
     * @brief   Returns the list of atoms lying in the cell containing the atom
     *          passed as parameter, plus those lying in the (at most) 26 neighboring cells.
     *
     * @param atom      The atom whose neighbors are required.
     *
     * @note            The atom passed as argument is not inserted in the returned list.
     * @note            The returned vector is not ordered.
     */
    virtual std::vector<AtomSP> getNeighbors(AtomSP atom) const;

    /* Copy/move constructors/operators are disabled */
    AtomsGrid(const AtomsGrid& other) = delete;
    AtomsGrid(AtomsGrid&& other) = delete;
    AtomsGrid& operator=(const AtomsGrid& other) = delete;
    AtomsGrid& operator=(AtomsGrid&& other) = delete;

protected:

    /** @brief Structure containing data related to a cell of the grid. */
    struct Cell
    {
        /** @brief Atom serials */
        std::vector<AtomSP> _atoms;

        /** @brief Destructor. */
        virtual ~Cell() = default;

        /** @brief Adds an atom to the cell */
        void addAtom(AtomSP atom)
        {
            _atoms.push_back(atom);
        }

		/** @brief Returns the list of atoms present in the cell */
        virtual const std::vector<AtomSP>& getAtoms() const
        {
            return _atoms;
        }
    };

protected:

    /** @brief Origin of the grid */
    std::vector<double> _origin;

    /**
     * @brief   Numbers of cells along the 3 axis.
     *          Each side of the cell is GRID_SIDE_LENGTH in length.
     */
    unsigned _numCells[3];

    /** @brief  The frame number, of the trajectory described by the
     *          molecular system, for which the grid is computed. */
    size_t _frameNumber;

    /** @brief Grid */
    std::vector<std::vector<std::vector<Cell>>> _grid;
};

/** @brief AtomsGridSP is a smart pointer to an AtomsGrid object. */
typedef std::shared_ptr< AtomsGrid > AtomsGridSP;

}

#endif // ATOMICGRID_H
