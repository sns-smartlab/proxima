#include "forcefieldparameters.h"
#include "atomicproperties.h"
#include <cmath>

Proxima::ForceFieldParameters::ForceFieldParameters()
{

    //Harmonic
    this->f2.insert(std::pair<std::pair<int,int>,std::pair<double,double>>(std::pair<int,int>(1,2),std::pair<double,double>(1.54,0.64)));
    this->f2.insert(std::pair<std::pair<int,int>,std::pair<double,double>>(std::pair<int,int>(1,3),std::pair<double,double>(1.80,0.69)));
    this->f2.insert(std::pair<std::pair<int,int>,std::pair<double,double>>(std::pair<int,int>(1,4),std::pair<double,double>(1.98,0.95)));
    this->f2.insert(std::pair<std::pair<int,int>,std::pair<double,double>>(std::pair<int,int>(1,5),std::pair<double,double>(2.08,0.96)));
    this->f2.insert(std::pair<std::pair<int,int>,std::pair<double,double>>(std::pair<int,int>(1,6),std::pair<double,double>(2.06,0.78)));

    this->f2.insert(std::pair<std::pair<int,int>,std::pair<double,double>>(std::pair<int,int>(2,2),std::pair<double,double>(1.73,0.47)));
    this->f2.insert(std::pair<std::pair<int,int>,std::pair<double,double>>(std::pair<int,int>(2,3),std::pair<double,double>(2.02,0.53)));
    this->f2.insert(std::pair<std::pair<int,int>,std::pair<double,double>>(std::pair<int,int>(2,4),std::pair<double,double>(2.15,0.60)));
    this->f2.insert(std::pair<std::pair<int,int>,std::pair<double,double>>(std::pair<int,int>(2,5),std::pair<double,double>(2.36,0.76)));
    this->f2.insert(std::pair<std::pair<int,int>,std::pair<double,double>>(std::pair<int,int>(2,6),std::pair<double,double>(2.47,0.87)));

    this->f2.insert(std::pair<std::pair<int,int>,std::pair<double,double>>(std::pair<int,int>(3,3),std::pair<double,double>(2.40,0.70)));
    this->f2.insert(std::pair<std::pair<int,int>,std::pair<double,double>>(std::pair<int,int>(3,4),std::pair<double,double>(2.54,0.98)));
    this->f2.insert(std::pair<std::pair<int,int>,std::pair<double,double>>(std::pair<int,int>(3,5),std::pair<double,double>(2.63,0.96)));
    this->f2.insert(std::pair<std::pair<int,int>,std::pair<double,double>>(std::pair<int,int>(3,6),std::pair<double,double>(2.71,1.09)));

    this->f2.insert(std::pair<std::pair<int,int>,std::pair<double,double>>(std::pair<int,int>(4,4),std::pair<double,double>(2.70,1.12)));
    this->f2.insert(std::pair<std::pair<int,int>,std::pair<double,double>>(std::pair<int,int>(4,5),std::pair<double,double>(2.66,1.48)));
    this->f2.insert(std::pair<std::pair<int,int>,std::pair<double,double>>(std::pair<int,int>(4,6),std::pair<double,double>(2.73,1.31)));

    this->f2.insert(std::pair<std::pair<int,int>,std::pair<double,double>>(std::pair<int,int>(5,5),std::pair<double,double>(2.85,0.94)));
    this->f2.insert(std::pair<std::pair<int,int>,std::pair<double,double>>(std::pair<int,int>(5,6),std::pair<double,double>(2.84,1.09)));


    //Cubic
    this->f3.insert(std::pair<std::pair<int,int>,std::pair<double,double>>(std::pair<int,int>(1,2),std::pair<double,double>(1.58,0.48)));
    this->f3.insert(std::pair<std::pair<int,int>,std::pair<double,double>>(std::pair<int,int>(1,3),std::pair<double,double>(1.85,0.59)));
    this->f3.insert(std::pair<std::pair<int,int>,std::pair<double,double>>(std::pair<int,int>(1,4),std::pair<double,double>(2.01,0.74)));
    this->f3.insert(std::pair<std::pair<int,int>,std::pair<double,double>>(std::pair<int,int>(1,5),std::pair<double,double>(2.07,0.74)));
    this->f3.insert(std::pair<std::pair<int,int>,std::pair<double,double>>(std::pair<int,int>(1,6),std::pair<double,double>(2.12,0.90)));

    this->f3.insert(std::pair<std::pair<int,int>,std::pair<double,double>>(std::pair<int,int>(2,2),std::pair<double,double>(1.78,0.39)));
    this->f3.insert(std::pair<std::pair<int,int>,std::pair<double,double>>(std::pair<int,int>(2,3),std::pair<double,double>(2.10,0.48)));
    this->f3.insert(std::pair<std::pair<int,int>,std::pair<double,double>>(std::pair<int,int>(2,4),std::pair<double,double>(2.26,0.55)));
    this->f3.insert(std::pair<std::pair<int,int>,std::pair<double,double>>(std::pair<int,int>(2,5),std::pair<double,double>(2.41,0.57)));
    this->f3.insert(std::pair<std::pair<int,int>,std::pair<double,double>>(std::pair<int,int>(2,6),std::pair<double,double>(2.48,0.68)));

    this->f3.insert(std::pair<std::pair<int,int>,std::pair<double,double>>(std::pair<int,int>(3,3),std::pair<double,double>(2.48,0.61)));
    this->f3.insert(std::pair<std::pair<int,int>,std::pair<double,double>>(std::pair<int,int>(3,4),std::pair<double,double>(2.57,0.72)));
    this->f3.insert(std::pair<std::pair<int,int>,std::pair<double,double>>(std::pair<int,int>(3,5),std::pair<double,double>(2.70,0.73)));
    this->f3.insert(std::pair<std::pair<int,int>,std::pair<double,double>>(std::pair<int,int>(3,6),std::pair<double,double>(2.81,1.09)));

    this->f3.insert(std::pair<std::pair<int,int>,std::pair<double,double>>(std::pair<int,int>(4,4),std::pair<double,double>(2.77,0.89)));
    this->f3.insert(std::pair<std::pair<int,int>,std::pair<double,double>>(std::pair<int,int>(4,5),std::pair<double,double>(2.76,1.19)));
    this->f3.insert(std::pair<std::pair<int,int>,std::pair<double,double>>(std::pair<int,int>(4,6),std::pair<double,double>(2.83,1.05)));

    this->f3.insert(std::pair<std::pair<int,int>,std::pair<double,double>>(std::pair<int,int>(5,5),std::pair<double,double>(2.95,0.70)));
    this->f3.insert(std::pair<std::pair<int,int>,std::pair<double,double>>(std::pair<int,int>(5,6),std::pair<double,double>(2.93,0.78)));


    //Quartic
    this->f4.insert(std::pair<std::pair<int,int>,std::pair<double,double>>(std::pair<int,int>(1,2),std::pair<double,double>(1.57,0.43)));
    this->f4.insert(std::pair<std::pair<int,int>,std::pair<double,double>>(std::pair<int,int>(1,3),std::pair<double,double>(1.77,0.47)));
    this->f4.insert(std::pair<std::pair<int,int>,std::pair<double,double>>(std::pair<int,int>(1,4),std::pair<double,double>(1.81,0.80)));
    this->f4.insert(std::pair<std::pair<int,int>,std::pair<double,double>>(std::pair<int,int>(1,5),std::pair<double,double>(2.04,0.59)));
    this->f4.insert(std::pair<std::pair<int,int>,std::pair<double,double>>(std::pair<int,int>(1,6),std::pair<double,double>(2.04,0.69)));

    this->f4.insert(std::pair<std::pair<int,int>,std::pair<double,double>>(std::pair<int,int>(2,2),std::pair<double,double>(1.81,0.36)));
    this->f4.insert(std::pair<std::pair<int,int>,std::pair<double,double>>(std::pair<int,int>(2,3),std::pair<double,double>(2.06,0.41)));
    this->f4.insert(std::pair<std::pair<int,int>,std::pair<double,double>>(std::pair<int,int>(2,4),std::pair<double,double>(2.08,0.34)));
    this->f4.insert(std::pair<std::pair<int,int>,std::pair<double,double>>(std::pair<int,int>(2,5),std::pair<double,double>(2.18,0.32)));
    this->f4.insert(std::pair<std::pair<int,int>,std::pair<double,double>>(std::pair<int,int>(2,6),std::pair<double,double>(2.54,0.68)));

    this->f4.insert(std::pair<std::pair<int,int>,std::pair<double,double>>(std::pair<int,int>(3,3),std::pair<double,double>(2.35,0.46)));
    this->f4.insert(std::pair<std::pair<int,int>,std::pair<double,double>>(std::pair<int,int>(3,4),std::pair<double,double>(2.53,0.70)));
    this->f4.insert(std::pair<std::pair<int,int>,std::pair<double,double>>(std::pair<int,int>(3,5),std::pair<double,double>(2.64,0.51)));
    this->f4.insert(std::pair<std::pair<int,int>,std::pair<double,double>>(std::pair<int,int>(3,6),std::pair<double,double>(2.60,1.30)));

    this->f4.insert(std::pair<std::pair<int,int>,std::pair<double,double>>(std::pair<int,int>(4,4),std::pair<double,double>(2.66,1.06)));
}

double Proxima::ForceFieldParameters::getHarmonicForceConstant(int atom1, int atom2, double dist)
{
    double a = this->getA(atom1,atom2,2);
    double b = this->getB(atom1,atom2,2);

    return std::pow(10.0,(a-dist)/b);
}

double Proxima::ForceFieldParameters::getCubicForceConstant(int atom1, int atom2, double dist)
{
    double a = this->getA(atom1,atom2,3);
    double b = this->getB(atom1,atom2,3);

    return (-1.0)*std::pow(10.0,(a-dist)/b);
}

double Proxima::ForceFieldParameters::getQuarticForceConstant(int atom1, int atom2, double dist)
{
    double a = this->getA(atom1,atom2,4);
    double b = this->getB(atom1,atom2,4);

    return std::pow(10.0,(a-dist)/b);
}

double Proxima::ForceFieldParameters::getA(int atom1, int atom2, int fc)
{
    int row1 = Element::getRow(atom1);
    int row2 = Element::getRow(atom2);

    std::pair<int,int> row_pair = std::pair<int,int>(std::min(row1,row2),std::max(row1,row2));

    if (fc==2)
    {
        if (this->f2.find(row_pair)==this->f2.end())
            throw std::invalid_argument("ForceFieldParameters::getA - Invalid row/row combination");
        return this->f2.at(row_pair).first;
    }
    else if (fc==3)
    {
        if (this->f3.find(row_pair)==this->f3.end())
            throw std::invalid_argument("ForceFieldParameters::getA - Invalid row/row combination");
        return this->f3.at(row_pair).first;
    }
    else if (fc==4)
    {
        if (this->f4.find(row_pair)==this->f4.end())
            throw std::invalid_argument("ForceFieldParameters::getA - Invalid row/row combination");
        return this->f4.at(row_pair).first;
    }

    throw std::logic_error("INVALID BRANCH");
}

double Proxima::ForceFieldParameters::getB(int atom1, int atom2, int fc)
{
    int row1 = Element::getRow(atom1);
    int row2 = Element::getRow(atom2);

    std::pair<int,int> row_pair = std::pair<int,int>(std::min(row1,row2),std::max(row1,row2));

    if (fc==2)
    {
        if (this->f2.find(row_pair)==this->f2.end())
            throw std::invalid_argument("ForceFieldParameters::getA - Invalid row/row combination");
        return this->f2.at(row_pair).second;
    }
    else if (fc==3)
    {
        if (this->f3.find(row_pair)==this->f3.end())
            throw std::invalid_argument("ForceFieldParameters::getA - Invalid row/row combination");
        return this->f3.at(row_pair).second;
    }
    else if (fc==4)
    {
        if (this->f4.find(row_pair)==this->f4.end())
            throw std::invalid_argument("ForceFieldParameters::getA - Invalid row/row combination");
        return this->f4.at(row_pair).second;
    }

    throw std::logic_error("INVALID BRANCH");
}
