#ifndef PROXIMA_BOPARAMETERS_H
#define PROXIMA_BOPARAMETERS_H


#include <map>
#include <set>
#include "atom.h"

/**
 * @author Federico Lazzari
 */
namespace Proxima
{


class BOParameters {

public:
    BOParameters();
    virtual ~BOParameters() = default;

    virtual double computeBO_peter(int atom1, int atom2, std::vector<double> atom1Pos, std::vector<double> atom2Pos);


protected:

    virtual double computeExponent(int atom1, int atom2, double dist);

private:

    std::set<int> supportedTypes;
    std::map<int,double> peterSingleBondParameters;
    std::map<int,double> peterMultipleBondParameters;
    std::map<int,double> peterElectronegativities;


};

}
#endif // BOPARAMETERS_H
