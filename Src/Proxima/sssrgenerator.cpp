#include "sssrgenerator.h"
#include "molecularsystem.h"
#include "bitset"
#include <queue>
#include <deque>
#include "omp.h"

Proxima::SSSRGenerator::SSSRGenerator(MolecularSystem *ms)
{
    this->molecularSystem = ms;
}

bool Proxima::SSSRGenerator::addBond(Proxima::BondSP bond)
{
    if (bond==nullptr) return false;
    long long edge1 = bond->getMinor();
    long long edge2 = bond->getHigher();
    std::pair<long long, long long> edge(edge1, edge2);

    //Setting the Bond Space

    if (this->bondSpace_fromBondToIndex.find(edge) != this->bondSpace_fromBondToIndex.end()) return false;

    size_t index=this->bondSpace.size();

    this->bondSpace.push_back(bond);
    this->bondSpace_fromBondToIndex.insert(std::pair<std::pair<long long, long long>,size_t>(edge,index));
    this->bondSpace_fromIndexToBond.insert(std::pair<size_t,BondSP>(index,bond));


    //Adjusting the atom space
    if (this->atomSpace_fromAtomToIndex.find(bond->getMinor())==this->atomSpace_fromAtomToIndex.end())
    {
        size_t atomIndex = this->atomSpace.size();
        this->atomSpace.push_back(bond->getMinor());
        this->atomSpace_fromAtomToIndex.insert(std::pair<long long,size_t>(bond->getMinor(),atomIndex));
        this->atomSpace_fromIndexToAtom.insert(std::pair<size_t,long long>(index,bond->getMinor()));
    }

    if (this->atomSpace_fromAtomToIndex.find(bond->getHigher())==this->atomSpace_fromAtomToIndex.end())
    {
        size_t atomIndex = this->atomSpace.size();
        this->atomSpace.push_back(bond->getHigher());
        this->atomSpace_fromAtomToIndex.insert(std::pair<long long,size_t>(bond->getHigher(),atomIndex));
        this->atomSpace_fromIndexToAtom.insert(std::pair<size_t,long long>(atomIndex,bond->getHigher()));
    }

    return true;
}

bool Proxima::SSSRGenerator::setBonds(std::vector<Proxima::BondSP> bonds)
{
    if (this->bondSpace.size()!=0) return false;

    for (auto it=bonds.begin(); it!=bonds.end(); it++)
    {
        this->addBond(*it);
    }

    return true;
}

bool Proxima::SSSRGenerator::setBonds(std::set<Proxima::BondSP> bonds)
{
    if (this->bondSpace.size()!=0) return false;

    for (auto it=bonds.begin(); it!=bonds.end(); it++)
    {
        this->addBond(*it);
    }

    return true;
}

size_t Proxima::SSSRGenerator::getNumBonds() const
{
    return this->bondSpace.size();
}

size_t Proxima::SSSRGenerator::getNumAtoms() const
{
    return this->atomSpace.size();
}

Proxima::SSSRGenerator::atom_iterator Proxima::SSSRGenerator::atomBegin()
{
    return this->atomSpace.begin();
}

Proxima::SSSRGenerator::atom_iterator Proxima::SSSRGenerator::atomEnd()
{
    return this->atomSpace.end();
}

std::vector<std::vector<Proxima::BondSP> > Proxima::SSSRGenerator::findSSSR_Horton(size_t numCycles)
{
    //Computes all of the possibile cycles
    std::set<std::vector<bool>, bitvector_compare> cycles = this->getAllCycles(0);

    //Performs a Gauss Elimination (Greedy-Algorithm) to detect SSSR cycles only
    std::vector<std::vector<bool> > _cycles = this->gaussElimination(cycles, numCycles);

    //Converting bitvectors to BondSP and AtomSP objects
    std::vector<std::vector<BondSP>> bonds;
    if (numCycles!=0)
    {
        for (auto it=_cycles.begin(); it!=(_cycles.end()); it++)
        {
            std::vector<BondSP> _cycle;
            std::vector<bool> cycle = *it;
            //_cycle.resize(cycle.size());

//#ifdef OMP_INNER
//double t0 = omp_get_wtime();
//#pragma omp parallel for
//#endif
            for (size_t i=0; i<cycle.size(); i++)
            {
                if (cycle[i]==1)
                {
                    BondSP bond = this->index_to_bond(i);
                    _cycle.push_back(bond);
                }
            }
            bonds.push_back(_cycle);
//#ifdef OMP_INNER
//double t1 = omp_get_wtime();
//std::cout << t1-t0 << "\n";
//#endif
        }
    }

    else
    {
        for (auto it=_cycles.begin(); it!=(_cycles.end()); it++)
        {
            std::vector<BondSP> _cycle;
            std::vector<bool> cycle = *it;
            //_cycle.resize(cycle.size());

//#ifdef OMP_INNER
//double t0 = omp_get_wtime();
//#pragma omp parallel for
//#endif
            for (size_t i=0; i<cycle.size(); i++)
            {
                if (cycle[i]==1)
                {
                    BondSP bond = this->index_to_bond(i);
                    //_cycle.push_back(bond);
                    // not using push_back because of OMP
                    _cycle.push_back(bond);
                }
            }
//#ifdef OMP_INNER
//double t1 = omp_get_wtime();
//std::cout << t1-t0 << "\n";
//#endif
            bonds.push_back(_cycle);
        }
    }

    return bonds;
}


size_t Proxima::SSSRGenerator::bond_to_index(Proxima::BondSP bond)
{
    long long edge1 = bond->getMinor();
    long long edge2 = bond->getHigher();
    std::pair<long long, long long> edge(edge1, edge2);
    return this->bondSpace_fromBondToIndex.at(edge);
}

Proxima::BondSP Proxima::SSSRGenerator::index_to_bond(size_t index)
{
    return this->bondSpace_fromIndexToBond.at(index);
}

size_t Proxima::SSSRGenerator::atom_to_index(long long atom)
{
    return this->atomSpace_fromAtomToIndex.at(atom);
}

long long Proxima::SSSRGenerator::index_to_atom(size_t index)
{
    return this->atomSpace_fromIndexToAtom.at(index);
}

std::set<std::vector<bool>, Proxima::SSSRGenerator::bitvector_compare> Proxima::SSSRGenerator::getAllCycles(size_t frameNum)
{
    //1) Generating the minimum distance matrix
    std::vector<std::vector<std::vector<bool> > > pathsMatrix = this->pathBuilder(frameNum);

    std::set<std::vector<bool>, bitvector_compare> output;

    //2) For each Vertex
    for (auto it=this->atomSpace.begin(); it!=this->atomSpace.end(); it++)
    {
        long long vertex = *it;

        //For each edge
        for (auto itEdge=this->bondSpace.begin(); itEdge!=this->bondSpace.end(); itEdge++)
        {
            BondSP bond = *itEdge;
            long long edge1 = bond->getMinor();
            long long edge2 = bond->getHigher();
            if (edge1==vertex || edge2==vertex) continue;

            std::pair<long long, long long> edge(edge1, edge2);

            size_t vertex_index = this->atomSpace_fromAtomToIndex.at(vertex);
            size_t edge1_index =  this->atomSpace_fromAtomToIndex.at(edge1);
            size_t edge2_index =  this->atomSpace_fromAtomToIndex.at(edge2);

            std::vector<bool> fromVertex_to_Edge1 = pathsMatrix[vertex_index][edge1_index];
            std::vector<bool> fromEdge2_to_vertex = pathsMatrix[vertex_index][edge2_index];

            //XOR between the two paths
            std::vector<bool> newCycle(fromVertex_to_Edge1.size());

            //XOR OPERATION
            std::transform(fromVertex_to_Edge1.begin(),fromVertex_to_Edge1.end(),fromEdge2_to_vertex.begin(),newCycle.begin(), std::bit_xor<bool>());
            fromVertex_to_Edge1=newCycle;

            //Setting the edge
            if (fromVertex_to_Edge1[this->bondSpace_fromBondToIndex.at(edge)]==1)
                    fromVertex_to_Edge1[this->bondSpace_fromBondToIndex.at(edge)]=0;
            else fromVertex_to_Edge1[this->bondSpace_fromBondToIndex.at(edge)]=1;

            //Only selecting cycles
            if (std::count(fromVertex_to_Edge1.begin(),fromVertex_to_Edge1.end(),1)<3) continue;

            output.insert(fromVertex_to_Edge1);
        }

    }

    return output;
}

std::vector<std::vector<std::vector<bool> > > Proxima::SSSRGenerator::pathBuilder(size_t frameNum)
{
    std::vector<std::vector<std::vector<bool> > > paths(this->atomSpace.size(),std::vector<std::vector<bool> >(this->atomSpace.size()));

    //For each vertex
    for (auto it=this->atomSpace.begin(); it!=this->atomSpace.end(); it++)
    {
        long long source = *it;
        this->pathBuilder_fromSource(source,frameNum,paths);
    }

    return paths;

}

void Proxima::SSSRGenerator::pathBuilder_fromSource(long long source, size_t frameNum, std::vector<std::vector<std::vector<bool> > > &paths)
{
    std::map<long long, long long> prev = this->BFS(source,frameNum);

    //Building all of the possibile paths
    for (auto it=this->atomSpace.begin(); it!= this->atomSpace.end();it++)
    {
        long long u=*it;
        if (u==source) continue;
        long long vertex = u;

        //This is an additional controll to check
        if (std::count(paths[this->atomSpace_fromAtomToIndex.at(source)][this->atomSpace_fromAtomToIndex.at(u)].begin(),
                       paths[this->atomSpace_fromAtomToIndex.at(source)][this->atomSpace_fromAtomToIndex.at(u)].end(),1)!=0) continue;

        std::vector<bool> path(this->bondSpace.size());

        while (prev.at(u)!=std::numeric_limits<long long>::max())
        {
            std::pair<long long, long long> edge(std::min(u,prev.at(u)), std::max(u,prev.at(u)));
            size_t idx = this->bondSpace_fromBondToIndex.at(edge);
            path[idx]=1;
            u = prev.at(u);
        }

        paths[this->atomSpace_fromAtomToIndex.at(source)][this->atomSpace_fromAtomToIndex.at(vertex)] = path;
        paths[this->atomSpace_fromAtomToIndex.at(vertex)][this->atomSpace_fromAtomToIndex.at(source)] = path;
    }

}

std::map<long long, long long> Proxima::SSSRGenerator::BFS(long long source, size_t frameNum)
{
    std::queue<long long> queue;
    std::map<long long, bool> visited;
    std::map<long long, long long> prev;

    //Initializing vector and maps.
    for (auto it=this->atomSpace.begin(); it!=this->atomSpace.end(); it++)
    {
        long long serial = *it;
        if (serial != source)
        {
            prev.insert(std::pair<long long, long long>(serial,std::numeric_limits<long long>::max()));
            visited.insert(std::pair<long long, bool>(serial,false));
        }
    }

    prev.insert(std::pair<long long, long long>(source,std::numeric_limits<long long>::max()));
    visited.insert(std::pair<long long, bool>(source,false));
    queue.push(source);

    //Exploring the graph
    while (queue.size()!=0)
    {
        long long u = queue.front();
        queue.pop();

        //It doesn't visit nodes that are already been explored
        if (visited.at(u)) continue;

        visited.at(u)=true;

        for (auto it=this->molecularSystem->getAtom(u)->getBondBegin(frameNum); it!=this->molecularSystem->getAtom(u)->getBondEnd(frameNum); it++)
        {
            BondSP bond = *it;
            //If this bond is not part of the bond space, continue
            if (this->bondSpace_fromBondToIndex.find(std::pair<long long, long long>(bond->getMinor(), bond->getHigher()))==this->bondSpace_fromBondToIndex.end())
                continue;

            long long v = (bond->getHigher() == u) ? bond->getMinor() : bond->getHigher();

            //It cannot go back during the BFS
            if (v==prev.at(u)) continue;

            queue.push(v);

            if (prev.at(v) == std::numeric_limits<long long>::max())
            {
                prev.at(v)=u;
            }
        }

    }

    return prev;

}

std::vector<std::vector<bool> > Proxima::SSSRGenerator::gaussElimination(std::set<std::vector<bool>, bitvector_compare> &cycles, size_t numCycles)
{
    //A vector object is better suited for matrix operations than a set
    std::vector<std::vector<bool> > matrix;
    std::copy(cycles.begin(), cycles.end(), std::back_inserter(matrix));
    std::set<std::vector<bool> > encountered;

    int idx = -1;

    std::vector<std::vector<bool> > output;

    //The first cycle (the cycle of minimal length) must be added automatically
    for (auto it = matrix.begin(); it != matrix.end(); it++)
    {
        idx++;
        std::vector<bool> current = *it;
        if (std::count(current.begin(),current.end(),1)>=3)
        {
            output.push_back(current);
            encountered.insert(current);
            idx++;
            break;
        }
    }

    //For each cycle...
    while (output.size() != numCycles)
    {
        if (idx==matrix.size())
            break;

        std::vector<std::vector<bool> > test = output;
        test.push_back(matrix[size_t(idx)]);
        bool isIndependent = this->testIndependence(test);
        if (isIndependent && !encountered.count(matrix[size_t(idx)]))
        {
            output.push_back(matrix[size_t(idx)]);
            encountered.insert(matrix[size_t(idx)]);
        }

        idx++;
    }

    return output;

}


bool Proxima::SSSRGenerator::testIndependence(std::vector<std::vector<bool> > &cycles)
{
    size_t idx1 = 0;
    size_t idx2 = idx1;

    //For each row
    while(idx1 < cycles.size())
    {

        if (idx2 == this->getNumBonds())
            return false;

        //SETTING FIRST ROW (ITS FIRST ELEMENT MUST BE 1)
        //----------------------------------------------
        //Swapping lines
        if (cycles[idx1][idx2] == false)
        {
            size_t i=(idx1+1);
            while (i<cycles.size())
            {
                if (cycles[i][idx2] != false)
                {
                    std::iter_swap(cycles.begin()+idx1,cycles.begin()+i);
                    i=cycles.size();
                }
                i=i+1;
            }
        }

        //If it is still 0, move to the next column
        //Moving to the next column
        if (cycles[idx1][idx2] == false)
        {
            idx2++;
            continue;
        }
        //----------------------------------------------

        //XOR OPERATIONS
        //----------------------------------------------
        //else
        //{
        size_t i=(idx1+1);
        while (i<cycles.size())
        {

            if (cycles[i][idx2] != false)
            {
                std::vector<bool> newCycle(cycles[i].size());

                //XOR OPERATION
                std::transform(cycles[i].begin(),cycles[i].end(),cycles[idx1].begin(),newCycle.begin(), std::bit_xor<bool>());
                cycles[i]=newCycle;
            }

            i++;
        }
        //}
        //----------------------------------------------

        /* Se non ci sono altri legami da controllare,
               sono linearmente dipendenti */
        if (idx2 >= this->getNumBonds())
            return false;

        idx1++;
    }

    //If the last row has non-zero elements than these rows are linearly independents
    auto it=std::find(cycles[cycles.size()-1].begin(), cycles[cycles.size()-1].end(), 1);
    if (it!=cycles[cycles.size()-1].end()) return true;

    //Otherwise are linearly dependents
    else return false;

}



