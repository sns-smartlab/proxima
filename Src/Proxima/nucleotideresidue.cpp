#include "nucleotideresidue.h"
#include "molecularsystem.h"


using namespace Proxima;



NucleotideResidue::NucleotideResidue(long long serial, long long sequenceNumber, char iCode, const std::string &name) :
    Residue(serial,sequenceNumber,iCode,name) {}



void NucleotideResidue::clone(std::shared_ptr<NucleotideResidue> &other)
{
    this->_serial = other->_serial;
    this->_sequenceNumber = other->_sequenceNumber;
    this->_insertionCode = other->_insertionCode;
    this->_name = other->_name;
    this->_atomsSerials = other->_atomsSerials;

    this->_phosphate = other->_phosphate;
    this->_sugar = other->_sugar;
    this->nitrogenous_base = other->nitrogenous_base;
    this->generic_atoms = other->generic_atoms;
}







bool NucleotideResidue::addAtom(long long serial, int role)
{

    //CHECKING FOR THE PHOSPHATE GROUP

    //---------------------------------------------------

    //Verifying whether the given role is allowed.
    if (role==int(AtomRole::PHOSPHATE_P))
    {
        if (this->_phosphate.is_P_Valid) {return false;}
        bool addition = Residue::addAtom(serial,role);
        if (addition)
        {
            this->_phosphate.is_P_Valid = true;
            this->_phosphate.P = serial;
            return true;
        } else return false;
    }

    if (role==int(AtomRole::PHOSPHATE_O1P))
    {
        if (this->_phosphate.is_O1P_Valid) {return false;}
        bool addition = Residue::addAtom(serial,role);
        if (addition)
        {
            this->_phosphate.is_O1P_Valid = true;
            this->_phosphate.O1P = serial;
            return true;
        } else return false;
    }

    if (role==int(AtomRole::PHOSPHATE_O2P))
    {
        if (this->_phosphate.is_O2P_Valid) {return false;}
        bool addition = Residue::addAtom(serial,role);
        if (addition)
        {
            this->_phosphate.is_O2P_Valid = true;
            this->_phosphate.O2P = serial;
            return true;
        } else return false;
    }

    //CHECKING FOR THE SUGAR

    //---------------------------------------------------

    if (role==int(AtomRole::SUGAR_C1))
    {
        if (this->_sugar.is_C1_Valid) {return false;}
        bool addition = Residue::addAtom(serial,role);
        if (addition)
        {
            this->_sugar.is_C1_Valid = true;
            this->_sugar.C1 = serial;
            return true;
        } else return false;
    }

    if (role==int(AtomRole::SUGAR_C2))
    {
        if (this->_sugar.is_C2_Valid) {return false;}
        bool addition = Residue::addAtom(serial,role);
        if (addition)
        {
            this->_sugar.is_C2_Valid = true;
            this->_sugar.C2 = serial;
            return true;
        } else return false;
    }

    if (role==int(AtomRole::SUGAR_C3))
    {
        if (this->_sugar.is_C3_Valid) {return false;}
        bool addition = Residue::addAtom(serial,role);
        if (addition)
        {
            this->_sugar.is_C3_Valid = true;
            this->_sugar.C3 = serial;
            return true;
        } else return false;
    }

    if (role==int(AtomRole::SUGAR_C4))
    {
        if (this->_sugar.is_C4_Valid) {return false;}
        bool addition = Residue::addAtom(serial,role);
        if (addition)
        {
            this->_sugar.is_C4_Valid = true;
            this->_sugar.C4 = serial;
            return true;
        } else return false;
    }

    if (role==int(AtomRole::SUGAR_C5))
    {
        if (this->_sugar.is_C5_Valid) {return false;}
        bool addition = Residue::addAtom(serial,role);
        if (addition)
        {
            this->_sugar.is_C5_Valid = true;
            this->_sugar.C5 = serial;
            return true;
        } else return false;
    }

    if (role==int(AtomRole::SUGAR_O2))
    {
        if (this->_sugar.is_O2_Valid) {return false;}
        bool addition = Residue::addAtom(serial,role);
        if (addition)
        {
            this->_sugar.is_O2_Valid = true;
            this->_sugar.O2 = serial;
            return true;
        } else return false;
    }

    if (role==int(AtomRole::SUGAR_O3))
    {
        if (this->_sugar.is_O3_Valid) {return false;}
        bool addition = Residue::addAtom(serial,role);
        if (addition)
        {
            this->_sugar.is_O3_Valid = true;
            this->_sugar.O3 = serial;
            return true;
        } else return false;
    }

    if (role==int(AtomRole::SUGAR_O4))
    {
        if (this->_sugar.is_O4_Valid) {return false;}
        bool addition = Residue::addAtom(serial,role);
        if (addition)
        {
            this->_sugar.is_O4_Valid = true;
            this->_sugar.O4 = serial;
            return true;
        } else return false;
    }

    if (role==int(AtomRole::SUGAR_O5))
    {
        if (this->_sugar.is_O5_Valid) {return false;}
        bool addition = Residue::addAtom(serial,role);
        if (addition)
        {
            this->_sugar.is_O5_Valid = true;
            this->_sugar.O5 = serial;
            return true;
        } else return false;
    }

    if (role==int(AtomRole::SUGAR_H1))
    {
        if (this->_sugar.is_H1_valid) {return false;}
        bool addition = Residue::addAtom(serial,role);
        if (addition)
        {
            this->_sugar.is_H1_valid = true;
            this->_sugar.H1 = serial;
            return true;
        } else return false;
    }

    if (role==int(AtomRole::SUGAR_H21))
    {
        if (this->_sugar.is_H21_valid) {return false;}
        bool addition = Residue::addAtom(serial,role);
        if (addition)
        {
            this->_sugar.is_H21_valid = true;
            this->_sugar.H21 = serial;
            return true;
        } else return false;
    }

    if (role==int(AtomRole::SUGAR_H22))
    {
        if (this->_sugar.is_H22_valid) {return false;}
        bool addition = Residue::addAtom(serial,role);
        if (addition)
        {
            this->_sugar.is_H22_valid = true;
            this->_sugar.H22 = serial;
            return true;
        } else return false;
    }

    if (role==int(AtomRole::SUGAR_H3))
    {
        if (this->_sugar.is_H3_valid) {return false;}
        bool addition = Residue::addAtom(serial,role);
        if (addition)
        {
            this->_sugar.is_H3_valid = true;
            this->_sugar.H3 = serial;
            return true;
        } else return false;
    }

    if (role==int(AtomRole::SUGAR_H4))
    {
        if (this->_sugar.is_H4_valid) {return false;}
        bool addition = Residue::addAtom(serial,role);
        if (addition)
        {
            this->_sugar.is_H4_valid = true;
            this->_sugar.H4 = serial;
            return true;
        } else return false;
    }

    if (role==int(AtomRole::SUGAR_H51))
    {
        if (this->_sugar.is_O5_Valid) {return false;}
        bool addition = Residue::addAtom(serial,role);
        if (addition)
        {
            this->_sugar.is_H51_valid = true;
            this->_sugar.H51 = serial;
            return true;
        } else return false;
    }

    if (role==int(AtomRole::SUGAR_H52))
    {
        if (this->_sugar.is_H52_valid) {return false;}
        bool addition = Residue::addAtom(serial,role);
        if (addition)
        {
            this->_sugar.is_H52_valid = true;
            this->_sugar.H52 = serial;
            return true;
        } else return false;
    }

    if (role==int(NucleotideResidue::AtomRole::NITROGENOUSBASE))
    {
        this->nitrogenous_base.push_back(serial);
        return Residue::addAtom(serial,role);
    }

    else
    {
        this->generic_atoms.push_back(serial);
        return Residue::addAtom(serial,role);
    }

    //return Residue::addAtom(serial,role);

}

std::vector<std::pair<AtomSP, int>> NucleotideResidue::getPhosphateGroup() const
{
    std::vector<std::pair<AtomSP, int>> out;
    MolecularSystem* molSys = this->getParentMolecularSystem();

    if (this->_phosphate.is_P_Valid)
    {
        long long P = this->_phosphate.P;
        out.push_back(std::make_pair(molSys->getAtom(P), (int) AtomRole::PHOSPHATE_P));
    }


    if (this->_phosphate.is_O1P_Valid)
    {
        long long O1P = this->_phosphate.O1P;
        out.push_back(std::make_pair(molSys->getAtom(O1P), (int) AtomRole::PHOSPHATE_O1P));
    }

    if (this->_phosphate.is_O2P_Valid)
    {
        long long O2P = this->_phosphate.O2P;
        out.push_back(std::make_pair(molSys->getAtom(O2P), (int) AtomRole::PHOSPHATE_O2P));
    }

    return out;
}

std::vector<std::pair<AtomSP, int>> NucleotideResidue::getSugar() const
{
    std::vector<std::pair<AtomSP, int>> out;
    MolecularSystem* molSys = this->getParentMolecularSystem();

    if (this->_sugar.is_C1_Valid)
    {
        long long C1 = this->_sugar.C1;
        out.push_back(std::make_pair(molSys->getAtom(C1), (int) AtomRole::SUGAR_C1));
    }


    if (this->_sugar.is_C2_Valid)
    {
        long long C2 = this->_sugar.C2;
        out.push_back(std::make_pair(molSys->getAtom(C2), (int) AtomRole::SUGAR_C2));
    }

    if (this->_sugar.is_C3_Valid)
    {
        long long C3 = this->_sugar.C3;
        out.push_back(std::make_pair(molSys->getAtom(C3), (int) AtomRole::SUGAR_C3));
    }

    if (this->_sugar.is_C4_Valid)
    {
        long long C4 = this->_sugar.C4;
        out.push_back(std::make_pair(molSys->getAtom(C4), (int) AtomRole::SUGAR_C4));
    }


    if (this->_sugar.is_C5_Valid)
    {
        long long C5 = this->_sugar.C5;
        out.push_back(std::make_pair(molSys->getAtom(C5), (int) AtomRole::SUGAR_C5));
    }

    if (this->_sugar.is_O2_Valid)
    {
        long long O2 = this->_sugar.O2;
        out.push_back(std::make_pair(molSys->getAtom(O2), (int) AtomRole::SUGAR_O2));
    }

    if (this->_sugar.is_O3_Valid)
    {
        long long O3 = this->_sugar.O3;
        out.push_back(std::make_pair(molSys->getAtom(O3), (int) AtomRole::SUGAR_O3));
    }


    if (this->_sugar.is_O4_Valid)
    {
        long long O4 = this->_sugar.O4;
        out.push_back(std::make_pair(molSys->getAtom(O4), (int) AtomRole::SUGAR_O4));
    }

    if (this->_sugar.is_O5_Valid)
    {
        long long O5 = this->_sugar.O5;
        out.push_back(std::make_pair(molSys->getAtom(O5), (int) AtomRole::SUGAR_O5));
    }

    if (this->_sugar.is_H1_valid)
    {
        long long H1 = this->_sugar.H1;
        out.push_back(std::make_pair(molSys->getAtom(H1), (int) AtomRole::SUGAR_H1));
    }

    if (this->_sugar.is_H21_valid)
    {
        long long H21 = this->_sugar.H21;
        out.push_back(std::make_pair(molSys->getAtom(H21), (int) AtomRole::SUGAR_H21));
    }

    if (this->_sugar.is_H22_valid)
    {
        long long H22 = this->_sugar.H22;
        out.push_back(std::make_pair(molSys->getAtom(H22), (int) AtomRole::SUGAR_H22));
    }

    if (this->_sugar.is_H3_valid)
    {
        long long H3 = this->_sugar.H3;
        out.push_back(std::make_pair(molSys->getAtom(H3), (int) AtomRole::SUGAR_H3));
    }

    if (this->_sugar.is_H4_valid)
    {
        long long H4 = this->_sugar.H4;
        out.push_back(std::make_pair(molSys->getAtom(H4), (int) AtomRole::SUGAR_H4));
    }

    if (this->_sugar.is_H51_valid)
    {
        long long H51 = this->_sugar.H51;
        out.push_back(std::make_pair(molSys->getAtom(H51), (int) AtomRole::SUGAR_H51));
    }

    if (this->_sugar.is_H52_valid)
    {
        long long H52 = this->_sugar.H52;
        out.push_back(std::make_pair(molSys->getAtom(H52), (int) AtomRole::SUGAR_H52));
    }

    return out;

}

std::vector<std::pair<AtomSP, int>> NucleotideResidue::getNitrogenousBase() const
{
    std::vector<std::pair<AtomSP, int>> out;

    MolecularSystem* molSys = this->getParentMolecularSystem();

    for (auto it=this->nitrogenous_base.begin(); it!=this->nitrogenous_base.end(); it++)
    {
        long long serial = *it;
        AtomSP atom = molSys->getAtom(serial);
        out.push_back(std::make_pair(atom, (int) AtomRole::NITROGENOUSBASE));
    }

    return out;
}

std::vector<std::pair<AtomSP, int>> NucleotideResidue::getGenericAtoms() const
{
    std::vector<std::pair<AtomSP, int>> out;

    MolecularSystem* molSys = this->getParentMolecularSystem();

    for (auto it=this->generic_atoms.begin(); it!=this->generic_atoms.end(); it++)
    {
        long long serial = *it;
        AtomSP atom = molSys->getAtom(serial);
        out.push_back(std::make_pair(atom, (int) AtomRole::GENERIC_ATOM));
    }

    return out;
}
