#ifndef PROXIMA_BOND_H
#define PROXIMA_BOND_H

#include <utility>
#include <memory>

/**
 * @author Federico Lazzari
 */
namespace Proxima
{

class BOGenerator;
class MolecularSystem;

/**
 * @brief This class defines the internal structure of a Bond object.
 *
 *	      A Bond is represented by an object called Bond which contains
 *		  serial numbers of the atoms involved in the bond with the
 *        order of the bond.
 */
class Bond
{

public:

	/* Constructor and destructor */

    /**
     * @brief Constructor.
	 * @param atom1			Serial number of the first atom involved in the bond.
	 * @param atom2			Serial number of the second atom involved in the bond.
	 * @param order			The order of the Bond (e.g. 2 is a double bond).
	 *					    1 by default.
	 *
	 * @note atom1 is the atom with the minor serial number between atom1 and atom2.
	 *		 If the user invokes the constructor of bond with a value of atom1 higher than that of atom2,
	 *		 the constructor automatically swaps their content so as to follow the rule that atom1 has the
	 *		 minor serial between the two atoms.

	 */
    Bond(long long atom1, long long atom2, double order=1, bool isPlanar=false);

	/** @brief Destructor. */
	virtual ~Bond() = default;

    virtual void clone(const std::shared_ptr<Bond> &bnd);

	/* "Get" methods */

	/** @brief Gets the order of the Bond
	 *		   (e.g. 2 is a double bond)
	*/
    virtual double getOrder() const;

	/** @brief Gets the atom with the lowest serial number involved in the bond. */
	virtual long long getMinor() const;

	/** @brief Gets the atom with the highest serial number involved in the bond. */
	virtual long long getHigher() const;


    /**
     * @brief It returns true whether the current bond has been perceived as part of a planar cycle.
     *
     * @note  If cycles and hybridisation has not been perceived, the following is set to false by default.
     */
    virtual bool isBondPlanar() const;

    /* Copy/move constructors/operators are disabled */
    //Bond(const Bond& other) = delete;
	Bond(Bond&& other) = delete;
    Bond& operator=(const Bond& other) = delete;
	Bond& operator=(Bond&& other) = delete;


	/** @brief Operator == for the Bond object */
    bool isEqual(const Bond& other) const;


protected:
    friend class MolecularSystem;

    /**
     * @brief The MolecularSystem class called this method to set the internal planarity value of the bond.
     */
    virtual bool setPlanarity();


protected:

    /**
     * @brief The serial number of the atom with the lowest serial number that is part of the current bond.
     */
	long long _minorAtom;

    /**
     * @brief The serial number of the atom with the highest serial number that is part of the current bond.
     */
	long long _higherAtom;

    /**
     * @brief The bond order.
     */
    double _order;

    /**
     * @brief A boolean value determining whether the current bond is part of a planar cycle.
     */
    bool _isPlanar;

};

/** @brief BondSP is a smart pointer to a Bond object. */
typedef std::shared_ptr< Proxima::Bond > BondSP;

/**
 * @brief	Functor implementing a "less then" operator between two BondSP objects.
 *			This functor has been designed to be used to store BondSP objects in
 *			sorted data structures (e.g. std::map or std::set).
 *
 *			The comparison is performed by comparing the serial number of the first atoms,
 *			or (if the two first atoms are equal) by comparing the serial number of the second atom of each bond.
 *
 *			Since this functor has been designed to be used to store BondSP objects in sorted data structures,
 *			it does not take into account the order of the bonds. In fact, two bonds bonding the same pair
 *			of atoms must be recognised as equals, regardless of their bond order.
*/
class BondSPLessFunctor
{
public:
	bool operator() (const BondSP& bond1, const BondSP& bond2) const
	{
        if(bond1->getMinor()			<	bond2->getMinor())	return true;
        if(bond1->getMinor()			>	bond2->getMinor())	return false;
        if(bond1->getHigher()			<	bond2->getHigher())	return true;
        if(bond1->getHigher()			>	bond2->getHigher())	return false;
		return false;
	}
};

/**
 * @brief Functor implementing a "equal" operator between two BondSP objects.
 *		  This functor has been designed to be used to store BondSP objects in
 *		  sorted data structures (e.g. std::map or std::set).
 *
 *		  The comparison is performed by comparing the serial number of the atoms,
 *
 *		  Since this functor has been designed to be used to store BondSP objects in sorted data structures,
 *		  it does not take into account the order of the bonds. In fact, two bonds bonding the same pair
 *		  of atoms must be recognised as equals, regardless of their bond order.
 */
class BondSPEqualFunctor
{
public:
    bool operator() (const BondSP& bond1, const BondSP& bond2) const
    {
        if(bond1->getMinor()		==	bond2->getMinor() &&
        bond1->getHigher()			==	bond2->getHigher())	return true;
        return false;
    }
};



}


#endif // BOND_H
