//#include "utilities/WarningsOff.h"
#include <stdexcept>
#include <cmath>
#include <queue>
#include <deque>

#include<chrono>
//#include "utilities/WarningsOff.h"
#include <iostream>
#include <fstream>

#include "molecularsystem.h"
#include "hbinteraction.h"
#include "./utilities/utilities.h"
#include "./utilities/floating_point.h"
#include "sssrgenerator.h"
#include "gasteigerparameters.h"
#include "boparameters.h"
#include <omp.h>

#include "forcefieldparameters.h"
#include "ringgenerator.h"
#include "atomicproperties.h"
#include <map>


#ifndef M_PI
    #define M_PI 3.14159265358979323846
#endif


using namespace Proxima;

void MolecularSystem::printZMatrix(std::string path, size_t frameNum)
{
    //Apro il file
    std::ofstream outFile;

    outFile.open(path);

    std::map<long long, long long> serialToIndex;
    std::map<long long, long long> indexToSerial;

    std::map<long long, bool> visitedAtoms;

    //Segno tutti gli atomi come non visitati
    for (auto it=this->atomsBegin(); it!=this->atomsEnd(); it++)
    {
        visitedAtoms.insert(std::pair<long long, bool>(it->first,false));
    }


    //Creo delle code per gli atomi da visitare
    std::queue<long long> atomsToVisit;

    long long previousAtom;
    long long lastVisited;
    bool isFirst=true;

    size_t idx=2;

    //Parto dal primo atomo
    atomsToVisit.push(this->atomsBegin()->first);
    visitedAtoms.at(this->atomsBegin()->first) = true;
    serialToIndex.insert(std::pair<long long, long long>(this->atomsBegin()->first,1));
    indexToSerial.insert(std::pair<long long, long long>(1,this->atomsBegin()->first));

    lastVisited=1;
    previousAtom=1;

    //Stampo la prima riga
    outFile << this->atomsBegin()->second->getName() << std::endl;

    //Fino a quando c'è roba da visitare...
    while(atomsToVisit.empty() != true)
    {
        //Prendi l'elemento in cima
        long long currentSerial = atomsToVisit.front();
        long long currentIndex = serialToIndex.at(currentSerial);
        atomsToVisit.pop();

        //Per ogni legame... (o vertice adiacente)
        for (auto itb = this->getAtom(currentSerial)->getBondBegin(frameNum); itb!=this->getAtom(currentSerial)->getBondEnd(frameNum); itb++)
        {
            BondSP bnd = *itb;
            long long otherSerial = (bnd->getMinor()==currentSerial) ? bnd->getHigher() : bnd->getMinor();

            //Se l'altro atomo non è stato ancora visitato
            if (!visitedAtoms.at(otherSerial))
            {
                //Segnalo come visitato e aggiungilo alla coda di atomi da visitare
                visitedAtoms.at(otherSerial) = true;
                atomsToVisit.push(otherSerial);

                //Inserisci l'indice
                serialToIndex.insert(std::pair<long long, long long>(otherSerial,idx));
                indexToSerial.insert(std::pair<long long, long long>(idx,otherSerial));

                //Calcolo distanza
                double d = Utilities::calculateDistance(this->getAtom(otherSerial)->getPosition(frameNum),
                                                        this->getAtom(currentSerial)->getPosition(frameNum));


                //Se c'è un angolo di legame (cioè c'è un last atom) ...
                if (lastVisited!=1 && previousAtom==1)
                {
                    //Calcolo angolo
                    std::vector<double> otherPos = this->getAtom(otherSerial)->getPosition(frameNum);
                    std::vector<double> currentPos = this->getAtom(currentSerial)->getPosition(frameNum);

                    std::vector<double> lastPos = this->getAtom(indexToSerial.at(lastVisited))->getPosition(frameNum);
                    std::vector<double> l1 = Utilities::minus(otherPos, currentPos);
                    std::vector<double> l2 = Utilities::minus(lastPos, currentPos);
                    double ang = Utilities::calculateAngle(l1, l2);

                    outFile <<  this->getAtom(otherSerial)->getName() << " " << currentIndex << " " << d << " " << lastVisited << " " << ang << std::endl;
                }

                //Se c'è una torsione (cioè un previous atom).
                else if (lastVisited!=1 && previousAtom!=1)
                {
                    //Calcolo angolo
                    std::vector<double> otherPos = this->getAtom(otherSerial)->getPosition(frameNum);
                    std::vector<double> currentPos = this->getAtom(currentSerial)->getPosition(frameNum);

                    std::vector<double> lastPos = this->getAtom(indexToSerial.at(lastVisited))->getPosition(frameNum);
                    std::vector<double> l1 = Utilities::minus(otherPos, currentPos);
                    std::vector<double> l2 = Utilities::minus(lastPos, currentPos);
                    double ang = Utilities::calculateAngle(l1, l2);


                    //Calcolo torsione
                    std::vector<double> prevPos = this->getAtom(indexToSerial.at(previousAtom))->getPosition(frameNum);
                    std::vector<double> l3 = Utilities::minus(lastPos,prevPos);
                    double tor = Utilities::calculateDihedral(l1,l2,l3);

                    outFile << this->getAtom(otherSerial)->getName() << " " << currentIndex << " " << d << " " << lastVisited << " " << ang << " " << previousAtom << " " << tor << std::endl;
                }

                else
                    outFile << this->getAtom(otherSerial)->getName() << " " << currentIndex << " " << d << std::endl;


                idx++;

                if (isFirst)
                {
                    previousAtom=lastVisited;
                    lastVisited=otherSerial;
                }

            }
        }

        if (isFirst)
            isFirst=false;
        else
        {
            //Aggiorno la situazione degli indici
            previousAtom = lastVisited;
            lastVisited = currentSerial;

        }

    }

    outFile.close();


}

Proxima::MolecularSystem::MolecularSystem(std::string name, double totalCharge)
{
	_name = name;
	_framesList.push_back(Frame());
    _frameLabel.push_back("");

    this->_totalCharge=totalCharge;

//#ifdef Eigen_lib
    if (this->hardness.size()==0)
    {
        this->hardness.insert(std::pair<int,double>(1,13.8904*23));
        this->hardness.insert(std::pair<int,double>(6,10.126*23));
        this->hardness.insert(std::pair<int,double>(7,11.760*23));
        this->hardness.insert(std::pair<int,double>(8,13.364*23));

        this->hardness.insert(std::pair<int,double>(15,8*23));
        this->hardness.insert(std::pair<int,double>(16,8.972*23));
    }

    if (this->electronegativity.size()==0)
    {
        this->electronegativity.insert(std::pair<int,double>(1,4.5280*23));
        this->electronegativity.insert(std::pair<int,double>(6,5.343*23));
        this->electronegativity.insert(std::pair<int,double>(7,6.899*23));
        this->electronegativity.insert(std::pair<int,double>(8,8.741*23));

        this->electronegativity.insert(std::pair<int,double>(15,5.463*23));
        this->electronegativity.insert(std::pair<int,double>(16,6.972*23));
    }
//#endif

}


std::string Proxima::MolecularSystem::getName() const
{
	return _name;
}

size_t MolecularSystem::getNumFrames() const
{
	return _framesList.size();
}

void MolecularSystem::addFrame(std::string frameLabel)
{
	_framesList.push_back(Frame());
	_frameLabel.push_back(frameLabel);
	for (auto it=_atomsMap.begin(); it!=_atomsMap.end(); it++)
		it->second->addFrame();
}

void MolecularSystem::setFrameLabel(size_t frameNum, const std::string& frameLabel)
{
	if(frameNum >= _framesList.size())
		throw std::invalid_argument("MolecularSystem::setFrameLabel - frame number out of range!");

	_frameLabel[frameNum] = frameLabel;
}

std::string MolecularSystem::getFrameLabel(size_t frameNum)
{
	if(frameNum >= _framesList.size())
		throw std::invalid_argument("MolecularSystem::getFrameLabel - frame number out of range!");

	return _frameLabel[frameNum];
}


/* Atoms */

MolecularSystem::atom_iterator MolecularSystem::atomsBegin() const
{
	return _atomsMap.begin();
}

MolecularSystem::atom_iterator MolecularSystem::atomsEnd() const
{
	return _atomsMap.end();
}

void MolecularSystem::addAtom(AtomSP atom)
{
    if(!atom)
		throw std::invalid_argument("MolecularSystem::addAtom - Atom is a null pointer!");
	if(_framesList.size()>1)
		throw std::invalid_argument("MolecularSystem::addAtom - You're not allowed to add new atoms!");
	if(_atomsMap.find(atom->_serial)!=_atomsMap.end())
		throw std::invalid_argument("MolecularSystem::addAtom - Trying to add multiple atoms "
									"with the same serial number!");
	_atomsMap.insert(std::pair<long long, AtomSP>(atom->_serial, atom));
}

int MolecularSystem::getNumAtoms() const
{
	return int(_atomsMap.size());
}

bool MolecularSystem::containsAtom(long long serial) const
{
	return _atomsMap.find(serial) != _atomsMap.end();
}

AtomSP MolecularSystem::getAtom(long long serial) const
{
	if(!containsAtom(serial))
		throw std::invalid_argument("MolecularSystem::getAtom - Missing atom");
    return  _atomsMap.at(serial);
}

int MolecularSystem::getSpinMultiplicity()
{
    if (this->_spin_multiplicity!=0)
        return this->_spin_multiplicity;

    //Computing the total number of electrons
    long long numElectrons = 0;
    for (auto itAtom=this->atomsBegin(); itAtom!=this->atomsEnd(); itAtom++)
    {
        AtomSP atom = itAtom->second;
        numElectrons = numElectrons + atom->getAtomicNum();
    }
    numElectrons=numElectrons-this->_charges;

    int remainder = numElectrons % 2;

    if (remainder==0)
    {
        this->_spin_multiplicity = 1;
        return 1;
    }

    else
    {
        this->_spin_multiplicity = 2;
        return 2;
    }


}

void MolecularSystem::setSpinMultiplicity(int spin)
{
    if (spin<=0)
        throw std::invalid_argument("MolecularSystem::setSpinMultiplicity - Invalid spin argument");

    this->_spin_multiplicity=spin;

}

void MolecularSystem::computePeterBondOrder(size_t frameNum)
{
    BOParameters bo_parameters = BOParameters();

    //For each bond
    for (auto it=this->bondsBegin(frameNum); it!=this->bondsEnd(frameNum); it++)
    {
        BondSP currentBond = *it;
        AtomSP atom1 = this->getAtom(currentBond->getMinor());
        AtomSP atom2 = this->getAtom(currentBond->getHigher());

        try {
            double bo = bo_parameters.computeBO_peter(atom1->getAtomicNum(), atom2->getAtomicNum(),atom1->getPosition(frameNum),atom2->getPosition(frameNum));
            currentBond->_order=bo;
        } catch (std::invalid_argument) {
            throw std::invalid_argument("MolecularSystem::computePeterBondOrder - Invalid atom types");
        }

    }
}



MolecularSystem::fragment_iterator MolecularSystem::fragmentsBegin() const
{
    return this->_fragments.begin();
}

MolecularSystem::fragment_iterator MolecularSystem::fragmentsEnd() const
{
    return this->_fragments.end();
}





bool MolecularSystem::addFragment(FragmentSP fragment)
{
    if (fragment->getParentMolecularSystem()!=nullptr) return false;
    //Puoi aggiungere più frammenti con lo stesso chainID
    //if (this->containsFragment(fragment->getChainID())) return false;

    _fragments.insert(std::make_pair(fragment->getChainID(),fragment));
    fragment->setParentMolecularSystem(this);
    return true;
}

bool MolecularSystem::containsFragment(char chainID) const
{
    return _fragments.find(chainID) != _fragments.end();
}

std::vector<FragmentSP> MolecularSystem::getFragment(char chainID) const
{
    std::vector<FragmentSP> fragments;
    fragments.clear();

    if (!this->containsFragment(chainID)) return fragments;

    //std::pair<std::multimap<char,FragmentSP>::iterator, std::multimap<char,FragmentSP>::iterator> possibleFragments;
    auto possibleFragments = (this->_fragments).equal_range(chainID);

    for (auto it=possibleFragments.first; it!=possibleFragments.second; ++it)
    {
        fragments.push_back(it->second);
    }

    return fragments;
}

size_t MolecularSystem::getNumFragments() const
{
    return _fragments.size();
}

MolecularSystem::cycle_iterator MolecularSystem::getCyclesBegin(size_t frameNum) const
{
    if(frameNum >= _framesList.size())
        throw std::invalid_argument("MolecularSystem::getCyclesBegin - frame number out of range!");

    return (_framesList[frameNum]._cyclesSet).begin();
}

MolecularSystem::cycle_iterator MolecularSystem::getCyclesEnd(size_t frameNum) const
{
    if(frameNum >= _framesList.size())
        throw std::invalid_argument("MolecularSystem::getCyclesEnd - frame number out of range!");

    return _framesList[frameNum]._cyclesSet.end();
}

std::vector<BondSP> MolecularSystem::getCycle(size_t frameNum, size_t idx)
{
    if(frameNum >= _framesList.size())
        throw std::invalid_argument("MolecularSystem::getCycle - frame number out of range!");

    if (idx>=this->getNumCycles(frameNum))
        throw std::invalid_argument("MolecularSystem::getCycle - cycle index out of range!");

    auto it = std::next(this->_framesList[frameNum]._cyclesSet.begin(),idx);
    return *it;
}

bool MolecularSystem::clearCycles(size_t frameNum)
{
    if(frameNum >= _framesList.size())
        throw std::invalid_argument("MolecularSystem::clearCycles - frame number out of range!");

    if (this->getNumCycles(frameNum)==0)
        return false;

    this->_framesList[frameNum]._cyclesSet.erase(this->_framesList[frameNum]._cyclesSet.begin(),
                                                 this->_framesList[frameNum]._cyclesSet.end());
    return true;
}

size_t MolecularSystem::getNumCycles(size_t frameNum) const
{
    return this->_framesList[frameNum]._cyclesSet.size();
}


/* Bonds */

MolecularSystem::bond_iterator MolecularSystem::bondsBegin(size_t frameNum) const
{
	if(frameNum >= _framesList.size())
		throw std::invalid_argument("MolecularSystem::bondsBegin - frame number out of range!");
	return _framesList[frameNum]._bondsSet.begin();
}

MolecularSystem::bond_iterator MolecularSystem::bondsEnd(size_t frameNum) const
{
	if(frameNum >= _framesList.size())
		throw std::invalid_argument("MolecularSystem::bondsEnd - frame number out of range!");
	return _framesList[frameNum]._bondsSet.end();
}

void MolecularSystem::addBond(size_t frameNum, BondSP newBond)
{
    if(frameNum >= _framesList.size())
        throw std::invalid_argument("MolecularSystem::addBond - frame number out of range!");

    if(!newBond)
		throw std::invalid_argument("MolecularSystem::addBond - Bond is a null pointer!");

	AtomSP atom1(nullptr);
    auto atom1Iter = _atomsMap.find(newBond->getMinor());
	if(atom1Iter !=_atomsMap.end()) atom1 = atom1Iter->second;

	AtomSP atom2(nullptr);
    auto atom2Iter = _atomsMap.find(newBond->getHigher());
	if(atom2Iter !=_atomsMap.end()) atom2 = atom2Iter->second;

	if( (!atom1) || (!atom2) )
		throw std::invalid_argument("MolecularSystem::addBond - atoms are not in the system!");

    if(containsBond(frameNum,newBond))
		throw std::invalid_argument("MolecularSystem::addBond - Trying to add the same bond multiple times!");

    _framesList[frameNum]._bondsSet.insert(newBond);

	//Updating atoms
    atom1->addBond(frameNum,newBond);
    atom2->addBond(frameNum,newBond);
}

int Proxima::MolecularSystem::getNumBonds(size_t frameNum) const
{
	if(frameNum >= _framesList.size() )
		throw std::invalid_argument("MolecularSystem::getNumBonds - frame number out of range!");
	return int(_framesList[frameNum]._bondsSet.size());
}

bool MolecularSystem::containsBond(size_t frameNum, BondSP bond)
{
	if(frameNum >= _framesList.size())
		throw std::invalid_argument("MolecularSystem::containsBond - frame number out of range!");
    return _framesList[frameNum]._bondsSet.find(bond) != _framesList[frameNum]._bondsSet.end();
}

BondSP MolecularSystem::getBond(size_t frameNum, long idx)
{
    if(frameNum >= _framesList.size())
        throw std::invalid_argument("MolecularSystem::getBond - frame number out of range!");
    if (idx>=this->getNumBonds(frameNum))
        throw std::invalid_argument("MolecularSystem::getBond - bond index out of range!");
    auto it = std::next(this->_framesList[frameNum]._bondsSet.begin(),idx);
    return *it;
}

/* HBonds */

MolecularSystem::hbond_iterator MolecularSystem::hbondsBegin(size_t frameNum) const
{
	if(frameNum >= _framesList.size())
		throw std::invalid_argument("MolecularSystem::hbondsBegin - frame number out of range!");
	return _framesList[frameNum]._hbondsSet.begin();
}

MolecularSystem::hbond_iterator MolecularSystem::hbondsEnd(size_t frameNum) const
{
	if(frameNum >= _framesList.size())
		throw std::invalid_argument("MolecularSystem::hbondsEnd - frame number out of range!");
	return _framesList[frameNum]._hbondsSet.end();
}

void MolecularSystem::addHBond(size_t frameNum, HBondSP bond)
{
	if(bond==nullptr)
		throw std::invalid_argument("MolecularSystem::addHBond - HBond is a null pointer!");

	AtomSP hydrogen(nullptr);
	auto hydrogenIter = _atomsMap.find(bond->getHydrogen());
	if(hydrogenIter !=_atomsMap.end()) hydrogen = hydrogenIter->second;

	AtomSP donor(nullptr);
	auto donorIter = _atomsMap.find(bond->getDonor());
	if(donorIter !=_atomsMap.end()) donor = donorIter->second;

	AtomSP acceptor(nullptr);
	auto acceptorIter = _atomsMap.find(bond->getAcceptor());
	if(acceptorIter !=_atomsMap.end()) acceptor = acceptorIter->second;

	if( (!hydrogen) || (!donor) || (!acceptor) )
		throw std::invalid_argument("MolecularSystem::addHBond - Atoms are not in the system!");

    if( ! (donor->findBond(frameNum,hydrogen->getSerial()) ) )
		throw std::invalid_argument("MolecularSystem::addHBond - The donor is not "
									"bonded with the hydrogen at the specified frame!");

    if( donor->findHBond(frameNum, acceptor->getSerial(), hydrogen->getSerial()))
		throw std::invalid_argument("MolecularSystem::addHBond - Trying to add "
									"the same hydrogen bond multiple times!");

	_framesList[frameNum]._hbondsSet.insert(bond);

	//Updating atoms
	//Adding the HBond also to the Hydrogen atom because it's a three atom interaction
    hydrogen->addHBond(frameNum,bond);
    donor->addHBond(frameNum,bond);
    acceptor->addHBond(frameNum,bond);
}

int MolecularSystem::getNumHBonds(size_t frameNum) const
{
	if(frameNum >= _framesList.size() )
		throw std::invalid_argument("MolecularSystem::getNumHBonds - frame number out of range!");
	return int(_framesList[frameNum]._hbondsSet.size());
}

bool MolecularSystem::containsHBond(size_t frameNum, HBondSP bond)
{
	if(bond==nullptr)
		throw std::invalid_argument("MolecularSystem::containsHBond - hbond is a null pointer!");
	if(frameNum >= _framesList.size() )
		throw std::invalid_argument("MolecularSystem::containsHBond - frame number out of range!");
	return _framesList[frameNum]._hbondsSet.find(bond) != _framesList[frameNum]._hbondsSet.end();
}

void MolecularSystem::removeBond(size_t frameNum, BondSP bond)
{
	if(frameNum >= _framesList.size() )
		throw std::invalid_argument("MolecularSystem::removeBond - frame number out of range!");

	getAtom(bond->getMinor())->removeBond(frameNum,bond);
	getAtom(bond->getHigher())->removeBond(frameNum,bond);

	_framesList[frameNum]._bondsSet.erase(bond);
}

void MolecularSystem::removeHBond(size_t frameNum, HBondSP bond)
{
	if(frameNum >= _framesList.size() )
		throw std::invalid_argument("MolecularSystem::removeHBond - frame number out of range!");

    getAtom(bond->getAcceptor())->removeHBond(frameNum,bond);
    getAtom(bond->getDonor())->removeHBond(frameNum,bond);
    getAtom(bond->getHydrogen())->removeHBond(frameNum,bond);

    _framesList[frameNum]._hbondsSet.erase(bond);
}

HBondSP MolecularSystem::getHBond(size_t frameNum, long idx)
{
    if(frameNum >= _framesList.size())
        throw std::invalid_argument("MolecularSystem::getHBond - frame number out of range!");
    if (idx>=this->getNumHBonds(frameNum))
        throw std::invalid_argument("MolecularSystem::getHBond - bond index out of range!");
    auto it = std::next(this->_framesList[frameNum]._hbondsSet.begin(),idx);
    return *it;
}

//std::vector<std::vector<BondSP> > MolecularSystem::computeCycles_Fast(size_t frame)
//{
//    AtomsGridSP grid(new AtomsGrid(this,frameNum));

//    for (unsigned x=0; x!=grid->getNumCellsX(); x++)
//    {
//        for (unsigned y=0; y!=grid->getNumCellsY(); y++)
//        {
//            for (unsigned z=0; z!=grid->getNumCellsZ(); z++)
//            {
//                //Detecting all nearby atoms
//                std::vector<AtomSP> atoms = grid->getAtomsInCell(x,y,z);
//                for (unsigned i=-1; i!=2; i++)
//                {
//                    for (unsigned j=-1; j!=2; j++)
//                    {
//                        for (unsigned k=-1; k!=2; k++)
//                        {
//                            std::vector<AtomSP> newAtoms = grid->getAtomsInCell(x+i,y+j,z+k);
//                            atoms.insert(atoms.end(),newAtoms.begin(),newAtoms.end());
//                        }
//                    }
//                }




//            }
//        }
//    }
//}




/* Computes */

void MolecularSystem::computeBondsInFrame_Slow(size_t frameNum,
                                               double tolerance_distance)
{
	if(frameNum >= _framesList.size() )
		throw std::invalid_argument("MolecularSystem::computeBondsInFrame_Slow"
									" - frame number out of range!");

	/* Removing old bonds */
	for(const BondSP& bond : _framesList[frameNum]._bondsSet)
	{
		getAtom(bond->getMinor())->removeBond(frameNum,bond);
		getAtom(bond->getHigher())->removeBond(frameNum,bond);
	}

	_framesList[frameNum]._bondsSet.clear();

	if(this->getNumAtoms() < 2) return;

	/*  For each couple of atoms... */
	auto last_atom_iter = std::prev(atomsEnd(),1);
	for (auto it1 = atomsBegin(); it1 != last_atom_iter; it1++)
	{
		for(auto it2 = std::next(it1,1); it2 != atomsEnd(); it2++)
		{
			AtomSP atom1 = it1->second;
			AtomSP atom2 = it2->second;

            this->checkDistanceAndCreateBond(frameNum,
                                             atom1,atom2,
                                                 tolerance_distance);

		} //For each atom 2
	} //For each atom 1

	this->maximumCoordination(frameNum);

}



void MolecularSystem::computeBondsInFrame_Fast(size_t frameNum, double tolerance_distance)
{
	if(frameNum >= _framesList.size() )
		throw std::invalid_argument("MolecularSystem::computeBondsInFrame_Fast"
									" - frame number out of range!");
	/* Removing old bonds */
	for(const BondSP& bond : _framesList[frameNum]._bondsSet)
	{
		getAtom(bond->getMinor())->removeBond(frameNum,bond);
		getAtom(bond->getHigher())->removeBond(frameNum,bond);
	}

	_framesList[frameNum]._bondsSet.clear();

	if(this->getNumAtoms() < 2) return;

	/* Creating a molecular grid */
	AtomsGridSP grid(new AtomsGrid(this,frameNum));

	/* for each atom */
	for (auto it1 = atomsBegin(); it1 != atomsEnd(); it1++)
	{
		AtomSP atom1 = it1->second;
		/* for each position */
		std::vector<AtomSP> neighbors(grid->getNeighbors(atom1));
		/* for each neighbor */
		for (auto it2 = neighbors.begin(); it2!=neighbors.end(); it2++)
			{
				//Qui sto selezionando una specifica posizione di uno specifico atomo.
				AtomSP atom2 = *it2;

                if(atom1->getSerial()<atom2->getSerial()) // Avoids to create the same bond two times:
								// one for <atomX, atomY> and one for <atomY, atomX>
				{

                   this->checkDistanceAndCreateBond(frameNum,atom1,atom2,tolerance_distance);

				} //atom1 < atom2

			} //for neighbor

        } //for atom


    this->maximumCoordination(frameNum);

    }

void MolecularSystem::computeBonds_Slow(double tolerance_distance)
{
	for(size_t frameNum = 0; frameNum < this->getNumFrames(); frameNum++)
        this->computeBondsInFrame_Slow(frameNum, tolerance_distance);
}



void MolecularSystem::computeBonds_Fast(double tolerance_distance)
{
	for(size_t frameNum = 0; frameNum < this->getNumFrames(); frameNum++)
        this->computeBondsInFrame_Fast(frameNum, tolerance_distance);
}





BoundingBox MolecularSystem::computeBBox(size_t frameNum)
{
	if(frameNum >= _framesList.size() )
		throw std::invalid_argument("MolecularSystem::computeBBox - frame number out of range!");

	BoundingBox box;

	for(auto iter = atomsBegin(); iter!=atomsEnd(); iter++)
	{
		AtomSP atom = iter->second;
        std::vector<double> position = atom->getPosition(frameNum);
			double vdwRadius = Element::getWaalsRadius(atom->getAtomicNum());
			double minX = position[0] - vdwRadius;
			double minY = position[1] - vdwRadius;
			double minZ = position[2] - vdwRadius;
			double maxX = position[0] + vdwRadius;
			double maxY = position[1] + vdwRadius;
			double maxZ = position[2] + vdwRadius;

			if(minX < box.minX) box.minX = minX;
			if(minY < box.minY) box.minY = minY;
			if(minZ < box.minZ) box.minZ = minZ;
			if(maxX > box.maxX) box.maxX = maxX;
			if(maxY > box.maxY) box.maxY = maxY;
			if(maxZ > box.maxZ) box.maxZ = maxZ;


	}

    return box;
}


void MolecularSystem::computeHBondVector(size_t frameNum, std::vector<HBondSP> &out_vector)
{
    if(frameNum >= _framesList.size() )
        throw std::invalid_argument("MolecularSystem::computeHBondVector - frame number out of range!");
    for(hbond_iterator it=this->hbondsBegin(frameNum); it!=this->hbondsEnd(frameNum); it++)
    {
        out_vector.push_back(*it);
    }
    //	std::sort(out_vector.begin(),out_vector.end(), HBondSPLessFunctor());
}



std::set<BondSP> MolecularSystem::getCyclicBonds(size_t frameNum)
{
    if(frameNum >= _framesList.size() )
        throw std::invalid_argument("MolecularSystem::removeTerminalAtoms - frame number out of range!");

    RingGenerator rg(this,frameNum);
    std::set<std::set<BondSP>> bonds = rg.getBlocks();

    std::set<BondSP> output;

    for (auto it=bonds.begin(); it!=bonds.end(); it++)
    {
        std::set<BondSP> currents = *it;
        output.insert(currents.begin(),currents.end());
    }

    return output;

}


bool MolecularSystem::computeHortonCycles(size_t frameNum)
{
    if(frameNum >= _framesList.size() )
        throw std::invalid_argument("MolecularSystem::computeHortonCycles - frame number out of range!");

    if (this->_framesList[frameNum]._cyclesSet.size()!=0) return false;

    RingGenerator rg(this,frameNum);
    std::vector<std::vector<BondSP > > cycles = rg.getHortonCycles();

    this->_framesList[frameNum]._cyclesSet=cycles;
    return true;

}



void MolecularSystem::normalizeBO(size_t frameNum)
{

    //Creo un vettore dei legami originali
    std::map<BondSP, size_t> fromBondToIndex;
    std::map<size_t, BondSP> fromIndexToBond;

    std::map<long long, size_t> fromAtomToIndex;
    std::map<size_t, long long> fromIndexToAtom;

    //Rinumero gli atomi
    long long idx = 0;
    for (auto it=this->atomsBegin(); it!=this->atomsEnd(); it++)
    {
        long long ser = it->first;
        fromAtomToIndex.insert(std::pair<long long, size_t>(ser,idx));
        fromIndexToAtom.insert(std::pair<size_t, long long>(idx,ser));

        idx++;
    }

    Eigen::MatrixXf BO = Eigen::MatrixXf::Zero(this->getNumBonds(frameNum)+this->getNumAtoms(),this->getNumBonds(frameNum)+this->getNumAtoms());
    Eigen::VectorXf y(this->getNumBonds(frameNum)+this->getNumAtoms());


    idx=0;
    for (auto it=this->bondsBegin(frameNum); it!=this->bondsEnd(frameNum); it++)
    {
        BondSP bnd = *it;
        fromBondToIndex.insert(std::pair<BondSP,size_t>(bnd,idx));
        fromIndexToBond.insert(std::pair<size_t,BondSP>(idx,bnd));

        BO(idx,idx) = 2;
        BO(idx, static_cast<long long>( this->getNumBonds(frameNum)+fromAtomToIndex.at(bnd->getMinor()) )  ) = 1;
        BO(idx, static_cast<long long>( this->getNumBonds(frameNum)+fromAtomToIndex.at(bnd->getHigher()) ) ) = 1;

        y[idx] = static_cast<float>(2 * bnd->getOrder());

        idx++;
    }

    for (auto it=this->atomsBegin(); it!=this->atomsEnd(); it++)
    {
        int currentRow = this->getNumBonds(frameNum) + fromAtomToIndex.at(it->first);

        //Per ogni legame dell'atomo, imposta i valori nella riga
        for (auto itb=it->second->getBondBegin(frameNum); itb!=it->second->getBondEnd(frameNum); itb++)
        {
            BondSP currentBnd = *itb;
            BO(currentRow,fromBondToIndex.at(currentBnd)) = 1;
        }

        y[currentRow] = Element::getValence(it->second->getAtomicNum()) + it->second->getCharge(frameNum);

    }



    //Minimizzo la lagrangiana
    Eigen::VectorXf result = BO.colPivHouseholderQr().solve(y);



    //Rimetto i risultati del calcolo dentro i legami
    idx=0;
    while (idx<this->getNumBonds(frameNum))
    {
        BondSP bnd = fromIndexToBond.at(idx);
        bnd->_order = result(idx);
        idx++;
    }


}



void MolecularSystem::normalizeBO_noTerminal(size_t frameNum)
{

    //Creo un vettore dei legami originali
    std::map<BondSP, size_t> fromBondToIndex;
    std::map<size_t, BondSP> fromIndexToBond;

    std::map<long long, size_t> fromAtomToIndex;
    std::map<size_t, long long> fromIndexToAtom;

    //Rinumero gli atomi (escludendo quelli terminali)
    size_t idx = 0;
    for (auto it=this->atomsBegin(); it!=this->atomsEnd(); it++)
    {
        long long ser = it->first;
        if(it->second->getNumBonds(frameNum)>1)
        {
            fromAtomToIndex.insert(std::pair<long long, size_t>(ser,idx));
            fromIndexToAtom.insert(std::pair<size_t, long long>(idx,ser));
            idx++;
        }
    }

    Eigen::MatrixXf BO = Eigen::MatrixXf::Zero(this->getNumBonds(frameNum)+fromAtomToIndex.size(),this->getNumBonds(frameNum)+fromAtomToIndex.size());
    Eigen::VectorXf y(this->getNumBonds(frameNum)+fromAtomToIndex.size());


    idx=0;
    for (auto it=this->bondsBegin(frameNum); it!=this->bondsEnd(frameNum); it++)
    {
        BondSP bnd = *it;
        fromBondToIndex.insert(std::pair<BondSP,size_t>(bnd,idx));
        fromIndexToBond.insert(std::pair<size_t,BondSP>(idx,bnd));

        BO(idx,idx) = 2;

        //Fai attenzione che sia un atomo valido (non terminale)
        auto itMinor = fromAtomToIndex.find(bnd->getMinor());
        auto itHigher = fromAtomToIndex.find(bnd->getHigher());

        if (itMinor!=fromAtomToIndex.end())
            BO(idx,this->getNumBonds(frameNum)+fromAtomToIndex.at(bnd->getMinor())) = 1;

        if (itHigher!=fromAtomToIndex.end())
            BO(idx,this->getNumBonds(frameNum)+fromAtomToIndex.at(bnd->getHigher())) = 1;

        y[idx] = 2*bnd->getOrder();

        idx++;
    }

    for (auto it=fromAtomToIndex.begin(); it!=fromAtomToIndex.end(); it++)
    {
        AtomSP atm = this->getAtom(it->first);
        size_t currentRow = this->getNumBonds(frameNum) + it->second;

        //Per ogni legame dell'atomo, imposta i valori nella riga
        for (auto itb=atm->getBondBegin(frameNum); itb!=atm->getBondEnd(frameNum); itb++)
        {
            BondSP currentBnd = *itb;
            BO(currentRow,fromBondToIndex.at(currentBnd)) = 1;
        }

        y[currentRow] = Element::getValence(atm->getAtomicNum()) + atm->getCharge(frameNum);

    }



    //Minimizzo la lagrangiana
    Eigen::VectorXf result = BO.colPivHouseholderQr().solve(y);



    //Rimetto i risultati del calcolo dentro i legami
    idx=0;
    while (int(idx) < this->getNumBonds(frameNum))
    {
        BondSP bnd = fromIndexToBond.at(idx);
        bnd->_order = result(idx);
        idx++;
    }



}












bool MolecularSystem::computeGasteigerCharges(size_t frameNum, size_t numIterations)
{
    if(frameNum >= _framesList.size() )
        throw std::invalid_argument("MolecularSystem::computeGasteigerCharges - frame number out of range!");

    size_t idx = 0;

    std::map<long long, double> maxElectr = this->computeMAXElectronegativities(frameNum);

    double alpha = 1;

    while (idx<numIterations)
    {
        alpha = alpha*0.5;

        std::map<long long,double> electronegativities = this->computeElectronegativities(frameNum);

        for (auto it=this->bondsBegin(frameNum); it!=this->bondsEnd(frameNum); it++)
        {
            BondSP bond = *it;

            double denom = 20.02;

            AtomSP atom1 = this->getAtom(bond->getMinor());
            AtomSP atom2 = this->getAtom(bond->getHigher());

            double chi1 = electronegativities.at(atom1->getSerial());
            double chi2 = electronegativities.at(atom2->getSerial());

            double denom1 = (maxElectr.find(atom1->getSerial()))->second;
            double denom2 = (maxElectr.find(atom2->getSerial()))->second;

            if (chi1 >= chi2 && atom2->getAtomicNum()!=1)
                denom = denom2;

            if (chi1 < chi2 && atom1->getAtomicNum()!=1)
                denom = denom1;

            double charge = (chi1-chi2)/denom;

            double charge1 = atom1->getCharge(frameNum) - alpha*charge;
            double charge2 = atom2->getCharge(frameNum) + alpha*charge;

            atom1->setCharge(frameNum,charge1);
            atom2->setCharge(frameNum,charge2);

        }

        idx++;
    }

    this->_charges=true;
    return true;
}

CubeGridSP MolecularSystem::computeElectrostaticPotential(double dx, double dy, double dz, size_t frameNum)
{
    CubeGridSP out(new CubeGrid(this,dx,dy,dz,frameNum));

    //For each Voxel...
    for (size_t idX = 0; idX != size_t(out->getNumX()); idX++)
    {
        for (size_t idY = 0; idY != size_t(out->getNumY()); idY++)
        {
            for (size_t idZ = 0; idZ != size_t(out->getNumZ()); idZ++)
            {
                std::vector<double> centerPoint{out->getOrigin()[0]+ idX*dx + dx/2.0, out->getOrigin()[1]+ idY*dy + dy/2.0, out->getOrigin()[2]+ idZ*dz + dz/2.0};
                double pot = this->computePotential(centerPoint,frameNum);
                out->data[idX][idY][idZ]=pot;
            }
        }

    }

    return out;
}


std::set<BondSP > MolecularSystem::getBridgeBonds(size_t frameNum)
{
    //Va come i cicli al quadrato

    std::set<BondSP> out;


    //Per ogni ciclo
    for (auto it=this->getCyclesBegin(frameNum); it!=this->getCyclesEnd(frameNum); it++)
    {
        std::vector<BondSP> cycle1 = *it;


        if(cycle1.size()<=6)
        {
            std::pair<double,std::set<long long>> torsion1 = this->getMeanTorsion(cycle1,frameNum);
            //If the average torsion is <= 7.5° is planar
            if (torsion1.first<=7.5)
            {
                //Per ogni altro ciclo
                auto it2 = std::next(it);
                for (;it2!=this->getCyclesEnd(frameNum); it2++)
                {
                    std::vector<BondSP> cycle2 = *it2;

                    if(cycle2.size()<=6)
                    {

                        std::pair<double,std::set<long long>> torsion2 = this->getMeanTorsion(cycle2,frameNum);

                        //If the average torsion is <= 7.5° is planar
                        if (torsion2.first<=7.5)
                        {
                            std::vector<BondSP> bonds = this->findBondBetweenCycles(cycle1,cycle2,frameNum);
                            if (bonds.size()==1)
                                out.insert(bonds[0]);
                        }

                    }

                }
            }

        }


    }


    return out;
}

void MolecularSystem::toMOL_V2000(std::string path, size_t frameNum)
{
    //Opening file
    std::ofstream f(path);

    //Checking wheter the file is open
    if (!(f.is_open()))
        throw std::invalid_argument("MolecularSystem::toMOL_V2000- Cannot open file!");

    f << "\n";
    f << " Proxima\n";
    f << "\n";


    //Printing counts line
    std::string  countsLine = "        0  0  0  0  0  0  0  0999 V2000\n";

    std::stringstream ss;
    ss << this->getNumAtoms();
    std::string atomsString = ss.str();
    ss.str("");
    ss.clear();

    ss << this->getNumBonds(frameNum);
    std::string bondsString = ss.str();
    ss.str("");
    ss.clear();

    size_t blanckAtoms = 3-atomsString.length();
    size_t blanckBonds = 3-bondsString.length();

    countsLine.replace(blanckAtoms,3-blanckAtoms,atomsString);
    countsLine.replace(3+blanckBonds,3-blanckBonds,bondsString);

    f << countsLine;

    //Printing atoms
    for (auto it=this->atomsBegin(); it!=this->atomsEnd(); it++)
    {
        std::ostringstream strs;

        std::string atomLine = "                                   0  0  0  0  0  0  0  0  0  0  0  0\n";

        AtomSP current = it->second;


        double xPos = current->getPosition(frameNum)[0];
        std::string xPos_str = Utilities::outStrPos_molParser(xPos);
        if (xPos<0)
        {
            atomLine.replace(2,1,"-");
        }

        atomLine.replace(3,7,xPos_str);
        strs.str("");
        strs.clear();



        double yPos = current->getPosition(frameNum)[1];
        std::string yPos_str = Utilities::outStrPos_molParser(yPos);
        if (yPos<0)
        {
            atomLine.replace(12,1,"-");
        }

        atomLine.replace(13,7,yPos_str);
        strs.str("");
        strs.clear();

        double zPos = current->getPosition(frameNum)[2];
        std::string zPos_str = Utilities::outStrPos_molParser(zPos);
        if (zPos<0)
        {
            atomLine.replace(22,1,"-");
        }

        atomLine.replace(23,7,zPos_str);
        strs.str("");
        strs.clear();

        std::string atomName = Element::getElementSymbol(current->getAtomicNum());
        atomName.resize(2,' ');
        //std::locale loc;
        for (std::string::size_type i=1; i<atomName.length(); ++i)
        {
            atomName[i] = std::tolower(atomName[i]/*,loc*/);
        }

        atomLine.replace(31,2,atomName);
        strs.str("");
        strs.clear();

        f << atomLine;
    }

    //Printing bonds
    for (auto it=this->bondsBegin(frameNum); it!=this->bondsEnd(frameNum); it++)
    {
        BondSP current = *it;
        std::string bondLine = "        1  0  0  0  0\n";
        std::string minorString;
        std::string higherString;

        std::stringstream ss;
        ss << current->getMinor();
        minorString = ss.str();
        ss.str("");
        ss.clear();

        ss << current->getHigher();
        higherString = ss.str();
        ss.str("");
        ss.clear();

        size_t blanckMinor = 3-minorString.length();
        size_t blanckHigher = 3-higherString.length();


        bondLine.replace(blanckMinor,3-blanckMinor,minorString);
        bondLine.replace(3+blanckHigher,3-blanckHigher,higherString);

        f << bondLine;
    }

    f << "M  END";
    f.close();
}

void MolecularSystem::toMOL_V3000(std::string path, size_t frameNum)
{
    //Opening file
    std::ofstream f(path);

    std::map<long long, size_t> fromSerialToNumber;
    std::map<size_t, long long> fromNumberToSerial;

    std::string content = this->mol3000(frameNum, fromSerialToNumber, fromNumberToSerial);
    f << content;

//    size_t atomNum=1;
//    std::map<long long, size_t> fromSerialToNumber;
//    std::map<size_t, long long> fromNumberToSerial;


//    //Associating atom serials to unique index
//    //---------------------------------------------------------------
//    for (auto it=this->atomsBegin(); it!=this->atomsEnd(); it++)
//    {
//        long long current = it->first;
//        fromNumberToSerial.insert(std::pair<size_t, long long>(atomNum,current));
//        fromSerialToNumber.insert(std::pair<long long, size_t>(current,atomNum));
//        atomNum++;
//    }
//    //---------------------------------------------------------------


//    //Checking wheter the file is open
//    if (!(f.is_open()))
//        throw std::invalid_argument("MolecularSystem::toMOL_V3000 - Cannot open file!");

//    f << "\n";
//    f << " Proxima\n";
//    f << "\n";
//    f << "  0  0  0     0  0            999 V3000\n";
//    f << "M  V30 BEGIN CTAB\n";


//    //Printing counts line
//    //---------------------------------------------------------------
//    std::string countsLine = "M  V30 COUNTS           0 0 1\n";

//    std::stringstream ss;
//    ss << this->getNumAtoms();
//    std::string atomsString = ss.str();
//    ss.str("");
//    ss.clear();

//    ss << this->getNumBonds(frameNum);
//    std::string bondsString = ss.str();
//    ss.str("");
//    ss.clear();

//    size_t blanckAtoms = 4-atomsString.length();
//    size_t blanckBonds = 4-bondsString.length();

//    countsLine.replace(14+blanckAtoms,4-blanckAtoms,atomsString);
//    countsLine.replace(19+blanckBonds,4-blanckBonds,bondsString);

//    f << countsLine;
//    //---------------------------------------------------------------


//    f << "M V30 BEGIN ATOM\n";

//    //Printing atoms
//    //---------------------------------------------------------------
//    for (auto it =this->atomsBegin(); it!=this->atomsEnd(); it++)
//    {
//        AtomSP current = it->second;
//        long long currentSerial = it->first;
//        size_t currentIndex = fromSerialToNumber.at(currentSerial);
//        f << "M  V30 " << currentIndex << " " << Element::getElementSymbol(current->getAtomicNum()) << " " << current->getPosition(frameNum)[0] << " " << current->getPosition(frameNum)[1] << " " << current->getPosition(frameNum)[2] << " 0\n";
//    }
//    //---------------------------------------------------------------


//    f << "M  V30 END ATOM\n";
//    f << "M  V30 BEGIN BOND\n";

//    //Printing bonds
//    //---------------------------------------------------------------
//    size_t numBond = 1;
//    for (auto it=this->bondsBegin(frameNum); it!=this->bondsEnd(frameNum); it++)
//    {
//        BondSP current = *it;
//        size_t minorIndex = fromSerialToNumber.at(current->getMinor());
//        size_t higherIndex = fromSerialToNumber.at(current->getHigher());
//        f << "M  V30 "<<numBond<<" "<<current->getOrder()<<" "<<minorIndex<<" "<<higherIndex<<"\n";
//        numBond++;
//    }
//    //---------------------------------------------------------------

//    f << "M V30 END BOND\n";
//    f << "M V30 END CTAB\n";
//    f << "M END\n";

}

void MolecularSystem::toMol2(std::string path, size_t frameNum)
{
    std::ofstream f(path);
    //Checking wheter the file is open
    if (!(f.is_open()))
        throw std::invalid_argument("MolecularSystem::toMol2- Cannot open file!");


    std::map<long long, size_t> fromSerialToIndex;
    std::map<size_t, long long> fromIndexToSerial;

    f<<"@<TRIPOS>MOLECULE"<<std::endl;
    f<<"*****"<<std::endl;
    f<<this->getNumAtoms()<<" "<<this->getNumBonds(frameNum)<<" 0 0 0"<<std::endl;
    f<<"SMALL"<<std::endl;
    f<<"GASTEIGER"<<std::endl;
    f<<std::endl;

    f<<"@<TRIPOS>ATOM"<<std::endl;

    size_t atmIdx=1;
    for(auto it=this->atomsBegin(); it!=this->atomsEnd(); it++)
    {
        AtomSP currentAtm = it->second;

        fromIndexToSerial.insert(std::pair<size_t,long long>(atmIdx,currentAtm->getSerial()));
        fromSerialToIndex.insert(std::pair<long long, size_t>(currentAtm->getSerial(),atmIdx));


        std::stringstream ss;
        ss << atmIdx;
        std::string serialString = ss.str();
        ss.str("");
        ss.clear();
        size_t blanckAtoms = 8-serialString.length();


        std::vector<double> pos = currentAtm->getPosition(frameNum);

        ss << pos[0];

        std::string xStr = ss.str();
        ss.str("");
        ss.clear();

        ss << pos[1];

        std::string yStr = ss.str();
        ss.str("");
        ss.clear();

        ss << pos[2];

        std::string zStr = ss.str();
        ss.str("");
        ss.clear();

        for (size_t i=0;i!=blanckAtoms;i++)
        {
            f<<" ";
        }
        f << serialString << " "<<Element::getElementSymbol(currentAtm->getAtomicNum())<<"          "<<xStr<<"    "<<yStr<<"    "<<zStr<<"    "<<Element::getElementSymbol(currentAtm->getAtomicNum())<<"       1  LIG1        0.0000"<<std::endl;

        atmIdx++;
    }


    f<<"@<TRIPOS>BOND"<<std::endl;

    size_t bndIdx = 1;
    for(auto it=this->bondsBegin(frameNum); it!=this->bondsEnd(frameNum); it++)
    {
        BondSP currentBnd = *it;

        std::stringstream ss;
        ss << bndIdx;
        std::string indexString = ss.str();
        ss.str("");
        ss.clear();
        size_t blanckAtoms = 8-indexString.length();


        for (size_t i=0; i!=blanckAtoms; i++)
        {
            f << " ";
        }

        f<<indexString;

        ss << fromSerialToIndex.at(currentBnd->getMinor());
        std::string minorString = ss.str();
        ss.str("");
        ss.clear();
        size_t blanckMinor = 8-minorString.length();


        for (size_t i=0; i!=blanckMinor; i++)
        {
            f << " ";
        }

        f<<minorString;

        ss << fromSerialToIndex.at(currentBnd->getHigher());
        std::string higherString = ss.str();
        ss.str("");
        ss.clear();
        size_t blanckHigher = 8-higherString.length();


        for (size_t i=0; i!=blanckHigher; i++)
        {
            f << " ";
        }

        f<<higherString<<"    1"<<std::endl;

        bndIdx++;
    }


    f.close();


}

void MolecularSystem::toMultimodelXYZ(std::string path, std::vector<std::string>& comments)
{
    std::ofstream f(path);

    //Checking wheter the file is open
    if (!(f.is_open()))
        throw std::invalid_argument("MolecularSystem::toXYZ - Cannot open file!");

    //Checking the comments
    if(comments.size()!=this->getNumFrames() && comments.size()!=0)
        throw std::invalid_argument("MolecularSystem::toMultimodelXYZ - Wrong number of comments");


    for (size_t frame=0; frame!=this->getNumFrames(); frame++)
    {
        f<<this->getNumAtoms()<<std::endl;

        //Vediamo se aggiungere il commento
        if (comments.size()==0)
            f<<std::endl;
        else
            f<<comments[frame]<<std::endl;


        for (auto it=this->atomsBegin(); it!=this->atomsEnd(); it++)
        {
            std::stringstream ss;
            AtomSP current = it->second;

            std::vector<double> pos = current->getPosition(frame);

            ss << pos[0];

            std::string xStr = ss.str();
            ss.str("");
            ss.clear();

            ss << pos[1];

            std::string yStr = ss.str();
            ss.str("");
            ss.clear();

            ss << pos[2];

            std::string zStr = ss.str();
            ss.str("");
            ss.clear();

            f << Element::getElementSymbol(current->getAtomicNum()) <<" "<<xStr<<" "<<yStr<<" "<<zStr<<" "<<std::endl;

        }

        f<<std::endl;

    }

    f.close();
}




void MolecularSystem::toJSON(std::string path, size_t frameNum, bool computeBridges)
{
    //Opening file
    std::ofstream f(path);

    std::map<long long, size_t> fromSerialToNumber;
    std::map<size_t, long long> fromNumberToSerial;

    //Checking wheter the file is open
    if (!(f.is_open()))
        throw std::invalid_argument("MolecularSystem::toMOL_V2000- Cannot open file!");

    //Intro
    f << "{\n";
    f << "    \"section_run\": {\n";
    f << "        \"program_info\": {\"program_name\": \"Proxima\"},\n";
    f << "        \"section_system\": {\n";
    double spinMult = this->getSpinMultiplicity();
    f << "        \"spin_multiplicity\": "<<spinMult<<",\n";

    f << "        \"atom_labels\": [";
    for (auto itAtm=this->atomsBegin(); itAtm!=this->atomsEnd(); itAtm++)
    {
        AtomSP atom = itAtm->second;
        f << "\"" << Element::getElementSymbol(atom->getAtomicNum())<<"\"";
        if (itAtm!=std::prev(this->atomsEnd()))
            f << ",";
        else
            f << "],\n";
    }




    f << "        \"atom_positions\": [";
    double totCharge=0;
    for (auto itAtm=this->atomsBegin(); itAtm!=this->atomsEnd(); itAtm++)
    {
        AtomSP atom = itAtm->second;
        f << "        [" << atom->getPosition(frameNum)[0] << ", " << atom->getPosition(frameNum)[1] << ", " << atom->getPosition(frameNum)[2] << "]";
        totCharge=totCharge+atom->getCharge(frameNum);
        if (itAtm!=std::prev(this->atomsEnd()))
            f << ",\n";
        else
            f << "],\n";
    }

    f << "        \"total_charge\": " << totCharge << "},\n";

    f << "        \"section_single_configuration_calculation\": {\n";
    f << "			\"vms_proxima\": \n";


    //MOL file
    f << "				{\n";
    std::string stringMOL=this->mol3000(frameNum,fromSerialToNumber,fromNumberToSerial,false);
    f << "				\"data_mol\": \"" << stringMOL << "\"";

    //CHARGES
    f << ",\n";
    f << "				\"vms_charges\":[";
    if(this->_charges)
    {
        for (auto itAtom=this->atomsBegin(); itAtom!=this->atomsEnd(); itAtom++)
        {
            AtomSP atom = itAtom->second;
            f << atom->getCharge(frameNum);
            if (itAtom!=std::prev(this->atomsEnd(),1))
                f << ",";
        }
    }
    f <<"]";

    //Bond Orders
    f << ",\n";
    f << "				\"vms_bonds\":[";

    for (auto itBond=this->bondsBegin(frameNum); itBond!=this->bondsEnd(frameNum); itBond++)
    {
        BondSP bond = *itBond;

        f << "				{\n";
        f << "				   \"bond_atoms\"   :  ["<<bond->getMinor()<<","<<bond->getHigher()<<"],\n";
        f << "				   \"bond_order\"   :  "<<bond->getOrder()<<"\n";

        if (itBond==std::prev(this->bondsEnd(frameNum),1))
            f << "				}";
        else
            f << "				},\n";
    }

    f <<"]";







    //Bridge Bonds

    std::set<BondSP> bridges;

    if(computeBridges)
            bridges = this->getBridgeBonds(frameNum);

    f << ",\n";
    f << "				\"vms_bridgebonds\":[\n";

    for (auto itBond=bridges.begin(); itBond!=bridges.end(); itBond++)
    {
        BondSP bond = *itBond;

        f << "				{\n";
        f << "				   \"bond_atoms\"   :  ["<<bond->getMinor()<<","<<bond->getHigher()<<"],\n";
        f << "				   \"bond_order\"   :  "<<bond->getOrder()<<"\n";

        if (itBond==std::prev(bridges.end(),1))
            f << "				}";
        else
            f << "				},\n";
    }

    f <<"]";












    //Fragments - Cycles
    f << ",\n";
    f << "				\"vms_frags\":[\n";
    if (this->getNumCycles(frameNum)!=0)
    {
        size_t idCycl = 1;
        for (auto it=this->getCyclesBegin(frameNum); it!=this->getCyclesEnd(frameNum); it++)
        {
            std::vector<BondSP> cycle = *it;
            std::set<long long> atoms;

            if (cycle.size()==0)
                continue;

            //Estraggo gli atomi dai legami
            for (auto itBond=cycle.begin(); itBond!=cycle.end(); itBond++)
            {
                BondSP bond = *itBond;
                atoms.insert(bond->getMinor());
                atoms.insert(bond->getHigher());
            }

            f << "				{\n";
            f << "				   \"fr_name\"   :   \"cycle "<<idCycl<<" - " << cycle.size()<<" bonds\"  , \n";
            idCycl++;
            for (auto itAtom = atoms.begin(); itAtom!=atoms.end(); itAtom++)
            {
                long long atom = *itAtom;
                size_t idAtom = fromSerialToNumber.at(atom);
                if (itAtom==atoms.begin())
                    f << "				   \"fr_index\"   :    ["<<idAtom;
                else
                    f << ","<<idAtom;
            }

            f << "]\n";

            if (it==this->getCyclesEnd(frameNum)-1)
                f << "				}";
            else
                f << "				},\n";
        }
    }
    f << "    ]";

    //HBonds
    f<<",\n";
    f << "\"vms_hbonds\" : [";
    if (this->getNumHBonds(frameNum)!=0)
    {
        for (auto itHB=this->hbondsBegin(frameNum); itHB!=this->hbondsEnd(frameNum);itHB++)
        {
            HBondSP hb = *itHB;
            f<<"{";
            f<<"\"hb_force\": "<<hb->getForce()<<",";
            f<<"\"hb_atoms\": ["<<hb->getDonor()<<","<<hb->getAcceptor()<<","<<hb->getHydrogen()<<"]";

            if (itHB==std::prev(this->hbondsEnd(frameNum),1))
                f<<"}";
            else
                f<<"},";
        }
    }
    f<<"]\n";


    f << "				}\n";
    f << "						    }\n";
    f << "		    }\n";
    f << "}";

    f.close();
}

void MolecularSystem::computeBondOrders(size_t frameNum)
{
    if (frameNum >= this->getNumFrames())
        throw std::invalid_argument("MolecularSystem::computeBondOrders - Invalid frame number");

    for (auto it=this->bondsBegin(frameNum); it!=this->bondsEnd(frameNum); it++)
    {
        BondSP currentBond = *it;

        std::vector<double> pos1 = this->getAtom(currentBond->getMinor())->getPosition(frameNum);
        std::vector<double> pos2 = this->getAtom(currentBond->getHigher())->getPosition(frameNum);

        double dist = Utilities::calculateDistance(pos1,pos2);

        double bo = Element::getBondOrder(dist,this->getAtom(currentBond->getMinor())->getAtomicNum(), this->getAtom(currentBond->getHigher())->getAtomicNum());
        currentBond->_order = bo;
    }
}



void MolecularSystem::printV2(BondSP bond, size_t numBonds, std::string path, size_t frameNum)
{
    MolecularSystem ms("out");

    long long atom1 = bond->getMinor();
    long long atom2 = bond->getHigher();

    this->BFS_Biaryl(ms,atom1,atom2,numBonds,frameNum);
    this->BFS_Biaryl(ms,atom2,atom1,numBonds,frameNum);

    AtomSP newAtm1 = AtomSP(new Atom(this->getAtom(atom1)->getAtomicNum(),this->getAtom(atom1)->getName(),this->getAtom(atom1)->getSerial()));
    AtomSP newAtm2 = AtomSP(new Atom(this->getAtom(atom2)->getAtomicNum(),this->getAtom(atom2)->getName(),this->getAtom(atom2)->getSerial()));
    newAtm1->setPosition(0,this->getAtom(atom1)->getPosition(frameNum));
    newAtm2->setPosition(0,this->getAtom(atom2)->getPosition(frameNum));
    ms.addAtom(newAtm1);
    ms.addAtom(newAtm2);

    ms.computeBonds_Fast();

    std::cout<< "Number of bonds computed " << ms.getNumBonds(0) << std::endl;


    std::stringstream ss;
    ss << path << ".txt";
    std::string nameTXT = ss.str();
    ss.str("");
    ss.clear();

    ss << path << ".mol2";
    std::string nameMOL2 = ss.str();
    ss.str("");
    ss.clear();

    ms.toMol2(nameMOL2,frameNum);

    std::ofstream finp;
    finp.open(nameTXT);


    finp<<"mol2File	"<<nameMOL2<<std::endl;
    finp<<"center1	"<<bond->getMinor()<<std::endl;
    finp<<"center2	"<<bond->getHigher()<<std::endl;
    finp.close();

}



double MolecularSystem::getJ_ab(AtomSP atom1, size_t idx1, AtomSP atom2, size_t idx2, size_t frameNum, Eigen::VectorXf oldCharges)
{
    double dist = Utilities::calculateDistance(atom1->getPosition(frameNum), atom2->getPosition(frameNum));
    dist = dist*dist*0.52918;


    double hard1;
    double hard2;

    if (atom1->getAtomicNum()==1)
    {
        hard1 = this->getJ_hh(atom1,oldCharges[idx1]);
    }
    else
    {
        hard1 = this->hardness[atom1->getAtomicNum()];
    }

    if (atom2->getAtomicNum()==1)
    {
        hard2 = this->getJ_hh(atom2,oldCharges[idx2]);
    }
    else
    {
        hard2 = this->hardness[atom2->getAtomicNum()];
    }

    double hard = 0.5*(0.5*hard1 + 0.5*hard2);
    return 2.0*(hard/sqrt(1+hard*hard*dist));
}

double MolecularSystem::getJ_hh(AtomSP atom, double charge)
{
    if (atom->getAtomicNum()!=1) throw std::invalid_argument("MolecularSystem::getJ_hh - Non hydrogen atom given");
    double chi = 1.0698;
    return (1+charge/chi)*this->hardness[1];
}

void MolecularSystem::iteration(Eigen::VectorXf& charges, Eigen::VectorXf C, std::map<int,AtomSP> fromIndexToAtom, size_t frameNum)
{
    int matr_size = this->getNumAtoms()+1;
    Eigen::MatrixXf J(matr_size,matr_size);

    Eigen::VectorXf oldCharges = charges;

    //Filling matrix of hardness
    int i=0;
    while(i<this->getNumAtoms()+1)
    {
        int j = 0;
        while (j<this->getNumAtoms()+1)
        {

            if (j==this->getNumAtoms() || i==this->getNumAtoms())
            {
                if (i==j)
                {
                    J(i,j)=0;
                    j++;
                    continue;
                }

                else
                {
                    J(i,j)=1;
                    j++;
                    continue;
                }
            }

            J(i,j) = getJ_ab(fromIndexToAtom.at(i), i, fromIndexToAtom.at(j), j, frameNum, oldCharges);
            j++;
        }
        i++;
     }

     //Risolvo il sistema lineare
     charges = J.colPivHouseholderQr().solve(C);
}

double MolecularSystem::getJ_ab_old(AtomSP atom1, AtomSP atom2, size_t frameNum)
{
    double dist = Utilities::calculateDistance(atom1->getPosition(frameNum), atom2->getPosition(frameNum));
    dist = dist*dist*0.52918;
    double hard1 = Element::getHardness(atom1->getAtomicNum());
    double hard2 = Element::getHardness(atom2->getAtomicNum());

    double hard = 0.5*(0.5*hard1 + 0.5*hard2);
    return 2.0*(hard/sqrt(1+hard*hard*dist));
}

double MolecularSystem::getJ_ab_xuff(AtomSP atom1, AtomSP atom2, size_t frameNum)
{
    double dist = Utilities::calculateDistance(atom1->getPosition(frameNum), atom2->getPosition(frameNum));
    dist = dist*dist*0.52918;
    double hard1 = Element::getHardness_XUFF(this->getAtomType_XUFF(atom1->getSerial(),frameNum))*23;
    double hard2 = Element::getHardness_XUFF(this->getAtomType_XUFF(atom2->getSerial(), frameNum))*23;

    double hard = 0.5*(0.5*hard1 + 0.5*hard2);
    return 2.0*(hard/sqrt(1+hard*hard*dist));
}

void MolecularSystem::computeFQCharges(size_t frameNum)
{
    int matr_size = this->getNumAtoms()+1;
    Eigen::MatrixXf J(matr_size,matr_size);
    Eigen::VectorXf C(matr_size);

    //Filling the vector of electronegativity
    int i = 0;
    auto it=this->atomsBegin();
    std::map<int,AtomSP> fromIndexToAtom;
    std::map<AtomSP,int> fromAtomToIndex;

    while (i<this->getNumAtoms())
    {
        auto currentIt = std::next(it,i);
        AtomSP current = currentIt->second;
        fromIndexToAtom.insert(std::pair<int,AtomSP>(i,current));

        double elec = Element::getElectronegativity(current->getAtomicNum()) * 23;

        if (fuzzyEqual(elec, 0))
            throw std::invalid_argument("MolecularSystem::computeFQCharges - Invalid atom type");

        C[i] = (-1.0)*elec;
        i++;
    }

    //Condition on the total charge
    C[this->getNumAtoms()] = this->_totalCharge;

    //Filling matrix of hardness
    i=0;
    while(i<this->getNumAtoms()+1)
    {
        int j = 0;
        while (j<this->getNumAtoms()+1)
        {

            if (j==this->getNumAtoms() || i==this->getNumAtoms())
            {
                if (i==j)
                {
                    J(i,j)=0;
                    j++;
                    continue;
                }

                else
                {
                    J(i,j)=1;
                    j++;
                    continue;
                }
            }

            J(i,j) = getJ_ab_old(fromIndexToAtom.at(i), fromIndexToAtom.at(j),frameNum);
            j++;
        }
        i++;
     }
     //std::cout<<J<<std::endl;
     //std::cout<<C<<std::endl;

     //Risolvo il sistema lineare
     Eigen::VectorXf result = J.colPivHouseholderQr().solve(C);
     //std::cout<<"RESULT: "<<result<<std::endl;


     //Assegno le cariche agli atomi
     i=0;
     while(i<this->getNumAtoms())
     {
         AtomSP currentAtom = fromIndexToAtom.at(i);
         currentAtom->setCharge(frameNum,result[i]);
         i++;
     }

     this->_charges=true;

}

//http://openbabel.org/wiki/Bond_Orders
//http://www.daylight.com/meetings/mug01/Sayle/m4xbondage.html
void MolecularSystem::computeHybridisation(size_t frameNum, bool useCycles, double tolerance)
{
    if(frameNum >= _framesList.size())
        throw std::invalid_argument("Atom::computeHybridisation - frame number out of range!");

    if (useCycles==true)
    {
        //1) Computes cycles
        this->computeHortonCycles(frameNum);

        //2) For each cycle...
        //AUTOMATIC PERCEPTION OF ORGANIC MOLECULES
        for (cycle_iterator it=this->getCyclesBegin(frameNum); it!=this->getCyclesEnd(frameNum); it++)
        {
            std::vector<BondSP> cycle = *it;

            if (cycle.size()<=5)
            {
                std::pair<double,std::set<long long>> torsion = this->getMeanTorsion(cycle,frameNum);

                //If the average torsion is <= 7.5° is planar -> SP2 TO ALL
                if (torsion.first<=7.5)
                {
                    for (auto itBond=cycle.begin(); itBond!=cycle.end(); itBond++)
                    {
                        BondSP bond = *itBond;
                        bond->setPlanarity();
                    }

                    for (auto itAtom=torsion.second.begin(); itAtom!=torsion.second.end(); itAtom++)
                    {
                        AtomSP atom = this->getAtom(*itAtom);
                        atom->setPlanarity(true,frameNum);

                        if (atom->getAtomicNum()<21 && atom->getNumBonds(frameNum)<4)
                        {
                            atom->addHybridisation(frameNum,Hybridisation(1,2,0));

                        }
                    }

                }
            }

            else if (cycle.size()>=6)
            {
                std::pair<double,std::set<long long>> torsion = this->getMeanTorsion(cycle,frameNum);
                //If the average torsion is <= 15° is planar -> SP2 TO ALL
                if (torsion.first<=15)
                {

                    for (auto itBond=cycle.begin(); itBond!=cycle.end(); itBond++)
                    {
                        BondSP bond = *itBond;
                        bond->setPlanarity();
                    }

                    for (auto itAtom=torsion.second.begin(); itAtom!=torsion.second.end(); itAtom++)
                    {
                        AtomSP atom = this->getAtom(*itAtom);
                        if (atom->getAtomicNum()<21 && atom->getNumBonds(frameNum)<4)
                        {
                            atom->addHybridisation(frameNum,Hybridisation(1,2,0));
                        }
                    }

                }

            }

        }
    }

    //3) For each atom...
    for (atom_iterator it=this->atomsBegin(); it!=this->atomsEnd(); it++)
    {

        AtomSP atom = it->second;

        //If it is a hydrogen atom...
        if (atom->getAtomicNum()==1)
        {
            atom->addHybridisation(frameNum,Hybridisation(1,0,0));
            continue;
        }

        //If it is a oxygen atom...
        if (atom->getAtomicNum()==8)
        {
            if (atom->getNumBonds(frameNum)==2)
            {
                atom->addHybridisation(frameNum,Hybridisation(1,3,0));
                continue;
            }
        }

        //If it is a terminal atom, no sufficient data to compute hybridisation by itself
        if (atom->getNumBonds(frameNum)<2)
        {
            atom->addHybridisation(frameNum,Hybridisation(0,0,0));
            continue;
        }


        Hybridisation h(0,0,0);

        if ((atom->getHybridisation(frameNum).isEqual(h)))
        {
            this->computeAtomicHybridisation(frameNum,atom,tolerance);
        }
    }


    //4) For each terminal atom...
    Hybridisation sp(1,1,0);

    std::set<AtomSP> terminalNitrogens;

    for (atom_iterator it=this->atomsBegin(); it!=this->atomsEnd(); it++)
    {
        AtomSP atom = it->second;

        if (atom->getNumBonds(frameNum)<2 && atom->getNumBonds(frameNum)>0)
        {

            if (atom->getAtomicNum() == 7)
            {
                terminalNitrogens.insert(atom);
                continue;
            }

            //If it is a oxygen atom
            if (atom->getAtomicNum()==8)
            {
                BondSP bond  = *(atom->getBondBegin(frameNum));
                long long other = (bond->getMinor()==atom->getSerial()) ? bond->getHigher() : bond->getMinor();

                //If it is bonded to a SP2 atom
                Hybridisation sp2(1,2,0);
                if (this->getAtom(other)->getHybridisation(frameNum).isEqual(sp2))
                {
                    atom->addHybridisation(frameNum,sp2);
                }

                //If it is bonded to a SP3 atom
                else atom->addHybridisation(frameNum,Hybridisation(0,0,0));

            } //If it is a oxygen atom


            //If it is a carbon atom
            else if (atom->getAtomicNum()==6)
            {
                //Se il carbonio a cui è legato è un SP2 "isolato" mettilo come SP2,
                //altrimenti mettilo come SP3
                BondSP bond  = *(atom->getBondBegin(frameNum));
                long long other = (bond->getMinor()==atom->getSerial()) ? bond->getHigher() : bond->getMinor();

                //If the other atom is a SP2 atom
                Hybridisation sp2(1,2,0);
                if (this->getAtom(other)->getHybridisation(frameNum).isEqual(sp2))
                {
                    bool hasMoreSp2=false;
                    AtomSP otherAtom = this->getAtom(other);
                    for (auto it2=otherAtom->getBondBegin(frameNum); it2!=otherAtom->getBondEnd(frameNum); it2++)
                    {
                        BondSP bond2 = *it2;
                        if (bond2->isEqual(*bond)) continue;
                        long long other2 = (bond2->getMinor()==otherAtom->getSerial()) ? bond2->getHigher() : bond2->getMinor();
                        if (this->getAtom(other2)->getHybridisation(frameNum).isEqual(sp2))
                        {
                            hasMoreSp2=true;
                            break;
                        }
                    }

                    if (hasMoreSp2 == false) atom->addHybridisation(frameNum,Hybridisation(1,2,0));
                    else atom->addHybridisation(frameNum,Hybridisation(1,3,0));
                } //If the other atom is a SP2 atom

                //If the other atom is a SP atom
                else if (this->getAtom(other)->getHybridisation(frameNum).isEqual(sp))
                {
                    bool hasMoreSp=false;
                    AtomSP otherAtom = this->getAtom(other);
                    for (auto it2=otherAtom->getBondBegin(frameNum); it2!=otherAtom->getBondEnd(frameNum); it2++)
                    {
                        BondSP bond2 = *it2;
                        if (bond2->isEqual(*bond)) continue;
                        long long other2 = (bond2->getMinor()==otherAtom->getSerial()) ? bond2->getHigher() : bond2->getMinor();
                        if (this->getAtom(other2)->getHybridisation(frameNum).isEqual(sp))
                        {
                            hasMoreSp=true;
                            break;
                        }
                    }

                    if (hasMoreSp==false) atom->addHybridisation(frameNum,Hybridisation(1,1,0));
                    else atom->addHybridisation(frameNum,Hybridisation(1,3,0));
                } //If the other atom is a SP atom

                else atom->addHybridisation(frameNum,Hybridisation(1,3,0));

            } //If it is a carbon atom

        }

    } //For each terminal atom


    for (auto it = terminalNitrogens.begin(); it != terminalNitrogens.end(); it++)
    {
        AtomSP atom = *it;
        //Se il carbonio a cui è legato è un SP2 "isolato" mettilo come SP2,
        //altrimenti mettilo come SP3
        BondSP bond  = *(atom->getBondBegin(frameNum));
        long long other = (bond->getMinor()==atom->getSerial()) ? bond->getHigher() : bond->getMinor();

        //If the other atom is a SP2 atom
        Hybridisation sp2(1,2,0);
        if (this->getAtom(other)->getHybridisation(frameNum).isEqual(sp2))
        {
            bool hasMoreSp2=false;
            AtomSP otherAtom = this->getAtom(other);
            for (auto it2=otherAtom->getBondBegin(frameNum); it2!=otherAtom->getBondEnd(frameNum); it2++)
            {
                BondSP bond2 = *it2;
                if (bond2->isEqual(*bond)) continue;
                long long other2 = (bond2->getMinor()==otherAtom->getSerial()) ? bond2->getHigher() : bond2->getMinor();
                if (this->getAtom(other2)->getHybridisation(frameNum).isEqual(sp2))
                {
                    hasMoreSp2=true;
                    break;
                }
            }

            if (hasMoreSp2 == false) atom->addHybridisation(frameNum,Hybridisation(1,2,0));
            else atom->addHybridisation(frameNum,Hybridisation(1,3,0));
        } //If the other atom is a SP2 atom

        //If the other atom is a SP atom
        else if (this->getAtom(other)->getHybridisation(frameNum).isEqual(sp))
        {
            bool hasMoreSp=false;
            AtomSP otherAtom = this->getAtom(other);
            for (auto it2=otherAtom->getBondBegin(frameNum); it2!=otherAtom->getBondEnd(frameNum); it2++)
            {
                BondSP bond2 = *it2;
                if (bond2->isEqual(*bond)) continue;
                long long other2 = (bond2->getMinor()==otherAtom->getSerial()) ? bond2->getHigher() : bond2->getMinor();
                if (this->getAtom(other2)->getHybridisation(frameNum).isEqual(sp))
                {
                    hasMoreSp=true;
                    break;
                }
            }

            if (hasMoreSp==false) atom->addHybridisation(frameNum,Hybridisation(1,1,0));
            else atom->addHybridisation(frameNum,Hybridisation(1,3,0));
        } //If the other atom is a SP atom

        else atom->addHybridisation(frameNum,Hybridisation(1,3,0));

    } //If it is a nitrogen atom

}



void MolecularSystem::computeFQCharges_XUFF(size_t frameNum)
{

    this->computeHortonCycles(frameNum);
    this->computeHybridisation(frameNum,true);


    int matr_size = this->getNumAtoms()+1;
    Eigen::MatrixXf J(matr_size,matr_size);
    Eigen::VectorXf C(matr_size);

    //Filling the vector of electronegativity
    int i = 0;
    auto it=this->atomsBegin();
    std::map<int,AtomSP> fromIndexToAtom;
    std::map<AtomSP,int> fromAtomToIndex;

    while (i<this->getNumAtoms())
    {
        auto currentIt = std::next(it,i);
        AtomSP current = currentIt->second;
        fromIndexToAtom.insert(std::pair<int,AtomSP>(i,current));

        //double elec = Element::getElectronegativity(current->getAtomicNum()) * 23;
        double elec = Element::getElectronegativity_XUFF(this->getAtomType_XUFF(current->getSerial(),frameNum))*23;


        if (fuzzyEqual(elec, 0))
            throw std::invalid_argument("MolecularSystem::computeFQCharges - Invalid atom type");

        C[i] = (-1.0)*elec;
        i++;
    }

    //Condition on the total charge
    C[this->getNumAtoms()] = this->_totalCharge;

    //Filling matrix of hardness
    i=0;
    while(i<this->getNumAtoms()+1)
    {
        int j = 0;
        while (j<this->getNumAtoms()+1)
        {

            if (j==this->getNumAtoms() || i==this->getNumAtoms())
            {
                if (i==j)
                {
                    J(i,j)=0;
                    j++;
                    continue;
                }

                else
                {
                    J(i,j)=1;
                    j++;
                    continue;
                }
            }

            J(i,j) = getJ_ab_xuff(fromIndexToAtom.at(i), fromIndexToAtom.at(j),frameNum);
            j++;
        }
        i++;
     }
     //std::cout<<J<<std::endl;
     //std::cout<<C<<std::endl;

     //Risolvo il sistema lineare
     Eigen::VectorXf result = J.colPivHouseholderQr().solve(C);
     //std::cout<<"RESULT: "<<result<<std::endl;


     //Assegno le cariche agli atomi
     i=0;
     while(i<this->getNumAtoms())
     {
         AtomSP currentAtom = fromIndexToAtom.at(i);
         currentAtom->setCharge(frameNum,result[i]);
         i++;
     }

     this->_charges=true;
}

bool MolecularSystem::computeFQCharges_it(size_t frameNum, size_t numIteration)
{
    int matr_size = this->getNumAtoms()+1;

    Eigen::VectorXf C(matr_size);
    Eigen::VectorXf charges(matr_size-1);

    //Filling the vector of electronegativity
    int i = 0;
    auto it=this->atomsBegin();
    std::map<int,AtomSP> fromIndexToAtom;
    std::map<AtomSP,int> fromAtomToIndex;

    while (i<this->getNumAtoms())
    {
        auto currentIt = std::next(it,i);
        AtomSP current = currentIt->second;
        fromIndexToAtom.insert(std::pair<int,AtomSP>(i,current));

        if (this->electronegativity.find(current->getAtomicNum()) == this->electronegativity.end()) return false;

        C[i] = this->electronegativity.at(current->getAtomicNum());
        charges[i]=0;
        i++;
    }

    C[this->getNumAtoms()] = 0;


    size_t currentIt = 0;
    while (currentIt<numIteration)
    {
        this->iteration(charges,C,fromIndexToAtom,frameNum);
        currentIt++;
    }

    //Assegno le cariche agli atomi
    i=0;
    while(i<this->getNumAtoms())
    {
        AtomSP currentAtom = fromIndexToAtom.at(i);
        currentAtom->setCharge(frameNum,charges[i]);
        i++;
    }

    this->_charges=true;

    return true;
}

std::vector<double> MolecularSystem::computeDipoleMoment(size_t frameNum)
{
    if(frameNum >= _framesList.size() )
        throw std::invalid_argument("MolecularSystem::computeDipoleMoment - frame number out of range!");

    std::vector<double> dipole{0.,0.,0.};

    for (auto it=this->atomsBegin(); it!=this->atomsEnd(); it++)
    {
        AtomSP atom = it->second;

        std::vector<double> posAtom = atom->getPosition(frameNum);
        std::vector<double> weighted = Utilities::product(posAtom,atom->getCharge(frameNum));

        dipole = Utilities::sum(dipole, weighted);
    }

    return Utilities::product(dipole,4.80268770983);
}

double MolecularSystem::computeAngle(long long centerAtom, long long atom2, long long atom3, size_t frameNum)
{
    std::vector<double> centerPos = this->getAtom(centerAtom)->getPosition(frameNum);
    std::vector<double> pos2 = this->getAtom(atom2)->getPosition(frameNum);
    std::vector<double> pos3 = this->getAtom(atom3)->getPosition(frameNum);

    std::vector<double> vec1 = Utilities::minus(pos2,centerPos);
    std::vector<double> vec2 = Utilities::minus(pos3,centerPos);

    return Utilities::calculateAngle(vec1,vec2);
}

std::string MolecularSystem::getAtomType_XUFF(long long atm, size_t frameNum)
{
    AtomSP currentAtm = this->getAtom(atm);

    //H
    if (currentAtm->getAtomicNum()==1)
    {

        if (currentAtm->getNumBonds(0)!=0)
        {
            BondSP bnd = *currentAtm->getBondBegin(frameNum);
            long long otherAtm = (bnd->getMinor() == atm) ? bnd->getHigher() : bnd->getMinor();
            if (this->getAtom(otherAtm)->getAtomicNum()==6)
                return "HC";
            else if (this->getAtom(otherAtm)->getAtomicNum()==7)
                return "HN";
            else if (this->getAtom(otherAtm)->getAtomicNum()==8)
                return "HO";
            else
                return "H";
        }
        else
            return "H";
    }


    //C
    if (currentAtm->getAtomicNum()==6)
    {
        if (currentAtm->isAtomAromatic(frameNum)==true)
            return "CR";

        else if (currentAtm->getHybridisation(frameNum).isEqual(Hybridisation(1,1,0)))
            return "C1";

        else if (currentAtm->getHybridisation(frameNum).isEqual(Hybridisation(1,2,0)))
            return "C2";

        else
            return "C3";
    }




    //N
    if (currentAtm->getAtomicNum()==7)
    {



        if (currentAtm->getHybridisation(frameNum).isEqual(Hybridisation(1,3,0)) && currentAtm->getNumBonds(frameNum)==4)
            return "N3+";


        else if (currentAtm->isAtomAromatic(frameNum))
            return "NR";

        else if (currentAtm->getHybridisation(frameNum).isEqual(Hybridisation(1,1,0)))
            return "N1";

        else if (currentAtm->getHybridisation(frameNum).isEqual(Hybridisation(1,2,0)))
            return "N2";

        else
            return "N3";
    }


    //O
    if (currentAtm->getAtomicNum()==8)
    {
        if (currentAtm->isAtomAromatic(frameNum))
            return "OR";

        else if (currentAtm->getHybridisation(frameNum).isEqual(Hybridisation(1,2,0)))
            return "O2";

        else
            return "O3";


    }


    return "UNK";

}



void MolecularSystem::BFS_Biaryl(MolecularSystem &ms, long long origin, long long avoid, size_t numBonds, size_t frameNum)
{
    std::map<long long,bool> visited;

    std::map<long long, size_t> dist;


    for (auto it=this->atomsBegin(); it!=this->atomsEnd(); it++)
    {
        visited.insert(std::pair<long long,bool>(it->first,false));
        dist.insert(std::pair<long long,size_t>(it->first,0));
    }

    std::queue<long long> toVisit;

    toVisit.push(origin);
    visited.at(origin)=true;
    visited.at(avoid)=true;


    while (toVisit.size()!=0)
    {
        long long current = toVisit.front();
        toVisit.pop();

        if (dist.at(current)<=numBonds)
        {
            for (auto itb=this->getAtom(current)->getBondBegin(frameNum); itb!=this->getAtom(current)->getBondEnd(frameNum); itb++)
            {
                BondSP currentBnd = *itb;

                long long otherAtm = (currentBnd->getMinor() == current) ? currentBnd->getHigher() : currentBnd->getMinor();

                if (visited.at(otherAtm)==false)
                {
                    //Se sei in fondo, taglia e mettici un idrogeno
                    if (dist.at(current)+1 == numBonds)
                    {
                        AtomSP newAtm = AtomSP(new Atom(1,"H",otherAtm));

                        //Imposto la posizione dell'atomo di idrogeno
                        std::vector<double> posOld = this->getAtom(otherAtm)->getPosition(frameNum);
                        std::vector<double> center = this->getAtom(current)->getPosition(frameNum);
                        double l = Utilities::calculateDistance(posOld,center);

                        double realD = Element::getCovalentRadius(1)+Element::getCovalentRadius(this->getAtom(otherAtm)->getAtomicNum());
                        double c = realD/l;

                        std::vector<double> newPos = Utilities::minus(posOld,center);
                        newPos[0] = newPos[0]*c;
                        newPos[1] = newPos[1]*c;
                        newPos[2] = newPos[2]*c;

                        newPos = Utilities::sum(newPos,center);

                        newAtm->setPosition(frameNum,newPos);
                        ms.addAtom(newAtm);
                        visited.at(otherAtm)=true;
                    }

                    else
                    {

                        AtomSP newAtm = AtomSP(new Atom(this->getAtom(otherAtm)->getAtomicNum(),this->getAtom(otherAtm)->getName(),this->getAtom(otherAtm)->getSerial()));
                        newAtm->setPosition(0,this->getAtom(otherAtm)->getPosition(frameNum));
                        ms.addAtom(newAtm);


                        //ms.addAtom(this->getAtom(otherAtm));
                        toVisit.push(otherAtm);
                        visited.at(otherAtm)=true;
                        dist.at(otherAtm)=dist.at(current)+1;
                    }
                }
            }

        }

    }
}

bool MolecularSystem::isThereOneCommonBond(std::vector<BondSP> &bnd1, std::vector<BondSP> &bnd2)
{
    bool res = false;
    for (auto it=bnd1.begin(); it!=bnd1.end(); it++)
    {
        BondSP bnd = *it;
        std::vector<BondSP>::iterator pos = std::find(bnd2.begin(), bnd2.end(), bnd);
        if (pos!=bnd2.end())
        {
            //L'ha trovato
            if (res==false)
                res=true;
        }
    }

    return res;
}

std::vector<BondSP> MolecularSystem::findBondBetweenCycles(std::vector<BondSP> &bnd1, std::vector<BondSP> &bnd2, size_t frameNum)
{

    std::vector<BondSP> out;

    std::set<AtomSP> atm1;
    std::set<AtomSP> atm2;

    for (auto it=bnd1.begin(); it!=bnd1.end(); it++)
    {
        BondSP bnd = *it;
        atm1.insert(this->getAtom(bnd->getMinor()));
        atm1.insert(this->getAtom(bnd->getHigher()));
    }

    for (auto it=bnd2.begin(); it!=bnd2.end(); it++)
    {
        BondSP bnd = *it;
        atm2.insert(this->getAtom(bnd->getMinor()));
        atm2.insert(this->getAtom(bnd->getHigher()));
    }


    //Per ogni atomo del primo ciclo
    for (auto it=atm1.begin(); it!=atm1.end(); it++)
    {
        AtomSP atm = *it;
        //Per ogni suo legame
        for (auto itb=atm->getBondBegin(frameNum); itb!=atm->getBondEnd(frameNum); itb++)
        {
            BondSP bnd = *itb;
            long long other = (bnd->getMinor() == atm->getSerial()) ? bnd->getHigher() : bnd->getMinor();

            //Se l'altro atomo è nell'altro ciclo
            auto isInOther = atm2.find(this->getAtom(other));
            if (isInOther!=atm2.end())
                out.push_back(bnd);
        }
    }


    return out;
}

std::pair<double, std::set<long long> > MolecularSystem::getMeanTorsion(std::vector<BondSP> &cycle, size_t frameNum) const
{
    double torsion=0;
    size_t n=0;

    std::set<long long> atoms;

    for (auto it1=cycle.begin(); it1!=cycle.end(); it1++)
    {
        BondSP bond1 = *it1;
        long long minor = bond1->getMinor();


        long long higher = bond1->getHigher();
        atoms.insert(minor);
        atoms.insert(higher);

        AtomSP highAtom = this->getAtom(higher);

        std::vector<double> highAtomPos = highAtom->getPosition(frameNum);
        std::vector<double> minorAtomPos = this->getAtom(minor)->getPosition(frameNum);
        std::vector<double> b1 = Utilities::minus(highAtomPos,minorAtomPos);

        for (auto it2=cycle.begin(); it2!=cycle.end(); it2++)
        {
            BondSP bond2 = *it2;

            //If the bond is the same of the previous one... skip it
            if (bond2->isEqual(*bond1)) continue;

            //If the new bond does not contains the higher atom of the previous bond... skip it
            if (bond2->getHigher()!=higher && bond2->getMinor()!=higher) continue;

            long long otherAtom = (bond2->getMinor() == higher) ? bond2->getHigher() : bond2->getMinor();
            if (otherAtom==minor) continue;
            atoms.insert(otherAtom);
            AtomSP other1 = this->getAtom(otherAtom);

            std::vector<double> otherAtomPos =other1->getPosition(frameNum);
            std::vector<double> b2 = Utilities::minus(otherAtomPos,highAtomPos);

            for (auto it3=cycle.begin(); it3!=cycle.end(); it3++)
            {
                BondSP bond3 = *it3;

                //If the bond is the same of the previous one... skip it
                if (bond3->isEqual(*bond2)) continue;

                //If the new bond does not contains the higher atom of the previous bond... skip it
                if (bond3->getHigher()!=otherAtom && bond3->getMinor()!=otherAtom) continue;

                long long otherAtom2 = (bond3->getMinor() == otherAtom) ? bond3->getHigher() : bond3->getMinor();
                if (otherAtom2==higher) continue;
                atoms.insert(otherAtom2);

                AtomSP other2 = this->getAtom(otherAtom2);

                std::vector<double> other2AtomPos = other2->getPosition(frameNum);
                std::vector<double> b3 = Utilities::minus(other2AtomPos,otherAtomPos);

                torsion = torsion + std::abs(Utilities::calculateDihedral(b1,b2,b3));
                n++;
            }
        }
    }

    torsion = torsion/n;

    return std::pair<double, std::set<long long>>(torsion,atoms);
}

std::map<long long, double> MolecularSystem::computeElectronegativities(size_t frameNum)
{
    std::map<long long,double> electronegativities;

    for (auto it=this->_atomsMap.begin(); it!=this->_atomsMap.end(); it++)
    {
        AtomSP atom = it->second;
        Hybridisation hybr = atom->getHybridisation(frameNum);

        std::string _hybr;
        if (hybr.sCharacter==1 && hybr.pCharacter==3 && hybr.dCharacter==0) _hybr="sp3";
        else if (hybr.sCharacter==1 && hybr.pCharacter==2 && hybr.dCharacter==0) _hybr="sp2";
        else if (hybr.sCharacter==1 && hybr.pCharacter==1 && hybr.dCharacter==0) _hybr="sp";
        else _hybr="";

        double electronegativity = GasteigerParameters::getElectronegativity(atom->getAtomicNum(),atom->getCharge(frameNum),_hybr);
        std::pair<long long, double> current(atom->getSerial(),electronegativity);

        electronegativities.insert(current);
    }

    return electronegativities;
}

std::map<long long, double> MolecularSystem::computeMAXElectronegativities(size_t frameNum)
{
    std::map<long long,double> electronegativities;

    for (auto it=this->_atomsMap.begin(); it!=this->_atomsMap.end(); it++)
    {
        AtomSP atom = it->second;
        Hybridisation hybr = atom->getHybridisation(frameNum);

        std::string _hybr;
        if (hybr.sCharacter==1 && hybr.pCharacter==3 && hybr.dCharacter==0) _hybr="sp3";
        else if (hybr.sCharacter==1 && hybr.pCharacter==2 && hybr.dCharacter==0) _hybr="sp2";
        else if (hybr.sCharacter==1 && hybr.pCharacter==1 && hybr.dCharacter==0) _hybr="sp";
        else _hybr="";

        double electronegativity = GasteigerParameters::getElectronegativity_plusState(atom->getAtomicNum(),_hybr);

        electronegativities.insert(std::pair<long long,double>(atom->getSerial(),electronegativity));
    }

    return electronegativities;

}

double MolecularSystem::computePotential(std::vector<double> point, size_t frameNum)
{
    double out = 0;

    for (auto it = this->atomsBegin(); it!=this->atomsEnd(); it++)
    {
        AtomSP atom = it->second;
        double charge = atom->getCharge(frameNum);
        double dist = Utilities::calculateDistance(point, atom->getPosition(frameNum));

        out = out + charge/dist;
    }

    return out;

}

std::string MolecularSystem::mol3000(size_t frameNum, std::map<long long, size_t>& fromSerialToNumber, std::map<size_t, long long>& fromNumberToSerial, bool useNewLine)
{
    std::stringstream f;

    size_t atomNum=1;


    //Associating atom serials to unique index
    //---------------------------------------------------------------
    for (auto it=this->atomsBegin(); it!=this->atomsEnd(); it++)
    {
        long long current = it->first;
        fromNumberToSerial.insert(std::pair<size_t, long long>(atomNum,current));
        fromSerialToNumber.insert(std::pair<long long, size_t>(current,atomNum));
        atomNum++;
    }
    //---------------------------------------------------------------


    if (useNewLine)
    {
        f << "\n";
        f << " Proxima\n";
        f << "\n";
        f << "  0  0  0     0  0            999 V3000\n";
        f << "M  V30 BEGIN CTAB\n";

        //Printing counts line
        //---------------------------------------------------------------
        std::string countsLine = "M  V30 COUNTS           0 0 1\n";

        std::stringstream ss;
        ss << this->getNumAtoms();
        std::string atomsString = ss.str();
        ss.str("");
        ss.clear();

        ss << this->getNumBonds(frameNum);
        std::string bondsString = ss.str();
        ss.str("");
        ss.clear();

        size_t blanckAtoms = 4-atomsString.length();
        size_t blanckBonds = 4-bondsString.length();

        countsLine.replace(14+blanckAtoms,4-blanckAtoms,atomsString);
        countsLine.replace(19+blanckBonds,4-blanckBonds,bondsString);

        f << countsLine;
        //---------------------------------------------------------------

        f << "M V30 BEGIN ATOM\n";

        //Printing atoms
        //---------------------------------------------------------------
        for (auto it =this->atomsBegin(); it!=this->atomsEnd(); it++)
        {
            AtomSP current = it->second;
            long long currentSerial = it->first;
            size_t currentIndex = fromSerialToNumber.at(currentSerial);
            f << "M  V30 " << currentIndex << " " << Element::getElementSymbol(current->getAtomicNum()) << " " << current->getPosition(frameNum)[0] << " " << current->getPosition(frameNum)[1] << " " << current->getPosition(frameNum)[2] << " 0\n";
        }
        //---------------------------------------------------------------


        f << "M  V30 END ATOM\n";
        f << "M  V30 BEGIN BOND\n";

        //Printing bonds
        //---------------------------------------------------------------
        size_t numBond = 1;
        for (auto it=this->bondsBegin(frameNum); it!=this->bondsEnd(frameNum); it++)
        {
            BondSP current = *it;
            size_t minorIndex = fromSerialToNumber.at(current->getMinor());
            size_t higherIndex = fromSerialToNumber.at(current->getHigher());
            f << "M  V30 "<<numBond<<" "<<current->getOrder()<<" "<<minorIndex<<" "<<higherIndex<<"\n";
            numBond++;
        }
        //---------------------------------------------------------------

        f << "M  V30 END BOND\n";
        f << "M  V30 END CTAB\n";
        f << "M  END\n";


    }
    else
    {
        f << "\\n";
        f << " Proxima\\n";
        f << "\\n";
        f << "  0  0  0     0  0            999 V3000\\n";
        f << "M  V30 BEGIN CTAB\\n";

        //Printing counts line
        //---------------------------------------------------------------
        std::string countsLine = "M  V30 COUNTS           0 0 1\\n";

        std::stringstream ss;
        ss << this->getNumAtoms();
        std::string atomsString = ss.str();
        ss.str("");
        ss.clear();

        ss << this->getNumBonds(frameNum);
        std::string bondsString = ss.str();
        ss.str("");
        ss.clear();

        size_t blanckAtoms = 4-atomsString.length();
        size_t blanckBonds = 4-bondsString.length();

        countsLine.replace(14+blanckAtoms,4-blanckAtoms,atomsString);
        countsLine.replace(19+blanckBonds,4-blanckBonds,bondsString);

        f << countsLine;
        //---------------------------------------------------------------

        f << "M V30 BEGIN ATOM\\n";

        //Printing atoms
        //---------------------------------------------------------------
        for (auto it =this->atomsBegin(); it!=this->atomsEnd(); it++)
        {
            AtomSP current = it->second;
            long long currentSerial = it->first;
            size_t currentIndex = fromSerialToNumber.at(currentSerial);
            f << "M  V30 " << currentIndex << " " << Element::getElementSymbol(current->getAtomicNum()) << " " << current->getPosition(frameNum)[0] << " " << current->getPosition(frameNum)[1] << " " << current->getPosition(frameNum)[2] << " 0\\n";
        }
        //---------------------------------------------------------------


        f << "M  V30 END ATOM\\n";
        f << "M  V30 BEGIN BOND\\n";

        //Printing bonds
        //---------------------------------------------------------------
        size_t numBond = 1;
        for (auto it=this->bondsBegin(frameNum); it!=this->bondsEnd(frameNum); it++)
        {
            BondSP current = *it;
            size_t minorIndex = fromSerialToNumber.at(current->getMinor());
            size_t higherIndex = fromSerialToNumber.at(current->getHigher());
            f << "M  V30 "<<numBond<<" "<<current->getOrder()<<" "<<minorIndex<<" "<<higherIndex<<"\\n";
            numBond++;
        }
        //---------------------------------------------------------------

        f << "M V30 END BOND\\n";
        f << "M V30 END CTAB\\n";
        f << "M END\\n";

    }

    return f.str();

}


void MolecularSystem::checkDistanceAndCreateBond(size_t frameNum,
                                                AtomSP& atom1,
                                                AtomSP& atom2,
                                                double tolerance_distance)
{

    if (atom1->getAtomicNum()==0 ||
            atom2->getAtomicNum()==0)
        return;

	double dist = Utilities::calculateDistance(atom1->getPosition(frameNum),
											   atom2->getPosition(frameNum));


    double re = Element::getCovalentRadius(atom1->getAtomicNum()) + Element::getCovalentRadius(atom2->getAtomicNum())
                + this->computetoleranceFactor(atom1,atom2,(-1.0)*tolerance_distance);

    double sigma = (re+tolerance_distance*2)/(3);

    double strength;

    if (dist>re)
    {
        strength = exp(-(re-dist)*(re-dist)/(2*sigma*sigma));
    }
    else
        strength = 1;


    if (strength>0.39)
    {
        BondSP newBond(new Bond(atom1->getSerial(), atom2->getSerial(), 1));
        newBond->_order = strength;
        this->addBond(frameNum,newBond);
    }

}





double MolecularSystem::computetoleranceFactor(AtomSP atom1, AtomSP atom2,
                                                double tolerance_distance)
{
    /* NOTE: Conversion from angstrom to picometer. */
    double deltaElectNeg = Element::getElectronegativity(atom2->getAtomicNum()) -
                           Element::getElectronegativity(atom1->getAtomicNum());

    return ((-7.0/100.0) * deltaElectNeg * deltaElectNeg) + tolerance_distance;
}

void MolecularSystem::maximumCoordination(size_t frameNum)
{
	if(frameNum >= _framesList.size() )
		throw std::invalid_argument("MolecularSystem::checkMaximumCoordination - "
									"frame number out of range!");

	/* Sorting atoms by covalent radius */
	std::list<AtomSP> covalentOrderedAtoms;
	for (auto it=atomsBegin(); it!=atomsEnd();it++)
	{
		AtomSP atom = it->second;
		covalentOrderedAtoms.push_back(atom);
	}

	covalentOrderedAtoms.sort([](AtomSP atom1,AtomSP atom2)
	{
		return (fuzzyEqual(Element::getCovalentRadius(atom1->getAtomicNum()),
						   Element::getCovalentRadius(atom2->getAtomicNum()))) ?
				atom1<atom2 : Element::getCovalentRadius(atom1->getAtomicNum())<
							  Element::getCovalentRadius(atom2->getAtomicNum());
	});


    for (auto it1=covalentOrderedAtoms.begin(); it1!=covalentOrderedAtoms.end();it1++)
	{
		AtomSP atom = *it1;

		//Coordination is ok
		if (!(atom->getNumBonds(frameNum)<=Element::getCoordination(atom->getAtomicNum())))
		{
			this->checkCoordinationAndDeleteBond(frameNum,atom);
		}

    }
}


void MolecularSystem::checkCoordinationAndDeleteBond(size_t frameNum, AtomSP atom)
{
	std::vector<BondSP> bondsOrderedByRatios;

	unsigned coordination = Element::getCoordination(atom->getAtomicNum());

	//Is there boron bonded with hydrogen?
	bool boron = false;

	//For each bond
	for(auto it=atom->getBondBegin(frameNum); it!=atom->getBondEnd(frameNum); it++)
	{
		BondSP bond = *it;

		//If boron is in the bond
		if (MolecularSystem::getAtom(bond->getMinor())->getAtomicNum()==5 ||
			MolecularSystem::getAtom(bond->getHigher())->getAtomicNum()==5)
		{
			boron=true;
		}

		bondsOrderedByRatios.push_back(bond);
	}

	std::sort(bondsOrderedByRatios.begin(),
			  bondsOrderedByRatios.end(),
			  [this,&frameNum](BondSP bond1, BondSP bond2)
	{
		double ratio1=this->computeRatio(frameNum,bond1);
		double ratio2=this->computeRatio(frameNum,bond2);
		return (fuzzyEqual(ratio1,ratio2)) ? bond1<bond2 : ratio1<ratio2;
	});

	//If the hydrogen is connected to a boron it has 2 as coordination
	if (atom->getAtomicNum()==1 && boron)
	{
		coordination=2;
	}

	while(atom->getNumBonds(frameNum) > coordination)
	{
		//Removing the bond with the largest ratio
		this->removeBond(frameNum,bondsOrderedByRatios.back());
		bondsOrderedByRatios.pop_back();
	}

}

double MolecularSystem::computeRatio(size_t frameNum, BondSP bond)
{
	AtomSP atom1 = this->getAtom(bond->getMinor());
	AtomSP atom2 = this->getAtom(bond->getHigher());
    double lenght=Utilities::calculateDistance(atom1->getPosition(frameNum),
                                               atom2->getPosition(frameNum));
    double theoretical_lenght = Element::getCovalentRadius(atom1->getAtomicNum())+
                                Element::getCovalentRadius(atom2->getAtomicNum());

    //For boron-boron and aluminum-aluminum bonds, theoretical bond distance is decreased by 0.2
    if ((atom1->getAtomicNum()==5  && atom2->getAtomicNum()==5) ||
        (atom1->getAtomicNum()==13 && atom2->getAtomicNum()==13))
    {
        theoretical_lenght=theoretical_lenght-0.2;
    }
    return lenght/theoretical_lenght;
}

void MolecularSystem::computeHBondsInFrame_Slow(size_t frameNum)
{
    // Removing old HBonds
	for(const HBondSP& hbond : _framesList[frameNum]._hbondsSet)
	{
		getAtom(hbond->getDonor())->removeHBond(frameNum,hbond);
		getAtom(hbond->getAcceptor())->removeHBond(frameNum,hbond);
		getAtom(hbond->getHydrogen())->removeHBond(frameNum,hbond);
	}

	_framesList[frameNum]._hbondsSet.clear();

	if(this->getNumAtoms() < 3) return;

	// For each atom
	for (auto it1=atomsBegin(); it1!= atomsEnd(); it1++)
	{
		//For each hydrogen
		AtomSP hydrogen = it1->second;
		if (hydrogen->getAtomicNum()==1)
		{
			// For each Donor
			for(auto bondIterator = hydrogen->getBondBegin(frameNum);
				bondIterator != hydrogen->getBondEnd(frameNum);
				bondIterator++)
			{
				BondSP bond = *bondIterator;

				long long donorSerial = (bond->getMinor()==hydrogen->getSerial()) ?
											bond->getHigher() : bond->getMinor();

				AtomSP donor = this->getAtom(donorSerial);

				if(HBInteractions::canBeDonorOrAcceptor(donor->getAtomicNum()))
				{
					//For each atom / For each acceptor
					for (auto it2 = _atomsMap.begin(); it2!=_atomsMap.end(); it2++)
					{
						//Acceptor != Donor && Is a good acceptor
						if(it2->first!=donorSerial && HBInteractions::canBeDonorOrAcceptor(it2->second->getAtomicNum()))
						{
							AtomSP acceptor = it2->second;

								//DONOR IS NOT COVALENTLY BONDED WITH ACCEPTOR
                                if(!(donor->findBond(frameNum,it2->first)))
								{
                                    double force = this->computeHBondIntensity(frameNum,hydrogen,donor,acceptor);
									if (force>10E-4)
									{
                                        HBondSP hbond(new HBond(hydrogen->getSerial(),donor->getSerial(),acceptor->getSerial(),force));
										this->addHBond(frameNum,hbond);
									}
								}


						} //Is a good acceptor?
					} //For each acceptor
				} //Is a good donor
			} // For each Donor
		} //For each hydrogen
    } //For each atom
}

double MolecularSystem::computeBondStrength(BondSP bnd, size_t frameNum)
{
    long long atm1 = bnd->getMinor();
    long long atm2 = bnd->getHigher();

    AtomSP atom1 = this->getAtom(atm1);
    AtomSP atom2 = this->getAtom(atm2);

    double re = Element::getCovalentRadius(atom1->getAtomicNum()) + Element::getCovalentRadius(atom2->getAtomicNum())
            - 0.07*(Element::getElectronegativity(atom2->getAtomicNum()) - Element::getElectronegativity(atom1->getAtomicNum()))*(Element::getElectronegativity(atom2->getAtomicNum()) - Element::getElectronegativity(atom1->getAtomicNum()))
            -0.4;

    double sigma = (re+0.4*2)/(2*2);

    double d = Utilities::calculateDistance(atom2->getPosition(frameNum), atom1->getPosition(frameNum));

    if (d>re)
    {
        return exp(-(re-d)*(re-d)/(2*sigma*sigma));
    }
    else
        return 1;
}



void MolecularSystem::computeHBondsInFrame_Fast(size_t frameNum)
{
	// Removing old HBonds
	for(const HBondSP& hbond : _framesList[frameNum]._hbondsSet)
	{
		getAtom(hbond->getDonor())->removeHBond(frameNum,hbond);
		getAtom(hbond->getAcceptor())->removeHBond(frameNum,hbond);
		getAtom(hbond->getHydrogen())->removeHBond(frameNum,hbond);
	}

	_framesList[frameNum]._hbondsSet.clear();

	if(this->getNumAtoms() < 3) return;

	// Creating a Grid
	AtomsGridSP grid(new AtomsGrid(this,frameNum));

	// For each atom
	for (auto it1=atomsBegin(); it1!= atomsEnd(); it1++)
	{
		//For each hydrogen
		AtomSP hydrogen = it1->second;
		if (hydrogen->getAtomicNum()==1)
		{
			// For each Donor
			for(auto bondIterator = hydrogen->getBondBegin(frameNum);
				bondIterator != hydrogen->getBondEnd(frameNum);
				bondIterator++)
			{
				BondSP bond = *bondIterator;

				long long donorSerial = (bond->getMinor()==hydrogen->getSerial()) ?
											bond->getHigher() : bond->getMinor();

				AtomSP donor = this->getAtom(donorSerial);

				if(HBInteractions::canBeDonorOrAcceptor(donor->getAtomicNum()))
				{


                    std::vector<AtomSP> neighbors(grid->getNeighbors(hydrogen));

					//For each neighbor / For each acceptor
					for (auto it2 = neighbors.begin(); it2!=neighbors.end(); it2++)
					{

                        AtomSP acceptor = *it2;

						//Checks if there is a good acceptor
                        if(acceptor->getSerial()!=donorSerial && HBInteractions::canBeDonorOrAcceptor(acceptor->getAtomicNum()))
						{

								//DONOR IS NOT COVALENTLY BONDED WITH ACCEPTOR
                                if(!(donor->findBond(frameNum,acceptor->getSerial())))
								{
                                    double force = this->computeHBondIntensity(frameNum,hydrogen,donor,acceptor);
									if (force>10E-4)
									{
                                        HBondSP hbond(new HBond(hydrogen->getSerial(),donor->getSerial(),acceptor->getSerial(),force));
										this->addHBond(frameNum,hbond);
									}
								}


						} //Is a good acceptor?
					} //For each acceptor
				} //Is a good donor
			} // For each Donor
		} //For each hydrogen
    } //For each atom
}

void MolecularSystem::computeHBonds_Fast()
{
    for(size_t frameNum = 0; frameNum < this->getNumFrames(); frameNum++)
        this->computeHBondsInFrame_Fast(frameNum);
}












void MolecularSystem::computeHBondInFrame_Fast_Parallel(size_t frameNum)
{
    // Removing old HBonds
    for(const HBondSP& hbond : _framesList[frameNum]._hbondsSet)
    {
        getAtom(hbond->getDonor())->removeHBond(frameNum,hbond);
        getAtom(hbond->getAcceptor())->removeHBond(frameNum,hbond);
        getAtom(hbond->getHydrogen())->removeHBond(frameNum,hbond);
    }

    _framesList[frameNum]._hbondsSet.clear();

    if(this->getNumAtoms() < 3) return;

    // Creating a Grid
    AtomsGridSP grid(new AtomsGrid(this,frameNum));

    long numCandidates = (long) this->_framesList[frameNum]._candidateHBonds.size();

    omp_set_num_threads(6);
#pragma omp parallel for
    for (long id=0; id < numCandidates; id++)
    {
        std::pair<long long, long long> currentCandidate = this->_framesList[frameNum]._candidateHBonds[id];
        AtomSP hydrogen = this->getAtom(currentCandidate.first);
        AtomSP donor = this->getAtom(currentCandidate.second);

        std::vector<AtomSP> neighbors(grid->getNeighbors(hydrogen));


        //For each neighbor / For each acceptor
        for (auto it2 = neighbors.begin(); it2!=neighbors.end(); it2++)
        {

            AtomSP acceptor = *it2;

            //Checks if there is a good acceptor
            if(acceptor->getSerial()!=currentCandidate.second && HBInteractions::canBeDonorOrAcceptor(acceptor->getAtomicNum()))
            {

                //DONOR IS NOT COVALENTLY BONDED WITH ACCEPTOR
                if(!(donor->findBond(frameNum,acceptor->getSerial())))
                {
                    double force = this->computeHBondIntensity(frameNum,hydrogen,donor,acceptor);
                    if (force>10E-4)
                    {
                        #pragma omp critical
                        {
                            HBondSP hbond(new HBond(hydrogen->getSerial(),donor->getSerial(),acceptor->getSerial(),force));
                            this->addHBond(frameNum,hbond);
                        }

                    }
                }


            } //Is a good acceptor?
        } //For each acceptor

    }



}



//HBOND PARAMETERS
//---------------------------

double MolecularSystem::supportFunction_For_HBond(double x, double x_eq, double s)
{
	if (x<=x_eq) return 1;

	double exponent = ((-1)*(x_eq-x)*(x_eq-x))/(2*s*s);

    return std::exp(exponent);
}

void MolecularSystem::computeAtomicHybridisation(size_t frameNum, AtomSP atom, double tolerance)
{

    //The final hybridisation object to be returned
    Hybridisation hybrid(0,0,0);

    //A vector containing all the possible angle values beetween couples of bonds.
    std::vector<double> angles;

    if (atom->getAtomicNum()>=21) tolerance=20;

    Atom::bond_iterator bondBegin = atom->getBondBegin(frameNum);
    Atom::bond_iterator bondEnd = atom->getBondEnd(frameNum);
    std::vector<double> atomPosition = atom->getPosition(frameNum);

    bool is120=false;
    //COMPUTING ANGLES
    for(auto it1=bondBegin; it1!=std::prev(bondEnd,1); it1++)
    {
        BondSP bond1=*it1;

        auto it2=std::next(it1,1);

        //The position of the first atom (atom of the first bond) bonded with our main atom.
        std::vector<double> firstBondedAtomPosition = (bond1->getMinor()==atom->getSerial()) ?
                                                   this->getAtom(bond1->getHigher())->getPosition(frameNum) :
                                                   this->getAtom(bond1->getMinor())->getPosition(frameNum);

        for(;it2!=bondEnd;it2++)
        {
            BondSP bond2=*it2;

            //The position of the second atom (atom of the second bond) bonded with our main atom.
            std::vector<double> secondBondedAtomPosition = (bond2->getMinor()==atom->getSerial()) ?
                                                        this->getAtom(bond2->getHigher())->getPosition(frameNum) :
                                                        this->getAtom(bond2->getMinor())->getPosition(frameNum);

            //Computing the angle beetween the two bonds
            std::vector<double> firstDist = Utilities::minus(firstBondedAtomPosition,atomPosition);
            std::vector<double> secondDist = Utilities::minus(secondBondedAtomPosition,atomPosition);
            double angle = Utilities::calculateAngle(firstDist, secondDist);

            //Adding the angle beetween bonds to the angles vector
            //If the angle has already been added it is not added anymore.
            if(std::find_if(angles.begin(),angles.end(),[angle](const double& d)
            {return fuzzyEqual(d,angle);})  == angles.end())
                if (angle<120+tolerance && angle>120-tolerance) is120 = true;
                angles.push_back(angle);
        }
    }

    //COMPUTING HYBRIDISATION
    double minAngle = *std::min_element(angles.begin(),angles.end());
    double maxAngle = *std::max_element(angles.begin(),angles.end());

    //SD
    if (atom->getNumBonds(frameNum)<3 && atom->getAtomicNum()>=21 && maxAngle<90+tolerance && minAngle>90-tolerance)
    {
        hybrid.sCharacter = 1;
        hybrid.pCharacter=0;
        hybrid.dCharacter=1;
    }

    //SP hybridisation
    if (atom->getNumBonds(frameNum)<3 && ((maxAngle+minAngle)/2)<180+tolerance && ((maxAngle+minAngle)/2)>180-tolerance)
    {
        hybrid.sCharacter = 1;
        hybrid.pCharacter=1;
        hybrid.dCharacter=0;
    }

    //SD2
    else if (atom->getNumBonds(frameNum)<4 && atom->getAtomicNum()>=21 && maxAngle<90+tolerance && minAngle>90-tolerance )
    {
        hybrid.sCharacter = 1;
        hybrid.pCharacter=0;
        hybrid.dCharacter=2;
    }

    //SP2 hybridisation
    // 5.5
    else if (atom->getNumBonds(frameNum)<4 && (((maxAngle+minAngle)/2)<(120+tolerance*0.55)) && ((minAngle+maxAngle)/2)>(120-tolerance*0.55) )
    {
        hybrid.sCharacter = 1;
        hybrid.pCharacter=2;
        hybrid.dCharacter=0;
    }

    //SD3 hybridisation
    else if (atom->getNumBonds(frameNum)<5 && atom->getAtomicNum()>=21 && maxAngle<109.5+tolerance && minAngle>109.5-tolerance )
    {
        hybrid.sCharacter = 1;
        hybrid.pCharacter=0;
        hybrid.dCharacter=3;
    }

    //SP3 hybridisation
    else if (atom->getNumBonds(frameNum)<5 && maxAngle<109.5+tolerance && minAngle>109.5-tolerance )
    {
        hybrid.sCharacter = 1;
        hybrid.pCharacter=3;
        hybrid.dCharacter=0;
    }

    //SD4
    else if (atom->getNumBonds(frameNum)<7 && atom->getAtomicNum()>=21 && maxAngle<123+tolerance && minAngle>73-tolerance )
    {
        hybrid.sCharacter = 1;
        hybrid.pCharacter=0;
        hybrid.dCharacter=4;
    }

    //SP2D hybridisation
    else if (atom->getNumBonds(frameNum)<5 && atom->getAtomicNum()>=21 && maxAngle<90+tolerance && minAngle>90-tolerance )
    {
        hybrid.sCharacter = 1;
        hybrid.pCharacter=2;
        hybrid.dCharacter=1;
    }

    //SP3D hybridisation //180 O 120
    else if (atom->getNumBonds(frameNum)<6 && atom->getAtomicNum()>=21 && maxAngle<180+tolerance && minAngle>90-tolerance && is120)
    {
        hybrid.sCharacter = 1;
        hybrid.pCharacter=3;
        hybrid.dCharacter=1;
    }

    //SD5
    else if (atom->getNumBonds(frameNum)<7 && atom->getAtomicNum()>=21 && maxAngle<116.5+tolerance && minAngle>63.5-tolerance )
    {
        hybrid.sCharacter = 1;
        hybrid.pCharacter=0;
        hybrid.dCharacter=5;
    }

    //SP3D2 hybridisation
    else if (atom->getNumBonds(frameNum)<7 && atom->getAtomicNum()>=21 && maxAngle<180+tolerance && minAngle>90-tolerance )
    {
        hybrid.sCharacter = 1;
        hybrid.pCharacter=3;
        hybrid.dCharacter=2;
    }


    //SP3D3 hybridisation //180 O 120
    else if (atom->getNumBonds(frameNum)<8 && atom->getAtomicNum()>=21 && maxAngle<180+tolerance && minAngle>72-tolerance && is120)
    {
        hybrid.sCharacter = 1;
        hybrid.pCharacter=3;
        hybrid.dCharacter=3;
    }

    atom->addHybridisation(frameNum, hybrid);
}





void MolecularSystem::setLabelVector(std::vector<std::string> vec)
{
    this->_frameLabel = vec;
}

double MolecularSystem::computeHBondIntensity(size_t frameNum, AtomSP hydrogen, AtomSP donor, AtomSP acceptor)
{
	if(frameNum >= _framesList.size() )
		throw std::invalid_argument("MolecularSystem::computeHBondFactor - frame number out of range!");
	if(hydrogen==nullptr || donor==nullptr || acceptor==nullptr)
		throw std::invalid_argument("MolecularSystem::computeHBondFactor - null pointers passed as argument!");

	/* Vectors */
    std::vector<double> _hydrogen = hydrogen->getPosition(frameNum);
    std::vector<double> _donor = donor->getPosition(frameNum);
    std::vector<double> _acceptor = acceptor->getPosition(frameNum);

	/* Calculating geometrical properties
	 * r: distance between the hydrogen and the acceptor
	 * d: distance between the donor and the acceptor
	*/

	double d = Utilities::calculateDistance(_donor,_acceptor);
	if (d>4) return 0;
	double r = Utilities::calculateDistance(_hydrogen,_acceptor);

    std::vector<double> hydrogenAcceptor = Utilities::minus(_hydrogen,_donor);
    std::vector<double> donorAcceptor = Utilities::minus(_acceptor,_donor);
	double angle = Utilities::calculateAngle(hydrogenAcceptor,donorAcceptor);

	/* HBond Parameters */
	return MolecularSystem::supportFunction_For_HBond(r,HBInteractions::getHBParameter_r_e(donor->getAtomicNum(),acceptor->getAtomicNum()),
														HBInteractions::getHBParameter_s_r(donor->getAtomicNum(),acceptor->getAtomicNum()))      *
		   MolecularSystem::supportFunction_For_HBond(angle,HBInteractions::getHBParameter_theta_e(donor->getAtomicNum(),acceptor->getAtomicNum()),
													  HBInteractions::getHBParameter_s_theta(donor->getAtomicNum(),acceptor->getAtomicNum()));


}





