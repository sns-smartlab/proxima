#ifndef PROXIMA_ATOMICPROPERTIES_H
#define PROXIMA_ATOMICPROPERTIES_H

#include "./utilities/WarningsOff.h"

#include <string>
#include <array>
#include <map>
#include <vector>

#include "./utilities/WarningsOff.h"

/**
 * @author Federico Lazzari
 */
namespace Proxima
{
    /**
     * @brief This class contains several atomic properties
     *        for each element.
     */
	class Element
	{
	public:

		/**
		 * @brief   Returns the atomic number from the element symbol.
		 *          Returns 0 for unknown symbols.
		 */
		static int getAtomicNumber(std::string symbol);

		/**
		 * @brief Returns the Van der Waals radius (expressed in Angstrom) from the atomic number.
		 * @note Values for Van der Waals raddi are obtained from the following publication:
		 *		 Alvarez, Santiago. "A cartography of the van der Waals territories."
		 *		 Dalton Transactions 42.24 (2013): 8617-8636.
		 * @note Unknown values for Van der Waals radii are taken to be 2.5
		*/
		static double getWaalsRadius(int atomicNum);

		/** @brief Returns the Covalent radius (expressed in Angstrom) from the atomic number.
		 *  @note Values for covalent radii are obtained from the following publication:
		 *	      B. Cordero, V. Gómez, A. E. Platero-Prats, M. Revés, J. Echeverría, E. Cremades,
		 *		  F. Barragán and S. Alvarez.
				  "Covalent radii revisited"
				  Dalton Trans., 2008, 2832.
		 *  @note Unknown values for Covalent radii are taken to be 1.5
		 */
		static double getCovalentRadius(int atomicNum);

		/** @brief Returns the electronegativity of the element from the atomic number.
		 * @note Values for electronegativities are obtained from the following publication:
		 *		 Elbert J. Little Jr. and Mark M. Jones. "A complete table of electronegativities."
				 Journal of Chemical Education 1960 37 (5), 231.
		 */
		static double getElectronegativity(int atomicNum);

        static unsigned getValence(int atomicNum);

        /**
         * @brief It returns the bond order for the given couple of atoms
         */
        static double getBondOrder(double dist, int atomicNum1, int atomicNum2);


        static double getBO_Morse(double dist, int atomicNum1, int atomicNum2);



        /**
         * @brief It returns the bond radius for the single bond order
         * @param atomicNum The atomic number
         */
        static double getSingleBondRadius(int atomicNum);

        static double getDoubleBondRadius(int atomicNum);

        static double getTripleBondRadius(int atomicNum);



        static double getA(int atomicNum);
        static double getB(int atomicNum);


        static double getElectronegativity_XUFF(std::string atmType);
        static double getHardness_XUFF(std::string atmType);




		/** @brief Returns the element symbol from the atomic number. */
		static std::string getElementSymbol(int atomicNum);

        /** @brief Returns the maximum coordination of the element from the atomic number.
         * @note Values are taken by S. Artemova, L. Jaillet, and S. Redon, “Automatic molecular structure perception for the universal force
         *       field,” Journal of computational chemistry, 2016.
         */
		static unsigned getCoordination(int atomicNum);

        static double getHardness(int atomicNum);

        /**
         * @brief Checks whether the given atom is a transition element.
         * @param atomicNum The atomic number.
         */
        static unsigned isTransitionElement(int atomicNum);

        static int getRow(int atomicNum);

		//TODO: doc!!!
		// Colori presi da Jmol
		static std::array<float,3> getColor(int atomicNum);

    private:



        struct Parameters
        {
            Parameters();

            Parameters(const std::string& pSymbol,
                       double pWaalsRadius,
                       double pCovalentRadius,
                       double pElectronegativity,
                       unsigned pCoordination,
                       unsigned pValence,
                       const std::array<float,3>& pColor);

            std::string symbol;
            double waalsRadius;
            double covalentRadius;
            double electronegativity;
            unsigned coordination;
            unsigned valence;
            std::array<float,3> color;
        };

        /**
         * @brief The atomic numbers of the atoms associated to the element symbol.
         */
        static std::map<std::string, int> atomicNumbers;



        static std::map<int,double> qeqHardness;

        static std::map<int,double> qeqPotential;

        static std::map<int,double> singleBondRadii;

        static std::map<int,double> doubleBondRadii;

        static std::map<int,double> tripleBondRadii;

        static std::map<int,double> boParameters_a;

        static std::map<int,double> boParameters_b;

        //TODO: Parametri dipendenti dal tipo di atomo
        static std::map<std::string, double> xuff_hardness;
        static std::map<std::string, double> xuff_electronegativity;


        static std::map<int, double> boParameters_Morse;




        /**
         * @brief This is a container for all of the most relevant atomic
         *        properties for each element identified by an atomic number.
         */
        static Parameters atomicProperties[119];


        /**
         * @brief A boolean value used  for detecting whether the atomic parameters have been initialized.
         */
        static bool atomicPropetiesInitialized;

        /**
         * @brief This method initialize the list of atomic number associated to each element symbol.
         *
         * @note This method is only called once.
         */
        static void initAtomicNumbers();

        /**
         * @brief This method initialize the list of atomic properties associated to each atomic number.
         *
         * @note This method is only called once.
         */
        static void initAtomicProperties();

        static void initQeqProperties();
	};

}

#endif //ATOMICPROPERTIES_H
