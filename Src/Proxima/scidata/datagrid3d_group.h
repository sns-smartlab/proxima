#ifndef DATAGRID3D_GROUP_H
#define DATAGRID3D_GROUP_H

#include <vector>
#include <memory>

#include <scidata/datagrid3d.h>
#include <utilities/log.h>

/**
 * @author Andrea Salvadori
 */
namespace Proxima
{

/**
 * @brief	A DataGrid3DGroup instance represent a group of volumetric datasets
 *			related to the same region of space. All the datasets of the group
 *			have the same number of voxels and the same transformation matrix.
 *
 *			Once added to this manager, the volumetric dataset (logically)
 *			belongs to the manager, that provide it on request and in
 *			read-only mode. Any modification to the internal state of
 *			volumetric datasets owned by this manager is forbidden!!!
 */
template <typename DataGrid3DType>
class DataGrid3DGroup final
{

private:

	/**
	 * @brief A descriptive name of the group.
	 */
	std::string _name;

	/**
	 * @brief List of volumetric datasets.
	 */
	std::vector<std::shared_ptr<const DataGrid3DType>> _volumes;

public:

	/** @brief Default constructor. */
	DataGrid3DGroup() = default;

	/** @brief Destructor. */
	~DataGrid3DGroup() { /*qDebug() << "~DataGrid3DGroup";*/ }

	// Disables copy and move operations
	DataGrid3DGroup(const DataGrid3DGroup&) = delete;
	DataGrid3DGroup(DataGrid3DGroup&&) = delete;
	DataGrid3DGroup& operator=(const DataGrid3DGroup&) = delete;
	DataGrid3DGroup& operator=(DataGrid3DGroup&&) = delete;

	/**
	 * @brief	Adds a volumetric datasets to the group. It must be "conformable"
	 *			to the other volumes of the group (i.e. they must have the same
	 *			number of voxels and the same local->world change of reference
	 *			frame matrix), otherwise the insertion fails.
	 *
	 * @return	true if the insertion succeeds, false otherwise.
	 */
	bool addVolume(std::shared_ptr<DataGrid3DType> volume)
	{
		if(!volume) return false;
		if((!_volumes.empty()) && (!_volumes[0]->isConformableTo(*volume))) return false;

		_volumes.push_back(volume);
		return true;
	}

	/**
	 * @brief Returns a descriptive name associated to the group.
	 */
	std::string getName() const
	{ return _name; }

	/**
	 * @brief Sets a new descriptive name for the group.
	 */
	void setName(const std::string& newName)
	{ _name = newName; }

	/**
	 * @brief Returns the number of volumetric datasets contained in this group.
	 */
	size_t getNumberOfVolumes() const
	{ return _volumes.size(); }

	/**
	 * @brief	Returns the number of voxels along the X axis
	 *			of the volumes belonging to this group.
	 *
	 * @throw std::logic_error If the group is empty.
	 */
	unsigned getNumPointsX() const
	{
		if(_volumes.empty()) throwAndPrintError<std::logic_error>(
								"DataGrid3DGroup::getNumPointsX - "
								"Invalid call on an empty group!");

		return _volumes[0]->getNumPointsX();
	}

	/**
	 * @brief	Returns the number of voxels along the Y axis
	 *			of the volumes belonging to this group.
	 *
	 * @throw std::logic_error If the group is empty.
	 */
	unsigned getNumPointsY() const
	{
		if(_volumes.empty()) throwAndPrintError<std::logic_error>(
								"DataGrid3DGroup::getNumPointsY - "
								"Invalid call on an empty group!");

		return _volumes[0]->getNumPointsY();
	}

	/**
	 * @brief	Returns the number of voxels along the Z axis
	 *			of the volumes belonging to this group.
	 *
	 * @throw std::logic_error If the group is empty.
	 */
	unsigned getNumPointsZ() const
	{
		if(_volumes.empty()) throwAndPrintError<std::logic_error>(
								"DataGrid3DGroup::getNumPointsZ - "
								"Invalid call on an empty group!");

		return _volumes[0]->getNumPointsZ();
	}

	/**
	 * @brief	Returns the origin of the local reference frame
	 *			of the volumes belonging to this group,
	 *			expressed in the global reference frame.
	 */
	virtual mathfu::Vector<double,3> getOriginInWorldSpace() const
	{
		if(_volumes.empty()) throwAndPrintError<std::logic_error>(
								"DataGrid3DGroup::getOriginInWorldSpace - "
								"Invalid call on an empty group!");

		return _volumes[0]->getOriginInWorldSpace();
	}

	/**
	 * @brief	Returns the X axis of the local reference frame
	 *			of the volumes belonging to this group,
	 *			expressed in the global reference frame.
	 */
	virtual mathfu::Vector<double,3> getLocalXAxisInWorldSpace() const
	{
		if(_volumes.empty()) throwAndPrintError<std::logic_error>(
								"DataGrid3DGroup::getLocalXAxisInWorldSpace - "
								"Invalid call on an empty group!");

		return _volumes[0]->getLocalXAxisInWorldSpace();
	}

	/**
	 * @brief	Returns the Y axis of the local reference frame
	 *			of the volumes belonging to this group,
	 *			expressed in the global reference frame.
	 */
	virtual mathfu::Vector<double,3> getLocalYAxisInWorldSpace() const
	{
		if(_volumes.empty()) throwAndPrintError<std::logic_error>(
								"DataGrid3DGroup::getLocalYAxisInWorldSpace - "
								"Invalid call on an empty group!");

		return _volumes[0]->getLocalYAxisInWorldSpace();
	}

	/**
	 * @brief	Returns the Z axis of the local reference frame
	 *			of the volumes belonging to this group,
	 *			expressed in the global reference frame.
	 */
	virtual mathfu::Vector<double,3> getLocalZAxisInWorldSpace() const
	{
		if(_volumes.empty()) throwAndPrintError<std::logic_error>(
								"DataGrid3DGroup::getLocalZAxisInWorldSpace - "
								"Invalid call on an empty group!");

		return _volumes[0]->getLocalZAxisInWorldSpace();
	}

	/**
	 * @brief	Returns the change of reference frame matrix from local
	 *			coordinates of the volumes belonging to this group to
	 *			world (global) coordinates.
	 *
	 * @throw std::logic_error If the group is empty.
	 */
	const mathfu::Matrix<double,4>& getLocalToWorldTransform() const
	{
		if(_volumes.empty()) throwAndPrintError<std::logic_error>(
								"DataGrid3DGroup::getLocalToWorldTransform - "
								"Invalid call on an empty group!");

		return _volumes[0]->getLocalToWorldTransform();
	}

	/**
	 * @brief	Returns the change of reference frame matrix from
	 *			world (global) coordinates to local coordinates of
	 *			of the volumes belonging to this group.
	 *
	 * @throw std::logic_error If the group is empty.
	 */
	const mathfu::Matrix<double,4>& getWorldToLocalTransform() const
	{
		if(_volumes.empty()) throwAndPrintError<std::logic_error>(
								"DataGrid3DGroup::getWorldToLocalTransform - "
								"Invalid call on an empty group!");

		return _volumes[0]->getWorldToLocalTransform();
	}

	/**
	 * @brief Returns a const pointer to the i-th volume contained in this group.
	 *
	 * @param volumeIdx	Index of the requested volume within this group.
	 *					Must be in the range [0, getNumberOfVolumes()-1].
	 *
	 * @throw std::invalid_argument If volumeIdx >= getNumberOfVolumes().
	 */
	const DataGrid3DType* getVolume(size_t volumeIdx) const
	{
		if(volumeIdx >= _volumes.size()) throwAndPrintError<std::invalid_argument>(
											"DataGrid3DGroup::getVolume - "
											"the specified index is out of range!");

		return _volumes[volumeIdx].get();
	}

	/**
	 * @brief Returns a const shared pointer to the i-th volume contained in this group.
	 *
	 * @param volumeIdx	Index of the requested volume within this group.
	 *					Must be in the range [0, getNumberOfVolumes()-1].
	 *
	 * @throw std::invalid_argument If volumeIdx >= getNumberOfVolumes().
	 */
	std::shared_ptr<const DataGrid3DType> getVolumeSharedPtr(size_t volumeIdx) const
	{
		if(volumeIdx >= _volumes.size()) throwAndPrintError<std::invalid_argument>(
											"DataGrid3DGroup::getVolumeSharedPtr - "
											"the specified index is out of range!");

		return _volumes[volumeIdx];
	}
};

typedef DataGrid3DGroup<DataGrid3D<double>> ScalarVolumesGroup;
typedef std::shared_ptr<DataGrid3DGroup<DataGrid3D<double>>> ScalarVolumesGroupSP;
typedef std::shared_ptr<const DataGrid3DGroup<DataGrid3D<double>>> ConstScalarVolumesGroupSP;

}

#endif // DATAGRID3D_GROUP_H
