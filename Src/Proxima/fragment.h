#ifndef PROXIMA_FRAGMENT_H
#define PROXIMA_FRAGMENT_H

#include "./utilities/WarningsOff.h"
#include <string>
#include <set>
#include "./utilities/WarningsOn.h"

#include "residue.h"
#include "aminoresidue.h"
#include "nucleotideresidue.h"

/**
 * @author Federico Lazzari
 */
namespace Proxima
{

//forward declaration
class MolecularSystem;

/**
 * @brief The Fragment class represents fragments within the molecular system.
 *
 *        A fragment object acts as a container for residues.
 */
class Fragment
{

public:

    /**
     * @brief Default name for a fragment.
     *        This constant is usually employed to assign a name
     *        to the default chain of a PDB file.
     */
    static const std::string DEFAULT_NAME;

    /**
     * @brief An iterator for general residues.
     */
    typedef std::map<std::pair<long long, char>,ResidueSP>::const_iterator const_genRes_iter;

    /**
     * @brief An iterator for amino residues.
     */
    typedef std::map<std::pair<long long, char>,AminoResidueSP>::const_iterator const_aminoRes_iter;

    /**
     * @brief An iterator for nucleotide residues.
     */
    typedef std::map<std::pair<long long, char>,NucleotideResidueSP>::const_iterator const_nucleotideRes_iter;

public:

    /**
     * @brief Constructor
     * @param name  The name of the fragment.
     *
     *              It may not be unique within a molecular system.
     *              If an empty string is passed as parameter,
     *              Fragment::DEFAULT_NAME will be used instead.
     */
    Fragment(char chainID, const std::string& name = DEFAULT_NAME);

    /**
     * @brief Destructor
     */
    virtual ~Fragment() {}

    //Fragment(const Fragment& other);

    virtual void clone(std::shared_ptr<Fragment> &other);

    /**
     * @brief This method checks the equality of the current fragment with another one.
     * @param other The other fragment.
     */
    bool isEqual(const Fragment& other) const;

    /**
     * @brief Returns the name of the fragment
     *        It may not be unique within a molecular system.
     */
    virtual std::string getName() const;

    /**
     * @brief Returns the chainID of the fragment
     */
    virtual char getChainID() const;

    /**
     * @brief Given the sequence number of a residue,
     *          returns true if the residue is contained
     *          in this fragment, false otherwise.
     *
     * @param sequenceNumber The sequence number of the residue.
     * @param insertionCode  The insertion code of the residue.
     */
    virtual bool containsResidue(long long sequenceNumber,
                                 char insertionCode) const;

    /**
     * @brief Returns the general residue having the specified sequence number.
     *          If such a residue is not contained in this fragment,
     *          throws a std::invalid_argument exception.
     *
     * @param sequenceNumber The sequence number of the residue.
     * @param insertionCode  The insertion code of the residue.
     */
    virtual ResidueSP getGenericResidue(long long sequenceNumber,
                                 char insertionCode) const;

    /**
     * @brief Returns the amino residue having the specified sequence number.
     *          If such a residue is not contained in this fragment,
     *          throws a std::invalid_argument exception.
     *
     * @param sequenceNumber The sequence number of the residue.
     * @param insertionCode  The insertion code of the residue.
     */
    virtual AminoResidueSP getAminoResidue(long long sequenceNumber,
                                      char insertionCode) const;

    /**
     * @brief Returns the nucleotide residue having the specified sequence number.
     *          If such a residue is not contained in this fragment,
     *          throws a std::invalid_argument exception.
     *
     * @param sequenceNumber The sequence number of the residue.
     * @param insertionCode  The insertion code of the residue.
     */
    virtual NucleotideResidueSP getNucleotideResidue(long long sequenceNumber,
                                                     char insertionCode) const;

    /**
     * @brief Adds a residue to the fragment.
     *          If one of the following conditions happens, the new residue
     *          will not be inserted in the fragment and this method will
     *          return false:
     *
     *          -This fragment already contains another residue with the same
     *          sequence number and icode.
     *          -The residue already belongs to another fragment.
     *          -The fragment has not been added already to a Molecular System
     *
     * @return True if the operation succeeds, false otherwise.
     * @note   Warning! Use this method carefully, if AminoResidue or NucleotideResidue
     *                  are passed as arguments of this function, they are trated as
     *                  general fragments within the residue and they lose the additional
     *                  informations of the specific residue they represent.
     * @throw  std::invalid_argument if the pointer passed as parameter is NULL
     */
    virtual bool addGenericResidue(ResidueSP residue);

    /**
     * @brief Adds an amino residue to the fragment.
     *          If one of the following conditions happens, the new residue
     *          will not be inserted in the fragment and this method will
     *          return false:
     *          -This fragment already contains another residue with the same
     *          sequence number and icode.
     *          -The residue already belongs to another fragment.
     *          -The fragment has not been added already to a Molecular System
     * @return True if the operation succeeds, false otherwise.
     * @throw  std::invalid_argument if the pointer passed as parameter is NULL
     */
    virtual bool addAminoResidue(AminoResidueSP residue);


    /**
     * @brief Adds a nucleotide residue to the fragment.
     *          If one of the following conditions happens, the new residue
     *          will not be inserted in the fragment and this method will
     *          return false:
     *          -This fragment already contains another residue with the same
     *          sequence number and icode.
     *          -The residue already belongs to another fragment.
     *          -The fragment has not been added already to a Molecular System
     * @return True if the operation succeeds, false otherwise.
     * @throw  std::invalid_argument if the pointer passed as parameter is NULL
     */
    virtual bool addNucleotideResidue(NucleotideResidueSP residue);

    /**
     * @brief Returns the total number of residues contained in the fragment.
     */
    virtual int getNumberOfResidues() const;

    /**
     * @brief Returns the number of generic residues contained in the fragment.
     */
    virtual int getNumberOfGenericResidues() const;

    /**
     * @brief Returns the number of amino residues contained in the fragment.
     */
    virtual int getNumberOfAminoResidues() const;

    /**
     * @brief Returns the number of nucleotide residue contained in the fragment.
     */
    virtual int getNumberOfNucleotideResidues() const;


    /**
     * @brief Returns the number of atoms contained in the fragment.
     */
    virtual long long getNumberOfAtoms() const;

    /**
     * @brief It returns an iterator to the first residue.
     */
    virtual const_genRes_iter getGenericResBegin() const;

    /**
     * @brief It returns an iterator to the "past-the-end" residue.
     */
    virtual const_genRes_iter getGenericResEnd() const;


    /**
     * @brief It returns an iterator to the first amino residue.
     */
    virtual const_aminoRes_iter getAminoResBegin() const;

    /**
     * @brief It returns an iterator to the "past-the-end" amino residue.
     */
    virtual const_aminoRes_iter getAminoResEnd() const;

    /**
     * @brief It returns an iterator to the first nucleotide residue.
     */
    virtual const_nucleotideRes_iter getNucleotideResBegin() const;

    /**
     * @brief It returns an iterator to the "past-the-end" nucleotide residue.
     */
    virtual const_nucleotideRes_iter getNucleotideResEnd() const;

    /**
     * @brief returns the molecular system this fragment belongs to.
     *          If this fragment does not belongs to any molecular system
     *          returns NULL.
     */
    virtual MolecularSystem* getParentMolecularSystem() const;

protected:

    friend class MolecularSystem;


    /**
     * @brief This method sets the reference molecular system for the current fragment
     *
     * @note  This method is only callable by the Molecular System.
     */
    virtual void setParentMolecularSystem(MolecularSystem* parentMolSys);

private:

    /**
     * @brief The name of the current fragment.
     */
    std::string _name;

    /**
     * @brief The chain identifier of the current fragment.
     */
    char _chainID;

    /**
     * @brief The reference Molecular System.
     */
    MolecularSystem* _parentMolSys;

    /**
     * @brief The generic residues that are part of the current fragment.
     */
    std::map<std::pair<long long, char>,ResidueSP> _residues;

    /**
     * @brief The amino residues that are part of the current fragment.
     */
    std::map<std::pair<long long, char>,AminoResidueSP> _aminoResidues;

    /**
     * @brief The nucleotide residues that are part of the current fragment.
     */
    std::map<std::pair<long long, char>,NucleotideResidueSP> _nucleotideResidues;


};

/**
 * @brief FragmentSP is a smart pointer to a Fragment object.
 */
typedef std::shared_ptr< Proxima::Fragment > FragmentSP;


}
#endif // FRAGMENT_H
