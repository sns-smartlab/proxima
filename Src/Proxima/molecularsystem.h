#ifndef PROXIMA_MOLECULARSYSTEM_H
#define PROXIMA_MOLECULARSYSTEM_H

#include "atomicproperties.h"
#include "atomsgrid.h"
#include "fragment.h"
#include "utilities/cubegrid.h"
#include "./utilities/WarningsOff.h"
#include <fstream>
#include<memory>
#include<string>
#include<set>
#include<map>
#include <unordered_set>
#include <Eigen/Dense>
#include <Eigen/QR>
#include "./utilities/WarningsOn.h"


/**
 * @author Federico Lazzari
 */
namespace Proxima
{
    /**
	 * @brief This class defines a Molecular System object which is not only a container
	 *		  for all Atom, Bond and HBond objects present in the molecular system
	 *        but also manages the reciprocal interactions between these different objects
	 *        and monitors the internal consistency of the molecular system.
	 *        There are also various functions and algorithms to perform computations of
	 *		  various properties of interest.
     *
     */
    class MolecularSystem
	{

	public:

        /* CONSTRUCTOR AND DESTRUCTOR */
        /**
         * @brief The Constructor of the Molecular System class.
         *        It creates a molecular system with a default frame (having index 0).
         *
         * @param name        The name of the molecular system.
         *
         * @param totalCharge The total charge of the molecular system (0 by default).
         */
        MolecularSystem(std::string name, double _totalCharge=0);

        /** @brief Destructor. */
        virtual ~MolecularSystem() = default;

        /**
         * @brief Copy constructor
         */
        MolecularSystem(const MolecularSystem &ms);


        /* ITERATORS */
        /**
         * @brief An iterator to the couples <atom serial, atom object>.
         */
        typedef std::map<long long, AtomSP>::const_iterator atom_iterator;

        /**
         * @brief An iterator to the bond objects.
         */
        typedef std::set<BondSP, BondSPLessFunctor>::iterator bond_iterator;

        /**
         * @brief An iterator to the hydrogen bond objects.
         */
        typedef std::set<HBondSP, HBondSPLessFunctor>::iterator hbond_iterator;

        /**
         * @brief An iterator to cycles (a cycle is a vector of bonds).
         */
        typedef std::vector<std::vector<BondSP>>::const_iterator cycle_iterator;

        /**
         * @brief An iterator to couples <chainID, fragment object>.
         */
        typedef std::multimap<char, FragmentSP>::const_iterator fragment_iterator;



        /* GENERAL PROPERTIES */
        /** @brief It returns the name of the molecular system. */
		virtual std::string getName() const;

        /**
         * @brief It returns the number of Frames.
         */
		virtual size_t getNumFrames() const;

        /** @brief It adds a new frame.
         *
         * @param frameLabel The label associated to the new frame.
        */
		virtual void addFrame(std::string frameLabel = "");

		/**
		 * @brief Sets a new label for the specified frame.
		 * @throw std::invalid_argument is frameNum >= getNumFrames().
		 */
		virtual void setFrameLabel(size_t frameNum, const std::string& frameLabel);

		/**
		 * @brief It returns the label associated to a certain frame.
		 *		  This could be a user-assigned label or it could be
		 *		  determined by the source of the structure
		 *		  (e.g. alternate locations)
		 * @param frameNum The frame number.
		 * @return The corrisponding frame label.
		 */
		virtual std::string getFrameLabel(size_t frameNum);


        /**
         * @brief It returns the spin multiplicity of the system.
         *
         * @note  If the spin multiplicity is not manually setted by the user,
         *          this method computes a guessed spin multiplicity by simply
         *          counting the total number of unpaired electrons.
         *          The total charge of the system is considered in this calculation.
         *
         */
        virtual int getSpinMultiplicity();

        /**
         * @brief It sets the given spin multiplicity to the current molecular system.
         * @param spin  The spin multiplicity.
         */
        virtual void setSpinMultiplicity(int spin);





        /* ATOMS */

        /** @brief It returns an iterator to the first atom of the molecular system. */
        virtual atom_iterator atomsBegin() const;

        /** @brief It returns an iterator referring the past-the-end element of the atoms list. */
        virtual atom_iterator atomsEnd() const;
		/**
		 * @brief	Adds an atom to the Molecular System.
		 *
		 *			Throw an exception if an atom with the same
		 *			serial number is already contained in this system.
		 *
		 * @note Once a new frame is created (apart the default one),
         *		 it is not allowed to add new atoms to the system!
		 */
		virtual void addAtom(AtomSP atom);

		/** @brief Returns the number of atoms conatined in the system. */
		virtual int getNumAtoms() const;

		/** @brief Returns true if the atom is contained in the system, false otherwise. */
        virtual bool containsAtom(long long serial) const;

		/**
		 * @brief	Returns a pointer to the atom having the specified serial number.
		 *
         *			Throws an exception if an atom with the given
		 *			serial number is not contained in this system.
		 */
        virtual AtomSP getAtom(long long serial) const;




        /* BONDS */

        /**
         * @brief	Returns an iterator referring the first element
         *			of the bonds list at the specified frame.
         */
        bond_iterator bondsBegin(size_t frameNum) const;

        /**
         * @brief	Returns an iterator referring the past-the-end
         *			element of the bonds list at the specified frame.
         */
        bond_iterator bondsEnd(size_t frameNum) const;

        /**
         * @brief	Adds a bond to the molecular system.
         *
         *			Throw an exception if the bonded atoms
         *			are not contained in this system.
         *
         *			Throw an exception if the bond is
         *			already contained in this system.
         */
        virtual void addBond(size_t frameNum, BondSP newBond);

        /**
         *	@brief	Returns the number of bonds conatined
         *			in the system at the specified frame.
         */
        virtual int getNumBonds(size_t frameNum) const;

        /**
         *	@brief	Returns true if the bond is contained
         *			in the system at the specified frame, false otherwise.
         */
        virtual bool containsBond(size_t frameNum, BondSP bond);

        /**
         * @brief It returns the bond associated to the given frame number and index.
         * @param frameNum The frame number
         * @param idx The index of the bond
         */
        virtual BondSP getBond(size_t frameNum, long idx);




        /* HBONDS */

        /**
         * @brief	Returns an iterator referring the first element
         *			of the hydrogen bonds list at the specified frame.
         */
        hbond_iterator hbondsBegin(size_t frameNum) const;

        /**
         * @brief	Returns an iterator referring the past-the-end element
         *			of the hydrogen bonds list at the specified frame.
         */
        hbond_iterator hbondsEnd(size_t frameNum) const;

        /**
         * @brief	Adds an hydrogen bond to the the molecular system.
         *
         *			Throw an exception if the bonded atoms
         *			are not contained in this system.
         *
         *			Throw an exception if the hydrogen bond
         *			is already contained in this system.
         */
        virtual void addHBond(size_t frameNum, HBondSP bond);

        /**
         *	@brief	Returns the number of hydrogen bonds conatined
         *			in the system at the specified frame.
         */
        virtual int getNumHBonds(size_t frameNum) const;

        /**
         *	@brief	Returns true if the hydrogen bond is contained
         *			in the system at the specified frame, false otherwise.
         */
        virtual bool containsHBond(size_t frameNum, HBondSP bond);

        /**
         * @brief Remove a bond from the molecular system at a specified frame.
         *        If the bond is not in the system the method does nothing.
        */
        virtual void removeBond(size_t frameNum, BondSP bond);

        /**
         * @brief Remove an hydrogen bond from the molecular system at a specified frame.
         *        if the bond is not in the system the method does nothing.
        */
        virtual void removeHBond(size_t frameNum, HBondSP bond);

        /**
         * @brief It returns the hydrogen bond associated to the given index at the given frame number.
         * @param frameNum The frame number
         * @param idx The index associated to the hydrogen bond
         */
        virtual HBondSP getHBond(size_t frameNum, long idx);





        /* FRAGMENTS */

        /** @brief Returns an iterator to the first fragment of the molecular system. */
        virtual fragment_iterator fragmentsBegin() const;

        /** @brief Returns an iterator referring the past-the-end element of the fragments list. */
        virtual fragment_iterator fragmentsEnd() const;

        /**
         * @brief Adds a fragment to the Molecular System.
         *
         *       Returns true whether the addition
         *       of the fragment has been successful,
         *       false otherwise.
         */
        virtual bool addFragment(FragmentSP fragment);

        /**
         * @brief Checks whether a fragment is included
         *        into the Molecular System.
         *
         *       Returns true whether the fragment
         *       is included in the Molecular System,
         *       false otherwise.
         */
        virtual bool containsFragment(char chainID) const;

        /**
         * @brief Returns the fragment with the associated chainID
         *
         *        If no fragment is found, then it returns a nullptr
         */
        virtual std::vector<FragmentSP> getFragment(char chainID) const;

        /**
         * @brief Returns the total number of fragments
         *        contained in the molecular system.
         */
        virtual size_t getNumFragments() const;





        /* CYCLES */

        /**
         * @brief It returns an iterator to the first cycle of the molecular system.
         */
        virtual cycle_iterator getCyclesBegin(size_t frameNum) const;

        /**
         * @brief It returns an iterator to the past-the-end element of the cycle list.
         */
        virtual cycle_iterator getCyclesEnd(size_t frameNum) const;

        /**
         * @brief It returns the cycle corresponding to the given index
         *
         * @param frameNum The frame number.
         * @param idx      The index of the bonds.
         */
        virtual std::vector<BondSP> getCycle(size_t frameNum, size_t idx);

        /**
         * @brief It clears the previously computed cycles for the given frame number.
         * @param frameNum  The frame number.
         */
        virtual bool clearCycles(size_t frameNum);

        /**
         * @brief It returns the number of cycles at the specified frame.
         * @param frameNum The frame number.
         */
        virtual size_t getNumCycles(size_t frameNum) const;




        /* COMPUTES */

        // COVALENT BONDS
        /**
         * @brief Computes the connectivity of bonds at a certain frame with a specified tolerance.
         *		  The slow method checks the mutual distance between each possible pair of atoms within the molecular system.
         *
         * @param frameNum the frame number.
         * @param tolerance_distance a tolerance parameter for the lenght of bonds.
         * @note  The tolerance recommended value is 0.4.
         * @note  Bond distance is estimated using electronegativities and covalent radii.
         *		   Porterfield, W. W. Inorganic Chemistry; Addison-Wesley: Reading, PA, 1984; p. 167.
         * @note Hydrogen bonded to boron is considered 2 as valence
         */
        virtual void computeBondsInFrame_Slow(size_t frameNum,
                                              double tolerance_distance = 0.4);



        /**
         * @brief Computes the connectivity of bonds at a certain frame with a specified tolerance.
         *		  The fast method checks the mutual distance between each possible pair of atoms
         *		  between an atom and its neighbors through the creation of a AtomsGrid.
         *
         * @param frameNum the frame number.
         * @param tolerance_distance a tolerance parameter for the lenght of bonds.
         * @note  The tolerance recommended value is 0.4.
         * @note  Bond distance is estimated using electronegativities and covalent radii.
         *		   Porterfield, W. W. Inorganic Chemistry; Addison-Wesley: Reading, PA, 1984; p. 167.
         * @note Hydrogen bonded to boron is considered 2 as valence
         */
        virtual void computeBondsInFrame_Fast(size_t frameNum,
                                              double tolerance_distance = 0.4);


        /**
         * @brief Computes the connectivity of bonds for all frames.
         *	      The slow method checks the mutual distance between each possible pair of atoms within the molecular system.
         *
         * @param tolerance_distance a tolerance parameter for the lenght of bonds.
         * @note The tolerance recommended value is 0.4
         * @note Bond distance is estimated using electronegativities and covalent radii.
         *		 Porterfield, W. W. Inorganic Chemistry; Addison-Wesley: Reading, PA, 1984; p. 167.
         * @note Hydrogen bonded to boron is considered 2 as valence
        */
        virtual void computeBonds_Slow(double tolerance_distance = 0.4);

        /**
         * @brief Computes the connectivity of bonds for all frames.
         *		  The fast method checks the mutual distance between each possible pair of atoms
         *		  between an atom and its neighbors through the creation of a AtomsGrid.
         *
         * @param tolerance_distance a tolerance parameter for the lenght of bonds.
         * @note The tolerance recommended value is 0.4
         * @note Bond distance is estimated using electronegativities and covalent radii.
         *		 Porterfield, W. W. Inorganic Chemistry; Addison-Wesley: Reading, PA, 1984; p. 167.
         * @note Hydrogen bonded to boron is considered 2 as valence
        */
        virtual void computeBonds_Fast(double tolerance_distance = 0.4);

        //TODO: Bond Strength
        virtual double computeBondStrength(BondSP bnd, size_t frameNum);




        // HBONDS

        /**
         * @brief Computes hydrogen bonds using statistical parameters.
         * @param frameNum  The frame number.
         * @note Hydrogen Bond Dynamics of Methyl Acetate in Methanol
         *       Marco Pagliai, Francesco Muniz-Miranda, Gianni Cardini,
         *       Roberto Righini, and Vincenzo Schettino
         *       The Journal of Physical Chemistry Letters
         *       2010 1 (19), 2951-2955
         *       DOI: 10.1021/jz1010994
         */
        virtual void computeHBondsInFrame_Slow(size_t frameNum);


        /**
         * @brief Computes hydrogen bonds within a certain frame (by means of AtomsGrid).
         * @param frameNum The frame number.
         */
        virtual void computeHBondsInFrame_Fast(size_t frameNum);

        /**
         * @brief Computes hydrogen bonds for each frame (by means of AtomsGrid).
         */
        virtual void computeHBonds_Fast();

        //TODO
        virtual void computeHBondInFrame_Fast_Parallel(size_t frameNum);

        /**
          * @brief              Computes a ordered vector of HBonds.
          *
          * @param frameNum     The number of the considered frame within the trajectory described by the molecular system.
          *
          * @param out_vector   Output parameter in which a vector of HBonds will be stored.
          *
          */
        virtual void computeHBondVector(size_t frameNum,
                                        std::vector<HBondSP>& out_vector);



        // BOND ORDERS

        /**
         * @brief It computes the bond order as stated in the Peter model.
         *        A continuous quantitative relationship between bond length,
         *        bond order, and electronegativity for homo and heteronuclear bonds.
         *        doi: 10.1021/ed063p123
         * @param frameNum The frame number.
         */
        virtual void computePeterBondOrder(size_t frameNum);

        virtual void normalizeBO(size_t frameNum);

        virtual void normalizeBO_noTerminal(size_t frameNum);

        virtual void computeBondOrders(size_t frameNum);


        // CHARGES

        /**
         * @brief It computes the charges within the molecular system
         *        by using the Gasteiger method.
         * @param frameNum      The frame number.
         * @param numIterations The number of iterations to do while
         *                      computing charges. This is set to 6
         *                      by default.
         *
         * @note  Johann Gasteiger, Mario Marsili,
         *        Iterative partial equalization of orbital electronegativity—a rapid access to atomic charges,
         *        Tetrahedron,
         *        Volume 36, Issue 22,
         *        1980,
         *        Pages 3219-3228,
         *        ISSN 0040-4020,
         *        https://doi.org/10.1016/0040-4020(80)80168-2.
         *        (http://www.sciencedirect.com/science/article/pii/0040402080801682)
         */
        virtual bool computeGasteigerCharges(size_t frameNum, size_t numIterations=6);

        /**
         * @brief This method computes the electrostatic potential at a given frame number.
         *        It uses atomic charges and outputs a CubeGrid object.
         *
         * @param dx       The step along the x direction used for computing the potential.
         * @param dy       The step along the y direction used for computing the potential.
         * @param dz       The step along the z direction used for computing the potential.
         * @param frameNum The frame number.
         */
        virtual CubeGridSP computeElectrostaticPotential(double dx, double dy, double dz, size_t frameNum=0);

        /**
         * @brief It computes the FQ charges.
         * @param frameNum  The frame number
         * @note  It currently support Rappé parameters only.
         */
        virtual void computeFQCharges(size_t frameNum);

        virtual void computeFQCharges_XUFF(size_t frameNum);

        virtual bool computeFQCharges_it(size_t frameNum, size_t numIteration=6);
        /**
         * @brief It computes the dipole moment of the molecular system
         *        as the simple charge weighted sum of the distances.
         * @param frameNum The frame number.
         */
        virtual std::vector<double> computeDipoleMoment(size_t frameNum);



        // CYCLES
        /**
         * @brief It returns a set of bonds that are defined between couples
         *        of cyclic atoms (that is all the bonds that are part of some cycle)
         */
        virtual std::set<BondSP> getCyclicBonds(size_t frameNum);

        /**
         * @brief It computes the cycles within the Molecular System
         *        by using the Horton algorithm.
         * @param frameNum The frame number.
         *
         * @note It returns False if cycles have already been computed.
         *
         * @note Horton, Joseph. (1987).
         *       A Polynomial-Time Algorithm to Find the Shortest Cycle Basis of a Graph.
         *       SIAM J. Comput.. 16. 358-366. 10.1137/0216026.
         */
        virtual bool computeHortonCycles(size_t frameNum);





        //BIARYL

        virtual std::set<BondSP> getBridgeBonds(size_t frameNum);

        //TODO: Trova biarili
        /**
         * @brief It print the input files required by the HTEQ software to work
         * @param bond          The bridge bond
         * @param numBonds      The maximum number of bonds to keep (after that it cuts)
         * @param path          The path where to output files (with the filename but without extension)
         * @param frameNum      The frame number
         */
        virtual void printV2(BondSP bond, size_t numBonds, std::string path="./out", size_t frameNum=0);




        // OTHERS


		/**
		 * @brief	Computes and returns the BoundingBox containing the
		 *			VdW surface of the molecular system at the specified frame.
		 */
        virtual BoundingBox computeBBox(size_t frameNum);

        /**
         * @brief It computes the hybridisation of all the atoms in the system
         * @param frameNum  The frame number
         * @param useCycles A booleane set to true whether the software must use the
         *                  information about cycles when computing hybridisation, false otherwise.
         *                  Setting this parameter to false is recommended when dealing with small
         *                  connected components made by cyclic atom only. The default value,
         *                  for large systems, is set to true.
         * @param tolerance A tolerance factor used in evaluating the similiarity of the
         *                   bond angles with reference values for hybridisation states.
         *                   20 degrees by default.
         */
        virtual void computeHybridisation(size_t frameNum, bool useCycles=false, double tolerance=10);



        /**
         * @brief This method computes the electrostatic potential in a given point at a given frame number.
         */
        virtual double computePotential(std::vector<double> point, size_t frameNum);


        /**
         * @brief It computes the angle between the three atoms given
         * @param centerAtom The serial number of the central atom of the angle.
         * @param atom2      The serial number of another atom.
         * @param atom3      The serial number of another atom.
         * @param frameNum   The frame number.
         */
        virtual double computeAngle(long long centerAtom, long long atom2, long long atom3, size_t frameNum);

        // OUTPUT FILES
        /**
         * @brief It outpus the current Molecular System to a MOL file
         * @param path      The path of the file to output.
         * @param frameNum  The frame number.
         * @note            This implementation outputs the MOL file in the V2000 version.
         */
        virtual void toMOL_V2000(std::string path, size_t frameNum);

        /**
         * @brief It outpus the current Molecular System to a MOL file
         * @param path      The path of the file to output.
         * @param frameNum  The frame number.
         * @note            This implementation outputs the MOL file in the V3000 version.
         */
        virtual void toMOL_V3000(std::string path, size_t frameNum);

        virtual void toMol2(std::string path, size_t frameNum);


        virtual void toMultimodelXYZ(std::string path, std::vector<std::string>& comments);

        virtual void toJSON(std::string path, size_t frameNum=0, bool computeBridges=false);

        /**
         * @brief This method prints a Z-Matrix file for the current system
         * @param frameNum  The frame number.
         * @param path      The path of the output file.
         */
        virtual void printZMatrix(std::string path, size_t frameNum);



    /* INTERNAL METHODS FOR CALCULATIONS */
	protected:

        //BIARYL
        /**
         * @brief It returns the XUFF atom type for the given atom.
         */
        virtual std::string getAtomType_XUFF(long long atm, size_t frameNum);

        /**
         * @brief It performs a BFS for computing biaryl rings.
         */
        virtual void BFS_Biaryl(MolecularSystem& ms, long long origin, long long avoid, size_t numBonds, size_t frameNum);

        /**
         * @brief It checks whether there is a commond bond between two sets of bonds.
         */
        virtual bool isThereOneCommonBond(std::vector<BondSP>& bnd1, std::vector<BondSP>& bnd2);

        /**
         * @brief It looks for commond bonds between two cycles.
         */
        virtual std::vector<BondSP> findBondBetweenCycles(std::vector<BondSP>& bnd1, std::vector<BondSP>& bnd2, size_t frameNum);

        /**
         * @brief It computes the mean torsion angle given a cycle
         * @param cycle    The cycle whose mean torsion angle must be computed.
         * @param frameNum The frame number.
         */
        virtual std::pair<double,std::set<long long>> getMeanTorsion(std::vector<BondSP>& cycle, size_t frameNum) const;




        // BONDS
        /**
         * @brief Checks the distance between two atoms and eventually creates a bond.
         * @param frameNum The frame number.
         * @param atom1 The first atom.
         * @param atom2 The second atom.
         * @param tolerance_distance The tolerance distance.
         */
        virtual void checkDistanceAndCreateBond(size_t frameNum,
                                                AtomSP &atom1, AtomSP &atom2,
                                                double tolerance_distance);


        /**
         * @brief It computes the tolerance factor (taking electronegativity into account).
         * @param frameNum The frame number.
         * @param atom1 The first atom.
         * @param atom2 The second atom.
         * @param tolerance_distance The tolerance distance.
         */
        virtual double computetoleranceFactor(AtomSP atom1, AtomSP atom2,
                                               double tolerance_distance=0.0);

        /**
         * @brief It checks coordination for all of the atoms.
         *        Bonds are removed for those atoms not satisfying
         *        their maximum coordination number.
         * @param frameNum The frame number.
         */
        virtual void maximumCoordination(size_t frameNum);

        /**
         * @brief It checks the coordination number of the specific atom and
         *        eventually removes bonds so as to adjust its maximum coordination number.
         * @param frameNum The frame number.
         * @param atom     The atom.
         */
        virtual void checkCoordinationAndDeleteBond(size_t frameNum, AtomSP atom);

        /**
         * @brief Computes the ratio between the actual
         *        and the theoretical lenght of the bond.
         *
         * @param frameNum The frame number.
         *
         * @param bond     The given bond.
         */
        virtual double computeRatio(size_t frameNum, BondSP bond);






        //BOND ORDERS





        // HBONDS

        /**
         * @brief  Computes the intensity of a HBond
         *         knowing the position and the type of atoms
         *         involved.
		 * @param  frameNum			 The frame number.
		 * @param  hydrogen			 The hydrogen atom.
		 * @param  donor			 The donor atom.
		 * @param  acceptor			 The acceptor atom.
         * @return The intensity of a theoretical HBond.
         */
		virtual double computeHBondIntensity(size_t frameNum,
                                          AtomSP hydrogen, AtomSP donor, AtomSP acceptor);

        /**
		 * @brief This is a support function used for computing HBonds.
		 * @param x		Variable of the function.
		 * @param x_eq	Mean value for x.
		 * @param s		Standard deviation for x.
         */
		virtual double supportFunction_For_HBond(double x, double x_eq, double s);




        //CHARGES

        /**
         * @brief In the context of the Gasteiger method, this method computes the electronegativites for each atom given at a given frame
         */
        virtual std::map<long long,double> computeElectronegativities(size_t frameNum);

        /**
         * @brief This method computes the electronegativity for each atom in the +1 charge state.
         */
        virtual std::map<long long,double> computeMAXElectronegativities(size_t frameNum);


        virtual double getJ_ab_old(AtomSP atom1, AtomSP atom2, size_t frameNum);


        virtual double getJ_ab_xuff(AtomSP atom1, AtomSP atom2, size_t frameNum);


        virtual double getJ_ab(AtomSP atom1, size_t idx1, AtomSP atom2, size_t idx2, size_t frameNum, Eigen::VectorXf oldCharges);
        virtual double getJ_hh(AtomSP atom, double charge);
        virtual void iteration(Eigen::VectorXf& _charges, Eigen::VectorXf C, std::map<int,AtomSP> fromIndexToAtom, size_t frameNum);



        // OTHERS
        /**
         * @brief It computes the hybridisation for an acyclic atom
         * @param frameNum The frame number
         * @param atom The atom
         * @param tolerance A tolerance factor used in evaluating the similiarity of the
         *                   bond angles with reference values for hybridisation states.
         *                   20 degrees by default.
         */
        virtual void computeAtomicHybridisation(size_t frameNum, AtomSP atom, double tolerance=10);

        virtual std::string mol3000(size_t frameNum, std::map<long long, size_t>& fromSerialToNumber, std::map<size_t, long long>& fromNumberToSerial, bool useNewLine=true);


    /* INTERNAL METHODS FOR THE PARSER */
    protected:
        //Only the PDBParser can access this methods
        friend class Parser;

        /**
         * @brief It sets the labels of the frames of the molecular system.
         *
         * @note  Only the Parser can call this method.
         */
        virtual void setLabelVector(std::vector<std::string> vec);


    /* INTERNAL PROPERTIES */
	protected:

        /** @brief Name of the Molecular System */
		std::string _name;

        /** @brief Map containing the atoms of the Molecular System
                   Each atom is associated with its serial number*/
		std::map<long long, AtomSP> _atomsMap;


        /** @brief Map containing the Fragments of the Molecular System
                   Each fragment is associated with its chainID */
        std::multimap<char, FragmentSP> _fragments;

        /**
         * @brief A boolean variable.
         *          If true, charges have been computed.
         *          Otherwise, false.
         *
         */
        bool _charges=false;

        /**
         * @brief The spin multiplicity of the molecular system.
         */
        int _spin_multiplicity=0;


        /**
         * @brief The total charge of the molecular system.
         */
        double _totalCharge;

        std::map<int, double> hardness;
        std::map<int, double> electronegativity;


        /** @brief Frame structure */
		struct Frame
		{
            /**
             * @brief The set of bonds at the current frame.
             */
			std::set<BondSP, BondSPLessFunctor> _bondsSet;

            /**
             * @brief The set of cycles at the current frame.
             */
            std::vector<std::vector<BondSP>> _cyclesSet;

            /**
             * @brief The set of hydrogen bonds at the current frame.
             */
			std::set<HBondSP, HBondSPLessFunctor> _hbondsSet;

            //A set of candidate hydrogen bonds (in the order: [H,Donor])
            std::vector<std::pair<long long, long long>> _candidateHBonds;
		};


        /** @brief Vector of frames */
        std::vector<Frame> _framesList;

        /**
         * @brief Frame labels.
         */
		std::vector<std::string> _frameLabel;
	};

    /**
     * @brief MolecularSystemSP is a smart pointer to a MolecularSystem object.
     */
	typedef std::shared_ptr< MolecularSystem > MolecularSystemSP;


}
#endif // MOLECULARSYSTEM_H
