#ifndef PROXIMA_HBINTERACTION_H
#define PROXIMA_HBINTERACTION_H

#include "./utilities/WarningsOff.h"
#include<map>
#include "./utilities/WarningsOn.h"



/**
 * @author Federico Lazzari
 */
namespace Proxima
{
	/**
     * @brief This class handles the needed physical parameters
     *        for the calculation of hydrogen bonds.
	 */
	class HBInteractions
	{
	public:
		/**
		 * @brief Checks whether an atom can be considered
         *		  donor or acceptor in a general hydrogen bond.
		 * @param atom atomic number
		 */
		static bool canBeDonorOrAcceptor(int atom);

		/**
		 * @brief get r_e for a couple of atoms (donor, acceptor).
         *
         *        r_e is the distance at which the radial distribution function
         *        for hydrogen bonds has the highest peak.
         *
         *        r is the distance between acceptor and hydrogen.
		 * @param atom1 donor atomic number
		 * @param atom2 acceptor atomic number 
		 */
        static double getHBParameter_r_e(int atom1, int atom2);

		/**
		 * @brief get s_r for a couple of atoms (donor, acceptor).
         *
         *        s_r is the half width at half-maximum in the radial distribution function
         *        for hydrogen bonds.
         *
         *        r is the distance between acceptor and hydrogen.
		 * @param atom1 donor atomic number
		 * @param atom2 acceptor atomic number
		 */
        static double getHBParameter_s_r(int atom1, int atom2);

		/**
		 * @brief get theta_e for a couple of atoms (donor, acceptor).
         *
         *        theta_e is the angle at which the angular distribution function
         *        for hydrogen bonds has the highest peak.
         *
         *        theta is the angle between hydrogen, donor and acceptor.
		 * @param atom1 donor atomic number
		 * @param atom2 acceptor atomic number
		 */
        static double getHBParameter_theta_e(int atom1, int atom2);

		/**
		 * @brief get s_theta for a couple of atoms (donor, acceptor).
         *
         *        s_theta is the half width at half-maximum in the
         *        angular distribution function for hydrogen bonds.
         *
         *        theta is the angle between hydrogen, donor and acceptor.
		 * @param atom1 donor atomic number
		 * @param atom2 acceptor atomic number
		 */
        static double getHBParameter_s_theta(int atom1, int atom2);

    private:
        struct HBParameters {
            HBParameters();

            HBParameters(double _R_e, double _s_r, double _theta_e, double _s_theta);
            double r_e;
            double s_r;
            double theta_e;
            double s_theta;
        };

        /**
         * @brief This associates each set of parameters for the hydrogen bond computation to each pair
         *        of atom identified by their atomic numbers.
         */
        static std::map<std::pair<int,int>,HBParameters> parameters;

        /**
         * @brief This method initializes the hydrogen bond parameters.
         */
        static void initHBParameters();


	};
}


#endif // HBINTERACTION_H
