#ifndef PROXIMA_AMINORESIDUE_H
#define PROXIMA_AMINORESIDUE_H
#include "residue.h"

/**
 * @author Federico Lazzari
 */
namespace Proxima
{

/**
 * @brief The AminoResidue class represents amino residues within Proxima.
 */
class AminoResidue : public Proxima::Residue
{

private:

    //Serial number of the atoms of the backbone
    long long _CA;
    long long _C;
    long long _O;
    long long _N;
    long long _HA;
    //GLYCINE ONLY
    long long _HA1;
    long long _HA2;

    //TERMINALS
    long long OXT;

    //Flags which indicate the validity of the atoms of the backbone
    /* e.g. if _is_backbone_CA_Valid is set to false
     * then the serial number _backbone_CA is not valid,
     * which means that associating that number to an Atom object
     * is not valid */
    bool _is_CA_Valid=false;
    bool _is_C_Valid=false;
    bool _is_O_Valid=false;
    bool _is_N_Valid=false;
    bool _is_HA_Valid=false;

    //glycine only
    bool _is_HA1_Valid=false;
    bool _is_HA2_Valid=false;

    //TERMINALS
    bool _is_OXT_Valid=false;


std::vector<long long> side_chain;

std::vector<long long> generic_atoms;


public:

    /**
     * @brief This structure defines the constants to be
     *          passed as "role" parameter of the addAtom()
     *          method in order to denote the specific role
     *          of the atom within the residue.
     */
    enum class AtomRole
    {
        GENERIC_ATOM = 0, // Denotes a generic atom.
        BACKBONE_N, // Denotes the nitrogen atom of the amino group.
        BACKBONE_CA, // Denotes the carbon-alpha atom.
        BACKBONE_C, // Denotes the carbon atom of the carboxyl group.
        BACKBONE_O, // Denotes the oxygen atom of the carboxyl group.
        BACKBONE_HA,
        //GLYCINE ONLY
        BACKBONE_HA1,
        BACKBONE_HA2,

        //TERMINALS
        BACKBONE_OXT,  //Denotes the terminal oxygen atom
        SIDE_CHAIN
    };

    /**
     * @brief Constructor
     * @param sequenceNumber The sequence number of the new residue within the polypeptide.
     *                       Sequence number must be unique!
     * @param name  The name of this residue.
     *              Usually this name is not unique, since denotes its type.
     *              If an empty string is passed as parameter,
     *              Residue::DEFAULT_NAME is used.
     */
    AminoResidue(long long serial, long long sequenceNumber, char iCode=0, const std::string& name=Residue::DEFAULT_NAME);


    /**
     * @brief Destructor
     */
    virtual ~AminoResidue() {}

    virtual void clone(std::shared_ptr<AminoResidue> &other);

    /**
     * @brief Adds an atom to the residue.
     *          If one of the following conditions happens, the insertion fails
     *          and this method returns false:
     *
     *          - The residue already contains an atom with the same serial number.
     *
     *          - The atom can't be employed in the specified role.
     *
     * @param atom The serial number of the atom to add to the residue.
     *
     * @param role The role of the atom within the aminoresidue.
     *
     * @return True if the operation succeeds, false otherwise.
     *
     * @throw std::invalid_argument if the smart pointer passed as parameter is NULL
     */
    virtual bool addAtom(long long serial, int role = int(AtomRole::GENERIC_ATOM)) override;

    /**
     * @brief Returns the data related to the carbon-alpha atom.
     */
    virtual AtomSP getAlphaCarbon() const;

    /**
     * @brief Returns the data related to the oxygen atom of the carboxyl group.
     */
    virtual AtomSP getOxygen() const;

    /**
     * @brief Returns the data related to the carbon atom of the carboxyl group.
     */
    virtual AtomSP getCarbon() const;

    /**
     * @brief Returns the data related to the nitrogen atom of the amino group.
     */
    virtual AtomSP getNitrogen() const;

    /**
     * @brief It returns all the atoms of the backbone
     */
    virtual std::vector<AtomSP> getBackbone() const;

    /**
     * @brief It returns all the atoms of the side chain
     */
    virtual std::vector<AtomSP> getSideChain() const;

    /**
     * @brief It returns all of the extra atoms that does not have
     *        specific roles within the residue
     */
    virtual std::vector<AtomSP> getGenericAtoms() const;

};

/** @brief AminoResidueSP is a smart pointer to an AminoResidue object. */
typedef std::shared_ptr< Proxima::AminoResidue > AminoResidueSP;

}
#endif // AMINORESIDUE_H
