#ifndef PROXIMA_HBOND_H
#define PROXIMA_HBOND_H

#include "./utilities/WarningsOff.h"
#include<memory>
#include<tuple>
#include "./utilities/WarningsOn.h"

/**
 * @author Federico Lazzari
 */
namespace Proxima
{

/**
 * @brief This class defines the internal structure of a Hydrogen Bond object.
 *
 *		  A Hydrogen Bond is represented by an object called HBond which contains
 *		  serial numbers of the donor, the acceptor and the hydrogen atoms with
 *		  their relative alternate locations and the force of the hydrogen bond.
 */
class HBond
{
public:

	// Constructor and destructor

    /**
     * @brief Constructor.
	 * @param H					Serial number of the hydrogen atom.
	 * @param donor				Serial number of the atom covalently bonded to hydrogen atom.
	 * @param acceptor			Serial number of the atom hydrogen bonded to the donor.
	 * @param force				A double indicating the intensity of the hydrogen bond.
	 *							Values lower than 10e-4 are considered as zeros and an exception is thrown.
     */
    HBond(long long H, long long donor, long long acceptor, double _force);


	/** @brief Destructor. */
	virtual ~HBond() = default;

    //HBond(const HBond &other);
    virtual void clone(std::shared_ptr<HBond> &other);

	// "Get" methods

    /** @brief Gets the serial number of the hydrogen atom involved into the hydrogen bond. */
    virtual long long getHydrogen() const;

	/** @brief Gets the serial number of the donor atom covalently bonded to the hydrogen.*/
	virtual long long getDonor() const;

	/** @brief Gets the serial number of the acceptor atom hydrogen bonded to the donor.*/
	virtual long long getAcceptor() const;

    /** @brief Gets the force of the hydrogen bond. */
    virtual double getForce() const;

    /* Copy/move constructors/operators are disabled */
    HBond(const HBond& other) = delete;
	HBond(HBond&& other) = delete;
    HBond& operator=(const HBond& other) = delete;
    HBond& operator=(HBond&& other) = delete;

    /**
     * @brief This function compares two HBondSP objects not considering
     *		  the force value of the hydrogen bond.
     * @param other HBondSP object
     */
	bool almostEqual(const HBond& other) const;

	/** @brief Operator == for the HBond object */
    bool isEqual(const HBond& other) const;


protected:

    /** @brief Serial number of the hydrogen atom */
    long long _hydrogen;
    /** @brief Serial number of the donor atom */
    long long _donor;
    /** @brief Serial number of the acceptor atom */
    long long _acceptor;
    /** @brief Intensity of the HBond */
    double _force;

};

/** @brief HBondSP is a smart pointer to an HBond object. */
typedef std::shared_ptr< HBond > HBondSP;

/** @brief  Functor implementing a "less then" operator between two HBondSP objects.
*			This functor has been designed to be used to store HBondSP objects in
*			sorted data structures (e.g. std::map or std::set).
*
*			The comparison is performed by comparing the serial number of the donors,
*			or (if the two donors are equal) by comparing the serial number of the acceptors or
*			(if the two acceptors are equal) by comparing the serial number of hydrogens.
*
*			Since this functor has been designed to be used to store HBondSP objects in sorted data structures,
*			it does not take into account the force factor of the hydrogen bonds. In fact, two hydrogen bonds
*			bonding the same atoms must be recognised as equals, regardless of their intensity.
 */
class HBondSPLessFunctor
{
public:
	bool operator() (const HBondSP& hbond1, const HBondSP& hbond2) const
	{
		if(hbond1->getDonor()			  <	hbond2->getDonor())				return true;
		if(hbond1->getDonor()			  >	hbond2->getDonor())				return false;
		if(hbond1->getAcceptor()		  <	hbond2->getAcceptor())			return true;
		if(hbond1->getAcceptor()		  >	hbond2->getAcceptor())			return false;
		if(hbond1->getHydrogen()          <	hbond2->getHydrogen())			return true;
		if(hbond1->getHydrogen()		  >	hbond2->getHydrogen())			return false;
		return false;
	}
};

}
#endif // HBOND_H
