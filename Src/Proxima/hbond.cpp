
//#include "utilities/WarningsOff.h"
#include <set>
#include <stdexcept>
//#include "utilities/WarningsOn.h"

#include "utilities/floating_point.h"
#include "hbond.h"

using namespace Proxima;

HBond::HBond(long long H, long long donor, long long acceptor, double force)
{
    // NOTE: Parameters are checked by MolecularSystem.

	if(H == donor || H == acceptor || donor == acceptor)
		throw std::invalid_argument("HBond::HBond - Atoms serials are equals!");

    if(force <= 10e-4)
        throw std::invalid_argument("HBond::HBond - Invalid force parameter!");

	_hydrogen = H;
	_donor = donor;
	_acceptor = acceptor;
    _force = force;

}

void HBond::clone(std::shared_ptr<HBond> &other)
{
    this->_hydrogen = other->_hydrogen;
    this->_donor    = other->_donor;
    this->_acceptor = other->_acceptor;
    this->_force    = other->_force;
}

//HBond::HBond(const HBond &other)
//{
//    this->_hydrogen = other._hydrogen;
//    this->_donor    = other._donor;
//    this->_acceptor = other._acceptor;
//    this->_force    = other._force;

//}

long long HBond::getHydrogen() const
{
	return _hydrogen;
}

long long HBond::getDonor() const
{
	return _donor;
}

long long HBond::getAcceptor() const
{
	return _acceptor;
}

double HBond::getForce() const
{
    return _force;
}

bool HBond::almostEqual(const HBond &other) const
{
	return (this->getDonor()			== other.getDonor()			     &&
			this->getAcceptor()			== other.getAcceptor()			 &&
            this->getHydrogen()			== other.getHydrogen()			 );
}



bool HBond::isEqual(const HBond& other) const
{
	if(this->getDonor()				== other.getDonor()				&&
	   this->getAcceptor()			== other.getAcceptor()			&&
	   this->getHydrogen()			== other.getHydrogen()			&&

       fuzzyEqual(this->getForce(),other.getForce())	) return true;
	else return false;
}



