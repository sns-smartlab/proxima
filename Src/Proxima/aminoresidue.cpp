#include "aminoresidue.h"
#include "residue.h"
#include "molecularsystem.h"

using namespace Proxima;

AminoResidue::AminoResidue(long long serial, long long sequenceNumber, char iCode, const std::string &name) :
    Residue(serial,sequenceNumber,iCode,name) {}

void AminoResidue::clone(std::shared_ptr<AminoResidue> &other)
{
    this->_serial = other->_serial;
    this->_sequenceNumber = other->_sequenceNumber;
    this->_insertionCode = other->_insertionCode;
    this->_name = other->_name;
    this->_atomsSerials = other->_atomsSerials;

    this->side_chain = other->side_chain;
    this->generic_atoms = other->generic_atoms;
    this->_CA   = other->_CA;
    this->_C    = other->_C;
    this->_O    = other->_O;
    this->_N    = other->_N;
    this->_HA   = other->_HA;
    this->_HA1  = other->_HA1;
    this->_HA2  = other->_HA2;

    this->OXT = other->OXT;
    this->_is_CA_Valid  = other->_is_CA_Valid;
    this->_is_C_Valid   = other->_is_C_Valid;
    this->_is_O_Valid   = other->_is_O_Valid;
    this->_is_N_Valid   = other->_is_N_Valid;
    this->_is_HA_Valid  = other->_is_HA_Valid;
    this->_is_HA1_Valid = other->_is_HA1_Valid;
    this->_is_HA2_Valid = other->_is_HA2_Valid;
    this->_is_OXT_Valid = other->_is_OXT_Valid;
}

bool AminoResidue::addAtom(long long serial, int role)
{
    //Verifying whether the given role is allowed.
    if (role==int(AminoResidue::AtomRole::BACKBONE_N))
    {
        if (this->_is_N_Valid) {return false;}
        bool addition = Residue::addAtom(serial,role);
        if (addition)
        {
            this->_is_N_Valid = true;
            this->_N = serial;
            return true;
        } else return false;
    }

    if (role==int(AminoResidue::AtomRole::BACKBONE_C))
    {
        if (this->_is_C_Valid) {return false;}
        bool addition = Residue::addAtom(serial,role);
        if (addition)
        {
            this->_is_C_Valid = true;
            this->_C = serial;
            return true;
        } else return false;
    }

    if (role==int(AminoResidue::AtomRole::BACKBONE_CA))
    {
        if (this->_is_CA_Valid) {return false;}
        bool addition = Residue::addAtom(serial,role);
        if (addition)
        {
            this->_is_CA_Valid = true;
            this->_CA = serial;
            return true;
        } else return false;
    }

    if (role==int(AminoResidue::AtomRole::BACKBONE_O))
    {
        if (this->_is_O_Valid) {return false;}
        bool addition = Residue::addAtom(serial,role);
        if (addition)
        {
            this->_is_O_Valid = true;
            this->_O = serial;
            return true;
        } else return false;
    }

    if (role==int(AminoResidue::AtomRole::BACKBONE_HA))
    {
        if (this->_is_HA_Valid) {return false;}
        bool addition = Residue::addAtom(serial,role);
        if (addition)
        {
            this->_is_HA_Valid = true;
            this->_HA = serial;
            return true;
        } else return false;
    }



    if (role==int(AminoResidue::AtomRole::BACKBONE_HA1))
    {
        if (this->_is_HA1_Valid) {return false;}
        bool addition = Residue::addAtom(serial,role);
        if (addition)
        {
            this->_is_HA1_Valid = true;
            this->_HA1 = serial;
            return true;
        } else return false;
    }

    if (role==int(AminoResidue::AtomRole::BACKBONE_HA2))
    {
        if (this->_is_HA2_Valid) {return false;}
        bool addition = Residue::addAtom(serial,role);
        if (addition)
        {
            this->_is_HA2_Valid = true;
            this->_HA2 = serial;
            return true;
        } else return false;
    }

    if (role==int(AminoResidue::AtomRole::BACKBONE_OXT))
    {
        if (this->_is_OXT_Valid) {return false;}
        bool addition = Residue::addAtom(serial,role);
        if (addition)
        {
            this->_is_OXT_Valid = true;
            this->OXT = serial;
            return true;
        } else return false;
    }


    if (role==int(AminoResidue::AtomRole::SIDE_CHAIN))
    {
        this->side_chain.push_back(serial);
        return Residue::addAtom(serial,role);
    }

    else
    {
        this->generic_atoms.push_back(serial);
        return Residue::addAtom(serial,role);
    }

}

AtomSP AminoResidue::getAlphaCarbon() const
{
    if (!this->_is_CA_Valid) return nullptr;

    long long serial = this->_CA;

    MolecularSystem* molSys = this->getParentMolecularSystem();

    return molSys->getAtom(serial);
}


AtomSP AminoResidue::getOxygen() const
{
    if (!this->_is_O_Valid) return nullptr;

    long long serial = this->_O;

    MolecularSystem* molSys = this->getParentMolecularSystem();

    return molSys->getAtom(serial);

}

AtomSP AminoResidue::getCarbon() const
{
    if (!this->_is_C_Valid) return nullptr;

    long long serial = this->_C;

    MolecularSystem* molSys = this->getParentMolecularSystem();

    return molSys->getAtom(serial);

}

AtomSP AminoResidue::getNitrogen() const
{
    if (!this->_is_N_Valid) return nullptr;

    long long serial = this->_N;

    MolecularSystem* molSys = this->getParentMolecularSystem();

    return molSys->getAtom(serial);

}

std::vector<AtomSP> AminoResidue::getBackbone() const
{
    std::vector<AtomSP> out;

    MolecularSystem* molSys = this->getParentMolecularSystem();

    if (this->_is_O_Valid)
    {
        long long O = this->_O;
        out.push_back(molSys->getAtom(O));
    }


    if (this->_is_N_Valid)
    {
        long long N = this->_N;
        out.push_back(molSys->getAtom(N));
    }

    if (this->_is_C_Valid)
    {
        long long C = this->_C;
        out.push_back(molSys->getAtom(C));
    }

    if (this->_is_CA_Valid)
    {
        long long C = this->_CA;
        out.push_back(molSys->getAtom(C));
    }

    if (this->_is_HA_Valid)
    {
        long long HA = this->_HA;
        out.push_back(molSys->getAtom(HA));
    }


    if (this->_is_HA1_Valid)
    {
        long long HA1 = this->_HA1;
        out.push_back(molSys->getAtom(HA1));
    }

    if (this->_is_HA2_Valid)
    {
        long long HA2 = this->_HA2;
        out.push_back(molSys->getAtom(HA2));
    }

    if (this->_is_OXT_Valid)
    {
        long long HA2 = this->OXT;
        out.push_back(molSys->getAtom(HA2));
    }

    return out;
}

std::vector<AtomSP> AminoResidue::getSideChain() const
{
    std::vector<AtomSP> out;

    MolecularSystem* molSys = this->getParentMolecularSystem();

    for (auto it=this->side_chain.begin(); it!=this->side_chain.end(); it++)
    {
        long long serial = *it;
        AtomSP atom = molSys->getAtom(serial);
        out.push_back(atom);
    }

    return out;
}

std::vector<AtomSP> AminoResidue::getGenericAtoms() const
{

    std::vector<AtomSP> out;

    MolecularSystem* molSys = this->getParentMolecularSystem();

    for (auto it=this->generic_atoms.begin(); it!=this->generic_atoms.end(); it++)
    {
        long long serial = *it;
        AtomSP atom = molSys->getAtom(serial);
        out.push_back(atom);
    }

    return out;

}

