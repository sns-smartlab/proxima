#include "atomsgrid.h"
#include "molecularsystem.h"

using namespace Proxima;

const double AtomsGrid::GRID_SIDE_LENGTH = 4.0;

AtomsGrid::AtomsGrid(MolecularSystem* const molecular_system, size_t frameNum)
{
    BoundingBox box = molecular_system->computeBBox(frameNum);
    _origin = std::vector<double>{box.minX, box.minY, box.minZ};

	unsigned Nx = unsigned( std::max( std::ceil((box.maxX-box.minX)/GRID_SIDE_LENGTH), 1.0) );
	unsigned Ny = unsigned( std::max( std::ceil((box.maxY-box.minY)/GRID_SIDE_LENGTH), 1.0) );
	unsigned Nz = unsigned( std::max( std::ceil((box.maxZ-box.minZ)/GRID_SIDE_LENGTH), 1.0) );

	_numCells[0]=Nx;
	_numCells[1]=Ny;
	_numCells[2]=Nz;
    _frameNumber = frameNum;

	_grid.resize(Nx);
	for(unsigned i=0; i < Nx; i++)
	{
		_grid[i].resize(Ny);
		for (unsigned j=0; j < Ny;j++)
		{
			_grid[i][j].resize(Nz);
		}
	}

    /* Filling grid */
    for (auto atom = molecular_system->atomsBegin(); atom!=molecular_system->atomsEnd(); atom++)
    {
            std::vector<double> position = atom->second->getPosition(frameNum);

			unsigned idx_x = unsigned((position[0]-box.minX)/GRID_SIDE_LENGTH);
			unsigned idx_y = unsigned((position[1]-box.minY)/GRID_SIDE_LENGTH);
			unsigned idx_z = unsigned((position[2]-box.minZ)/GRID_SIDE_LENGTH);
            _grid[idx_x][idx_y][idx_z].addAtom(atom->second);

    }
}

std::vector<double> AtomsGrid::getOrigin() const
{
    return _origin;
}

unsigned AtomsGrid::getNumCellsX() const
{
    return _numCells[0];
}

unsigned AtomsGrid::getNumCellsY() const
{
    return _numCells[1];
}

unsigned AtomsGrid::getNumCellsZ() const
{
    return _numCells[2];
}


std::array<unsigned,3> AtomsGrid::getCellIndex(AtomSP atom) const
{
    std::vector<double> position = atom->getPosition(_frameNumber);

	if (position[0] < _origin[0] || position[1] < _origin[1] || position[2] < _origin[2])
		throw std::invalid_argument("AtomsGrid::getNumCell - Atom is not in the grid.");

    unsigned nx = unsigned(((position[0]-_origin[0])/GRID_SIDE_LENGTH));
    unsigned ny = unsigned(((position[1]-_origin[1])/GRID_SIDE_LENGTH));
    unsigned nz = unsigned(((position[2]-_origin[2])/GRID_SIDE_LENGTH));

	if (nx >= getNumCellsX() || ny >= getNumCellsY() || nz >= getNumCellsZ())
		throw std::invalid_argument("AtomsGrid::getNumCell - Atom is not in the grid.");

	std::array<unsigned,3> index;
	index[0]=nx;
	index[1]=ny;
	index[2]=nz;

	return index;
}

std::vector<AtomSP> AtomsGrid::getAtomsInCell(unsigned x, unsigned y, unsigned z) const
{
    if (x >= getNumCellsX() || y >= getNumCellsY() || z >= getNumCellsZ())
        throw std::invalid_argument("AtomsGrid::getAtomsInCell - Invalid cell index.");

    return _grid[x][y][z].getAtoms();
}

std::vector<AtomSP> AtomsGrid::getAtomsInNeighborsCells(unsigned x, unsigned y, unsigned z) const
{

    std::vector<AtomSP> output;

    unsigned minX = (x>0) ? x-1 : 0;
    unsigned maxX = (x<(getNumCellsX()-1)) ? x+1 : x;
    unsigned minY = (y>0) ? y-1 : 0;
    unsigned maxY = (y<(getNumCellsY()-1)) ? y+1 : y;
    unsigned minZ = (z>0) ? z-1 : 0;
    unsigned maxZ = (z<(getNumCellsZ()-1)) ? z+1 : z;

    for (auto _x = minX; _x <= maxX; _x++)
    {
        for(auto _y=minY; _y <= maxY;_y++)
        {
            for(auto _z=minZ; _z <= maxZ; _z++)
            {
                for(const AtomSP& atomCell : _grid[_x][_y][_z].getAtoms())
                {
                        output.push_back(atomCell);
                }
            }
        }
    }

    return output;

}

//std::vector<BondSP> AtomsGrid::getBondsInCell(unsigned x, unsigned y, unsigned z) const
//{
//    std::vector<AtomSP> atoms = this->getAtomsInCell(x,y,z);

//    for (auto it1=atoms.begin(); it1!=std::prev(atoms.end(),1); it1++)
//    {
//        AtomSP atom1 = *it1;

//        auto it2=std::next(it1,1);
//        for (; it2!=atoms.end(); it2++)
//        {
//            AtomSP atom2=*it2;


//        }
//    }
//}

std::vector<AtomSP> AtomsGrid::getNeighbors(AtomSP atom) const
{
    std::vector<AtomSP> neighbors;

    std::array<unsigned,3> atomPos = getCellIndex(atom);

	/* Finding all possible cells */
	unsigned minX = (atomPos[0]>0) ? atomPos[0]-1 : 0;
	unsigned maxX = (atomPos[0]<(getNumCellsX()-1)) ? atomPos[0]+1 : atomPos[0];
	unsigned minY = (atomPos[1]>0) ? atomPos[1]-1 : 0;
	unsigned maxY = (atomPos[1]<(getNumCellsY()-1)) ? atomPos[1]+1 : atomPos[1];
	unsigned minZ = (atomPos[2]>0) ? atomPos[2]-1 : 0;
	unsigned maxZ = (atomPos[2]<(getNumCellsZ()-1)) ? atomPos[2]+1 : atomPos[2];
	for (auto x = minX; x <= maxX; x++)
	{
		for(auto y=minY; y <= maxY;y++)
		{
			for(auto z=minZ; z <= maxZ; z++)
			{
                for(const AtomSP& atomCell : _grid[x][y][z].getAtoms())
				{
                    if(atomCell->getSerial() != atom->getSerial())
                        neighbors.push_back(atomCell);
				}
			}
		}
	}

//    std::sort(neighbors.begin(),neighbors.end());
	return neighbors;
}



