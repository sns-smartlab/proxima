#include<algorithm>
#include<map>

#include "atomicproperties.h"
#include <math.h>

using namespace Proxima;

bool Element::atomicPropetiesInitialized = false;

std::map<std::string, int> Element::atomicNumbers;
Element::Parameters Element::atomicProperties[119];
std::map<int,double> Element::qeqHardness;
std::map<int,double> Element::qeqPotential;
std::map<int,double> Element::singleBondRadii;
std::map<int,double> Element::doubleBondRadii;
std::map<int,double> Element::tripleBondRadii;

std::map<std::string, double> Element::xuff_hardness;
std::map<std::string, double> Element::xuff_electronegativity;

std::map<int,double> Element::boParameters_a;
std::map<int,double> Element::boParameters_b;

std::map<int, double> Element::boParameters_Morse;

Element::Parameters::Parameters()
{
    symbol="";
    waalsRadius=0;
    covalentRadius=0;
    electronegativity=0;
    coordination=0;
    valence=0;
    //TODO: Colore di default? O 0 zero come qualunque variabile
    color = std::array<float,3>{ {0,0,0} };

}

Element::Parameters::Parameters(const std::string& pSymbol,
                                double pWaalsRadius,
                                double pCovalentRadius,
                                double pElectronegativity,
                                unsigned pCoordination,
                                unsigned pValence,
                                const std::array<float,3>& pColor)
{
    symbol=pSymbol;
    waalsRadius=pWaalsRadius;
    covalentRadius=pCovalentRadius;
    electronegativity=pElectronegativity;
    coordination = pCoordination;
    valence = pValence;
    color = pColor;
}

void Element::initAtomicNumbers()
{
    if(!Element::atomicNumbers.empty()) return;

    Element::atomicNumbers["H"]=1;
    Element::atomicNumbers["HE"]=2;
    Element::atomicNumbers["LI"]=3;
    Element::atomicNumbers["BE"]=4;
    Element::atomicNumbers["B"]=5;
    Element::atomicNumbers["C"]=6;
    Element::atomicNumbers["N"]=7;
    Element::atomicNumbers["O"]=8;
    Element::atomicNumbers["F"]=9;
    Element::atomicNumbers["NE"]=10;
    Element::atomicNumbers["NA"]=11;
    Element::atomicNumbers["MG"]=12;
    Element::atomicNumbers["AL"]=13;
    Element::atomicNumbers["SI"]=14;
    Element::atomicNumbers["P"]=15;
    Element::atomicNumbers["S"]=16;
    Element::atomicNumbers["CL"]=17;
    Element::atomicNumbers["AR"]=18;
    Element::atomicNumbers["K"]=19;
    Element::atomicNumbers["CA"]=20;
    Element::atomicNumbers["SC"]=21;
    Element::atomicNumbers["TI"]=22;
    Element::atomicNumbers["V"]=23;
    Element::atomicNumbers["CR"]=24;
    Element::atomicNumbers["MN"]=25;
    Element::atomicNumbers["FE"]=26;
    Element::atomicNumbers["CO"]=27;
    Element::atomicNumbers["NI"]=28;
    Element::atomicNumbers["CU"]=29;
    Element::atomicNumbers["ZN"]=30;
    Element::atomicNumbers["GA"]=31;
    Element::atomicNumbers["GE"]=32;
    Element::atomicNumbers["AS"]=33;
    Element::atomicNumbers["SE"]=34;
    Element::atomicNumbers["BR"]=35;
    Element::atomicNumbers["KR"]=36;
    Element::atomicNumbers["RB"]=37;
    Element::atomicNumbers["SR"]=38;
    Element::atomicNumbers["Y"]=39;
    Element::atomicNumbers["ZR"]=40;
    Element::atomicNumbers["NB"]=41;
    Element::atomicNumbers["MO"]=42;
    Element::atomicNumbers["TC"]=43;
    Element::atomicNumbers["RU"]=44;
    Element::atomicNumbers["RH"]=45;
    Element::atomicNumbers["PD"]=46;
    Element::atomicNumbers["AG"]=47;
    Element::atomicNumbers["CD"]=48;
    Element::atomicNumbers["IN"]=49;
    Element::atomicNumbers["SN"]=50;
    Element::atomicNumbers["SB"]=51;
    Element::atomicNumbers["TE"]=52;
    Element::atomicNumbers["I"]=53;
    Element::atomicNumbers["XE"]=54;
    Element::atomicNumbers["CS"]=55;
    Element::atomicNumbers["BA"]=56;
    Element::atomicNumbers["LA"]=57;
    Element::atomicNumbers["CE"]=58;
    Element::atomicNumbers["PR"]=59;
    Element::atomicNumbers["ND"]=60;
    Element::atomicNumbers["PM"]=61;
    Element::atomicNumbers["SM"]=62;
    Element::atomicNumbers["EU"]=63;
    Element::atomicNumbers["GD"]=64;
    Element::atomicNumbers["TB"]=65;
    Element::atomicNumbers["DY"]=66;
    Element::atomicNumbers["HO"]=67;
    Element::atomicNumbers["ER"]=68;
    Element::atomicNumbers["TM"]=69;
    Element::atomicNumbers["YB"]=70;
    Element::atomicNumbers["LU"]=71;
    Element::atomicNumbers["HF"]=72;
    Element::atomicNumbers["TA"]=73;
    Element::atomicNumbers["W"]=74;
    Element::atomicNumbers["RE"]=75;
    Element::atomicNumbers["OS"]=76;
    Element::atomicNumbers["IR"]=77;
    Element::atomicNumbers["PT"]=78;
    Element::atomicNumbers["AU"]=79;
    Element::atomicNumbers["HG"]=80;
    Element::atomicNumbers["TL"]=81;
    Element::atomicNumbers["PB"]=82;
    Element::atomicNumbers["BI"]=83;
    Element::atomicNumbers["PO"]=84;
    Element::atomicNumbers["AT"]=85;
    Element::atomicNumbers["RN"]=86;
    Element::atomicNumbers["FR"]=87;
    Element::atomicNumbers["RA"]=88;
    Element::atomicNumbers["AC"]=89;
    Element::atomicNumbers["TH"]=90;
    Element::atomicNumbers["PA"]=91;
    Element::atomicNumbers["U"]=92;
    Element::atomicNumbers["NP"]=93;
    Element::atomicNumbers["PU"]=94;
    Element::atomicNumbers["AM"]=95;
    Element::atomicNumbers["CM"]=96;
    Element::atomicNumbers["BK"]=97;
    Element::atomicNumbers["CF"]=98;
    Element::atomicNumbers["ES"]=99;
    Element::atomicNumbers["FM"]=100;
    Element::atomicNumbers["MD"]=101;
    Element::atomicNumbers["NO"]=102;
    Element::atomicNumbers["LR"]=103;
    Element::atomicNumbers["RF"]=104;
    Element::atomicNumbers["DB"]=105;
    Element::atomicNumbers["SG"]=106;
    Element::atomicNumbers["BH"]=107;
    Element::atomicNumbers["HS"]=108;
    Element::atomicNumbers["MT"]=109;
    Element::atomicNumbers["DS"]=110;
    Element::atomicNumbers["RG"]=111;
    Element::atomicNumbers["CN"]=112;
    Element::atomicNumbers["UUT"]=113;
    Element::atomicNumbers["FL"]=114;
    Element::atomicNumbers["UUP"]=115;
    Element::atomicNumbers["LV"]=116;
    Element::atomicNumbers["UUS"]=117;
    Element::atomicNumbers["UUO"]=118;
}

std::array<float,3> toFloatColor(unsigned char r, unsigned char g, unsigned char b)
{
    return {{float(r)/255.0f, float(g)/255.0f, float(b)/255.0f}};
}

void Element::initAtomicProperties()
{
    if(Element::atomicPropetiesInitialized) return;

    //Fake atom
    Element::atomicProperties[0]=Element::Parameters("UNK",    0.73, 0,    0,    0, 0, toFloatColor(255,20,147) );

	//Elements
    //atomicProperties = (Symbol, WaalsRadius, CovalentRadius, Electronegativity, Coordination)
    Element::atomicProperties[1]=Element::Parameters("H",	1.20, 0.31, 2.1,  1, 1,  toFloatColor(255,255,255) );
    Element::atomicProperties[2]=Element::Parameters("HE",  1.43, 0.28, 4.16, 4, 4, toFloatColor(217,255,255) );
    Element::atomicProperties[3]=Element::Parameters("LI",  2.12, 1.28, 0.97, 1, 1, toFloatColor(204,128,255) );
    Element::atomicProperties[4]=Element::Parameters("BE",  1.98, 0.96, 1.47, 4, 4, toFloatColor(194,255,  0) );
    Element::atomicProperties[5]=Element::Parameters("B",	1.91, 0.84, 2.01, 4, 4, toFloatColor(255,181,181) );
    Element::atomicProperties[6]=Element::Parameters("C",	1.77, 0.76, 2.50, 4, 4, toFloatColor(144,144,144) );
    Element::atomicProperties[7]=Element::Parameters("N",	1.66, 0.71, 3.07, 4, 3, toFloatColor( 48, 80,248) );
    Element::atomicProperties[8]=Element::Parameters("O",	1.50, 0.66, 3.50, 2, 2, toFloatColor(255, 13, 13) ); //Coordination 3?
    Element::atomicProperties[9]=Element::Parameters("F",	1.46, 0.57, 4.10, 1, 1, toFloatColor(144,224, 80) );
    Element::atomicProperties[10]=Element::Parameters("NE", 1.58, 0.58, 4.78, 4, 4, toFloatColor(179,227,245) );
    Element::atomicProperties[11]=Element::Parameters("NA", 2.50, 1.66, 1.01, 1, 1, toFloatColor(171,92,242 ) );
    Element::atomicProperties[12]=Element::Parameters("MG", 2.51, 1.41, 1.23, 4, 4, toFloatColor(138,255,0  ) );
    Element::atomicProperties[13]=Element::Parameters("AL", 2.25, 1.21, 1.47, 4, 4, toFloatColor(191,166,166) );
    Element::atomicProperties[14]=Element::Parameters("SI", 2.19, 1.11, 1.74, 4, 4, toFloatColor(240,200,160) );
    Element::atomicProperties[15]=Element::Parameters("P",  1.90, 1.07, 2.06, 4, 4, toFloatColor(255,128,0  ) );
    Element::atomicProperties[16]=Element::Parameters("S",  1.89, 1.05, 2.44, 4, 4, toFloatColor(255,255,48 ) );
    Element::atomicProperties[17]=Element::Parameters("CL", 1.82, 1.02, 2.83, 1, 1, toFloatColor(31,240,31  ) );
    Element::atomicProperties[18]=Element::Parameters("AR", 1.83, 1.06, 3.24, 4, 4, toFloatColor(128,209,227) );
    Element::atomicProperties[19]=Element::Parameters("K",  2.73, 2.03, 0.91, 1, 1, toFloatColor(143,64,212 ) );
    Element::atomicProperties[20]=Element::Parameters("CA", 2.62, 1.76, 1.04, 6, 6, toFloatColor(61,255,0   ) );
    Element::atomicProperties[21]=Element::Parameters("SC", 2.58, 1.70, 1.20, 4, 4, toFloatColor(230,230,230) );
    Element::atomicProperties[22]=Element::Parameters("TI", 2.46, 1.60, 1.32, 6, 6, toFloatColor(191,194,199) );
    Element::atomicProperties[23]=Element::Parameters("V",  2.42, 1.53, 1.45, 4, 4, toFloatColor(166,166,171) );
    Element::atomicProperties[24]=Element::Parameters("CR", 2.45, 1.39, 1.56, 6, 6, toFloatColor(138,153,199) );
    Element::atomicProperties[25]=Element::Parameters("MN", 2.45, 1.39, 1.60, 6, 6, toFloatColor(156,122,199) );
    Element::atomicProperties[26]=Element::Parameters("FE", 2.44, 1.32, 1.64, 6, 6, toFloatColor(224,102,51 ) );
    Element::atomicProperties[27]=Element::Parameters("CO", 2.40, 1.26, 1.70, 6, 6, toFloatColor(240,144,160) );
    Element::atomicProperties[28]=Element::Parameters("NI", 2.40, 1.24, 1.75, 4, 4, toFloatColor(80,208,80  ) );
    Element::atomicProperties[29]=Element::Parameters("CU", 2.38, 1.32, 1.75, 4, 4, toFloatColor(200,128,51 ) );
    Element::atomicProperties[30]=Element::Parameters("ZN", 2.39, 1.22, 1.66, 4, 4, toFloatColor(125,128,176) );
    Element::atomicProperties[31]=Element::Parameters("GA", 2.32, 1.22, 1.82, 4, 4, toFloatColor(194,143,143) );
    Element::atomicProperties[32]=Element::Parameters("GE", 2.29, 1.20, 2.02, 4, 4, toFloatColor(102,143,143) );
    Element::atomicProperties[33]=Element::Parameters("AS", 1.88, 1.19, 2.20, 4, 4, toFloatColor(189,128,227) );
    Element::atomicProperties[34]=Element::Parameters("SE", 1.82, 1.20, 2.48, 4, 4, toFloatColor(255,161,0  ) );
    Element::atomicProperties[35]=Element::Parameters("BR", 1.86, 1.20, 2.74, 1, 1, toFloatColor(166,41,41  ) );
    Element::atomicProperties[36]=Element::Parameters("KR", 2.25, 1.16, 2.97, 4, 4, toFloatColor(92,184,209 ) );
    Element::atomicProperties[37]=Element::Parameters("RB", 3.21, 2.20, 0.89, 1, 1, toFloatColor(112,46,176 ) );
    Element::atomicProperties[38]=Element::Parameters("SR", 2.84, 1.95, 0.99, 6, 6, toFloatColor(0,255,0    ) );
    Element::atomicProperties[39]=Element::Parameters("Y",  2.75, 1.90, 1.11, 4, 4, toFloatColor(148,255,255) );
    Element::atomicProperties[40]=Element::Parameters("ZR", 2.52, 1.75, 1.22, 4, 4, toFloatColor(148,224,224) );
    Element::atomicProperties[41]=Element::Parameters("NB", 2.56, 1.64, 1.23, 4, 4, toFloatColor(115,194,201) );
    Element::atomicProperties[42]=Element::Parameters("MO", 2.45, 1.54, 1.30, 6, 6, toFloatColor(84,181,181 ) );
    Element::atomicProperties[43]=Element::Parameters("TC", 2.44, 1.47, 1.36, 6, 6, toFloatColor(59,158,158 ) );
    Element::atomicProperties[44]=Element::Parameters("RU", 2.46, 1.46, 1.42, 6, 6, toFloatColor(36,143,143 ) );
    Element::atomicProperties[45]=Element::Parameters("RH", 2.44, 1.42, 1.45, 6, 6, toFloatColor(10,125,140 ) );
    Element::atomicProperties[46]=Element::Parameters("PD", 2.15, 1.39, 1.35, 4, 4, toFloatColor(0,105,133  ) );
    Element::atomicProperties[47]=Element::Parameters("AG", 2.53, 1.45, 1.42, 2, 2, toFloatColor(192,192,192) );
    Element::atomicProperties[48]=Element::Parameters("CD", 2.49, 1.44, 1.46, 4, 4, toFloatColor(255,217,143) );
    Element::atomicProperties[49]=Element::Parameters("IN", 2.43, 1.42, 1.49, 4, 4, toFloatColor(166,117,115) );
    Element::atomicProperties[50]=Element::Parameters("SN", 2.42, 1.39, 1.72, 4, 4, toFloatColor(102,128,128) );
    Element::atomicProperties[51]=Element::Parameters("SB", 2.47, 1.39, 1.82, 4, 4, toFloatColor(158,99,181 ) );
    Element::atomicProperties[52]=Element::Parameters("TE", 1.99, 1.38, 2.01, 4, 4, toFloatColor(212,122,0  ) );
    Element::atomicProperties[53]=Element::Parameters("I",  2.04, 1.39, 2.21, 1, 1, toFloatColor(148,0,148  ) );
    Element::atomicProperties[54]=Element::Parameters("XE", 2.06, 1.40, 2.58, 4, 4, toFloatColor(66,158,176 ) );
    Element::atomicProperties[55]=Element::Parameters("CS", 3.48, 2.44, 0.86, 1, 1, toFloatColor(87,23,143  ) );
    Element::atomicProperties[56]=Element::Parameters("BA", 3.03, 2.15, 0.97, 6, 6, toFloatColor(0,201,0    ) );
    Element::atomicProperties[57]=Element::Parameters("LA", 2.98, 2.07, 1.08, 4, 4, toFloatColor(112,212,255) );
    Element::atomicProperties[58]=Element::Parameters("CE", 2.88, 2.04, 1.08, 6, 6, toFloatColor(255,255,199) );
    Element::atomicProperties[59]=Element::Parameters("PR", 2.92, 2.03, 1.07, 6, 6, toFloatColor(217,255,199) );
    Element::atomicProperties[60]=Element::Parameters("ND", 2.95, 2.01, 1.07, 6, 6, toFloatColor(199,255,199) );
    Element::atomicProperties[61]=Element::Parameters("PM", 2.5,  1.99, 1.07, 6, 6, toFloatColor(163,255,199) );
    Element::atomicProperties[62]=Element::Parameters("SM", 2.90, 1.98, 1.07, 6, 6, toFloatColor(143,255,199) );
    Element::atomicProperties[63]=Element::Parameters("EU", 2.87, 1.98, 1.01, 6, 6, toFloatColor(97,255,199 ) );
    Element::atomicProperties[64]=Element::Parameters("GD", 2.83, 1.96, 1.11, 6, 6, toFloatColor(69,255,199 ) );
    Element::atomicProperties[65]=Element::Parameters("TB", 2.79, 1.94, 1.10, 6, 6, toFloatColor(48,255,199 ) );
    Element::atomicProperties[66]=Element::Parameters("DY", 2.87, 1.92, 1.10, 6, 6, toFloatColor(31,255,199 ) );
    Element::atomicProperties[67]=Element::Parameters("HO", 2.81, 1.92, 1.10, 6, 6, toFloatColor(0,255,156  ) );
    Element::atomicProperties[68]=Element::Parameters("ER", 2.83, 1.89, 1.11, 6, 6, toFloatColor(0,230,117  ) );
    Element::atomicProperties[69]=Element::Parameters("TM", 2.79, 1.90, 1.11, 6, 6, toFloatColor(0,212,82   ) );
    Element::atomicProperties[70]=Element::Parameters("YB", 2.80, 1.87, 1.06, 6, 6, toFloatColor(0,191,56   ) );
    Element::atomicProperties[71]=Element::Parameters("LU", 2.74, 1.87, 1.14, 6, 6, toFloatColor(0,171,36   ) );
    Element::atomicProperties[72]=Element::Parameters("HF", 2.63, 1.75, 1.23, 4, 4, toFloatColor(77,194,255 ) );
    Element::atomicProperties[73]=Element::Parameters("TA", 2.53, 1.70, 1.33, 4, 4, toFloatColor(77,166,255 ) );
    Element::atomicProperties[74]=Element::Parameters("W",  2.57, 1.62, 1.40, 6, 6, toFloatColor(33,148,214 ) );
    Element::atomicProperties[75]=Element::Parameters("RE", 2.49, 1.51, 1.46, 6, 6, toFloatColor(38,125,171 ) );
    Element::atomicProperties[76]=Element::Parameters("OS", 2.48, 1.44, 1.52, 6, 6, toFloatColor(38,102,150 ) );
    Element::atomicProperties[77]=Element::Parameters("IR", 2.41, 1.41, 1.55, 6, 6, toFloatColor(23,84,135  ) );
    Element::atomicProperties[78]=Element::Parameters("PT", 2.29, 1.36, 1.44, 4, 4, toFloatColor(208,208,224) );
    Element::atomicProperties[79]=Element::Parameters("AU", 2.32, 1.36, 1.42, 4, 4, toFloatColor(255,209,35 ) );
    Element::atomicProperties[80]=Element::Parameters("HG", 2.45, 1.32, 1.44, 2, 2, toFloatColor(184,184,208) );
    Element::atomicProperties[81]=Element::Parameters("TL", 2.47, 1.45, 1.44, 4, 4, toFloatColor(166,84,77  ) );
    Element::atomicProperties[82]=Element::Parameters("PB", 2.60, 1.46, 1.55, 4, 4, toFloatColor(87,89,97   ) );
    Element::atomicProperties[83]=Element::Parameters("BI", 2.54, 1.48, 1.67, 3, 3, toFloatColor(158,79,181 ) );
    Element::atomicProperties[84]=Element::Parameters("PO", 2.5,  1.40, 1.76, 4, 4, toFloatColor(171,92,0   ) );
    Element::atomicProperties[85]=Element::Parameters("AT", 2.5,  1.50, 1.90, 1, 1, toFloatColor(117,79,69  ) );
    Element::atomicProperties[86]=Element::Parameters("RN", 2.5,  1.50, 2.60, 4, 4, toFloatColor(66,130,150 ) );
    Element::atomicProperties[87]=Element::Parameters("FR", 2.5,  2.60, 0.86, 1, 1, toFloatColor(66,0,102   ) );
    Element::atomicProperties[88]=Element::Parameters("RA", 2.5,  2.21, 0.97, 6, 6, toFloatColor(0,125,0    ) );
    Element::atomicProperties[89]=Element::Parameters("AC", 2.8,  2.15, 1.00, 6, 6, toFloatColor(112,171,250) );
    Element::atomicProperties[90]=Element::Parameters("TH", 2.93, 2.06, 1.11, 6, 6, toFloatColor(0,186,255  ) );
    Element::atomicProperties[91]=Element::Parameters("PA", 2.88, 2.00, 1.14, 6, 6, toFloatColor(0,161,255  ) );
    Element::atomicProperties[92]=Element::Parameters("U",  2.71, 1.96, 1.22, 6, 6, toFloatColor(0,143,255  ) );
    Element::atomicProperties[93]=Element::Parameters("NP", 2.82, 1.90, 1.22, 6, 6, toFloatColor(0,128,255  ) );
    Element::atomicProperties[94]=Element::Parameters("PU", 2.81, 1.87, 1.22, 6, 6, toFloatColor(0,107,255  ) );
    Element::atomicProperties[95]=Element::Parameters("AM", 2.83, 1.80, 1.2,  6, 6, toFloatColor(84,92,242  ) );
    Element::atomicProperties[96]=Element::Parameters("CM", 3.05, 1.69, 1.2,  6, 6, toFloatColor(120,92,227 ) );
    Element::atomicProperties[97]=Element::Parameters("BK", 3.4,  1.5,  1.2,  6, 6, toFloatColor(138,79,227 ) );
    Element::atomicProperties[98]=Element::Parameters("CF", 3.05, 1.5,  1.2,  6, 6, toFloatColor(161,54,212 ) );
    Element::atomicProperties[99]=Element::Parameters("ES", 2.7,  1.5,  1.2,  6, 6, toFloatColor(179,31,212 ) );

    Element::atomicPropetiesInitialized=true;



    if (Element::qeqHardness.size()==0)
    {
        Element::qeqHardness.insert(std::pair<int,double>(1,13.8904*23));



        Element::qeqHardness.insert(std::pair<int,double>(2,24.59*23));
        Element::qeqHardness.insert(std::pair<int,double>(3,4.77*23));
        Element::qeqHardness.insert(std::pair<int,double>(4,9.32*23));
        Element::qeqHardness.insert(std::pair<int,double>(5,8.02*23));


        Element::qeqHardness.insert(std::pair<int,double>(6,10.126*23));
        Element::qeqHardness.insert(std::pair<int,double>(7,11.760*23));
        Element::qeqHardness.insert(std::pair<int,double>(8,13.364*23));


        Element::qeqHardness.insert(std::pair<int,double>(9,14.02*23));
        Element::qeqHardness.insert(std::pair<int,double>(10,21.57*23));
        Element::qeqHardness.insert(std::pair<int,double>(11,4.59*23));
        Element::qeqHardness.insert(std::pair<int,double>(12,7.65*23));
        Element::qeqHardness.insert(std::pair<int,double>(13,5.55*23));
        Element::qeqHardness.insert(std::pair<int,double>(14,6.76*23));


        Element::qeqHardness.insert(std::pair<int,double>(15,8*23));
        Element::qeqHardness.insert(std::pair<int,double>(16,8.972*23));


        Element::qeqHardness.insert(std::pair<int,double>(17,9.36*23));
        Element::qeqHardness.insert(std::pair<int,double>(18,15.76*23));
        Element::qeqHardness.insert(std::pair<int,double>(19,3.84*23));
        Element::qeqHardness.insert(std::pair<int,double>(20,6.09*23));
        Element::qeqHardness.insert(std::pair<int,double>(21,6.37*23));
        Element::qeqHardness.insert(std::pair<int,double>(22,6.75*23));

        Element::qeqHardness.insert(std::pair<int,double>(23,6.22*23));
        Element::qeqHardness.insert(std::pair<int,double>(24,6.09*23));
        Element::qeqHardness.insert(std::pair<int,double>(25,7.43*23));
        Element::qeqHardness.insert(std::pair<int,double>(26,7.75*23));
        Element::qeqHardness.insert(std::pair<int,double>(27,7.22*23));
        Element::qeqHardness.insert(std::pair<int,double>(28,6.48*23));
        Element::qeqHardness.insert(std::pair<int,double>(29,6.49*23));
        Element::qeqHardness.insert(std::pair<int,double>(30,9.40*23));


        Element::qeqHardness.insert(std::pair<int,double>(31,5.57*23));
        Element::qeqHardness.insert(std::pair<int,double>(32,6.66*23));
        Element::qeqHardness.insert(std::pair<int,double>(33,8.99*23));
        Element::qeqHardness.insert(std::pair<int,double>(34,7.74*23));
        Element::qeqHardness.insert(std::pair<int,double>(35,8.45*23));
        Element::qeqHardness.insert(std::pair<int,double>(36,14.00*23));
        Element::qeqHardness.insert(std::pair<int,double>(37,3.69*23));
        Element::qeqHardness.insert(std::pair<int,double>(38,5.63*23));


        Element::qeqHardness.insert(std::pair<int,double>(39,5.90*23));
        Element::qeqHardness.insert(std::pair<int,double>(40,6.21*23));
        Element::qeqHardness.insert(std::pair<int,double>(41,5.86*23));
        Element::qeqHardness.insert(std::pair<int,double>(42,6.35*23));
        Element::qeqHardness.insert(std::pair<int,double>(43,6.41*23));
        Element::qeqHardness.insert(std::pair<int,double>(44,6.29*23));
        Element::qeqHardness.insert(std::pair<int,double>(45,6.32*23));
        Element::qeqHardness.insert(std::pair<int,double>(46,7.78*23));

        Element::qeqHardness.insert(std::pair<int,double>(47,6.27*23));
        Element::qeqHardness.insert(std::pair<int,double>(48,8.99*23));
        Element::qeqHardness.insert(std::pair<int,double>(49,5.40*23));
        Element::qeqHardness.insert(std::pair<int,double>(50,6.23*23));
        Element::qeqHardness.insert(std::pair<int,double>(51,7.58*23));
        Element::qeqHardness.insert(std::pair<int,double>(52,7.05*23));
        Element::qeqHardness.insert(std::pair<int,double>(53,7.39*23));
        Element::qeqHardness.insert(std::pair<int,double>(54,12.13*23));


        Element::qeqHardness.insert(std::pair<int,double>(55,3.42*23));
        Element::qeqHardness.insert(std::pair<int,double>(56,5.06*23));
        Element::qeqHardness.insert(std::pair<int,double>(57,5.10*23));
        Element::qeqHardness.insert(std::pair<int,double>(58,4.90*23));
        Element::qeqHardness.insert(std::pair<int,double>(59,4.50*23));
        Element::qeqHardness.insert(std::pair<int,double>(60,5.36*23));
        Element::qeqHardness.insert(std::pair<int,double>(61,5.45*23));
        Element::qeqHardness.insert(std::pair<int,double>(62,5.48*23));

        Element::qeqHardness.insert(std::pair<int,double>(63,5.54*23));
        Element::qeqHardness.insert(std::pair<int,double>(64,6.01*23));
        Element::qeqHardness.insert(std::pair<int,double>(65,5.42*23));
        Element::qeqHardness.insert(std::pair<int,double>(66,5.58*23));
        Element::qeqHardness.insert(std::pair<int,double>(67,5.68*23));
        Element::qeqHardness.insert(std::pair<int,double>(68,5.79*23));
        Element::qeqHardness.insert(std::pair<int,double>(69,6.17*23));
        Element::qeqHardness.insert(std::pair<int,double>(70,6.26*23));

        Element::qeqHardness.insert(std::pair<int,double>(71,5.08*23));
        Element::qeqHardness.insert(std::pair<int,double>(72,6.71*23));
        Element::qeqHardness.insert(std::pair<int,double>(73,7.23*23));
        Element::qeqHardness.insert(std::pair<int,double>(74,7.04*23));
        Element::qeqHardness.insert(std::pair<int,double>(75,7.67*23));
        Element::qeqHardness.insert(std::pair<int,double>(76,7.35*23));
        Element::qeqHardness.insert(std::pair<int,double>(77,7.40*23));
        Element::qeqHardness.insert(std::pair<int,double>(78,6.83*23));


        Element::qeqHardness.insert(std::pair<int,double>(79,6.91*23));
        Element::qeqHardness.insert(std::pair<int,double>(80,10.44*23));
        Element::qeqHardness.insert(std::pair<int,double>(81,5.73*23));
        Element::qeqHardness.insert(std::pair<int,double>(82,7.06*23));
        Element::qeqHardness.insert(std::pair<int,double>(83,6.34*23));
        Element::qeqHardness.insert(std::pair<int,double>(84,6.53*23));
        Element::qeqHardness.insert(std::pair<int,double>(85,6.54*23));
        Element::qeqHardness.insert(std::pair<int,double>(86,10.73*23));

        Element::qeqHardness.insert(std::pair<int,double>(87,3.58*23));
        Element::qeqHardness.insert(std::pair<int,double>(88,5.18*23));
        Element::qeqHardness.insert(std::pair<int,double>(89,5.23*23));
        Element::qeqHardness.insert(std::pair<int,double>(90,5.92*23));
        Element::qeqHardness.insert(std::pair<int,double>(91,5.49*23));
        Element::qeqHardness.insert(std::pair<int,double>(92,5.81*23));
        Element::qeqHardness.insert(std::pair<int,double>(93,5.95*23));
        Element::qeqHardness.insert(std::pair<int,double>(94,5.92*23));
        Element::qeqHardness.insert(std::pair<int,double>(95,5.89*23));
        Element::qeqHardness.insert(std::pair<int,double>(96,5.66*23));
    }

    if (Element::qeqPotential.size()==0)
    {
//        Element::qeqPotential.insert(std::pair<int,double>(1,4.5280*23));
//        Element::qeqPotential.insert(std::pair<int,double>(6,5.343*23));
//        Element::qeqPotential.insert(std::pair<int,double>(7,6.899*23));
//        Element::qeqPotential.insert(std::pair<int,double>(8,8.741*23));

//        Element::qeqPotential.insert(std::pair<int,double>(15,5.463*23));
//        Element::qeqPotential.insert(std::pair<int,double>(16,6.972*23));

        Element::qeqHardness.insert(std::pair<int,double>(1,7.18*23));



        Element::qeqHardness.insert(std::pair<int,double>(2,12.29*23));
        Element::qeqHardness.insert(std::pair<int,double>(3,3.00*23));
        Element::qeqHardness.insert(std::pair<int,double>(4,4.66*23));
        Element::qeqHardness.insert(std::pair<int,double>(5,4.29*23));


        Element::qeqHardness.insert(std::pair<int,double>(6,6.26*23));
        Element::qeqHardness.insert(std::pair<int,double>(7,7.27*23));
        Element::qeqHardness.insert(std::pair<int,double>(8,7.54*23));


        Element::qeqHardness.insert(std::pair<int,double>(9,10.41*23));
        Element::qeqHardness.insert(std::pair<int,double>(10,10.78*23));
        Element::qeqHardness.insert(std::pair<int,double>(11,2.84*23));
        Element::qeqHardness.insert(std::pair<int,double>(12,3.82*23));
        Element::qeqHardness.insert(std::pair<int,double>(13,3.21*23));
        Element::qeqHardness.insert(std::pair<int,double>(14,4.77*23));


        Element::qeqHardness.insert(std::pair<int,double>(15,5.62*23));
        Element::qeqHardness.insert(std::pair<int,double>(16,6.22*23));


        Element::qeqHardness.insert(std::pair<int,double>(17,8.29*23));
        Element::qeqHardness.insert(std::pair<int,double>(18,7.88*23));
        Element::qeqHardness.insert(std::pair<int,double>(19,2.42*23));
        Element::qeqHardness.insert(std::pair<int,double>(20,3.07*23));
        Element::qeqHardness.insert(std::pair<int,double>(21,3.37*23));
        Element::qeqHardness.insert(std::pair<int,double>(22,3.45*23));

        Element::qeqHardness.insert(std::pair<int,double>(23,3.64*23));
        Element::qeqHardness.insert(std::pair<int,double>(24,3.72*23));
        Element::qeqHardness.insert(std::pair<int,double>(25,3.72*23));
        Element::qeqHardness.insert(std::pair<int,double>(26,4.03*23));
        Element::qeqHardness.insert(std::pair<int,double>(27,4.27*23));
        Element::qeqHardness.insert(std::pair<int,double>(28,4.40*23));
        Element::qeqHardness.insert(std::pair<int,double>(29,4.48*23));
        Element::qeqHardness.insert(std::pair<int,double>(30,4.70*23));


        Element::qeqHardness.insert(std::pair<int,double>(31,3.21*23));
        Element::qeqHardness.insert(std::pair<int,double>(32,4.57*23));
        Element::qeqHardness.insert(std::pair<int,double>(33,5.30*23));
        Element::qeqHardness.insert(std::pair<int,double>(34,5.89*23));
        Element::qeqHardness.insert(std::pair<int,double>(35,7.59*23));
        Element::qeqHardness.insert(std::pair<int,double>(36,7.00*23));
        Element::qeqHardness.insert(std::pair<int,double>(37,2.33*23));
        Element::qeqHardness.insert(std::pair<int,double>(38,2.87*23));


        Element::qeqHardness.insert(std::pair<int,double>(39,3.26*23));
        Element::qeqHardness.insert(std::pair<int,double>(40,3.53*23));
        Element::qeqHardness.insert(std::pair<int,double>(41,3.83*23));
        Element::qeqHardness.insert(std::pair<int,double>(42,3.92*23));
        Element::qeqHardness.insert(std::pair<int,double>(43,3.92*23));
        Element::qeqHardness.insert(std::pair<int,double>(44,4.22*23));
        Element::qeqHardness.insert(std::pair<int,double>(45,4.30*23));
        Element::qeqHardness.insert(std::pair<int,double>(46,4.45*23));

        Element::qeqHardness.insert(std::pair<int,double>(47,4.44*23));
        Element::qeqHardness.insert(std::pair<int,double>(48,4.50*23));
        Element::qeqHardness.insert(std::pair<int,double>(49,3.09*23));
        Element::qeqHardness.insert(std::pair<int,double>(50,4.23*23));
        Element::qeqHardness.insert(std::pair<int,double>(51,4.83*23));
        Element::qeqHardness.insert(std::pair<int,double>(52,5.49*23));
        Element::qeqHardness.insert(std::pair<int,double>(53,6.76*23));
        Element::qeqHardness.insert(std::pair<int,double>(54,6.06*23));


        Element::qeqHardness.insert(std::pair<int,double>(55,2.18*23));
        Element::qeqHardness.insert(std::pair<int,double>(56,2.68*23));
        Element::qeqHardness.insert(std::pair<int,double>(57,3.02*23));
        Element::qeqHardness.insert(std::pair<int,double>(58,3.08*23));
        Element::qeqHardness.insert(std::pair<int,double>(59,3.22*23));
        Element::qeqHardness.insert(std::pair<int,double>(60,2.84*23));
        Element::qeqHardness.insert(std::pair<int,double>(61,2.86*23));
        Element::qeqHardness.insert(std::pair<int,double>(62,2.90*23));

        Element::qeqHardness.insert(std::pair<int,double>(63,2.89*23));
        Element::qeqHardness.insert(std::pair<int,double>(64,3.14*23));
        Element::qeqHardness.insert(std::pair<int,double>(65,3.15*23));
        Element::qeqHardness.insert(std::pair<int,double>(66,3.15*23));
        Element::qeqHardness.insert(std::pair<int,double>(67,3.18*23));
        Element::qeqHardness.insert(std::pair<int,double>(68,3.21*23));
        Element::qeqHardness.insert(std::pair<int,double>(69,3.10*23));
        Element::qeqHardness.insert(std::pair<int,double>(70,3.13*23));

        Element::qeqHardness.insert(std::pair<int,double>(71,2.88*23));
        Element::qeqHardness.insert(std::pair<int,double>(72,3.47*23));
        Element::qeqHardness.insert(std::pair<int,double>(73,3.94*23));
        Element::qeqHardness.insert(std::pair<int,double>(74,4.34*23));
        Element::qeqHardness.insert(std::pair<int,double>(75,3.99*23));
        Element::qeqHardness.insert(std::pair<int,double>(76,4.76*23));
        Element::qeqHardness.insert(std::pair<int,double>(77,5.27*23));
        Element::qeqHardness.insert(std::pair<int,double>(78,5.54*23));


        Element::qeqHardness.insert(std::pair<int,double>(79,5.77*23));
        Element::qeqHardness.insert(std::pair<int,double>(80,5.22*23));
        Element::qeqHardness.insert(std::pair<int,double>(81,3.24*23));
        Element::qeqHardness.insert(std::pair<int,double>(82,3.89*23));
        Element::qeqHardness.insert(std::pair<int,double>(83,4.11*23));
        Element::qeqHardness.insert(std::pair<int,double>(84,5.16*23));
        Element::qeqHardness.insert(std::pair<int,double>(85,6.06*23));
        Element::qeqHardness.insert(std::pair<int,double>(86,5.37*23));

        Element::qeqHardness.insert(std::pair<int,double>(87,2.28*23));
        Element::qeqHardness.insert(std::pair<int,double>(88,2.69*23));
        Element::qeqHardness.insert(std::pair<int,double>(89,2.76*23));
        Element::qeqHardness.insert(std::pair<int,double>(90,3.34*23));
        Element::qeqHardness.insert(std::pair<int,double>(91,3.14*23));
        Element::qeqHardness.insert(std::pair<int,double>(92,3.28*23));
        Element::qeqHardness.insert(std::pair<int,double>(93,3.29*23));
        Element::qeqHardness.insert(std::pair<int,double>(94,3.06*23));
        Element::qeqHardness.insert(std::pair<int,double>(95,3.02*23));
        Element::qeqHardness.insert(std::pair<int,double>(96,3.16*23));
    }


    if (Element::singleBondRadii.size()==0)
    {
        Element::singleBondRadii.insert(std::pair<int,double>(1,0.32));
        Element::singleBondRadii.insert(std::pair<int,double>(2,0.46));
        Element::singleBondRadii.insert(std::pair<int,double>(3,1.33));
        Element::singleBondRadii.insert(std::pair<int,double>(4,1.02));
        Element::singleBondRadii.insert(std::pair<int,double>(5,0.85));
        Element::singleBondRadii.insert(std::pair<int,double>(6,0.75));
        Element::singleBondRadii.insert(std::pair<int,double>(7,0.71));
        Element::singleBondRadii.insert(std::pair<int,double>(8,0.63));
        Element::singleBondRadii.insert(std::pair<int,double>(9,0.64));
        Element::singleBondRadii.insert(std::pair<int,double>(10,0.67));
        Element::singleBondRadii.insert(std::pair<int,double>(11,1.55));
        Element::singleBondRadii.insert(std::pair<int,double>(12,1.39));
        Element::singleBondRadii.insert(std::pair<int,double>(13,1.26));
        Element::singleBondRadii.insert(std::pair<int,double>(14,1.16));
        Element::singleBondRadii.insert(std::pair<int,double>(15,1.11));
        Element::singleBondRadii.insert(std::pair<int,double>(16,1.03));
        Element::singleBondRadii.insert(std::pair<int,double>(17,0.99));
        Element::singleBondRadii.insert(std::pair<int,double>(18,0.96));
        Element::singleBondRadii.insert(std::pair<int,double>(19,1.96));
        Element::singleBondRadii.insert(std::pair<int,double>(20,1.71));
        Element::singleBondRadii.insert(std::pair<int,double>(21,1.48));
        Element::singleBondRadii.insert(std::pair<int,double>(22,1.36));
        Element::singleBondRadii.insert(std::pair<int,double>(23,1.34));
        Element::singleBondRadii.insert(std::pair<int,double>(24,1.22));
        Element::singleBondRadii.insert(std::pair<int,double>(25,1.19));
        Element::singleBondRadii.insert(std::pair<int,double>(26,1.16));
        Element::singleBondRadii.insert(std::pair<int,double>(27,1.11));
        Element::singleBondRadii.insert(std::pair<int,double>(28,1.10));
        Element::singleBondRadii.insert(std::pair<int,double>(29,1.12));
        Element::singleBondRadii.insert(std::pair<int,double>(30,1.18));
        Element::singleBondRadii.insert(std::pair<int,double>(31,1.24));
        Element::singleBondRadii.insert(std::pair<int,double>(32,1.21));
        Element::singleBondRadii.insert(std::pair<int,double>(33,1.21));
        Element::singleBondRadii.insert(std::pair<int,double>(34,1.16));
        Element::singleBondRadii.insert(std::pair<int,double>(35,1.14));
        Element::singleBondRadii.insert(std::pair<int,double>(36,1.17));
        Element::singleBondRadii.insert(std::pair<int,double>(37,2.10));
        Element::singleBondRadii.insert(std::pair<int,double>(38,1.85));
        Element::singleBondRadii.insert(std::pair<int,double>(39,1.63));
        Element::singleBondRadii.insert(std::pair<int,double>(40,1.54));
        Element::singleBondRadii.insert(std::pair<int,double>(41,1.47));
        Element::singleBondRadii.insert(std::pair<int,double>(42,1.38));
        Element::singleBondRadii.insert(std::pair<int,double>(43,1.28));
        Element::singleBondRadii.insert(std::pair<int,double>(44,1.25));
        Element::singleBondRadii.insert(std::pair<int,double>(45,1.25));
        Element::singleBondRadii.insert(std::pair<int,double>(46,1.20));
        Element::singleBondRadii.insert(std::pair<int,double>(47,1.28));
        Element::singleBondRadii.insert(std::pair<int,double>(48,1.36));
        Element::singleBondRadii.insert(std::pair<int,double>(49,1.42));
        Element::singleBondRadii.insert(std::pair<int,double>(50,1.40));
        Element::singleBondRadii.insert(std::pair<int,double>(51,1.40));
        Element::singleBondRadii.insert(std::pair<int,double>(52,1.36));
        Element::singleBondRadii.insert(std::pair<int,double>(53,1.33));
        Element::singleBondRadii.insert(std::pair<int,double>(54,1.31));
        Element::singleBondRadii.insert(std::pair<int,double>(55,2.32));
        Element::singleBondRadii.insert(std::pair<int,double>(56,1.96));
        Element::singleBondRadii.insert(std::pair<int,double>(57,1.80));
        Element::singleBondRadii.insert(std::pair<int,double>(58,1.63));
        Element::singleBondRadii.insert(std::pair<int,double>(59,1.76));
        Element::singleBondRadii.insert(std::pair<int,double>(60,1.74));
        Element::singleBondRadii.insert(std::pair<int,double>(61,1.73));
        Element::singleBondRadii.insert(std::pair<int,double>(62,1.72));
        Element::singleBondRadii.insert(std::pair<int,double>(63,1.68));
        Element::singleBondRadii.insert(std::pair<int,double>(64,1.69));
        Element::singleBondRadii.insert(std::pair<int,double>(65,1.68));
        Element::singleBondRadii.insert(std::pair<int,double>(66,1.67));
        Element::singleBondRadii.insert(std::pair<int,double>(67,1.66));
        Element::singleBondRadii.insert(std::pair<int,double>(68,1.65));
        Element::singleBondRadii.insert(std::pair<int,double>(69,1.64));
        Element::singleBondRadii.insert(std::pair<int,double>(70,1.70));
        Element::singleBondRadii.insert(std::pair<int,double>(71,1.62));
        Element::singleBondRadii.insert(std::pair<int,double>(72,1.52));
        Element::singleBondRadii.insert(std::pair<int,double>(73,1.46));
        Element::singleBondRadii.insert(std::pair<int,double>(74,1.37));
        Element::singleBondRadii.insert(std::pair<int,double>(75,1.31));
        Element::singleBondRadii.insert(std::pair<int,double>(76,1.29));
        Element::singleBondRadii.insert(std::pair<int,double>(77,1.22));
        Element::singleBondRadii.insert(std::pair<int,double>(78,1.23));
        Element::singleBondRadii.insert(std::pair<int,double>(79,1.24));
        Element::singleBondRadii.insert(std::pair<int,double>(80,1.33));
        Element::singleBondRadii.insert(std::pair<int,double>(81,1.44));
        Element::singleBondRadii.insert(std::pair<int,double>(82,1.44));
        Element::singleBondRadii.insert(std::pair<int,double>(83,1.51));
        Element::singleBondRadii.insert(std::pair<int,double>(84,1.45));
        Element::singleBondRadii.insert(std::pair<int,double>(85,1.47));
        Element::singleBondRadii.insert(std::pair<int,double>(86,1.42));
        Element::singleBondRadii.insert(std::pair<int,double>(87,2.23));
        Element::singleBondRadii.insert(std::pair<int,double>(88,2.01));
        Element::singleBondRadii.insert(std::pair<int,double>(89,1.86));
        Element::singleBondRadii.insert(std::pair<int,double>(90,1.75));
        Element::singleBondRadii.insert(std::pair<int,double>(91,1.69));
        Element::singleBondRadii.insert(std::pair<int,double>(92,1.70));
        Element::singleBondRadii.insert(std::pair<int,double>(93,1.71));
        Element::singleBondRadii.insert(std::pair<int,double>(94,1.72));
        Element::singleBondRadii.insert(std::pair<int,double>(95,1.66));
        Element::singleBondRadii.insert(std::pair<int,double>(96,1.66));
        Element::singleBondRadii.insert(std::pair<int,double>(97,1.68));
        Element::singleBondRadii.insert(std::pair<int,double>(98,1.68));
        Element::singleBondRadii.insert(std::pair<int,double>(99,1.65));
        Element::singleBondRadii.insert(std::pair<int,double>(100,1.67));
        Element::singleBondRadii.insert(std::pair<int,double>(101,1.73));
        Element::singleBondRadii.insert(std::pair<int,double>(102,1.76));
        Element::singleBondRadii.insert(std::pair<int,double>(103,1.61));
        Element::singleBondRadii.insert(std::pair<int,double>(104,1.57));
        Element::singleBondRadii.insert(std::pair<int,double>(105,1.49));
        Element::singleBondRadii.insert(std::pair<int,double>(106,1.43));
        Element::singleBondRadii.insert(std::pair<int,double>(107,1.41));
        Element::singleBondRadii.insert(std::pair<int,double>(108,1.34));
        Element::singleBondRadii.insert(std::pair<int,double>(109,1.29));
        Element::singleBondRadii.insert(std::pair<int,double>(110,1.28));
        Element::singleBondRadii.insert(std::pair<int,double>(111,1.21));
        Element::singleBondRadii.insert(std::pair<int,double>(112,1.22));
        Element::singleBondRadii.insert(std::pair<int,double>(113,1.36));
        Element::singleBondRadii.insert(std::pair<int,double>(114,1.43));
        Element::singleBondRadii.insert(std::pair<int,double>(115,1.62));
        Element::singleBondRadii.insert(std::pair<int,double>(116,1.75));
        Element::singleBondRadii.insert(std::pair<int,double>(117,1.65));
        Element::singleBondRadii.insert(std::pair<int,double>(118,1.57));

    }




    if (Element::doubleBondRadii.size()==0)
    {

        Element::doubleBondRadii.insert(std::pair<int,double>(1,0));
        Element::doubleBondRadii.insert(std::pair<int,double>(2,0));
        Element::doubleBondRadii.insert(std::pair<int,double>(3,1.24));
        Element::doubleBondRadii.insert(std::pair<int,double>(4,0.90));
        Element::doubleBondRadii.insert(std::pair<int,double>(5,0.78));
        Element::doubleBondRadii.insert(std::pair<int,double>(6,0.67));
        Element::doubleBondRadii.insert(std::pair<int,double>(7,0.60));
        Element::doubleBondRadii.insert(std::pair<int,double>(8,0.57));
        Element::doubleBondRadii.insert(std::pair<int,double>(9,0.59));
        Element::doubleBondRadii.insert(std::pair<int,double>(10,0.96));
        Element::doubleBondRadii.insert(std::pair<int,double>(11,1.60));
        Element::doubleBondRadii.insert(std::pair<int,double>(12,1.32));
        Element::doubleBondRadii.insert(std::pair<int,double>(13,1.13));
        Element::doubleBondRadii.insert(std::pair<int,double>(14,1.07));
        Element::doubleBondRadii.insert(std::pair<int,double>(15,1.02));
        Element::doubleBondRadii.insert(std::pair<int,double>(16,0.94));
        Element::doubleBondRadii.insert(std::pair<int,double>(17,0.95));
        Element::doubleBondRadii.insert(std::pair<int,double>(18,1.07));
        Element::doubleBondRadii.insert(std::pair<int,double>(19,1.93));
        Element::doubleBondRadii.insert(std::pair<int,double>(20,1.47));
        Element::doubleBondRadii.insert(std::pair<int,double>(21,1.16));
        Element::doubleBondRadii.insert(std::pair<int,double>(22,1.17));
        Element::doubleBondRadii.insert(std::pair<int,double>(23,1.12));
        Element::doubleBondRadii.insert(std::pair<int,double>(24,1.11));
        Element::doubleBondRadii.insert(std::pair<int,double>(25,1.05));
        Element::doubleBondRadii.insert(std::pair<int,double>(26,1.09));
        Element::doubleBondRadii.insert(std::pair<int,double>(27,1.03));
        Element::doubleBondRadii.insert(std::pair<int,double>(28,1.01));
        Element::doubleBondRadii.insert(std::pair<int,double>(29,1.15));
        Element::doubleBondRadii.insert(std::pair<int,double>(30,1.20));
        Element::doubleBondRadii.insert(std::pair<int,double>(31,1.17));
        Element::doubleBondRadii.insert(std::pair<int,double>(32,1.11));
        Element::doubleBondRadii.insert(std::pair<int,double>(33,1.14));
        Element::doubleBondRadii.insert(std::pair<int,double>(34,1.07));
        Element::doubleBondRadii.insert(std::pair<int,double>(35,1.09));
        Element::doubleBondRadii.insert(std::pair<int,double>(36,1.21));
        Element::doubleBondRadii.insert(std::pair<int,double>(37,2.02));
        Element::doubleBondRadii.insert(std::pair<int,double>(38,1.57));
        Element::doubleBondRadii.insert(std::pair<int,double>(39,1.30));
        Element::doubleBondRadii.insert(std::pair<int,double>(40,1.27));
        Element::doubleBondRadii.insert(std::pair<int,double>(41,1.25));
        Element::doubleBondRadii.insert(std::pair<int,double>(42,1.21));
        Element::doubleBondRadii.insert(std::pair<int,double>(43,1.20));
        Element::doubleBondRadii.insert(std::pair<int,double>(44,1.14));
        Element::doubleBondRadii.insert(std::pair<int,double>(45,1.10));
        Element::doubleBondRadii.insert(std::pair<int,double>(46,1.17));
        Element::doubleBondRadii.insert(std::pair<int,double>(47,1.39));
        Element::doubleBondRadii.insert(std::pair<int,double>(48,1.44));
        Element::doubleBondRadii.insert(std::pair<int,double>(49,1.36));
        Element::doubleBondRadii.insert(std::pair<int,double>(50,1.30));
        Element::doubleBondRadii.insert(std::pair<int,double>(51,1.33));
        Element::doubleBondRadii.insert(std::pair<int,double>(52,1.28));
        Element::doubleBondRadii.insert(std::pair<int,double>(53,1.29));
        Element::doubleBondRadii.insert(std::pair<int,double>(54,1.35));
        Element::doubleBondRadii.insert(std::pair<int,double>(55,2.09));
        Element::doubleBondRadii.insert(std::pair<int,double>(56,1.61));
        Element::doubleBondRadii.insert(std::pair<int,double>(57,1.39));
        Element::doubleBondRadii.insert(std::pair<int,double>(58,1.37));
        Element::doubleBondRadii.insert(std::pair<int,double>(59,1.38));
        Element::doubleBondRadii.insert(std::pair<int,double>(60,1.37));
        Element::doubleBondRadii.insert(std::pair<int,double>(61,1.35));
        Element::doubleBondRadii.insert(std::pair<int,double>(62,1.34));
        Element::doubleBondRadii.insert(std::pair<int,double>(63,1.34));
        Element::doubleBondRadii.insert(std::pair<int,double>(64,1.35));
        Element::doubleBondRadii.insert(std::pair<int,double>(65,1.35));
        Element::doubleBondRadii.insert(std::pair<int,double>(66,1.33));
        Element::doubleBondRadii.insert(std::pair<int,double>(67,1.33));
        Element::doubleBondRadii.insert(std::pair<int,double>(68,1.33));
        Element::doubleBondRadii.insert(std::pair<int,double>(69,1.31));
        Element::doubleBondRadii.insert(std::pair<int,double>(70,1.29));
        Element::doubleBondRadii.insert(std::pair<int,double>(71,1.31));
        Element::doubleBondRadii.insert(std::pair<int,double>(72,1.28));
        Element::doubleBondRadii.insert(std::pair<int,double>(73,1.26));
        Element::doubleBondRadii.insert(std::pair<int,double>(74,1.20));
        Element::doubleBondRadii.insert(std::pair<int,double>(75,1.19));
        Element::doubleBondRadii.insert(std::pair<int,double>(76,1.16));
        Element::doubleBondRadii.insert(std::pair<int,double>(77,1.15));
        Element::doubleBondRadii.insert(std::pair<int,double>(78,1.12));
        Element::doubleBondRadii.insert(std::pair<int,double>(79,1.21));
        Element::doubleBondRadii.insert(std::pair<int,double>(80,1.42));
        Element::doubleBondRadii.insert(std::pair<int,double>(81,1.42));
        Element::doubleBondRadii.insert(std::pair<int,double>(82,1.35));
        Element::doubleBondRadii.insert(std::pair<int,double>(83,1.41));
        Element::doubleBondRadii.insert(std::pair<int,double>(84,1.35));
        Element::doubleBondRadii.insert(std::pair<int,double>(85,1.38));
        Element::doubleBondRadii.insert(std::pair<int,double>(86,1.45));
        Element::doubleBondRadii.insert(std::pair<int,double>(87,2.18));
        Element::doubleBondRadii.insert(std::pair<int,double>(88,1.73));
        Element::doubleBondRadii.insert(std::pair<int,double>(89,1.53));
        Element::doubleBondRadii.insert(std::pair<int,double>(90,1.43));
        Element::doubleBondRadii.insert(std::pair<int,double>(91,1.38));
        Element::doubleBondRadii.insert(std::pair<int,double>(92,1.34));
        Element::doubleBondRadii.insert(std::pair<int,double>(93,1.36));
        Element::doubleBondRadii.insert(std::pair<int,double>(94,1.35));
        Element::doubleBondRadii.insert(std::pair<int,double>(95,1.35));
        Element::doubleBondRadii.insert(std::pair<int,double>(96,1.36));
        Element::doubleBondRadii.insert(std::pair<int,double>(97,1.39));
        Element::doubleBondRadii.insert(std::pair<int,double>(98,1.40));
        Element::doubleBondRadii.insert(std::pair<int,double>(99,1.40));
        Element::doubleBondRadii.insert(std::pair<int,double>(100,0));
        Element::doubleBondRadii.insert(std::pair<int,double>(101,1.39));
        Element::doubleBondRadii.insert(std::pair<int,double>(102,1.59));
        Element::doubleBondRadii.insert(std::pair<int,double>(103,1.41));
        Element::doubleBondRadii.insert(std::pair<int,double>(104,1.40));
        Element::doubleBondRadii.insert(std::pair<int,double>(105,1.36));
        Element::doubleBondRadii.insert(std::pair<int,double>(106,1.28));
        Element::doubleBondRadii.insert(std::pair<int,double>(107,1.28));
        Element::doubleBondRadii.insert(std::pair<int,double>(108,1.25));
        Element::doubleBondRadii.insert(std::pair<int,double>(109,1.25));
        Element::doubleBondRadii.insert(std::pair<int,double>(110,1.16));
        Element::doubleBondRadii.insert(std::pair<int,double>(111,1.16));
        Element::doubleBondRadii.insert(std::pair<int,double>(112,1.37));
        Element::doubleBondRadii.insert(std::pair<int,double>(113,0));
        Element::doubleBondRadii.insert(std::pair<int,double>(114,0));
        Element::doubleBondRadii.insert(std::pair<int,double>(115,0));
        Element::doubleBondRadii.insert(std::pair<int,double>(116,0));
        Element::doubleBondRadii.insert(std::pair<int,double>(117,0));
        Element::doubleBondRadii.insert(std::pair<int,double>(118,0));

    }








    if (Element::tripleBondRadii.size()==0)
    {

        Element::tripleBondRadii.insert(std::pair<int,double>(1,0));
        Element::tripleBondRadii.insert(std::pair<int,double>(2,0));
        Element::tripleBondRadii.insert(std::pair<int,double>(3,0));
        Element::tripleBondRadii.insert(std::pair<int,double>(4,0.85));
        Element::tripleBondRadii.insert(std::pair<int,double>(5,0.73));
        Element::tripleBondRadii.insert(std::pair<int,double>(6,0.60));
        Element::tripleBondRadii.insert(std::pair<int,double>(7,0.54));
        Element::tripleBondRadii.insert(std::pair<int,double>(8,0.53));
        Element::tripleBondRadii.insert(std::pair<int,double>(9,0.53));
        Element::tripleBondRadii.insert(std::pair<int,double>(10,0));
        Element::tripleBondRadii.insert(std::pair<int,double>(11,0));
        Element::tripleBondRadii.insert(std::pair<int,double>(12,1.27));
        Element::tripleBondRadii.insert(std::pair<int,double>(13,1.11));
        Element::tripleBondRadii.insert(std::pair<int,double>(14,1.02));
        Element::tripleBondRadii.insert(std::pair<int,double>(15,0.94));
        Element::tripleBondRadii.insert(std::pair<int,double>(16,0.95));
        Element::tripleBondRadii.insert(std::pair<int,double>(17,0.93));
        Element::tripleBondRadii.insert(std::pair<int,double>(18,0.96));
        Element::tripleBondRadii.insert(std::pair<int,double>(19,0));
        Element::tripleBondRadii.insert(std::pair<int,double>(20,1.33));
        Element::tripleBondRadii.insert(std::pair<int,double>(21,1.14));
        Element::tripleBondRadii.insert(std::pair<int,double>(22,1.08));
        Element::tripleBondRadii.insert(std::pair<int,double>(23,1.06));
        Element::tripleBondRadii.insert(std::pair<int,double>(24,1.03));
        Element::tripleBondRadii.insert(std::pair<int,double>(25,1.03));
        Element::tripleBondRadii.insert(std::pair<int,double>(26,1.02));
        Element::tripleBondRadii.insert(std::pair<int,double>(27,0.96));
        Element::tripleBondRadii.insert(std::pair<int,double>(28,1.01));
        Element::tripleBondRadii.insert(std::pair<int,double>(29,1.20));
        Element::tripleBondRadii.insert(std::pair<int,double>(30,0));
        Element::tripleBondRadii.insert(std::pair<int,double>(31,1.21));
        Element::tripleBondRadii.insert(std::pair<int,double>(32,1.14));
        Element::tripleBondRadii.insert(std::pair<int,double>(33,1.06));
        Element::tripleBondRadii.insert(std::pair<int,double>(34,1.07));
        Element::tripleBondRadii.insert(std::pair<int,double>(35,1.10));
        Element::tripleBondRadii.insert(std::pair<int,double>(36,1.08));
        Element::tripleBondRadii.insert(std::pair<int,double>(37,0));
        Element::tripleBondRadii.insert(std::pair<int,double>(38,1.39));
        Element::tripleBondRadii.insert(std::pair<int,double>(39,1.24));
        Element::tripleBondRadii.insert(std::pair<int,double>(40,1.21));
        Element::tripleBondRadii.insert(std::pair<int,double>(41,1.16));
        Element::tripleBondRadii.insert(std::pair<int,double>(42,1.13));
        Element::tripleBondRadii.insert(std::pair<int,double>(43,1.10));
        Element::tripleBondRadii.insert(std::pair<int,double>(44,1.03));
        Element::tripleBondRadii.insert(std::pair<int,double>(45,1.06));
        Element::tripleBondRadii.insert(std::pair<int,double>(46,1.12));
        Element::tripleBondRadii.insert(std::pair<int,double>(47,1.37));
        Element::tripleBondRadii.insert(std::pair<int,double>(48,0));
        Element::tripleBondRadii.insert(std::pair<int,double>(49,1.46));
        Element::tripleBondRadii.insert(std::pair<int,double>(50,1.32));
        Element::tripleBondRadii.insert(std::pair<int,double>(51,1.27));
        Element::tripleBondRadii.insert(std::pair<int,double>(52,1.21));
        Element::tripleBondRadii.insert(std::pair<int,double>(53,1.25));
        Element::tripleBondRadii.insert(std::pair<int,double>(54,1.22));
        Element::tripleBondRadii.insert(std::pair<int,double>(55,0));
        Element::tripleBondRadii.insert(std::pair<int,double>(56,1.49));
        Element::tripleBondRadii.insert(std::pair<int,double>(57,1.39));
        Element::tripleBondRadii.insert(std::pair<int,double>(58,1.31));
        Element::tripleBondRadii.insert(std::pair<int,double>(59,1.28));
        Element::tripleBondRadii.insert(std::pair<int,double>(60,0));
        Element::tripleBondRadii.insert(std::pair<int,double>(61,0));
        Element::tripleBondRadii.insert(std::pair<int,double>(62,0));
        Element::tripleBondRadii.insert(std::pair<int,double>(63,0));
        Element::tripleBondRadii.insert(std::pair<int,double>(64,1.32));
        Element::tripleBondRadii.insert(std::pair<int,double>(65,0));
        Element::tripleBondRadii.insert(std::pair<int,double>(66,0));
        Element::tripleBondRadii.insert(std::pair<int,double>(67,0));
        Element::tripleBondRadii.insert(std::pair<int,double>(68,0));
        Element::tripleBondRadii.insert(std::pair<int,double>(69,0));
        Element::tripleBondRadii.insert(std::pair<int,double>(70,0));
        Element::tripleBondRadii.insert(std::pair<int,double>(71,1.31));
        Element::tripleBondRadii.insert(std::pair<int,double>(72,1.22));
        Element::tripleBondRadii.insert(std::pair<int,double>(73,1.19));
        Element::tripleBondRadii.insert(std::pair<int,double>(74,1.15));
        Element::tripleBondRadii.insert(std::pair<int,double>(75,1.10));
        Element::tripleBondRadii.insert(std::pair<int,double>(76,1.09));
        Element::tripleBondRadii.insert(std::pair<int,double>(77,1.07));
        Element::tripleBondRadii.insert(std::pair<int,double>(78,1.10));
        Element::tripleBondRadii.insert(std::pair<int,double>(79,1.23));
        Element::tripleBondRadii.insert(std::pair<int,double>(80,0));
        Element::tripleBondRadii.insert(std::pair<int,double>(81,1.50));
        Element::tripleBondRadii.insert(std::pair<int,double>(82,1.37));
        Element::tripleBondRadii.insert(std::pair<int,double>(83,1.35));
        Element::tripleBondRadii.insert(std::pair<int,double>(84,1.29));
        Element::tripleBondRadii.insert(std::pair<int,double>(85,1.38));
        Element::tripleBondRadii.insert(std::pair<int,double>(86,1.33));
        Element::tripleBondRadii.insert(std::pair<int,double>(87,0));
        Element::tripleBondRadii.insert(std::pair<int,double>(88,1.59));
        Element::tripleBondRadii.insert(std::pair<int,double>(89,1.40));
        Element::tripleBondRadii.insert(std::pair<int,double>(90,1.36));
        Element::tripleBondRadii.insert(std::pair<int,double>(91,1.29));
        Element::tripleBondRadii.insert(std::pair<int,double>(92,1.18));
        Element::tripleBondRadii.insert(std::pair<int,double>(93,1.16));
        Element::tripleBondRadii.insert(std::pair<int,double>(94,0));
        Element::tripleBondRadii.insert(std::pair<int,double>(95,0));
        Element::tripleBondRadii.insert(std::pair<int,double>(96,0));
        Element::tripleBondRadii.insert(std::pair<int,double>(97,0));
        Element::tripleBondRadii.insert(std::pair<int,double>(98,0));
        Element::tripleBondRadii.insert(std::pair<int,double>(99,0));
        Element::tripleBondRadii.insert(std::pair<int,double>(100,0));
        Element::tripleBondRadii.insert(std::pair<int,double>(101,0));
        Element::tripleBondRadii.insert(std::pair<int,double>(102,0));
        Element::tripleBondRadii.insert(std::pair<int,double>(103,0));
        Element::tripleBondRadii.insert(std::pair<int,double>(104,1.31));
        Element::tripleBondRadii.insert(std::pair<int,double>(105,1.26));
        Element::tripleBondRadii.insert(std::pair<int,double>(106,1.21));
        Element::tripleBondRadii.insert(std::pair<int,double>(107,1.19));
        Element::tripleBondRadii.insert(std::pair<int,double>(108,1.18));
        Element::tripleBondRadii.insert(std::pair<int,double>(109,1.13));
        Element::tripleBondRadii.insert(std::pair<int,double>(110,1.12));
        Element::tripleBondRadii.insert(std::pair<int,double>(111,1.18));
        Element::tripleBondRadii.insert(std::pair<int,double>(112,1.30));
        Element::tripleBondRadii.insert(std::pair<int,double>(113,0));
        Element::tripleBondRadii.insert(std::pair<int,double>(114,0));
        Element::tripleBondRadii.insert(std::pair<int,double>(115,0));
        Element::tripleBondRadii.insert(std::pair<int,double>(116,0));
        Element::tripleBondRadii.insert(std::pair<int,double>(117,0));
        Element::tripleBondRadii.insert(std::pair<int,double>(118,0));

    }


    if (Element::boParameters_a.size()==0)
    {
        Element::boParameters_a.insert(std::pair<int,double>(1,0.895879734614));
        Element::boParameters_a.insert(std::pair<int,double>(2,0.895879734614));
        Element::boParameters_a.insert(std::pair<int,double>(3,1.12095916551));
        Element::boParameters_a.insert(std::pair<int,double>(4,6.44802147537));
        Element::boParameters_a.insert(std::pair<int,double>(5,7.8370054062));
        Element::boParameters_a.insert(std::pair<int,double>(6,5.55016118532));
        Element::boParameters_a.insert(std::pair<int,double>(7,4.57016528668));
        Element::boParameters_a.insert(std::pair<int,double>(8,6.95836694507));
        Element::boParameters_a.insert(std::pair<int,double>(9,6.39405547127));
        Element::boParameters_a.insert(std::pair<int,double>(10,0.942506286565));
        Element::boParameters_a.insert(std::pair<int,double>(11,1.08723959235));
        Element::boParameters_a.insert(std::pair<int,double>(12,12.8066658488));
        Element::boParameters_a.insert(std::pair<int,double>(13,8.30693469974));
        Element::boParameters_a.insert(std::pair<int,double>(14,9.07961763461));
        Element::boParameters_a.insert(std::pair<int,double>(15,7.23676582919));
        Element::boParameters_a.insert(std::pair<int,double>(16,10.3450917073));
        Element::boParameters_a.insert(std::pair<int,double>(17,17.9798739685));
        Element::boParameters_a.insert(std::pair<int,double>(18,-0.706033808183));
        Element::boParameters_a.insert(std::pair<int,double>(19,1.10386750224));
        Element::boParameters_a.insert(std::pair<int,double>(20,4.94303371871));
        Element::boParameters_a.insert(std::pair<int,double>(21,4.07866418384));
        Element::boParameters_a.insert(std::pair<int,double>(22,5.26729819762));
        Element::boParameters_a.insert(std::pair<int,double>(23,4.95614804181));
        Element::boParameters_a.insert(std::pair<int,double>(24,7.10842717399));
        Element::boParameters_a.insert(std::pair<int,double>(25,7.31241504794));
        Element::boParameters_a.insert(std::pair<int,double>(26,9.15073454675));
        Element::boParameters_a.insert(std::pair<int,double>(27,8.19825062652));
        Element::boParameters_a.insert(std::pair<int,double>(28,10.9496412008));
        Element::boParameters_a.insert(std::pair<int,double>(29,-14.6231410302));
        Element::boParameters_a.insert(std::pair<int,double>(30,1.092682566));
        Element::boParameters_a.insert(std::pair<int,double>(31,11.238759573));
        Element::boParameters_a.insert(std::pair<int,double>(32,10.3826021557));
        Element::boParameters_a.insert(std::pair<int,double>(33,8.86160521899));
        Element::boParameters_a.insert(std::pair<int,double>(34,11.5468943572));
        Element::boParameters_a.insert(std::pair<int,double>(35,20.2990130226));
        Element::boParameters_a.insert(std::pair<int,double>(36,5.96761033096));
        Element::boParameters_a.insert(std::pair<int,double>(37,1.1114897964));
        Element::boParameters_a.insert(std::pair<int,double>(38,4.43862191498));
        Element::boParameters_a.insert(std::pair<int,double>(39,4.17745014593));
        Element::boParameters_a.insert(std::pair<int,double>(40,4.74604710823));
        Element::boParameters_a.insert(std::pair<int,double>(41,5.08539144799));
        Element::boParameters_a.insert(std::pair<int,double>(42,5.98485411626));
        Element::boParameters_a.insert(std::pair<int,double>(43,7.78044728232));
        Element::boParameters_a.insert(std::pair<int,double>(44,6.2900622886));
        Element::boParameters_a.insert(std::pair<int,double>(45,6.80789061726));
        Element::boParameters_a.insert(std::pair<int,double>(46,15.905372958));
        Element::boParameters_a.insert(std::pair<int,double>(47,-10.320683));
        Element::boParameters_a.insert(std::pair<int,double>(48,1.07763164639));
        Element::boParameters_a.insert(std::pair<int,double>(49,-3.39189612564));
        Element::boParameters_a.insert(std::pair<int,double>(50,12.489306409));
        Element::boParameters_a.insert(std::pair<int,double>(51,11.9183978626));
        Element::boParameters_a.insert(std::pair<int,double>(52,10.0372016274));
        Element::boParameters_a.insert(std::pair<int,double>(53,18.3123763112));
        Element::boParameters_a.insert(std::pair<int,double>(54,6.61950339839));
        Element::boParameters_a.insert(std::pair<int,double>(55,1.13041214846));
        Element::boParameters_a.insert(std::pair<int,double>(56,4.40429788841));
        Element::boParameters_a.insert(std::pair<int,double>(57,3.93313054221));
        Element::boParameters_a.insert(std::pair<int,double>(58,5.19940384002));
        Element::boParameters_a.insert(std::pair<int,double>(59,3.78006796401));
        Element::boParameters_a.insert(std::pair<int,double>(60,1.15729601393));
        Element::boParameters_a.insert(std::pair<int,double>(61,1.15850086119));
        Element::boParameters_a.insert(std::pair<int,double>(62,1.1587138285));
        Element::boParameters_a.insert(std::pair<int,double>(63,1.15542445889));
        Element::boParameters_a.insert(std::pair<int,double>(64,4.34999922413));
        Element::boParameters_a.insert(std::pair<int,double>(65,1.15428184117));
        Element::boParameters_a.insert(std::pair<int,double>(66,1.15565200035));
        Element::boParameters_a.insert(std::pair<int,double>(67,1.15474108119));
        Element::boParameters_a.insert(std::pair<int,double>(68,1.15379183784));
        Element::boParameters_a.insert(std::pair<int,double>(69,1.15520476107));
        Element::boParameters_a.insert(std::pair<int,double>(70,1.16181309743));
        Element::boParameters_a.insert(std::pair<int,double>(71,4.68169409702));
        Element::boParameters_a.insert(std::pair<int,double>(72,5.20808429401));
        Element::boParameters_a.insert(std::pair<int,double>(73,5.72691328214));
        Element::boParameters_a.insert(std::pair<int,double>(74,6.49895359141));
        Element::boParameters_a.insert(std::pair<int,double>(75,6.91037211167));
        Element::boParameters_a.insert(std::pair<int,double>(76,7.05506376407));
        Element::boParameters_a.insert(std::pair<int,double>(77,8.93431212863));
        Element::boParameters_a.insert(std::pair<int,double>(78,9.48335359801));
        Element::boParameters_a.insert(std::pair<int,double>(79,21.3377648231));
        Element::boParameters_a.insert(std::pair<int,double>(80,1.07439431085));
        Element::boParameters_a.insert(std::pair<int,double>(81,-11.2098228141));
        Element::boParameters_a.insert(std::pair<int,double>(82,14.1717682277));
        Element::boParameters_a.insert(std::pair<int,double>(83,10.3797865103));
        Element::boParameters_a.insert(std::pair<int,double>(84,9.96740805508));
        Element::boParameters_a.insert(std::pair<int,double>(85,14.632702332));
        Element::boParameters_a.insert(std::pair<int,double>(86,8.17977900901));
        Element::boParameters_a.insert(std::pair<int,double>(87,1.10627480022));
        Element::boParameters_a.insert(std::pair<int,double>(88,5.20896888205));
        Element::boParameters_a.insert(std::pair<int,double>(89,4.32065089202));
        Element::boParameters_a.insert(std::pair<int,double>(90,4.55647180661));
        Element::boParameters_a.insert(std::pair<int,double>(91,4.39733146716));
        Element::boParameters_a.insert(std::pair<int,double>(92,3.52456678733));
        Element::boParameters_a.insert(std::pair<int,double>(93,3.4109803047));
        Element::boParameters_a.insert(std::pair<int,double>(94,1.1577279183));
        Element::boParameters_a.insert(std::pair<int,double>(95,1.15233632662));
        Element::boParameters_a.insert(std::pair<int,double>(96,1.1510727142));
        Element::boParameters_a.insert(std::pair<int,double>(97,1.14931042716));
        Element::boParameters_a.insert(std::pair<int,double>(98,1.14797149503));
        Element::boParameters_a.insert(std::pair<int,double>(99,1.14439942122));
        Element::boParameters_a.insert(std::pair<int,double>(100,0.895879734614));
        Element::boParameters_a.insert(std::pair<int,double>(101,1.15430200637));
        Element::boParameters_a.insert(std::pair<int,double>(102,1.12967367962));
        Element::boParameters_a.insert(std::pair<int,double>(103,1.13740907249));
        Element::boParameters_a.insert(std::pair<int,double>(104,6.59806514828));
        Element::boParameters_a.insert(std::pair<int,double>(105,7.17834294122));
        Element::boParameters_a.insert(std::pair<int,double>(106,7.04524526539));
        Element::boParameters_a.insert(std::pair<int,double>(107,7.0867504463));
        Element::boParameters_a.insert(std::pair<int,double>(108,9.27488711943));
        Element::boParameters_a.insert(std::pair<int,double>(109,8.01252539698));
        Element::boParameters_a.insert(std::pair<int,double>(110,8.44677452304));
        Element::boParameters_a.insert(std::pair<int,double>(111,19.1277628596));
        Element::boParameters_a.insert(std::pair<int,double>(112,-5.67425941906));
        Element::boParameters_a.insert(std::pair<int,double>(113,0.895879734614));
        Element::boParameters_a.insert(std::pair<int,double>(114,0.895879734614));
        Element::boParameters_a.insert(std::pair<int,double>(115,0.895879734614));
        Element::boParameters_a.insert(std::pair<int,double>(116,0.895879734614));
        Element::boParameters_a.insert(std::pair<int,double>(117,0.895879734614));
        Element::boParameters_a.insert(std::pair<int,double>(118,0.895879734614));

    }






    if (Element::boParameters_b.size()==0)
    {
        Element::boParameters_b.insert(std::pair<int,double>(1,-2.79962417067));
        Element::boParameters_b.insert(std::pair<int,double>(2,-1.94756464047));
        Element::boParameters_b.insert(std::pair<int,double>(3,-0.611329971711));
        Element::boParameters_b.insert(std::pair<int,double>(4,-6.3365721866));
        Element::boParameters_b.insert(std::pair<int,double>(5,-9.2030748938));
        Element::boParameters_b.insert(std::pair<int,double>(6,-7.35580400333));
        Element::boParameters_b.insert(std::pair<int,double>(7,-6.44256021125));
        Element::boParameters_b.insert(std::pair<int,double>(8,-11.0308331595));
        Element::boParameters_b.insert(std::pair<int,double>(9,-9.8809130367));
        Element::boParameters_b.insert(std::pair<int,double>(10,-0.635435208875));
        Element::boParameters_b.insert(std::pair<int,double>(11,-0.466653748516));
        Element::boParameters_b.insert(std::pair<int,double>(12,-9.2030748938));
        Element::boParameters_b.insert(std::pair<int,double>(13,-6.60829846571));
        Element::boParameters_b.insert(std::pair<int,double>(14,-7.82987490296));
        Element::boParameters_b.insert(std::pair<int,double>(15,-6.48812313301));
        Element::boParameters_b.insert(std::pair<int,double>(16,-10.0149026207));
        Element::boParameters_b.insert(std::pair<int,double>(17,-18.1699869115));
        Element::boParameters_b.insert(std::pair<int,double>(18,1.30764578387));
        Element::boParameters_b.insert(std::pair<int,double>(19,-0.390705151023));
        Element::boParameters_b.insert(std::pair<int,double>(20,-2.89076312348));
        Element::boParameters_b.insert(std::pair<int,double>(21,-2.76302462495));
        Element::boParameters_b.insert(std::pair<int,double>(22,-3.88092385696));
        Element::boParameters_b.insert(std::pair<int,double>(23,-3.71496723188));
        Element::boParameters_b.insert(std::pair<int,double>(24,-5.81354822999));
        Element::boParameters_b.insert(std::pair<int,double>(25,-6.16069898306));
        Element::boParameters_b.insert(std::pair<int,double>(26,-7.84723063334));
        Element::boParameters_b.insert(std::pair<int,double>(27,-7.35580400333));
        Element::boParameters_b.insert(std::pair<int,double>(28,-9.95421927349));
        Element::boParameters_b.insert(std::pair<int,double>(29,13.1588422363));
        Element::boParameters_b.insert(std::pair<int,double>(30,-0.624490852424));
        Element::boParameters_b.insert(std::pair<int,double>(31,-8.81892796958));
        Element::boParameters_b.insert(std::pair<int,double>(32,-8.4844066468));
        Element::boParameters_b.insert(std::pair<int,double>(33,-7.27069096414));
        Element::boParameters_b.insert(std::pair<int,double>(34,-9.95421927349));
        Element::boParameters_b.insert(std::pair<int,double>(35,-17.7493332128));
        Element::boParameters_b.insert(std::pair<int,double>(36,-4.65637905307));
        Element::boParameters_b.insert(std::pair<int,double>(37,-0.374444155332));
        Element::boParameters_b.insert(std::pair<int,double>(38,-2.39586409058));
        Element::boParameters_b.insert(std::pair<int,double>(39,-2.57568128742));
        Element::boParameters_b.insert(std::pair<int,double>(40,-3.09611488942));
        Element::boParameters_b.insert(std::pair<int,double>(41,-3.47021001927));
        Element::boParameters_b.insert(std::pair<int,double>(42,-4.34483948375));
        Element::boParameters_b.insert(std::pair<int,double>(43,-6.01943641836));
        Element::boParameters_b.insert(std::pair<int,double>(44,-4.99369222122));
        Element::boParameters_b.insert(std::pair<int,double>(45,-5.46390392451));
        Element::boParameters_b.insert(std::pair<int,double>(46,-13.1588422363));
        Element::boParameters_b.insert(std::pair<int,double>(47,8.10737833396));
        Element::boParameters_b.insert(std::pair<int,double>(48,-0.514691239266));
        Element::boParameters_b.insert(std::pair<int,double>(49,2.82251128447));
        Element::boParameters_b.insert(std::pair<int,double>(50,-8.87466660639));
        Element::boParameters_b.insert(std::pair<int,double>(51,-8.49085852963));
        Element::boParameters_b.insert(std::pair<int,double>(52,-7.35580400333));
        Element::boParameters_b.insert(std::pair<int,double>(53,-13.7326536084));
        Element::boParameters_b.insert(std::pair<int,double>(54,-4.65637905307));
        Element::boParameters_b.insert(std::pair<int,double>(55,-0.362693191871));
        Element::boParameters_b.insert(std::pair<int,double>(56,-2.25714114545));
        Element::boParameters_b.insert(std::pair<int,double>(57,-2.18507252345));
        Element::boParameters_b.insert(std::pair<int,double>(58,-3.20335314404));
        Element::boParameters_b.insert(std::pair<int,double>(59,-2.16028154362));
        Element::boParameters_b.insert(std::pair<int,double>(60,-0.540234267703));
        Element::boParameters_b.insert(std::pair<int,double>(61,-0.546669842323));
        Element::boParameters_b.insert(std::pair<int,double>(62,-0.550451639308));
        Element::boParameters_b.insert(std::pair<int,double>(63,-0.554474803789));
        Element::boParameters_b.insert(std::pair<int,double>(64,-2.5821647255));
        Element::boParameters_b.insert(std::pair<int,double>(65,-0.551513549264));
        Element::boParameters_b.insert(std::pair<int,double>(66,-0.558398843936));
        Element::boParameters_b.insert(std::pair<int,double>(67,-0.559352432891));
        Element::boParameters_b.insert(std::pair<int,double>(68,-0.560273840364));
        Element::boParameters_b.insert(std::pair<int,double>(69,-0.567408411514));
        Element::boParameters_b.insert(std::pair<int,double>(70,-0.566448101356));
        Element::boParameters_b.insert(std::pair<int,double>(71,-2.88993462779));
        Element::boParameters_b.insert(std::pair<int,double>(72,-3.4409187594));
        Element::boParameters_b.insert(std::pair<int,double>(73,-3.93580060798));
        Element::boParameters_b.insert(std::pair<int,double>(74,-4.75943583468));
        Element::boParameters_b.insert(std::pair<int,double>(75,-5.26093246272));
        Element::boParameters_b.insert(std::pair<int,double>(76,-5.47272085395));
        Element::boParameters_b.insert(std::pair<int,double>(77,-7.27069096414));
        Element::boParameters_b.insert(std::pair<int,double>(78,-7.72704386227));
        Element::boParameters_b.insert(std::pair<int,double>(79,-16.9080258152));
        Element::boParameters_b.insert(std::pair<int,double>(80,-0.520517623027));
        Element::boParameters_b.insert(std::pair<int,double>(81,8.12413484207));
        Element::boParameters_b.insert(std::pair<int,double>(82,-9.78931375336));
        Element::boParameters_b.insert(std::pair<int,double>(83,-6.8729742533));
        Element::boParameters_b.insert(std::pair<int,double>(84,-6.8729742533));
        Element::boParameters_b.insert(std::pair<int,double>(85,-9.95421927349));
        Element::boParameters_b.insert(std::pair<int,double>(86,-5.41608989471));
        Element::boParameters_b.insert(std::pair<int,double>(87,-0.346273227085));
        Element::boParameters_b.insert(std::pair<int,double>(88,-2.59571241593));
        Element::boParameters_b.insert(std::pair<int,double>(89,-2.33198188034));
        Element::boParameters_b.insert(std::pair<int,double>(90,-2.61622377767));
        Element::boParameters_b.insert(std::pair<int,double>(91,-2.61473278263));
        Element::boParameters_b.insert(std::pair<int,double>(92,-2.08102864757));
        Element::boParameters_b.insert(std::pair<int,double>(93,-1.995551169));
        Element::boParameters_b.insert(std::pair<int,double>(94,-0.547695207056));
        Element::boParameters_b.insert(std::pair<int,double>(95,-0.553239040073));
        Element::boParameters_b.insert(std::pair<int,double>(96,-0.550151878596));
        Element::boParameters_b.insert(std::pair<int,double>(97,-0.539469645682));
        Element::boParameters_b.insert(std::pair<int,double>(98,-0.536413966193));
        Element::boParameters_b.insert(std::pair<int,double>(99,-0.538176653908));
        Element::boParameters_b.insert(std::pair<int,double>(100,-0.536454930907));
        Element::boParameters_b.insert(std::pair<int,double>(101,-0.53562389419));
        Element::boParameters_b.insert(std::pair<int,double>(102,-0.476794498398));
        Element::boParameters_b.insert(std::pair<int,double>(103,-0.536578724579));
        Element::boParameters_b.insert(std::pair<int,double>(104,-4.20617662981));
        Element::boParameters_b.insert(std::pair<int,double>(105,-4.80371517139));
        Element::boParameters_b.insert(std::pair<int,double>(106,-4.9346878385));
        Element::boParameters_b.insert(std::pair<int,double>(107,-5.01765254373));
        Element::boParameters_b.insert(std::pair<int,double>(108,-6.90527901567));
        Element::boParameters_b.insert(std::pair<int,double>(109,-6.06153044188));
        Element::boParameters_b.insert(std::pair<int,double>(110,-6.6147651966));
        Element::boParameters_b.insert(std::pair<int,double>(111,-15.6595856647));
        Element::boParameters_b.insert(std::pair<int,double>(112,4.83664208905));
        Element::boParameters_b.insert(std::pair<int,double>(113,-0.658735098981));
        Element::boParameters_b.insert(std::pair<int,double>(114,-0.626489324905));
        Element::boParameters_b.insert(std::pair<int,double>(115,-0.553012181861));
        Element::boParameters_b.insert(std::pair<int,double>(116,-0.511931276922));
        Element::boParameters_b.insert(std::pair<int,double>(117,-0.542957414918));
        Element::boParameters_b.insert(std::pair<int,double>(118,-0.570624034786));
    }


    if (Element::boParameters_Morse.size()==0)
    {
        Element::boParameters_Morse.insert(std::pair<int,double>(1,5.29850618461));
        Element::boParameters_Morse.insert(std::pair<int,double>(2,3.68591734589));
        Element::boParameters_Morse.insert(std::pair<int,double>(3,0.958734093958));
        Element::boParameters_Morse.insert(std::pair<int,double>(4,11.8404280631));
        Element::boParameters_Morse.insert(std::pair<int,double>(5,15.2347294796));
        Element::boParameters_Morse.insert(std::pair<int,double>(6,11.6890132126));
        Element::boParameters_Morse.insert(std::pair<int,double>(7,11.3419925458));
        Element::boParameters_Morse.insert(std::pair<int,double>(8,18.5405860612));
        Element::boParameters_Morse.insert(std::pair<int,double>(9,14.9651131609));
        Element::boParameters_Morse.insert(std::pair<int,double>(10,1.47809111232));
        Element::boParameters_Morse.insert(std::pair<int,double>(11,0.778516010298));
        Element::boParameters_Morse.insert(std::pair<int,double>(12,15.2347294309));
        Element::boParameters_Morse.insert(std::pair<int,double>(13,12.5056624202));
        Element::boParameters_Morse.insert(std::pair<int,double>(14,13.7253532043));
        Element::boParameters_Morse.insert(std::pair<int,double>(15,10.2805715436));
        Element::boParameters_Morse.insert(std::pair<int,double>(16,27.3627171384));
        Element::boParameters_Morse.insert(std::pair<int,double>(17,32.6390555016));
        Element::boParameters_Morse.insert(std::pair<int,double>(18,7.82919547671));
        Element::boParameters_Morse.insert(std::pair<int,double>(19,0.631784502036));
        Element::boParameters_Morse.insert(std::pair<int,double>(20,5.00997882896));
        Element::boParameters_Morse.insert(std::pair<int,double>(21,5.10125017402));
        Element::boParameters_Morse.insert(std::pair<int,double>(22,7.05719972427));
        Element::boParameters_Morse.insert(std::pair<int,double>(23,7.28367249444));
        Element::boParameters_Morse.insert(std::pair<int,double>(24,9.58642567636));
        Element::boParameters_Morse.insert(std::pair<int,double>(25,11.6010942666));
        Element::boParameters_Morse.insert(std::pair<int,double>(26,12.1882128306));
        Element::boParameters_Morse.insert(std::pair<int,double>(27,11.689013228));
        Element::boParameters_Morse.insert(std::pair<int,double>(28,18.8391331013));
        Element::boParameters_Morse.insert(std::pair<int,double>(29,7.5977221608));
        Element::boParameters_Morse.insert(std::pair<int,double>(30,1.03109303452));
        Element::boParameters_Morse.insert(std::pair<int,double>(31,12.4815836924));
        Element::boParameters_Morse.insert(std::pair<int,double>(32,16.6945835573));
        Element::boParameters_Morse.insert(std::pair<int,double>(33,11.0785085004));
        Element::boParameters_Morse.insert(std::pair<int,double>(34,18.8391331013));
        Element::boParameters_Morse.insert(std::pair<int,double>(35,44.6476572319));
        Element::boParameters_Morse.insert(std::pair<int,double>(36,10.9446049585));
        Element::boParameters_Morse.insert(std::pair<int,double>(37,0.597164774552));
        Element::boParameters_Morse.insert(std::pair<int,double>(38,4.0602094797));
        Element::boParameters_Morse.insert(std::pair<int,double>(39,4.93759004604));
        Element::boParameters_Morse.insert(std::pair<int,double>(40,6.02903015912));
        Element::boParameters_Morse.insert(std::pair<int,double>(41,6.50770180563));
        Element::boParameters_Morse.insert(std::pair<int,double>(42,7.91233965828));
        Element::boParameters_Morse.insert(std::pair<int,double>(43,9.07437725322));
        Element::boParameters_Morse.insert(std::pair<int,double>(44,7.75613543896));
        Element::boParameters_Morse.insert(std::pair<int,double>(45,10.7139273171));
        Element::boParameters_Morse.insert(std::pair<int,double>(46,19.3887324948));
        Element::boParameters_Morse.insert(std::pair<int,double>(47,6.65161781259));
        Element::boParameters_Morse.insert(std::pair<int,double>(48,0.874815158284));
        Element::boParameters_Morse.insert(std::pair<int,double>(49,4.88494356481));
        Element::boParameters_Morse.insert(std::pair<int,double>(50,22.3238299258));
        Element::boParameters_Morse.insert(std::pair<int,double>(51,13.5445854458));
        Element::boParameters_Morse.insert(std::pair<int,double>(52,11.689013228));
        Element::boParameters_Morse.insert(std::pair<int,double>(53,21.3293723754));
        Element::boParameters_Morse.insert(std::pair<int,double>(54,10.9446050368));
        Element::boParameters_Morse.insert(std::pair<int,double>(55,0.559644626103));
        Element::boParameters_Morse.insert(std::pair<int,double>(56,4.35640407988));
        Element::boParameters_Morse.insert(std::pair<int,double>(57,4.13541946126));
        Element::boParameters_Morse.insert(std::pair<int,double>(58,6.25236194963));
        Element::boParameters_Morse.insert(std::pair<int,double>(59,4.23559872974));
        Element::boParameters_Morse.insert(std::pair<int,double>(60,0.799138484107));
        Element::boParameters_Morse.insert(std::pair<int,double>(61,0.807321591813));
        Element::boParameters_Morse.insert(std::pair<int,double>(62,0.812672200996));
        Element::boParameters_Morse.insert(std::pair<int,double>(63,0.822370483823));
        Element::boParameters_Morse.insert(std::pair<int,double>(64,4.77625321351));
        Element::boParameters_Morse.insert(std::pair<int,double>(65,0.819325903534));
        Element::boParameters_Morse.insert(std::pair<int,double>(66,0.82792152184));
        Element::boParameters_Morse.insert(std::pair<int,double>(67,0.830419350466));
        Element::boParameters_Morse.insert(std::pair<int,double>(68,0.832933929983));
        Element::boParameters_Morse.insert(std::pair<int,double>(69,0.841817736987));
        Element::boParameters_Morse.insert(std::pair<int,double>(70,0.832932289256));
        Element::boParameters_Morse.insert(std::pair<int,double>(71,5.46942573837));
        Element::boParameters_Morse.insert(std::pair<int,double>(72,6.73986205206));
        Element::boParameters_Morse.insert(std::pair<int,double>(73,7.57577663569));
        Element::boParameters_Morse.insert(std::pair<int,double>(74,9.31179461791));
        Element::boParameters_Morse.insert(std::pair<int,double>(75,8.61870886832));
        Element::boParameters_Morse.insert(std::pair<int,double>(76,9.66361869722));
        Element::boParameters_Morse.insert(std::pair<int,double>(77,11.0785085004));
        Element::boParameters_Morse.insert(std::pair<int,double>(78,14.8131370539));
        Element::boParameters_Morse.insert(std::pair<int,double>(79,24.1438308042));
        Element::boParameters_Morse.insert(std::pair<int,double>(80,0.89038237313));
        Element::boParameters_Morse.insert(std::pair<int,double>(81,6.79646125035));
        Element::boParameters_Morse.insert(std::pair<int,double>(82,23.1566061018));
        Element::boParameters_Morse.insert(std::pair<int,double>(83,11.833797213));
        Element::boParameters_Morse.insert(std::pair<int,double>(84,11.8337972422));
        Element::boParameters_Morse.insert(std::pair<int,double>(85,18.8391331013));
        Element::boParameters_Morse.insert(std::pair<int,double>(86,11.5289094788));
        Element::boParameters_Morse.insert(std::pair<int,double>(87,0.557475611365));
        Element::boParameters_Morse.insert(std::pair<int,double>(88,4.66269113961));
        Element::boParameters_Morse.insert(std::pair<int,double>(89,4.40429090731));
        Element::boParameters_Morse.insert(std::pair<int,double>(90,5.08909610716));
        Element::boParameters_Morse.insert(std::pair<int,double>(91,5.11853764635));
        Element::boParameters_Morse.insert(std::pair<int,double>(92,3.83719426684));
        Element::boParameters_Morse.insert(std::pair<int,double>(93,3.47516159796));
        Element::boParameters_Morse.insert(std::pair<int,double>(94,0.809691209151));
        Element::boParameters_Morse.insert(std::pair<int,double>(95,0.824239156798));
        Element::boParameters_Morse.insert(std::pair<int,double>(96,0.821186980912));
        Element::boParameters_Morse.insert(std::pair<int,double>(97,0.807392513733));
        Element::boParameters_Morse.insert(std::pair<int,double>(98,0.804469461234));
        Element::boParameters_Morse.insert(std::pair<int,double>(99,0.811623681058));
        Element::boParameters_Morse.insert(std::pair<int,double>(100,1.01528262222));
        Element::boParameters_Morse.insert(std::pair<int,double>(101,0.795697023084));
        Element::boParameters_Morse.insert(std::pair<int,double>(102,0.73662452952));
        Element::boParameters_Morse.insert(std::pair<int,double>(103,0.818371252302));
        Element::boParameters_Morse.insert(std::pair<int,double>(104,7.45653797646));
        Element::boParameters_Morse.insert(std::pair<int,double>(105,7.82830995064));
        Element::boParameters_Morse.insert(std::pair<int,double>(106,9.00317442448));
        Element::boParameters_Morse.insert(std::pair<int,double>(107,8.36313488902));
        Element::boParameters_Morse.insert(std::pair<int,double>(108,11.227583869));
        Element::boParameters_Morse.insert(std::pair<int,double>(109,8.90048376443));
        Element::boParameters_Morse.insert(std::pair<int,double>(110,12.8105327153));
        Element::boParameters_Morse.insert(std::pair<int,double>(111,25.8214161235));
        Element::boParameters_Morse.insert(std::pair<int,double>(112,7.55682236277));
        Element::boParameters_Morse.insert(std::pair<int,double>(113,1.24670733758));
        Element::boParameters_Morse.insert(std::pair<int,double>(114,1.18567969297));
        Element::boParameters_Morse.insert(std::pair<int,double>(115,1.04661850563));
        Element::boParameters_Morse.insert(std::pair<int,double>(116,0.968869702351));
        Element::boParameters_Morse.insert(std::pair<int,double>(117,1.02758907825));
        Element::boParameters_Morse.insert(std::pair<int,double>(118,1.07995030513));
    }


    if (Element::xuff_electronegativity.size()==0)
    {
       Element::xuff_electronegativity.insert(std::pair<std::string,double>("HC",6.554));
       Element::xuff_electronegativity.insert(std::pair<std::string,double>("HN",5.449));
       Element::xuff_electronegativity.insert(std::pair<std::string,double>("HO",5.234));
       Element::xuff_electronegativity.insert(std::pair<std::string,double>("H",4.924));
       Element::xuff_electronegativity.insert(std::pair<std::string,double>("C1",7.020));
       Element::xuff_electronegativity.insert(std::pair<std::string,double>("C2",7.823));
       Element::xuff_electronegativity.insert(std::pair<std::string,double>("C3",6.814));
       Element::xuff_electronegativity.insert(std::pair<std::string,double>("CR",6.270));
       Element::xuff_electronegativity.insert(std::pair<std::string,double>("N1",10.940));
       Element::xuff_electronegativity.insert(std::pair<std::string,double>("N2",9.721));
       Element::xuff_electronegativity.insert(std::pair<std::string,double>("N3",7.925));
       Element::xuff_electronegativity.insert(std::pair<std::string,double>("NR",7.298));
       Element::xuff_electronegativity.insert(std::pair<std::string,double>("N3+",13.506));
       Element::xuff_electronegativity.insert(std::pair<std::string,double>("NO2",7.517));
       Element::xuff_electronegativity.insert(std::pair<std::string,double>("O2",7.768));
       Element::xuff_electronegativity.insert(std::pair<std::string,double>("O3",9.278));
       Element::xuff_electronegativity.insert(std::pair<std::string,double>("OR",8.733));
       Element::xuff_electronegativity.insert(std::pair<std::string,double>("O-",8.445));
       Element::xuff_electronegativity.insert(std::pair<std::string,double>("P3",7.505));
       Element::xuff_electronegativity.insert(std::pair<std::string,double>("P5",6.971));
       Element::xuff_electronegativity.insert(std::pair<std::string,double>("S2",7.156));
       Element::xuff_electronegativity.insert(std::pair<std::string,double>("S3",6.851));
       Element::xuff_electronegativity.insert(std::pair<std::string,double>("SR",8.808));
       Element::xuff_electronegativity.insert(std::pair<std::string,double>("SO2",7.207));
       Element::xuff_electronegativity.insert(std::pair<std::string,double>("F",6.492));
       Element::xuff_electronegativity.insert(std::pair<std::string,double>("Cl",7.466));
       Element::xuff_electronegativity.insert(std::pair<std::string,double>("Br",4.106));
       Element::xuff_electronegativity.insert(std::pair<std::string,double>("I",1.103));
       Element::xuff_electronegativity.insert(std::pair<std::string,double>("Fe2",7.350));
       Element::xuff_electronegativity.insert(std::pair<std::string,double>("Zn",7.293));
    }

    if (Element::xuff_hardness.size()==0)
    {
       Element::xuff_hardness.insert(std::pair<std::string,double>("HC",7.194));
       Element::xuff_hardness.insert(std::pair<std::string,double>("HN",19.899));
       Element::xuff_hardness.insert(std::pair<std::string,double>("HO",14.810));
       Element::xuff_hardness.insert(std::pair<std::string,double>("H",15.230));
       Element::xuff_hardness.insert(std::pair<std::string,double>("C1",10.475));
       Element::xuff_hardness.insert(std::pair<std::string,double>("C2",11.378));
       Element::xuff_hardness.insert(std::pair<std::string,double>("C3",9.146));
       Element::xuff_hardness.insert(std::pair<std::string,double>("CR",9.001));
       Element::xuff_hardness.insert(std::pair<std::string,double>("N1",16.503));
       Element::xuff_hardness.insert(std::pair<std::string,double>("N2",11.384));
       Element::xuff_hardness.insert(std::pair<std::string,double>("N3",13.005));
       Element::xuff_hardness.insert(std::pair<std::string,double>("NR",10.489));
       Element::xuff_hardness.insert(std::pair<std::string,double>("N3+",15.531));
       Element::xuff_hardness.insert(std::pair<std::string,double>("NO2",7.594));
       Element::xuff_hardness.insert(std::pair<std::string,double>("O2",15.068));
       Element::xuff_hardness.insert(std::pair<std::string,double>("O3",13.224));
       Element::xuff_hardness.insert(std::pair<std::string,double>("OR",14.517));
       Element::xuff_hardness.insert(std::pair<std::string,double>("O-",9.031));
       Element::xuff_hardness.insert(std::pair<std::string,double>("P3",11.931));
       Element::xuff_hardness.insert(std::pair<std::string,double>("P5",7.581));
       Element::xuff_hardness.insert(std::pair<std::string,double>("S2",8.300));
       Element::xuff_hardness.insert(std::pair<std::string,double>("S3",10.137));
       Element::xuff_hardness.insert(std::pair<std::string,double>("SR",11.702));
       Element::xuff_hardness.insert(std::pair<std::string,double>("SO2",8.259));
       Element::xuff_hardness.insert(std::pair<std::string,double>("F",9.275));
       Element::xuff_hardness.insert(std::pair<std::string,double>("Cl",9.839));
       Element::xuff_hardness.insert(std::pair<std::string,double>("Br",8.356));
       Element::xuff_hardness.insert(std::pair<std::string,double>("I",12.260));
       Element::xuff_hardness.insert(std::pair<std::string,double>("Fe2",8.925));
       Element::xuff_hardness.insert(std::pair<std::string,double>("Zn",9.330));
    }


}


int Element::getAtomicNumber(std::string symbol)
{
	// Converts the string in uppercase
	std::transform(symbol.begin(), symbol.end(), symbol.begin(), ::toupper);

    Element::initAtomicNumbers();
    auto iter = Element::atomicNumbers.find(symbol);
    if(iter != Element::atomicNumbers.end())
	{
        return iter->second;
    }
    else {return 0;}

}

std::string Element::getElementSymbol(int atomicNum)
{
    initAtomicProperties();
    if((atomicNum <= 0) || (atomicNum > 99))
        return atomicProperties[0].symbol;
    return atomicProperties[atomicNum].symbol;
}

unsigned Element::getCoordination(int atomicNum)
{
    initAtomicProperties();
    if(!(atomicNum <= 0 || atomicNum>99))
    {
        return atomicProperties[atomicNum].coordination;
    } else {return atomicProperties[0].coordination;}
}

double Element::getHardness(int atomicNum)
{
    initAtomicProperties();
    if(atomicNum<=0||atomicNum>99)
    {
        return 0;
    } else {return qeqHardness.at(atomicNum);}
}

unsigned Element::isTransitionElement(int atomicNum)
{
    if (atomicNum>=21 && atomicNum<=30) return true;
    else if (atomicNum>=39 && atomicNum<=48) return true;
    else if (atomicNum>=72 && atomicNum<=80) return true;
    else if (atomicNum>=104 && atomicNum<=112) return true;
    else return false;
}

int Element::getRow(int atomicNum)
{
    if (atomicNum<=0)
        throw std::invalid_argument("Element::getRow - Invalid atomic number");

    if (atomicNum<=2)
        return 1;

    else if (atomicNum<=10)
        return 2;

    else if (atomicNum<=18)
        return 3;

    else if (atomicNum<=36)
        return 4;

    else if (atomicNum<=54)
        return 5;

    else if (atomicNum<=86)
        return 6;

    else
        return 7;

}

double Element::getWaalsRadius(int atomicNum)
{
	initAtomicProperties();
	if(!(atomicNum<=0||atomicNum>99))
	{
        return atomicProperties[atomicNum].waalsRadius;
	} else {return atomicProperties[0].waalsRadius;}
}

double Element::getCovalentRadius(int atomicNum)
{
	initAtomicProperties();
	if(!(atomicNum<=0||atomicNum>99))
	{
		return atomicProperties[atomicNum].covalentRadius;
	} else {return atomicProperties[0].covalentRadius;}
}

double Element::getElectronegativity(int atomicNum)
{
	initAtomicProperties();
	if(!(atomicNum<=0||atomicNum>99))
	{
		return atomicProperties[atomicNum].electronegativity;
    } else {return atomicProperties[0].electronegativity;}
}

unsigned Element::getValence(int atomicNum)
{
    initAtomicProperties();
    if(!(atomicNum<=0||atomicNum>99))
    {
        return atomicProperties[atomicNum].valence;
    } else {return atomicProperties[0].valence;}

}

double Element::getBondOrder(double dist, int atomicNum1, int atomicNum2)
{
    initAtomicProperties();
    if(atomicNum1<=0 || atomicNum1>118 || atomicNum2<0 || atomicNum2>118)
        return 0;

    double a1 = boParameters_a.at(atomicNum1);
    double a2 = boParameters_a.at(atomicNum2);

    double b1 = boParameters_b.at(atomicNum1);
    double b2 = boParameters_b.at(atomicNum2);

    double out = dist + (a1/b1) + (a2/b2);
    out = out/((1/b1) + (1/b2));
    return exp(out);
}

double Element::getBO_Morse(double dist, int atomicNum1, int atomicNum2)
{
    initAtomicProperties();
    double a = Element::boParameters_Morse.at(atomicNum1);
    double b = Element::boParameters_Morse.at(atomicNum2);

    double rat = Element::tripleBondRadii.at(atomicNum1);
    double rbt = Element::tripleBondRadii.at(atomicNum2);

    double c = 1/(1/a + 1/b);
    return 3 - 3*( (1-exp(-c*(dist-rat-rbt)))*(1-exp(-c*(dist-rat-rbt))));
}

double Element::getSingleBondRadius(int atomicNum)
{
    initAtomicProperties();
    if(!(atomicNum<=0||atomicNum>118))
    {
        return singleBondRadii.at(atomicNum);
    } else {return 0;}
}

double Element::getDoubleBondRadius(int atomicNum)
{
    initAtomicProperties();
    if(!(atomicNum<=0||atomicNum>118))
    {
        return doubleBondRadii.at(atomicNum);
    } else {return 0;}
}

double Element::getTripleBondRadius(int atomicNum)
{
    initAtomicProperties();
    if(!(atomicNum<=0||atomicNum>118))
    {
        return tripleBondRadii.at(atomicNum);
    } else {return 0;}
}

double Element::getA(int atomicNum)
{
    return boParameters_a.at(atomicNum);
}

double Element::getB(int atomicNum)
{
    return boParameters_b.at(atomicNum);
}

double Element::getElectronegativity_XUFF(std::string atmType)
{
    if (atmType=="UNK")
        throw std::invalid_argument("INVALID ATOM TYPE");

    return Element::xuff_electronegativity.at(atmType);
}

double Element::getHardness_XUFF(std::string atmType)
{
    if (atmType=="UNK")
        throw std::invalid_argument("INVALID ATOM TYPE");

    return Element::xuff_hardness.at(atmType);
}

std::array<float,3> Element::getColor(int atomicNum)
{
	initAtomicProperties();
	if(!(atomicNum<=0||atomicNum>99))
	{
		return atomicProperties[atomicNum].color;
    } else {return atomicProperties[0].color;}
}



