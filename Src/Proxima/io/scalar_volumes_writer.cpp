#include "scalar_volumes_writer.h"

#include <fstream>
#include <cstdio>
#include <iomanip>

const double ANGSTROM_TO_BOHR = 1.8897259885789;

using namespace Proxima;

inline void printInCubeHeader(std::ostream& out, unsigned int n)
{
	const int INT_FIELD_WIDTH = 5;
	out << std::setw(INT_FIELD_WIDTH) << n;
}

inline void printInCubeHeader(std::ostream& out, int n)
{
	const int INT_FIELD_WIDTH = 5;
	out << std::setw(INT_FIELD_WIDTH) << n;
}

inline void printInCubeHeader(std::ostream& out, double n)
{
	const int FLOAT_FIELD_WIDTH = 12;
	const int FLOAT_FIELD_PRECISION = 6;
	out << std::fixed
		<< std::setprecision(FLOAT_FIELD_PRECISION)
		<< std::setw(FLOAT_FIELD_WIDTH)
		<< n;
}

inline void printInCubeHeader(std::ostream& out, const mathfu::Vector<double,3>& v)
{
	const int FLOAT_FIELD_WIDTH = 12;
	const int FLOAT_FIELD_PRECISION = 6;
	out << std::fixed
		<< std::setprecision(FLOAT_FIELD_PRECISION)
		<< std::setw(FLOAT_FIELD_WIDTH)
		<< v.x;
	out << std::fixed
		<< std::setprecision(FLOAT_FIELD_PRECISION)
		<< std::setw(FLOAT_FIELD_WIDTH)
		<< v.y;
	out << std::fixed
		<< std::setprecision(FLOAT_FIELD_PRECISION)
		<< std::setw(FLOAT_FIELD_WIDTH)
		<< v.z;
}

inline void printInCubePayload(std::ostream& out, double n)
{
	const int FLOAT_FIELD_WIDTH = 13;
	const int FLOAT_FIELD_PRECISION = 6;
	out << std::fixed
		<< std::setprecision(FLOAT_FIELD_PRECISION)
		<< std::setw(FLOAT_FIELD_WIDTH)
		<< n;
}

std::string Proxima::writeToFile(const std::string& filePath,
								 std::shared_ptr<DataGrid3D<double>> volume)
{
	std::unique_ptr<ScalarVolumesGroup> dummyGroup(new ScalarVolumesGroup());
	dummyGroup->setName(volume->getName());
	dummyGroup->addVolume(volume);
	return Proxima::writeToFile(filePath, dummyGroup.get());
}

std::string Proxima::writeToFile(const std::string& filePath,
								 const ScalarVolumesGroup* volGroup)
{
	std::string errorMessage;

	//Cube Dimension
	unsigned int numVolumes = (unsigned int) volGroup->getNumberOfVolumes();
	// Check the number of grid, if more than 1 value it is assumed to be
	// molecular orbitals so, according to the Gaussian Cube file format,
	// the number of atoms must be negative
	int numAtoms((numVolumes > 1)? -1 : 1);
	//Cube Dimension
	mathfu::Vector<double,3> origin(volGroup->getOriginInWorldSpace());
	mathfu::Vector<double,3> axisX(volGroup->getLocalXAxisInWorldSpace());
	mathfu::Vector<double,3> axisY(volGroup->getLocalYAxisInWorldSpace());
	mathfu::Vector<double,3> axisZ(volGroup->getLocalZAxisInWorldSpace());
	unsigned int nx = volGroup->getNumPointsX();
	unsigned int ny = volGroup->getNumPointsY();
	unsigned int nz = volGroup->getNumPointsZ();

	// Converts the reference frame from Angstrom to Bhor
	origin *= ANGSTROM_TO_BOHR;
	axisX *= ANGSTROM_TO_BOHR;
	axisY *= ANGSTROM_TO_BOHR;
	axisZ *= ANGSTROM_TO_BOHR;

	// US locale string
#ifdef Q_OS_WIN
	std::string us_locale("en-US");
#else
	std::string us_locale("en_US.UTF8");
#endif
	//To be sure that digits have dot separator in snprintf
	std::locale::global(std::locale(us_locale));
	// Creates an output file stream
	std::ofstream ofs ;
	//Fix Locale for stream
	std::locale fixlocale(us_locale);
	ofs.imbue(fixlocale);
	// Configure the ofstream to raise exceptions on error
	ofs.exceptions(std::ofstream::failbit | std::ofstream::badbit);

	try
	{
		//Opens the file
		ofs.open(filePath, std::ofstream::out | std::ofstream::trunc);

		// Writes the header
		ofs << " " << volGroup->getName().c_str() << "\n";
		ofs << " Cube file written with Caffeine\n";

		printInCubeHeader(ofs, numAtoms);
		printInCubeHeader(ofs, origin);
		printInCubeHeader(ofs, numVolumes);
		ofs << "\n";

		printInCubeHeader(ofs, nx);
		printInCubeHeader(ofs, axisX);
		ofs << "\n";

		printInCubeHeader(ofs, ny);
		printInCubeHeader(ofs, axisY);
		ofs << "\n";

		printInCubeHeader(ofs, nz);
		printInCubeHeader(ofs, axisZ);
		ofs << "\n";

		//Write a dummy hydrogen atoms
		ofs << "    1    1.000000    0.000000    0.000000    0.000000\n";

		// Write the MO number and their labels
		if(numVolumes > 1)
		{
			printInCubeHeader(ofs, numVolumes);
			for(unsigned int i=1; i <= numVolumes; ++i)
			{
				printInCubeHeader(ofs, i);
				if((i+1) % 10 == 0) ofs << "\n"; //MO labels orderd in row of 10
			}
			if((numVolumes+1) %10 != 0) ofs << "\n";
		}

		// Write the cube
		const int BUFFER_SIZE = 128;
		char buffer[BUFFER_SIZE];
		double numbers[6];
		unsigned int numbers_per_line = 0;

		for (unsigned int i = 0; i < nx; ++i)
		{
			for (unsigned int j = 0; j < ny; ++j)
			{
				for (unsigned int k = 0; k < nz; ++k)
				{
					// The cube files are stored in staggered z
					for (unsigned int l = 0; l < numVolumes; ++l)
					{
						const DataGrid3D<double>* volume = volGroup->getVolume(l);
						mathfu::Vector<unsigned int,3> elemIdx(i,j,k);
						if(numbers_per_line < 6)
						{
							numbers[numbers_per_line] = volume->getValue(elemIdx);
							numbers_per_line++;
						}

						if(numbers_per_line == 6)
						{
							int strLen = snprintf(buffer,BUFFER_SIZE, " %12.5E %12.5E %12.5E %12.5E %12.5E %12.5E\n",
												  numbers[0], numbers[1], numbers[2], numbers[3], numbers[4], numbers[5]);
							numbers_per_line = 0;
							ofs.write(buffer, strLen);
						}
					}
				}
			}
		}
		if (numbers_per_line != 0)
		{
			for(unsigned int l = 0; l < numbers_per_line; ++l)
			{
				int strLen = snprintf(buffer, BUFFER_SIZE," %12.5E", numbers[l]);
				ofs.write(buffer, strLen);
			}
			ofs << std::endl;
		}

		ofs.flush();
		ofs.close();
	}
	catch(const std::ios_base::failure& e)
	{
		 errorMessage = std::string("Error while writing the cube file ") +
						filePath + ":\n" + e.what();
	}

	return errorMessage;
}
