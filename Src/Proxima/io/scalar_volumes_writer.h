#ifndef SCALAR_VOLUMES_WRITER_H
#define SCALAR_VOLUMES_WRITER_H

#include <memory>
#include <vector>

#include <scidata/datagrid3d_group.h>

/**
 * @author Marco Fusè
 */
namespace Proxima
{

/**
 * @brief   Writes one volumetric dataset to file.
 *			If the operation succeeds returns an empty string, otherwise
 *			returns an error message.
 */
std::string writeToFile(const std::string& filePath,
						std::shared_ptr<DataGrid3D<double>> volume);

/**
 * @brief   Writes one or more scalar volumetric datasets to file.
 *			If the operation succeeds returns an empty string, otherwise
 *			returns an error message.
 */
std::string writeToFile(const std::string& filePath,
						const Proxima::ScalarVolumesGroup* volGroup);

}


#endif // SCALAR_VOLUMES_WRITER_H
