#ifndef PROXIMA_PARSER_H
#define PROXIMA_PARSER_H

#include "./utilities/WarningsOff.h"
#include <string>
#include "./utilities/WarningsOn.h"

#include "molecularsystem.h"


/**
 * @author Federico Lazzari
 */
namespace Proxima
{
    /**
     * @brief The Parser object implements methods for parsing different molecular file formats.
     *
     * @warning The only format currently supported is the PDB file format.
     */
    class Parser
	{
	public:
        /**
         * @brief Parses a PDB file and returns a corresponding MolecularSystem instance.
         *        The Parser throws exception whether atoms have invalid symbols.
         *        The atoms are always added to Molecular System except whether the
         *        atomic number or the serial number are invalid.
         *
         *        The Parser creates different frames for different alternate locations.
         *        "MODEL" records have the priority over alternate locations, in that case
         *        the alternate locations are ignored.
         *
         * @param path The path of the PDB file.
         * @param name The name to be assigned to the returned Molecular System.
		*/
        static MolecularSystemSP readPDB(const std::string& path, const std::string& name, double total_charge=0);


        /**
         * @brief Parses a XYZ file and returns a corresponding MolecularSystem instance.
         *        The Parser throws exception whether atoms have invalid symbols.
         *        The atoms are always added to Molecular System except whether the
         *        atomic number or the serial number are invalid.
         * @param path The path of the XYZ file.
         * @param name The name to be assigned to the returned Molecular System.
         *
         * @note  The current implementation does not parse multi-model XYZ files.
         *        The motivation is that the current Proxima implementation does not allow
         *        different frames of a molecular system to have different numbers of atoms.
         *        The XYZ multi-model format, however, allows for such possibility.
         */
        static MolecularSystemSP readXYZ(const std::string& path, const std::string& name, double total_charge=0);

        /**
             * @brief It reads a Mol file
         */
        static MolecularSystemSP readMOL(const std::string& path, const std::string& name, double total_charge=0);
	};
}

#endif //PARSER_H
