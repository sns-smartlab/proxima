#include "gasteigerparameters.h"

#include <stdexcept>

using namespace Proxima;

std::map<int,GasteigerParameters::Parameters> GasteigerParameters::atomicParameters;
std::map<std::pair<int,std::string>,GasteigerParameters::Parameters> GasteigerParameters::atomicParameters_withHyb;

bool GasteigerParameters::atomicParametersInitialized = false;

GasteigerParameters::Parameters::Parameters(double a, double b, double c)
{
    this->a=a;
    this->b=b;
    this->c=c;
}

double GasteigerParameters::getElectronegativity(int atomicNum, double charge, std::string hyb)
{
    GasteigerParameters::initAtomicParameters();

    if (GasteigerParameters::atomicParameters.find(atomicNum)!=GasteigerParameters::atomicParameters.end())
    {
        Parameters current = GasteigerParameters::atomicParameters.at(atomicNum);
        return current.a + current.b*charge + current.c*charge*charge;
    }

    std::pair<int,std::string> currentAtomState(atomicNum,hyb);

    if (GasteigerParameters::atomicParameters_withHyb.find(currentAtomState)!=GasteigerParameters::atomicParameters_withHyb.end())
    {
        Parameters current = GasteigerParameters::atomicParameters_withHyb.at(currentAtomState);
        return current.a + current.b*charge + current.c*charge*charge;
    }

    else throw std::invalid_argument("Gasteiger::getElectronegativity - Invalid atomic number or hybridisation state");

}

double GasteigerParameters::getElectronegativity_plusState(int atomicNum, std::string hyb)
{
    GasteigerParameters::initAtomicParameters();

    if (GasteigerParameters::atomicParameters.find(atomicNum)!=GasteigerParameters::atomicParameters.end())
    {
        Parameters current = GasteigerParameters::atomicParameters.at(atomicNum);
        return current.a + current.b + current.c;
    }

    std::pair<int,std::string> currentAtomState(atomicNum,hyb);

    if (GasteigerParameters::atomicParameters_withHyb.find(currentAtomState)!=GasteigerParameters::atomicParameters_withHyb.end())
    {
        Parameters current = GasteigerParameters::atomicParameters_withHyb.at(currentAtomState);
        return current.a + current.b + current.c;
    }

    else throw std::invalid_argument("Gasteiger::getElectronegativity_plusState - Invalid atomic number or hybridisation state");

}

void GasteigerParameters::initAtomicParameters()
{
    if (GasteigerParameters::atomicParametersInitialized) return;

    Hybridisation sp3(1,3,0);
    Hybridisation sp2(1,2,0);
    Hybridisation sp(1,1,0);
    Hybridisation reference(0,0,0);


    //H
    GasteigerParameters::atomicParameters.insert(std::pair<int,GasteigerParameters::Parameters>(1,GasteigerParameters::Parameters(7.17,6.24,-0.56)));

    //F
    GasteigerParameters::atomicParameters.insert(std::pair<int,GasteigerParameters::Parameters>(9,GasteigerParameters::Parameters(14.66,13.85,2.31)));

    //CL
    GasteigerParameters::atomicParameters.insert(std::pair<int,GasteigerParameters::Parameters>(17,GasteigerParameters::Parameters(11.00,9.69,1.35)));

    //BR
    GasteigerParameters::atomicParameters.insert(std::pair<int,GasteigerParameters::Parameters>(35,GasteigerParameters::Parameters(10.08,8.47,1.16)));

    //S
    GasteigerParameters::atomicParameters.insert(std::pair<int,GasteigerParameters::Parameters>(16,GasteigerParameters::Parameters(10.14,9.13,1.38)));


    //C
    GasteigerParameters::atomicParameters_withHyb.insert(std::pair<std::pair<int,std::string>,GasteigerParameters::Parameters>(std::pair<int,std::string>(6,"sp3"),Parameters(7.98,9.18,1.88)));
    GasteigerParameters::atomicParameters_withHyb.insert(std::pair<std::pair<int,std::string>,GasteigerParameters::Parameters>(std::pair<int,std::string>(6,"sp2"),Parameters(8.79,9.32,1.51)));
    GasteigerParameters::atomicParameters_withHyb.insert(std::pair<std::pair<int,std::string>,GasteigerParameters::Parameters>(std::pair<int,std::string>(6,"sp"),Parameters(10.39,9.45,0.73)));

    //N
    GasteigerParameters::atomicParameters_withHyb.insert(std::pair<std::pair<int,std::string>,GasteigerParameters::Parameters>(std::pair<int,std::string>(7,"sp3"),Parameters(11.54,10.82,1.36)));
    GasteigerParameters::atomicParameters_withHyb.insert(std::pair<std::pair<int,std::string>,GasteigerParameters::Parameters>(std::pair<int,std::string>(7,"sp2"),Parameters(12.87,11.15,0.85)));
    GasteigerParameters::atomicParameters_withHyb.insert(std::pair<std::pair<int,std::string>,GasteigerParameters::Parameters>(std::pair<int,std::string>(7,"sp"),Parameters(15.68,11.7,-0.27)));

    //O
    GasteigerParameters::atomicParameters_withHyb.insert(std::pair<std::pair<int,std::string>,GasteigerParameters::Parameters>(std::pair<int,std::string>(8,"sp3"),Parameters(14.18,12.92,1.39)));
    GasteigerParameters::atomicParameters_withHyb.insert(std::pair<std::pair<int,std::string>,GasteigerParameters::Parameters>(std::pair<int,std::string>(8,"sp2"),Parameters(17.07,13.79,0.47)));

    GasteigerParameters::atomicParametersInitialized = true;

}
