#ifndef PROXIMA_ATOM_H
#define PROXIMA_ATOM_H

#include "./utilities/WarningsOff.h"

#include "./utilities/floating_point.h"
#include <string>
#include <list>
#include <vector>
#include <map>
#include <stdexcept>

#include "./utilities/WarningsOn.h"

#include "bond.h"
#include "hbond.h"

/**
 * @author Federico Lazzari
 */
namespace Proxima
{
	//forward declaration
	class MolecularSystem;

    /**
     * @brief The Hybridisation type.
     *        It defines an Hybridisation state by simply
     *        storing the amount of s, p and d character.
     *        (e.g. sp3 -> sCharacter=1 pCharacter=3 dCharacter=0).
     */
    struct Hybridisation
    {
        unsigned sCharacter;
        unsigned pCharacter;
        unsigned dCharacter;

        /**
         * @brief This represents the hybridisation of the atom.
         * @param _sCharacter The s character of the hybridisation
         * @param _pCharacter The p character if the hybridisation
         * @param _dCharacter The d character if the hybridisation
         */
        Hybridisation(unsigned _sCharacter=0, unsigned _pCharacter=0, unsigned _dCharacter=0)
        {
            if(_sCharacter>1 || _pCharacter>3 || _dCharacter>5)
                throw std::invalid_argument("Hybridisation(_sCharacter, _pCharacter, _dCharacter) - invalid arguments.");

            sCharacter=_sCharacter;
            pCharacter=_pCharacter;
            dCharacter=_dCharacter;
        }

        bool isEqual(const Hybridisation& b)
        {
            if (b.sCharacter == this->sCharacter &&
                b.pCharacter == this->pCharacter &&
                b.dCharacter == this->dCharacter) return true;
            else return false;
        }
    };

    typedef std::shared_ptr< Proxima::Hybridisation > HybridisationSP;

    /**
    * @brief This class defines the internal structure of an Atom object.
    *        It contains properties of the atom and various methods to access them.
    */
	class Atom
	{
	public:

		/**
         * @brief bond_iterator.
         *        An iterator to a Bond object.
		 */
        typedef std::list<BondSP>::const_iterator bond_iterator;

		/**
         * @brief hbond_iterator.
         *        An iterator to a HBond object.
         */
        typedef std::list< HBondSP >::const_iterator hbond_iterator;

		/* Constructor and destructor */

        /**
         * @brief Constructor of the Atom object.
         * @param atomicNum The atomic number of the atom.
         * @param name A label associated to the atom.
         * @param serial Serial number that uniquely identifies the atom.
         * @note  An Atom is created with a single frame by default.
         */
        Atom(int atomicNum, const std::string& name, long long serial);

		/** @brief Destructor for Atom. */
		virtual ~Atom() = default;

        //Atom(const Atom &atm);

        virtual void clone(std::shared_ptr<Atom> &atm);

		// "Get" methods
        /** @brief Returns the name of the atom. */
		virtual std::string getName() const;

        /** @brief Returns the serial number of the atom. */
		virtual long long getSerial() const;

        /** @brief Returns the atomic number. */
		virtual int getAtomicNum() const;

		/**
         * @brief Returns the position of the atom
		 *		  at the specified frame.
		 * @param frameNum the frame number.
			*/
        virtual std::vector<double> getPosition(size_t frameNum) const;


		/** @brief Returns the partial charge of the atom at a certain frame.
		 *  @param frameNum the frame number.
		*/
		virtual double getCharge(size_t frameNum) const;

		/** @brief Returns the number of bonds at a certain frame.
         *  @param frameNum the frame number
		*/
		virtual size_t getNumBonds(size_t frameNum) const;

		/** @brief Returns the number of hbonds at a certain frame.
		 *  @param frameNum the frame number.
		*/
		virtual size_t getNumHBonds(size_t frameNum) const;

        virtual bool isAtomAromatic(size_t frameNum) const;


		/**
		 * @brief	If this atom, at the specified frame, is bonded
		 *			with the atom having the serial number passed as
         *			parameter, returns the pointer to the bond.
         *          Otherwise returns a null pointer.
		 * @param frameNum the frame number.
		 * @param otherAtomSerial the serial number of the other atom.
		 */
        virtual BondSP findBond(size_t frameNum, long long otherAtomSerial) const;

		/**
		 * @brief	If this atom, at the specified frame, forms an hydrogen
         *			bond with the atoms having the serial number passed as
         *			parameter, returns the pointer to the hydrogen
         *			bond. Otherwise returns an empty bond.
		 * @param frameNum is the frame number.
		 * @param atom1 is one of three atoms bonded.
		 * @param atom2 is one of three atoms bonded.
		 */
        virtual HBondSP findHBond(size_t frameNum, long long atom1, long long atom2) const;

        /**
		 * @brief  This method returns an iterator
         *         to the first bond at the specified frame.
		 * @param frameNum the frame number.
         */
		virtual bond_iterator getBondBegin(size_t frameNum) const;

        /**
		 * @brief  This method returns an iterator
         *         to the first hydrogen bond at the specified frame.
		 * @param frameNum the frame number.
         */
		virtual hbond_iterator getHBondBegin(size_t frameNum) const;

        virtual HBondSP getHBond(size_t id, size_t frameNum) const;

        /**
		 * @brief  This method returns an iterator referring
         *         to the past-the-end element of the bonds list at the specified frame.
		 * @param frameNum the frame number.
         */
		virtual bond_iterator getBondEnd(size_t frameNum) const;

        /**
         * @brief It returns the bond object at the given ID
         */
        virtual BondSP getBond(size_t id, size_t frameNum) const;

        /**
		 * @brief  This method returns an iterator referring
         *         to the past-the-end element of the hydrogen bonds list at the specified frame.
		 * @param frameNum the frame number.
         */
		virtual hbond_iterator getHBondEnd(size_t frameNum) const;

		/** @brief Gets the number of frames of the atom. */
        virtual size_t getNumFrames() const;

		/** @brief Gets the occupancy of the atom.
		 * @param frameNum the frame number.
		*/
        virtual double getOccupancy(size_t frameNum) const;

		/** @brief Gets the Temperature Factor.
		 * @param frameNum the frame number.
		*/
        virtual double getTemperatureFact(size_t frameNum) const;

        /**
         * @brief It returns the hybridisation state of the atom
         *        at the given frame number.
         * @param frameNum The frame number.
         */
        virtual Hybridisation getHybridisation(size_t frameNum) const;


		/* "Set" methods */

		/**
		 * @brief Sets a position for the atom.
		 * @param frameNum Frame number.
		 * @param newPos New position of the atom.
		*/
        virtual void setPosition(size_t frameNum,
                                 const std::vector<double>& newPos);

		/** @brief Sets the partial charge of the atom at the specified frame.
		 * @param frameNum the frame number.
		 * @param charge the charge of the atom.
		*/
        virtual void setCharge(size_t frameNum, double charge);

		/** @brief Sets the occupancy of the atom at the specified frame.
		 * @param frameNum the frame number.
		 * @param occupancy the occupancy of the atom.
		*/
        virtual void setOccupancy(size_t frameNum, double occupancy);

		/** @brief Sets the temperature factor of the atom at the specified frame.
		 * @param frameNum the frame number.
		 * @param temperature the temperature factor of the atom.
		*/
        virtual void setTemperatureFact(size_t frameNum, double temperature);

		// Copy/move constructors/operators are disabled
        Atom(const Atom& other) = delete;
        Atom(Atom&& other) = delete;
        Atom& operator=(const Atom& other) = delete;
        Atom& operator=(Atom&& other) = delete;

		/** @brief Operator == for the Atom object
		*   @note The comparison is performed using the
		*		  properties defined in the constructor.
		*		  (Atomic Number, Name and Serial).
		*/
        bool isEqual(const Atom& other) const;

        /**
         * @brief It sets a new hybridisation state to the atom.
         * @param frameNum The frame number
         * @param hybridisation The given hybridisation state
         */
        virtual void setHybridisation(size_t frameNum, const Hybridisation& hybridisation);

    protected:
		friend class MolecularSystem;

        /**
         * @brief Adds a frame to the atom.
         * @note  Only the MolecularSystem can call this method!
         */
        virtual void addFrame();

        /**
         * @brief It sets the planarity for the current bond
         */
        virtual void setPlanarity(bool value, size_t frame);

        /**
         * @brief Adds a bond to the atom.
		 * @param frameNum the frame number.
		 * @param bond a pointer to the bond to be added.
         * @note  Only the MolecularSystem can call this method!
         */
        virtual void addBond(size_t frameNum, BondSP bond);

        /**
         * @brief Adds a Hydrogen Bond to the atom.
		 * @param frameNum the frame number.
		 * @param hydrogenBond a pointer to the hydrogen bond to be added.
         * @note  Only the MolecularSystem can call this method!
         */
        virtual void addHBond(size_t frameNum, HBondSP hydrogenBond);

		/**
         * @brief Removes a Bond.
		 * @param frameNum the frame number.
		 * @param bond the bond to be removed.
         * @note  Only the MolecularSystem can call this method!
         * @note  If the atom is not part of the bond, this method has no effect.
		 */
        virtual void removeBond(size_t frameNum, BondSP bond);

		/**
         * @brief Removes a Hydrogen Bond.
		 * @param frameNum the frame number.
		 * @param hydrogenBond a pointer to the hydrogen bond to be removed.
         * @note  Only the MolecularSystem can call this method!
         * @note  If the atom is not part of the bond, this method has no effect.
		 */
		virtual void removeHBond(size_t frameNum, HBondSP hydrogenBond);

        /**
         * @brief It adds a new hybridisation state to the atom
         * @param frameNum The frame number at which the atom has the given
         *                 hybridisation state.
         * @param hybridisation   The given hybridisation state
         * @note  If a previous hybridisation state was already added,
         *        this method overloads it.
         */
        virtual void addHybridisation(size_t frameNum, const Hybridisation& hybridisation);




	protected:

		/**
		 * @brief The atomic number.
		 */
		int _atomicNum;

		/**
		 * @brief The name of the atom.
		 */
		std::string _name;

		/**
		 * @brief The serial number of the atom.
		 */
		long long _serial;

		/**
		 * @brief The Frame struct.
		 */
		struct Frame
		{

			/**
			 * @brief List of BondSP.
			 */
			std::list<BondSP> _bondsList;

            bool isAromatic;

			/**
			 * @brief List of HBondSP.
			 */
			std::list<HBondSP> _hbondsList;

			/**
             * @brief The position of the atom.
			 */
            std::vector<double> _position;

            /**
             * @brief Hybridisation of the atom
             *        for the given frame number.
             */
            Hybridisation _hybridisation;

			/**
			 * @brief Charge of the atom.
			 */
			double _charge;

			/**
			 * @brief Temperature factor.
			 */
			double _temperature;

			/**
			 * @brief Occupancy factor.
			 */
			double _occupancy;

			/**
			 * @brief Frame constructor.
			 */
			Frame()
			{
				_occupancy = 0.0;
				_temperature = 0.0;
				_charge = 0.0;
                _hybridisation = Hybridisation(0,0,0);
                _position = std::vector<double>{0.,0.,0.};
                isAromatic = false;
			}

			/**
			 * @brief Copy constructor.
			 */
            Frame(const Frame &frame2)
			{
                _occupancy     =   frame2._occupancy;
                _temperature   =   frame2._temperature;
                _charge        =   frame2._charge;
                _position      =   frame2._position;
                _hybridisation =   frame2._hybridisation;
                isAromatic     =   frame2.isAromatic;
			}

            Frame& operator= (Frame &frame2)
            {
                _occupancy     =   frame2._occupancy;
                _temperature   =   frame2._temperature;
                _charge        =   frame2._charge;
                _position      =   frame2._position;
                _hybridisation =   frame2._hybridisation;
                isAromatic     =   frame2.isAromatic;
                return *this;
            }
		};

		/**
         * @brief Map containing frames related to specific
         *        alternate location indexes.
		 */
        std::vector<Frame> _frames;

	};

    /** @brief AtomSP is a smart pointer to an Atom object. */
	typedef std::shared_ptr< Proxima::Atom > AtomSP;

	/** @brief	Functor implementing a "less then" operator between two AtomSP objects.
	 *			This functor has been designed to be used to store AtomSP objects in
	 *			sorted data structures (e.g. std::map or std::set).
	 *
	 *			The comparison is performed by comparing the serial number of the atoms.
	*/
	class AtomSPLessFunctor
	{
	public:
		bool operator() (const AtomSP& atom1, const AtomSP& atom2) const
		{ return atom1->getSerial() < atom2->getSerial(); }
	};


}


#endif // ATOM_H
