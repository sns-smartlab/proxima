#ifndef PROXIMA_NUCLEOTIDERESIDUE_H
#define PROXIMA_NUCLEOTIDERESIDUE_H

#include "residue.h"

/**
 * @author Federico Lazzari
 */
namespace Proxima
{

/**
 * @brief The NucleotideResidue represents nucleotides within the molecular system.
 */
class NucleotideResidue : public Proxima::Residue
{

    //Serial number of the atoms of the Phosphate group
    struct Phosphate
    {
        //Flags which indicate the validity of the atoms
        bool is_P_Valid=false;
        bool is_O1P_Valid=false;
        bool is_O2P_Valid=false;

        long long P=0;
        long long O1P=0;
        long long O2P=0;
    };

    //Serial number of the atoms of the Sugar
    struct Sugar
    {
        //Flags which indicate the validity of the atoms
        bool is_O5_Valid=false;
        bool is_C5_Valid=false;
        bool is_C4_Valid=false;
        bool is_O4_Valid=false;
        bool is_C3_Valid=false;
        bool is_O3_Valid=false;
        bool is_C2_Valid=false;
        bool is_O2_Valid=false;
        bool is_C1_Valid=false;

        //Hydrogens
        bool is_H51_valid=false;
        bool is_H52_valid=false;
        bool is_H4_valid=false;
        bool is_H1_valid=false;
        bool is_H21_valid=false;
        bool is_H22_valid=false;
        bool is_H3_valid=false;

        //----------------------------
        long long C5=0;
        long long O5=0;
        long long C4=0;
        long long O4=0;
        long long C3=0;
        long long O3=0;
        long long C2=0;
        //Ribose only
        //-------------
        long long O2=0;
        //-------------
        long long C1=0;

        //Hydrogens
        long long H51;
        long long H52;
        long long H4;
        long long H1;
        long long H21;
        long long H22;
        long long H3;

    };

    //TODO: Nitrogen base


private:

    Phosphate _phosphate;
    Sugar _sugar;
    std::vector<long long> nitrogenous_base;
    std::vector<long long> generic_atoms;

    //TODO: Nucleotide

public:

    /**
     * @brief This structure defines the constants to be
     *          passed as "role" parameter of the addAtom()
     *          method in order to denote the specific role
     *          of the atom within the residue.
     */
    enum class AtomRole
    {
        GENERIC_ATOM = 0, // Denotes a generic atom.
        PHOSPHATE_P, // Denotes the nitrogen atom of the amino group.
        PHOSPHATE_O1P, // Denotes the carbon-alpha atom.
        PHOSPHATE_O2P, // Denotes the carbon atom of the carboxyl group.

        SUGAR_O5, // Denotes the oxygen atom of the carboxyl group.
        SUGAR_C5,
        SUGAR_C4,
        SUGAR_O4,
        SUGAR_C3,
        SUGAR_O3,
        SUGAR_C2,
        SUGAR_O2,
        SUGAR_C1,

        SUGAR_H51,
        SUGAR_H52,
        SUGAR_H4,
        SUGAR_H1,
        SUGAR_H21,
        SUGAR_H22,
        SUGAR_H3,

        NITROGENOUSBASE
    };

    /**
     * @brief Constructor
     * @param sequenceNumber The sequence number of the new residue within the polypeptide.
     *                       Sequence number must be unique!
     * @param name  The name of this residue.
     *              Usually this name is not unique, since denotes its type.
     *              If an empty string is passed as parameter,
     *              Residue::DEFAULT_NAME is used.
     */
    NucleotideResidue(long long serial, long long sequenceNumber, char iCode=0, const std::string& name=Residue::DEFAULT_NAME);


    /**
     * @brief Destructor
     */
    virtual ~NucleotideResidue() {}


    virtual void clone(std::shared_ptr<NucleotideResidue> &other);

    /**
     * @brief Adds an atom to the residue.
     *          If one of the following conditions happens, the insertion fails
     *          and this method returns false:
     *
     *          - The residue already contains an atom with the same serial number.
     *
     *          - The atom can't be employed in the specified role.
     *
     * @param atom The serial number of the atom to add to the residue.
     *
     * @param role The role of the atom within the aminoresidue.
     *
     * @return True if the operation succeeds, false otherwise.
     *
     * @throw std::invalid_argument if the smart pointer passed as parameter is NULL
     */
    virtual bool addAtom(long long serial, int role = int(AtomRole::GENERIC_ATOM)) override;


    /**
     * @brief It returns the phosphate group of the nucleotide.
     */
    virtual std::vector<std::pair<AtomSP, int>> getPhosphateGroup() const;

    /**
     * @brief It returns the sugar portion of the nucleotide.
     */
    virtual std::vector<std::pair<AtomSP, int>> getSugar() const;

    /**
     * @brief It returns the nitrogenous base of the nucleotide.
     */
    virtual std::vector<std::pair<AtomSP, int>> getNitrogenousBase() const;

    /**
     * @brief It returns all of the generic atoms that are part of the nucleotide.
     */
    virtual std::vector<std::pair<AtomSP, int>> getGenericAtoms() const;


};

/** @brief NucleotideResidueSP is a smart pointer to a NucleotideResidue object. */
typedef std::shared_ptr< Proxima::NucleotideResidue > NucleotideResidueSP;

}
#endif // NUCLEOTIDERESIDUE_H
