#ifndef PROXIMA_RINGGENERATOR_H
#define PROXIMA_RINGGENERATOR_H

#include "./utilities/WarningsOff.h"

#include<memory>
#include<string>
#include<set>
#include<map>

#include "./utilities/WarningsOn.h"

#include "atom.h"

/**
 * @author Federico Lazzari
 */
namespace Proxima
{

/**
 * @brief The RingGenerator class handles different methods
 *        for the Perception of rings within Molecular Systems.
 *
 *        In particular, it is focused on the partitioning of the
 *        molecular system in blocks of cyclic atoms.
 */
class RingGenerator
{

public:
    /**
     * @brief The constructor of the class.
     * @param ms       The reference Molecular System
     * @param frameNum The frame number
     */
    RingGenerator(MolecularSystem* ms, size_t frameNum);


    /** @brief Destructor. */
    virtual ~RingGenerator() = default;

    /**
     * @brief It returns a set of connected components
     *        of the graph containing cyclic atoms only.
     */
    virtual std::set<std::set<BondSP>> getBlocks();

    /**
     * @brief It computes the Horton cycles with the block partitioning scheme.
     */
    virtual std::vector<std::vector<BondSP>> getHortonCycles();

protected:

    /**
     * @brief It removes the terminal atoms from the molecular system
     * @param source The source terminal atom serial number
     */
    virtual bool removeTerminalAtoms_fromSource(long long source);

    /**
     * @brief Performs a DFS on a source atom (it is a recursive function) to check whether is a cyclic atom
     * @param source    The source atom from which the DFS started
     * @param current   The current atoms at which the DFS is running
     * @param lastBond  The last bond find during the DFS
     * @param visited   The set of visited nodes
     */
    virtual bool isPartOfCycle_DFS(long long source, long long current, BondSP lastBond, std::vector<int> &visited);

    /**
     * @brief Checks whether the given atom is cyclic by usage of a DFS
     * @param source The atom serial number
     */
    virtual bool isCyclicAtom(long long source);

    /**
     * @brief It removes the acyclic bonds from the system
     * @param current The current terminal atom serial number
     * @return
     */
    virtual bool removeAcyclicAtomsAndBonds_fromSource(long long current);

    /**
     * @brief It returns the connected components of cyclic vertices (The blocks)
     *        from a source atom
     * @param source The serial number of the source atom
     */
    virtual std::set<BondSP> connectedComponents_fromSource(long long source);


private:

    /**
     * @brief The total ensemble of bonds and atoms that defines the current space
     *        (there is no need to compute cycles in the overall molecular system)
     */
    std::map<BondSP,bool> bondSpace;
    std::map<long long,bool> atomSpace;

    /**
     * @brief The set of atoms already associated to some block
     */
    std::map<long long,bool> atomSpace_associatedToBlocks;

    /**
     * @brief A map associating to each atom the total number of bonds in which is involved
     */
    std::map<long long, size_t> numBonds;

    //This maps are usefull when dealing with vectors and arrays
    /**
     * @brief Map associating each atom index (in the internal atom space) to an atom
     */
    std::map<size_t,long long>  atomSpace_fromIndexToAtom;

    /**
     * @brief Map associating each atom to an atom index (in the internal atom space)
     */
    std::map<long long, size_t> atomSpace_fromAtomToIndex;

    bool areBlocksGenerated=false;
    std::set<std::set<BondSP>> _blocks;

    /**
     * @brief The reference molecular system
     */
    MolecularSystem* molecularSystem;

    size_t _frameNum;
};

/**
 * @brief RingGeneratorSP
 */
typedef std::shared_ptr< Proxima::RingGenerator > RingGeneratorSP;

}


#endif // RINGGENERATOR_H
