#include "molecularbuilderstate.h"

Proxima::PDBMolecularBuilderState::PDBMolecularBuilderState()
{
    this->molBuilder=NULL;
}

Proxima::PDBMolecularBuilder *Proxima::PDBMolecularBuilderState::getBuilder()
{
    return this->molBuilder;
}

bool Proxima::PDBMolecularBuilderState::setBuilder(Proxima::PDBMolecularBuilder *molBuilder)
{
    this->molBuilder=molBuilder;
    return true;
}
