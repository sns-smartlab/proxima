#include "molecularbuilder.h"
#include "firstmodelstate.h"

using namespace Proxima;





PDBMolecularBuilder::PDBMolecularBuilder(std::string molSysName, double total_charge)
{
    //Creates an empty molecular system
    MolecularSystemSP molSys(new MolecularSystem(molSysName,total_charge));
    this->ms = molSys;

    //Creates a FirstModelState as initial state
    PDBMolecularBuilderStateSP initialState(new PDBFirstModelState);
    this->setState(initialState);

    this->resState = nullptr;

    this->currentFragment = nullptr;
    this->latestAtomGenerated = nullptr;
    this->firstAltLoc = "";
}

bool PDBMolecularBuilder::handleModelRecord()
{
    //Calls the handleModelRecord of the corrisponding state
    return (this->state)->handleModelRecord();
}

bool PDBMolecularBuilder::handleAtomRecord(RecordData &atomInfo)
{
    //Calls the handleModelRecord of the corrisponding state
    return (this->state)->handleAtomRecord(atomInfo);
}

bool PDBMolecularBuilder::addAltLocAtoms()
{
    if (this->ms->getNumFrames()>1) return true;

    //Are there multiple alternate locations?
    if (this->encounteredAltLocList.size()>=1)
    {
        //YES: For each alternate location...
        for (auto it1=this->encounteredAltLocList.begin();
             it1!=this->encounteredAltLocList.end();
             it1++)
        {
            //Adds a new frame with its relative altLoc label
            std::string altLoc = *it1;
            this->getMolecularSystem()->addFrame(altLoc);

            //For each AtomData structure assigned to the altLoc...
            std::pair<MMAPIterator, MMAPIterator> result =
                    this->altLocLabel_to_atomData.equal_range(altLoc);

            for (MMAPIterator it2 = result.first; it2!=result.second; it2++)
            {
                //Retrieves the atom
                RecordData atomInfo = it2->second;
                AtomSP atom = this->getMolecularSystem()->getAtom(atomInfo.serial);
                if (atom==nullptr) return false;

                //Retrieves the last frame created frame (the one with the current altLoc label)
                size_t frameNum = this->getMolecularSystem()->getNumFrames()-1;

                //It updates all the atom informations
                atom->setCharge(frameNum,atomInfo.charge);
                atom->setOccupancy(frameNum,atomInfo.occupancy);
                atom->setPosition(frameNum,atomInfo.position);
                atom->setTemperatureFact(frameNum,atomInfo.temperature);

            } //For each AtomData...

        } //For each alternate location...

    } //Are there multiple alternate locations?
    return true;
}



MolecularSystemSP PDBMolecularBuilder::getMolecularSystem() const
{
    return this->ms;
}

bool PDBMolecularBuilder::setState(PDBMolecularBuilderStateSP state)
{
    if (state==NULL) return false;
    //It updates the current state
    this->state = state;
    //It sets the molecularBuilder inside the state
    state->setBuilder(this);
    return true;
}


bool PDBMolecularBuilder::parseAtomRecord(const std::string &record, unsigned numLine,
                                        RecordData &out_atomInfo)
{
    bool ok = true;

    //---------------
    //ATOM SECTION
    //---------------

    //ATOM SERIAL
    long long serial = 0;
    std::string serialStr(record.substr(6,5));
    serial = Utilities::stringToLongLong(serialStr, ok);
    if( !ok ) // Conversion failed
    {
        std::cerr << "WARNING! PDBParser - Invalid atom serial ("
                  << serialStr
                  << ")in the pdb at line "
                  << std::to_string(numLine)
                  << std::endl;
        return false;
    } else {
        out_atomInfo.serial = serial;
    }


    //ATOM NAME
    std::string name = Utilities::trim(record.substr(12,4));

    out_atomInfo.name = name;


    //ATOMIC SYMBOL AND NUMBER.
    int atomicNum;

    //There are two possible locations for atomic symbols: (76,2) and (12,2).
    std::string Symbol_76 = Utilities::trim(record.substr(76,2));
    std::string Symbol_12 = Utilities::trim(record.substr(12,2));

    Symbol_12.erase(std::remove_if(Symbol_12.begin(), Symbol_12.end(), [](const char ch) { return !isalpha(ch); }),Symbol_12.end());

    atomicNum = Element::getAtomicNumber(Symbol_76);
    if(atomicNum == 0)
    {
        atomicNum = Element::getAtomicNumber(Symbol_12);
        if (atomicNum == 0)
        {
            std::cerr<<("WARNING! PDBParser - Invalid atomic symbol for the atom pdb at line " + std::to_string(numLine))<<std::endl;
            out_atomInfo.atomicNumber = 0;
        }
        else
            out_atomInfo.atomicNumber = atomicNum;
    }
    else
        out_atomInfo.atomicNumber = atomicNum;


    //CHARGE
    double charge = 0;
    std::string chargeStr(record.substr(78,2));
    // In the PDB file format the sign of the charge is placed AFTER the intensity digit
    // (e.g. 2+). To convert such string in a double we have to swap the two characters.
    std::swap(chargeStr[0], chargeStr[1]);

    charge = Utilities::stringToDouble(chargeStr, ok);
    if( !ok ) // Conversion failed
    {
        charge=0;
        ok=true;
    } else {
        out_atomInfo.charge = charge;
    }


    //POSITION X
    double x = 0;
    std::string xStr(record.substr(30,8));
    x = Utilities::stringToDouble(xStr, ok);
    if( !ok ) // Conversion failed
    {
        std::cerr << "WARNING! PDBParser - Invalid X coordinate ("
                  << xStr
                  << ") in the pdb at line "
                  << std::to_string(numLine) << std::endl;
        x=0;
        ok=true;
    }


    //POSITION Y
    double y = 0;
    std::string yStr(record.substr(38,8));
    y = Utilities::stringToDouble(yStr, ok);
    if( !ok ) // Conversion failed
    {
        std::cerr << "WARNING! PDBParser - Invalid Y coordinate ("
                  << yStr
                  << ") in the pdb at line "
                  << std::to_string(numLine) << std::endl;
        y=0;
        ok=true;
    }

    //POSITION Z
    double z = 0;
    std::string zStr(record.substr(46,8));
    z = Utilities::stringToDouble(zStr, ok);
    if( !ok ) // Conversion failed
    {
        std::cerr << "WARNING! PDBParser - Invalid z coordinate ("
                  << zStr
                  << ") in the pdb at line "
                  << std::to_string(numLine) << std::endl;
        z=0;
        ok=true;
    }

    out_atomInfo.position = std::vector<double>{x,y,z};

    //OCCUPANCY

    double occupancy;
    std::string _occupancy(record.substr(54,6));
    if (!Utilities::containsDigit(_occupancy)) occupancy=1;
    else occupancy = std::stod(_occupancy,NULL);
    out_atomInfo.occupancy = occupancy;

    //TEMPERATURE

    double temperature;
    std::string _temperature(record.substr(60,6));
    if (!Utilities::containsDigit(_temperature)) temperature=0;
    else  temperature = std::stod(_temperature,NULL);
    out_atomInfo.temperature = temperature;


    //ALT LOC
    //The Alternate Location symbol is in the 17th column. But we start from 0 here.
    out_atomInfo.altLoc = record[16];


    //---------------
    //RESIDUE SECTION
    //---------------

    //CHAIN ID
    //(this->currentResidue).chainID = record[21];
    out_atomInfo.chainID = record[21];

    //RESIDUE SEQUENCE NUMBER
    long long resSeqNumber = 0;
    std::string resSeqNumStr(record.substr(22,4));
    resSeqNumber = Utilities::stringToLongLong(resSeqNumStr, ok);
    if( !ok ) // Conversion failed
    {
        std::cerr << "WARNING! PDBParser - Invalid residue sequence number ("
                  << resSeqNumStr
                  << ")in the pdb at line "
                  << std::to_string(numLine)
                  << std::endl;
    } else {
        //(this->currentResidue).residueSequenceNumber = resSeqNumber;
        out_atomInfo.residueSequenceNumber = resSeqNumber;
        //(this->currentResidue).isResidueValid = true;
        out_atomInfo.isResidueValid = true;
    }

    //RESIDUE NAME
    std::string resName = Utilities::trim(record.substr(17,3));
    //(this->currentResidue).residueName = resName;
    out_atomInfo.residueName = resName;

    //INSERTION CODE
    //(this->currentResidue).insertionCode = record[26];
    out_atomInfo.insertionCode = record[26];

    return true;
}


bool PDBMolecularBuilder::createAtom(const RecordData &atomInfo)
{
    //Checking whether the AtomData structure is not empty
    if (atomInfo.isEqual(RecordData())) return false;

    //NOTE: The creation of an atom is allowed for the first frame only
    if (this->getMolecularSystem()->getNumFrames()>1) return false;

    //If the atom is already contained in the Molecular System
    //there is no need to create it again, it just skips the
    //creation of the atom
    if ((this->ms)->containsAtom(atomInfo.serial))
    {
        std::cerr<<"WARNING! PDBParser - Trying to create an already existing atom."<<std::endl;
        return true;
    }

    //It creates a new atom with its relative atomic informations
    AtomSP atom(new Atom(atomInfo.atomicNumber,atomInfo.name,atomInfo.serial));
    atom->setCharge(0,atomInfo.charge);
    atom->setOccupancy(0,atomInfo.occupancy);
    atom->setPosition(0,atomInfo.position);
    atom->setTemperatureFact(0,atomInfo.temperature);

    try
    {
        (this->ms)->addAtom(atom);
        this->latestAtomGenerated = atom;
        return true;
    } catch(std::invalid_argument) {return false;}

}


bool PDBMolecularBuilder::updateAtom(size_t frameNum, const RecordData &atomInfo)
{
    //Checking whether the frame number is reasonable
    if(frameNum >= (this->ms)->getNumFrames()) return false;

    //Checking whether the AtomData structure is not empty
    if (atomInfo.isEqual(RecordData())) return false;

    //Retrieving the atom
    AtomSP atom = (this->ms)->getAtom(atomInfo.serial);

    //If the atom is not in the molecular system
    if (atom==NULL) return false;

    //Updating atomic informations
    atom->setCharge(frameNum,atomInfo.charge);
    atom->setOccupancy(frameNum,atomInfo.occupancy);
    atom->setPosition(frameNum,atomInfo.position);
    atom->setTemperatureFact(frameNum,atomInfo.temperature);

    return true;

}

bool PDBMolecularBuilder::addAltLoc(RecordData atomData)
{
    //When adding an alternate location the serial number
    //in the PDB changes, but it is always referred to the serial
    //number of the latest generated atom.
    atomData.serial = latestAtomGenerated->getSerial();

    //Checking whether an empty character is passed as an argument
    if (atomData.altLoc=="") return false;

    //Checking whether the AtomData structure is not empty
    if (atomData.isEqual(RecordData())) return false;

    try {
        //Adding the alternate location to the list of encountered alternate locations
        this->encounteredAltLocList.insert(atomData.altLoc);
        //Adding the couple (alternate location, atomic data)
        this->altLocLabel_to_atomData.insert(std::make_pair(atomData.altLoc,atomData));
        return true;
    } catch (std::invalid_argument) {return false;}


}

bool PDBMolecularBuilder::setFirstAltLoc(const std::string firstAltLoc)
{
    //Checking whether an empty character is passed as an argument
    if (firstAltLoc=="") return false;

    try {
        this->firstAltLoc = firstAltLoc;
        return true;
    } catch (std::invalid_argument) {return false;}

}

bool PDBMolecularBuilder::setFirstResidue(const RecordData &atomInfo)
{
    (this->currentResidue).atoms.clear();
    (this->currentResidue).atoms.insert(std::make_pair(atomInfo.name,atomInfo.serial));
    (this->currentResidue).chainID=atomInfo.chainID;
    (this->currentResidue).insertionCode = atomInfo.insertionCode;
    (this->currentResidue).residueName = atomInfo.residueName;
    (this->currentResidue).residueSequenceNumber = atomInfo.residueSequenceNumber;
    (this->currentResidue).isResidueValid = atomInfo.isResidueValid;
    return true;
}

bool PDBMolecularBuilder::checkResidue(RecordData &atomInfo)
{
    if (!atomInfo.isResidueValid) return false;

    //IF THE RESIDUE IS THE SAME
    if ((this->currentResidue).residueName==atomInfo.residueName                       &&
        (this->currentResidue).residueSequenceNumber == atomInfo.residueSequenceNumber &&
        (this->currentResidue).insertionCode == atomInfo.insertionCode)
    {
        //It adds the new atom to the current residue
        (this->currentResidue).atoms.insert(std::make_pair(atomInfo.name,atomInfo.serial));
        return true;
    }

    //OTHERWISE...
    else
    {
        //The Residue Builder handles the residue structure
        this->handleNewResidue();

        //The latest generated residue is setted as the current one
        (this->latestResidueGenerated)=(this->currentResidue);

        //It erases the current residue so as to be updated with new data
        (this->currentResidue).atoms.clear();
        (this->currentResidue).atoms.insert(std::make_pair(atomInfo.name,atomInfo.serial));
        (this->currentResidue).chainID=atomInfo.chainID;
        (this->currentResidue).insertionCode = atomInfo.insertionCode;
        (this->currentResidue).residueName = atomInfo.residueName;
        (this->currentResidue).residueSequenceNumber = atomInfo.residueSequenceNumber;
        (this->currentResidue).isResidueValid = atomInfo.isResidueValid;
        return true;
    }
}

std::string PDBMolecularBuilder::getFirstAltLoc()
{
    return this->firstAltLoc;
}

PDBResidueData PDBMolecularBuilder::getCurrentResidue()
{
    return this->currentResidue;
}

PDBResidueData PDBMolecularBuilder::getPreviousResidue()
{
    return this->latestResidueGenerated;
}

FragmentSP PDBMolecularBuilder::getCurrentFragment()
{
    return this->currentFragment;
}

void PDBMolecularBuilder::setCurrentResidueState(PDBResidueBuilderStateSP newState)
{

    newState->setBuilder(this);
    this->resState = newState;

}

void PDBMolecularBuilder::setCurrentFragment(FragmentSP frag)
{
    this->currentFragment = frag;
    this->getMolecularSystem()->addFragment(frag);
}

bool PDBMolecularBuilder::handleNewResidue()
{
    //It detects the type of the residue
    this->detectTypeOfResidue();

    //It checks whether it is bonded to the previous one
    if (this->resState->isResidueBondedToPreviousOne())
    {
        //It creates the residue and it adds atoms to the residue
        bool isCreated = (this->resState)->createResidue();
        bool areAtomsAdded = (this->resState)->addAtomsToResidue();
        return (isCreated && areAtomsAdded);
    }
    else
    {
        //It creates a new fragment, a new residue and adds atoms to the residue
        PDBResidueData current = this->getCurrentResidue();
        FragmentSP newFrag(new Fragment(current.chainID));
        this->setCurrentFragment(newFrag);

        bool isCreated= (this->resState)->createResidue();
        bool areAtomsAdded = (this->resState)->addAtomsToResidue();

        return (isCreated && areAtomsAdded);

    }

}

void PDBMolecularBuilder::detectTypeOfResidue()
{
    //It retrieves data about the current residue
    PDBResidueData resData = this->getCurrentResidue();

    //It checks whether is an Amino Acid
    if (this->isAminoAcid(resData))
    {
        //It sets the current state as an AminoResidueBuilderState
        PDBResidueBuilderStateSP newAminoResidue(new PDBAminoResidueBuilderState());
        this->setCurrentResidueState(newAminoResidue);
    }

    else if (this->isNucleotide(resData))
    {
        //It sets the current state as a NucleotideResidueBuilderState
        PDBResidueBuilderStateSP newNucleotideResidue(new PDBNucleotideResidueBuilderState());
        this->setCurrentResidueState(newNucleotideResidue);
    }

    else
    {
        //It sets the current state as an GenericResidueBuilderState
        PDBResidueBuilderStateSP newGenericResidue(new PDBGenericResidueBuilderState());
        this->setCurrentResidueState(newGenericResidue);
    }
}

void PDBMolecularBuilder::setCurrentResidue(PDBResidueData &resData)
{
    this->currentResidue = resData;
}

bool PDBMolecularBuilder::isAminoAcid(const PDBResidueData &resData)
{
    //It looks for characteristic names of atoms in amino residues
    bool isN = false,isCA = false,isC = false,isO = false;
    for (auto it=resData.atoms.begin(); it!=resData.atoms.end(); it++)
    {
        if (it->first=="N") isN = true;
        if (it->first=="CA")isCA = true;
        if (it->first=="C") isC = true;
        if (it->first=="O") isO = true;
    }

    return (isN && isCA && isC && isO);
}

bool PDBMolecularBuilder::isNucleotide(const PDBResidueData &resData)
{
    //It looks for the phosphate and the sugar.
    bool isO5 = false,isC5 = false,isC4 = false,isO4 = false,isC3 = false,isC2 = false,isC1 = false;

    for (auto it=resData.atoms.begin(); it!=resData.atoms.end(); it++)
    {
//        if (it->first=="P") isP = true;
//        if (it->first=="O1P")isO1P = true;
//        if (it->first=="O2P") isO2P = true;

        if (it->first=="O5" || it->first=="O5'"  || it->first=="O5*") isO5 = true;
        if (it->first=="C5" || it->first=="C5'"  || it->first=="C5*") isC5 = true;
        if (it->first=="C4" || it->first=="C4'"  || it->first=="C4*") isC4 = true;
        if (it->first=="O4" || it->first=="O4'"  || it->first=="O4*") isO4 = true;
        if (it->first=="C3" || it->first=="C3'"  || it->first=="C3*") isC3 = true;
        //if (it->first=="O3" || it->first=="O3'"  || it->first=="O3*") isO3 = true;
        if (it->first=="C2" || it->first=="C2'"  || it->first=="C2*") isC2 = true;
        if (it->first=="C1" || it->first=="C1'"  || it->first=="C1*") isC1 = true;
    }


    return (isO5 && isC5 && isC4 && isO4 && isC3 && isC2 && isC1);
}

//Constructor
RecordData::RecordData()
{
    this->altLoc ="";
    this->atomicNumber=0;
    this->charge=0;
    this->name = "";
    this->occupancy=0;
    this->position=std::vector<double>{0.,0.,0.};
    this->serial=0;
    this->temperature=0;
    this->chainID=' ';
    this->insertionCode=' ';
    this->residueName="";
    this->residueSequenceNumber=0;
    this->isResidueValid=false;
}

//Copy constructor
RecordData::RecordData(const RecordData &other)
{
    this->altLoc = other.altLoc;
    this->atomicNumber = other.atomicNumber;
    this->charge = other.charge;
    this->name = other.name;
    this->occupancy = other.occupancy;
    this->position = other.position;
    this->serial = other.serial;
    this->temperature = other.temperature;
    this->chainID=other.chainID;
    this->insertionCode=other.insertionCode;
    this->residueName=other.residueName;
    this->residueSequenceNumber=other.residueSequenceNumber;
    this->isResidueValid = other.isResidueValid;
}

//operator ==
bool RecordData::isEqual(const RecordData &other) const
{
    return (this->altLoc==other.altLoc                               &&
            this->atomicNumber == other.atomicNumber                 &&
            fuzzyEqual(this->charge, other.charge)                   &&
            this->name == other.name                                 &&
            fuzzyEqual(this->occupancy,other.occupancy)              &&
            Utilities::areVectorEqual(this->position,other.position) &&
            this->serial == other.serial                             &&
            this->chainID==other.chainID                             &&
            this->insertionCode==other.insertionCode                 &&
            this->residueName==other.residueName                     &&
            this->residueSequenceNumber==other.residueSequenceNumber &&
            this->isResidueValid==other.isResidueValid               &&
            fuzzyEqual(this->temperature,other.temperature));
}

PDBResidueData::PDBResidueData(const PDBResidueData &other)
{
    this->atoms = other.atoms;
    this->chainID = other.chainID;
    this->insertionCode = other.insertionCode;
    this->residueName = other.residueName;
    this->residueSequenceNumber = other.residueSequenceNumber;
    this->isResidueValid=other.isResidueValid;
}

bool PDBResidueData::isEqual(const PDBResidueData &other) const
{
    return (this->atoms == other.atoms                                &&
            this->chainID == other.chainID                            &&
            this->insertionCode == other.insertionCode                &&
            this->residueName == other.residueName                    &&
            this->residueSequenceNumber == other.residueSequenceNumber&&
            this->isResidueValid == other.isResidueValid);
}


PDBResidueData::PDBResidueData()
{
    (this->atoms).clear();
    this->chainID = ' ';
    this->insertionCode = ' ';
    this->residueName = " ";
    this->residueSequenceNumber = 0;
    this->isResidueValid=false;
}
