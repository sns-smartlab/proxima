#ifndef PROXIMA_PDBGENERICRESIDUEBUILDERSTATE_H
#define PROXIMA_PDBGENERICRESIDUEBUILDERSTATE_H

#include "residuebuilderstate.h"

/**
 * @author Federico Lazzari
 */
namespace Proxima
{

/**
 * @brief The PDBGenericResidueBuilderState class is a support class
 *        that handles the creation of generic Residues.
 */
class PDBGenericResidueBuilderState : public PDBResidueBuilderState
{
public:
    /**
     * @brief It checks whether the current residue is bonded to the previous one.
     */
    virtual bool isResidueBondedToPreviousOne();

    /**
     * @brief It adds the current atoms
     *        to the residue with the corresponding roles.
     */
    virtual bool addAtomsToResidue();

    /**
     * @brief It creates the residue.
     */
    virtual bool createResidue();
};

}
#endif // PDBGENERICRESIDUEBUILDERSTATE_H
