#ifndef PROXIMA_NUCLEOTIDERESIDUEBUILDERSTATE_H
#define PROXIMA_NUCLEOTIDERESIDUEBUILDERSTATE_H

#include "residuebuilderstate.h"
#include "../../molecularsystem.h"

/**
 * @author Federico Lazzari
 */
namespace Proxima
{

/**
 * @brief The PDBNucleotideResidueBuilderState class is a support class
 *        that handles the creation of nucleotide residues.
 */
class PDBNucleotideResidueBuilderState : public PDBResidueBuilderState
{
public:

    PDBNucleotideResidueBuilderState();

    /**
     * @brief It checks whether the current residue is bonded to the previous one.
     */
    virtual bool isResidueBondedToPreviousOne();

    /**
     * @brief It adds the current atoms
     *        to the residue with the corresponding roles.
     */
    virtual bool addAtomsToResidue();

    /**
     * @brief It creates the residue object.
     */
    virtual bool createResidue();

private:
    /**
     * @brief It checks the distance between the phosphorus and
     *        the oxygen atom of two nearby nucleotides.
     *        It then checks whether they are bonded together
     *        or not.
     */
    virtual bool checkDistance(AtomSP phosphorus, AtomSP oxygen);

    /**
     * @brief It checks whether the given atom name is compatible with
     *        a specific atom role in a Nucleotide Residue.
     */
    virtual bool isNucleotideRoleRecognized(std::string atomName);

    /**
     * @brief It returns the atom role corrisponding
     *        to the given atom name for a Nucleotide residue.
     */
    virtual int getNucleotideRole(std::string atomName);

private:

    //Typical names of atoms in a nucleotide residue;
    std::map<std::string,int> atomRoles_inNucleotides;

};


}

#endif // NUCLEOTIDERESIDUEBUILDERSTATE_H
