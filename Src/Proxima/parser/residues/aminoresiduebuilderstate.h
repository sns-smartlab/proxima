#ifndef PROXIMA_PDBAMINORESIDUEBUILDERSTATE_H
#define PROXIMA_PDBAMINORESIDUEBUILDERSTATE_H

#include "residuebuilderstate.h"
#include "../../molecularsystem.h"

/**
 * @author Federico Lazzari
 */
namespace Proxima
{

/**
 * @brief The PDBAminoResidueBuilderState class is a support class
 *        that handles the creation of AminoResidues during the parsing
 *        of PDB files.
 */
class PDBAminoResidueBuilderState : public PDBResidueBuilderState
{
public:

    PDBAminoResidueBuilderState();

    /**
     * @brief It checks whether the current residue is bonded to the previous one.
     */
    virtual bool isResidueBondedToPreviousOne();

    /**
     * @brief It adds the current atoms
     *        to the residue with the corresponding roles.
     */
    virtual bool addAtomsToResidue();

    /**
     * @brief It creates the residue.
     */
    virtual bool createResidue();

private:
    /**
     * @brief It checks the distance between the nitrogen and
     *        the carbon atom of two nearby amino residues.
     *        It then checks whether they are bonded together
     *        or not.
     */
    virtual bool checkDistance(AtomSP nitrogen, AtomSP carbon);

    /**
     * @brief It checks whether the given atom name is compatible with
     *        a specific atom role in an Amino Residue.
     */
    virtual bool isAminoRoleRecognized(std::string atomName);

    /**
     * @brief It returns the atom role corrisponding
     *        to the given atom name for an amino residue.
     */
    virtual int getAminoRole(std::string atomName);


private:
    //Typical names of atoms in an amino residue;
    std::map<std::string,int> atomRoles_inAminoAcids;

};

}
#endif // PDBAMINORESIDUEBUILDERSTATE_H
