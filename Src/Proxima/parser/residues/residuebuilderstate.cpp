#include "residuebuilderstate.h"

using namespace Proxima;

PDBResidueBuilderState::PDBResidueBuilderState()
{
    this->resBuilder=NULL;
}

bool PDBResidueBuilderState::setBuilder(PDBMolecularBuilder *builder)
{
    this->resBuilder = builder;
    return true;
}

PDBMolecularBuilder *PDBResidueBuilderState::getParentBuilder()
{
    return this->resBuilder;
}
