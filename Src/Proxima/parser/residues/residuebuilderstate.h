#ifndef PROXIMA_RESIDUEBUILDERSTATE_H
#define PROXIMA_RESIDUEBUILDERSTATE_H

#include "../../utilities/WarningsOff.h"
#include <memory>
#include "../../utilities/WarningsOn.h"

/**
 * @author Federico Lazzari
 */
namespace Proxima
{

class PDBMolecularBuilder;
struct PDBResidueData;

/**
 * @brief The MolecularBuilder class is a support class
 *        usefull for the proper functioning of the PDB Parser.

 *       It is an abstract class that represents the current
 *       residue state in a "state design pattern" for the PDB parser.
 *
 *       The class defines methods for the creation and the management
 *       of residues, both of this methods are overrided by the concrete
 *       classes AminoResidueBuilderState, NucleotideResidueState and
 *       GenericResidueBuilderState which represent the specific
 *       recognized residues that can be encountered during the parsing of the PDB.
 */
class PDBResidueBuilderState
{
public:
    PDBResidueBuilderState();

    virtual ~PDBResidueBuilderState() = default;

    /**
     * @brief It checks whether the current residue is bonded
     *        to the previous one.
     */
    virtual bool isResidueBondedToPreviousOne() = 0;

    /**
     * @brief It adds the current atoms
     *        to the residue with the corresponding roles.
     */
    virtual bool addAtomsToResidue() = 0;

    /**
     * @brief It creates the residue.
     */
    virtual bool createResidue() = 0;

protected:
    friend class PDBMolecularBuilder;
    //It sets the current Molecular Builder to the current ResidueBuilderState
    virtual bool setBuilder(PDBMolecularBuilder* builder);

protected:
    friend class PDBAminoResidueBuilderState;
    friend class PDBGenericResidueBuilderState;
    //It gets the current Molecular Builder
    virtual PDBMolecularBuilder* getParentBuilder();

private:

    PDBMolecularBuilder* resBuilder;

};

/**
 * @brief The shared_ptr type for the class MolecularBuilderState
 */
typedef std::shared_ptr< Proxima::PDBResidueBuilderState > PDBResidueBuilderStateSP;

}
#endif // RESIDUEBUILDERSTATE_H
