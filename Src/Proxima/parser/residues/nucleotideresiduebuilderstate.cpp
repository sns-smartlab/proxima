#include "nucleotideresiduebuilderstate.h"
#include "../molecularbuilder.h"

using namespace Proxima;


PDBNucleotideResidueBuilderState::PDBNucleotideResidueBuilderState()
{
    //Inserting the first main atomic roles in Nucleotides
    this->atomRoles_inNucleotides.insert(std::make_pair("P",  int(NucleotideResidue::AtomRole::PHOSPHATE_P)    ));
    this->atomRoles_inNucleotides.insert(std::make_pair("O1P",int(NucleotideResidue::AtomRole::PHOSPHATE_O1P)  ));
    this->atomRoles_inNucleotides.insert(std::make_pair("O2P",int(NucleotideResidue::AtomRole::PHOSPHATE_O2P)  ));
    this->atomRoles_inNucleotides.insert(std::make_pair("OP1",int(NucleotideResidue::AtomRole::PHOSPHATE_O1P)  ));
    this->atomRoles_inNucleotides.insert(std::make_pair("OP2",int(NucleotideResidue::AtomRole::PHOSPHATE_O2P)  ));

    this->atomRoles_inNucleotides.insert(std::make_pair("C1'",  int(NucleotideResidue::AtomRole::SUGAR_C1)     ));
    this->atomRoles_inNucleotides.insert(std::make_pair("C1*",  int(NucleotideResidue::AtomRole::SUGAR_C1)     ));
    this->atomRoles_inNucleotides.insert(std::make_pair("C2'",  int(NucleotideResidue::AtomRole::SUGAR_C2)     ));
    this->atomRoles_inNucleotides.insert(std::make_pair("C2*",  int(NucleotideResidue::AtomRole::SUGAR_C2)     ));
    this->atomRoles_inNucleotides.insert(std::make_pair("O2'",  int(NucleotideResidue::AtomRole::SUGAR_O2)     ));
    this->atomRoles_inNucleotides.insert(std::make_pair("O2*",  int(NucleotideResidue::AtomRole::SUGAR_O2)     ));
    this->atomRoles_inNucleotides.insert(std::make_pair("C3'",  int(NucleotideResidue::AtomRole::SUGAR_C3)     ));
    this->atomRoles_inNucleotides.insert(std::make_pair("C3*",  int(NucleotideResidue::AtomRole::SUGAR_C3)     ));
    this->atomRoles_inNucleotides.insert(std::make_pair("O3'",  int(NucleotideResidue::AtomRole::SUGAR_O3)     ));
    this->atomRoles_inNucleotides.insert(std::make_pair("O3*",  int(NucleotideResidue::AtomRole::SUGAR_O3)     ));
    this->atomRoles_inNucleotides.insert(std::make_pair("C4'",  int(NucleotideResidue::AtomRole::SUGAR_C4)     ));
    this->atomRoles_inNucleotides.insert(std::make_pair("C4*",  int(NucleotideResidue::AtomRole::SUGAR_C4)     ));
    this->atomRoles_inNucleotides.insert(std::make_pair("O4'",  int(NucleotideResidue::AtomRole::SUGAR_O4)     ));
    this->atomRoles_inNucleotides.insert(std::make_pair("O4*",  int(NucleotideResidue::AtomRole::SUGAR_O4)     ));
    this->atomRoles_inNucleotides.insert(std::make_pair("C5'",  int(NucleotideResidue::AtomRole::SUGAR_C5)     ));
    this->atomRoles_inNucleotides.insert(std::make_pair("C5*",  int(NucleotideResidue::AtomRole::SUGAR_C5)     ));
    this->atomRoles_inNucleotides.insert(std::make_pair("O5'",  int(NucleotideResidue::AtomRole::SUGAR_O5)     ));
    this->atomRoles_inNucleotides.insert(std::make_pair("O5*",  int(NucleotideResidue::AtomRole::SUGAR_O5)     ));

    this->atomRoles_inNucleotides.insert(std::make_pair("H5'1",  int(NucleotideResidue::AtomRole::SUGAR_H51)   ));
    this->atomRoles_inNucleotides.insert(std::make_pair("H5*1",  int(NucleotideResidue::AtomRole::SUGAR_H51)   ));
    this->atomRoles_inNucleotides.insert(std::make_pair("H5'2",  int(NucleotideResidue::AtomRole::SUGAR_H52)   ));
    this->atomRoles_inNucleotides.insert(std::make_pair("H5*2",  int(NucleotideResidue::AtomRole::SUGAR_H52)   ));
    this->atomRoles_inNucleotides.insert(std::make_pair("H4'",   int(NucleotideResidue::AtomRole::SUGAR_H4)    ));
    this->atomRoles_inNucleotides.insert(std::make_pair("H4*",   int(NucleotideResidue::AtomRole::SUGAR_H4)    ));
    this->atomRoles_inNucleotides.insert(std::make_pair("H1'",   int(NucleotideResidue::AtomRole::SUGAR_H1)    ));
    this->atomRoles_inNucleotides.insert(std::make_pair("H1*",   int(NucleotideResidue::AtomRole::SUGAR_H1)    ));
    //Uracile (Ribose)
    this->atomRoles_inNucleotides.insert(std::make_pair("H2'",  int(NucleotideResidue::AtomRole::SUGAR_H21)    ));
    this->atomRoles_inNucleotides.insert(std::make_pair("H2*",  int(NucleotideResidue::AtomRole::SUGAR_H21)    ));
    //Deoxyribose
    this->atomRoles_inNucleotides.insert(std::make_pair("H2'1",  int(NucleotideResidue::AtomRole::SUGAR_H21)   ));
    this->atomRoles_inNucleotides.insert(std::make_pair("H2*1",  int(NucleotideResidue::AtomRole::SUGAR_H21)   ));

    this->atomRoles_inNucleotides.insert(std::make_pair("H2'2",  int(NucleotideResidue::AtomRole::SUGAR_H22)   ));
    this->atomRoles_inNucleotides.insert(std::make_pair("H2*2",  int(NucleotideResidue::AtomRole::SUGAR_H22)   ));
    this->atomRoles_inNucleotides.insert(std::make_pair("H3'",   int(NucleotideResidue::AtomRole::SUGAR_H3)    ));
    this->atomRoles_inNucleotides.insert(std::make_pair("H3*",   int(NucleotideResidue::AtomRole::SUGAR_H3)    ));


}

bool PDBNucleotideResidueBuilderState::isResidueBondedToPreviousOne()
{
    //It retrieves data about the current residue
    PDBResidueData current = this->getParentBuilder()->getCurrentResidue();

    //It retrieves data about the latest generated residue
    PDBResidueData previous = this->getParentBuilder()->getPreviousResidue();

    if (!previous.isResidueValid) return false;

    //NucleotideResidue can be bonded to other NucleotideResidues only in this approach.
    if (!this->getParentBuilder()->isNucleotide(previous))
    {
        return false;
    }

    //It takes the Phosphorus of the current Amino Residue

    //if it does not have any phosphorus atom it cannot be bonded to previous one!
    if (current.atoms.find("P")==current.atoms.end()) return false;

    long long phosphorusAtomSerial = current.atoms.find("P")->second;
    AtomSP phosphorusAtom =
            (this->getParentBuilder()->getMolecularSystem()->getAtom(phosphorusAtomSerial));

    //It takes the Oxygen of the previous Amino Residue
    long long previousOxygenAtomSerial;
    if (previous.atoms.find("O3'")!=previous.atoms.end())
        previousOxygenAtomSerial=previous.atoms.find("O3'")->second;

    else previousOxygenAtomSerial=previous.atoms.find("O3*")->second;
    AtomSP previousOxygenAtom =
            (this->getParentBuilder()->getMolecularSystem()->getAtom(previousOxygenAtomSerial));

    //It evaluates the distance between those two atoms and decides whether they are bonded or not
    bool areBonded = this->checkDistance(phosphorusAtom,previousOxygenAtom);
    return areBonded;
}

bool PDBNucleotideResidueBuilderState::addAtomsToResidue()
{
    //It retrieves data about the current residue
    PDBResidueData current = this->getParentBuilder()->getCurrentResidue();

    //It retrieves data about the current fragment
    FragmentSP fragment = this->getParentBuilder()->getCurrentFragment();

    //It retrieves the current residue
    NucleotideResidueSP residue = fragment->getNucleotideResidue(current.residueSequenceNumber,current.insertionCode);

    bool areAtomsAdded = true;

    //For every atom in the current residue
    for (auto it=current.atoms.begin(); it!=current.atoms.end(); it++)
    {
        //It gets the serial
        long long serial = it->second;

       //It gets the atom name
        std::string atomName = it->first;

        //If it is a known atom with a defined atom role (Phosphate and Sugar)
        if (this->isNucleotideRoleRecognized(atomName))
        {
            //It adds the atom to the amino residue with its relative atom role
            int atomRole = this->getNucleotideRole(atomName);
            bool isAtomAdded = residue->addAtom(serial,atomRole);
            areAtomsAdded = (areAtomsAdded && isAtomAdded);
        }

        //GENERIC ATOMS
        else if (atomName=="HTER" || atomName=="OXT" || atomName=="HCAP" || atomName=="HO2'")
        {
            bool isAtomAdded = residue->addAtom(serial,int(NucleotideResidue::AtomRole::GENERIC_ATOM));
            areAtomsAdded = (areAtomsAdded && isAtomAdded);
        }

        //It adds the atom to the amino residue as a NITROGENBASE
        else
        {
            bool isAtomAdded = residue->addAtom(serial,int(NucleotideResidue::AtomRole::NITROGENOUSBASE));
            areAtomsAdded = (areAtomsAdded && isAtomAdded);
        }
    }
    return areAtomsAdded;
}

bool PDBNucleotideResidueBuilderState::createResidue()
{
    //It retrieves data about the current residue
    PDBResidueData current = this->getParentBuilder()->getCurrentResidue();

    //It retrieves data about the current fragment
    FragmentSP fragment = this->getParentBuilder()->getCurrentFragment();

    //The residue sequence number is computed again
    //Values from the PDB are not reliable
    long long serial = fragment->getNumberOfResidues()+1;
    //current.residueSequenceNumber=seqNumber;
    this->getParentBuilder()->setCurrentResidue(current);

    //It creates a new Amino Residue and adds it to the fragment
    NucleotideResidueSP newResidue(new NucleotideResidue(serial,current.residueSequenceNumber,current.insertionCode,current.residueName));
    bool isNucleotideResidueAdded = fragment->addNucleotideResidue(newResidue);
    return isNucleotideResidueAdded;
}

bool PDBNucleotideResidueBuilderState::checkDistance(AtomSP phosphorus, AtomSP oxygen)
{
    double distance = Utilities::calculateDistance(phosphorus->getPosition(0),oxygen->getPosition(0));

    if (distance < 2.5) return true;
    else return false;
}

bool PDBNucleotideResidueBuilderState::isNucleotideRoleRecognized(std::string atomName)
{
    if (this->atomRoles_inNucleotides.find(atomName)!=this->atomRoles_inNucleotides.end())
        return true;
    else return false;
}

int PDBNucleotideResidueBuilderState::getNucleotideRole(std::string atomName)
{
    return this->atomRoles_inNucleotides.find(atomName)->second;
}
