#include "genericresiduebuilderstate.h"
#include "../molecularbuilder.h"

using namespace Proxima;

bool Proxima::PDBGenericResidueBuilderState::isResidueBondedToPreviousOne()
{
    //It retrieves data about the current residue
    PDBResidueData current = this->getParentBuilder()->getCurrentResidue();

    //It retrieves data about the latest generated residue
    PDBResidueData previous = this->getParentBuilder()->getPreviousResidue();

    if (!previous.isResidueValid) return false;

    //It checks whether the latest generated residuo is a knwon residue.
    //GenericResidues can be bound to GenericResidues only, in this approach.

    if (this->getParentBuilder()->isAminoAcid(previous))
    {
        return false;
    }

    else if (this->getParentBuilder()->isNucleotide(previous))
    {
        return false;
    }

    //It checks whether the chain ID has remained the same
    else if (previous.chainID==current.chainID)
    {
        return true;
    }

    else return false;

}

bool Proxima::PDBGenericResidueBuilderState::addAtomsToResidue()
{
    //It retrieves data about the current residue
    PDBResidueData current = this->getParentBuilder()->getCurrentResidue();

    //It retrieves data about the current fragment
    FragmentSP fragment = this->getParentBuilder()->getCurrentFragment();

    //It retrieves the current residue
    ResidueSP residue = fragment->getGenericResidue(current.residueSequenceNumber,current.insertionCode);

    bool areAtomsAdded=true;

    //For every atom in the current residue
    for (auto it=current.atoms.begin(); it!=current.atoms.end(); it++)
    {
        //It adds the atom to the residue
        long long serial = it->second;
        bool isAtomAdded = residue->addAtom(serial);
        areAtomsAdded = (areAtomsAdded&&isAtomAdded);
    }

    return areAtomsAdded;

}

bool Proxima::PDBGenericResidueBuilderState::createResidue()
{
    //It retrieves data about the current residue
    PDBResidueData current = this->getParentBuilder()->getCurrentResidue();

    //It retrieves data about the current fragment
    FragmentSP fragment = this->getParentBuilder()->getCurrentFragment();

    //The residue sequence number is computed again
    //Values from the PDB are not reliable
    long long serial = fragment->getNumberOfResidues()+1;
    //current.residueSequenceNumber=seqNumber;
    this->getParentBuilder()->setCurrentResidue(current);

    //It creates a new residue
    ResidueSP newResidue(new Residue(serial,current.residueSequenceNumber,current.insertionCode,current.residueName));
    bool isResidueAdded = fragment->addGenericResidue(newResidue);
    return isResidueAdded;
}
