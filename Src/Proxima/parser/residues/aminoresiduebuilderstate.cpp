#include "aminoresiduebuilderstate.h"
#include "../molecularbuilder.h"

using namespace Proxima;



PDBAminoResidueBuilderState::PDBAminoResidueBuilderState()
{
    //Inserting the first main atomic roles in AminoAcids
    this->atomRoles_inAminoAcids.insert(std::make_pair("N",  int(AminoResidue::AtomRole::BACKBONE_N)  ));
    this->atomRoles_inAminoAcids.insert(std::make_pair("CA", int(AminoResidue::AtomRole::BACKBONE_CA) ));
    this->atomRoles_inAminoAcids.insert(std::make_pair("C",  int(AminoResidue::AtomRole::BACKBONE_C)  ));
    this->atomRoles_inAminoAcids.insert(std::make_pair("O",  int(AminoResidue::AtomRole::BACKBONE_O)  ));
    this->atomRoles_inAminoAcids.insert(std::make_pair("HA",  int(AminoResidue::AtomRole::BACKBONE_HA)  ));

    //GLYCINE ONLY
    this->atomRoles_inAminoAcids.insert(std::make_pair("HA1",  int(AminoResidue::AtomRole::BACKBONE_HA1)  ));
    this->atomRoles_inAminoAcids.insert(std::make_pair("HA2",  int(AminoResidue::AtomRole::BACKBONE_HA2)  ));

    //Terminals
    this->atomRoles_inAminoAcids.insert(std::make_pair("OXT",int(AminoResidue::AtomRole::BACKBONE_OXT)));
}

bool PDBAminoResidueBuilderState::isResidueBondedToPreviousOne()
{
    //It retrieves data about the current residue
    PDBResidueData current = this->getParentBuilder()->getCurrentResidue();

    //It retrieves data about the latest generated residue
    PDBResidueData previous = this->getParentBuilder()->getPreviousResidue();

    if (!previous.isResidueValid) return false;

    //It checks whether the latest generated residue is an amino residue.
    //If is not, it returns false.
    //AminoResidue can be bonded to other AminoResidues only in this approach.
    if (!this->getParentBuilder()->isAminoAcid(previous))
    {
        return false;
    }

    //It takes the nitrogen atom of the current Amino Residue
    long long nitrogenAtomSerial = current.atoms.find("N")->second;
    AtomSP nitrogenAtom =
            (this->getParentBuilder()->getMolecularSystem()->getAtom(nitrogenAtomSerial));

    //It takes the carbon atom of the previous Amino Residue
    long long previousCarbonAtomSerial = previous.atoms.find("C")->second;
    AtomSP previousCarbonAtom =
            (this->getParentBuilder()->getMolecularSystem()->getAtom(previousCarbonAtomSerial));

    //It evaluates the distance between those two atoms and decides whether they are bonded or not
    bool areBonded = this->checkDistance(nitrogenAtom,previousCarbonAtom);
    return areBonded;

}

bool PDBAminoResidueBuilderState::addAtomsToResidue()
{

    //It retrieves data about the current residue
    PDBResidueData current = this->getParentBuilder()->getCurrentResidue();

    //It retrieves data about the current fragment
    FragmentSP fragment = this->getParentBuilder()->getCurrentFragment();

    //It retrieves the current residue
    AminoResidueSP residue = fragment->getAminoResidue(current.residueSequenceNumber,current.insertionCode);

    bool areAtomsAdded = true;

    //For every atom in the current residue
    for (auto it=current.atoms.begin(); it!=current.atoms.end(); it++)
    {
        //It gets the serial
        long long serial = it->second;

       //It gets the atom name
        std::string atomName = it->first;

        //If it is a known atom with a defined atom role (Backbone)
        if (this->isAminoRoleRecognized(atomName))
        {
            //It adds the atom to the amino residue with its relative atom role
            int atomRole = this->getAminoRole(atomName);
            bool isAtomAdded = residue->addAtom(serial,atomRole);
            areAtomsAdded = (areAtomsAdded && isAtomAdded);
        }

        //The (terminal) hydrogen atoms
        else if (atomName=="HO" || atomName=="H" || atomName=="HN")
        {
            bool isAtomAdded = residue->addAtom(serial,int(AminoResidue::AtomRole::GENERIC_ATOM));
            areAtomsAdded = (areAtomsAdded && isAtomAdded);
        }

        //It adds the atom to the amino residue as a SIDE_CHAIN
        else
        {

            bool isAtomAdded = residue->addAtom(serial,int(AminoResidue::AtomRole::SIDE_CHAIN));
            areAtomsAdded = (areAtomsAdded && isAtomAdded);
        }
    }
    return areAtomsAdded;
}

bool PDBAminoResidueBuilderState::createResidue()
{
    //It retrieves data about the current residue
    PDBResidueData current = this->getParentBuilder()->getCurrentResidue();

    //It retrieves data about the current fragment
    FragmentSP fragment = this->getParentBuilder()->getCurrentFragment();

    //It creates a new Amino Residue and adds it to the fragment

    //The residue sequence number is computed again
    //Values from the PDB are not reliable
    long long serial = fragment->getNumberOfResidues()+1;
    //current.residueSequenceNumber=seqNumber;

    //It updates the sequence number of the current residue data
    this->getParentBuilder()->setCurrentResidue(current);

    AminoResidueSP newResidue(new AminoResidue(serial,current.residueSequenceNumber,current.insertionCode,current.residueName));
    bool isAminoResidueAdded = fragment->addAminoResidue(newResidue);
    return isAminoResidueAdded;
}

//NOTE: Frame 0. Once the atoms have been created (in the first frame only) the topology does not change.
bool PDBAminoResidueBuilderState::checkDistance(AtomSP nitrogen, AtomSP carbon)
{
    double distance = Utilities::calculateDistance(nitrogen->getPosition(0),carbon->getPosition(0));

    if (distance < 2.5) return true;
    else return false;
}

bool PDBAminoResidueBuilderState::isAminoRoleRecognized(std::string atomName)
{
    if (this->atomRoles_inAminoAcids.find(atomName)!=this->atomRoles_inAminoAcids.end())
        return true;
    else return false;
}

int PDBAminoResidueBuilderState::getAminoRole(std::string atomName)
{
    return this->atomRoles_inAminoAcids.find(atomName)->second;
}



