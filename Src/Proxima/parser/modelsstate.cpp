#include "modelsstate.h"
#include "molecularbuilder.h"

using namespace Proxima;

bool PDBModelsState::handleAtomRecord(RecordData &atomInfo)
{
    if ( (atomInfo.altLoc!=this->getBuilder()->getFirstAltLoc()) &&
         (atomInfo.altLoc!="A")                                  &&
         (atomInfo.altLoc!=" ")) return true;

    //Updates the atomic informations at the last frame
    size_t lastFrame = (this->getBuilder())->getMolecularSystem()->getNumFrames()-1;
    bool isAtomUpdated = (this->getBuilder())->updateAtom(lastFrame,atomInfo);

    return isAtomUpdated;

}

bool PDBModelsState::handleModelRecord()
{
    //It adds a frame
    (this->getBuilder())->getMolecularSystem()->addFrame("model");
    return true;
}


