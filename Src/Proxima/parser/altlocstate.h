#ifndef PROXIMA_ALTLOCSTATUS_H
#define PROXIMA_ALTLOCSTATUS_H

#include "molecularbuilderstate.h"

/**
 * @author Federico Lazzari
 */
namespace Proxima
{

/**
 * @brief The AltLocStatus class is a support class
 *        usefull for the proper functioning of the PDB Parser.
 *
 *        It represents the state, during the parser of the PDB,
 *        where there are alternate locations and there is just
 *        one frame (the first one) declared.
 *
 */
class PDBAltLocState : public PDBMolecularBuilderState
{
public:

    bool handleAtomRecord(RecordData& atomInfo);

    bool handleModelRecord();

};



}

#endif // ALTLOCSTATUS_H
