//#include "utilities/WarningsOff.h"
#include <iostream>
//#include "utilities/WarningsOn.h"
#include "altlocstate.h"
#include "molecularbuilder.h"
#include "modelsstate.h"

using namespace Proxima;

bool PDBAltLocState::handleAtomRecord(RecordData &atomInfo)
{
    //Is the altLoc the same as the first one?
    if(atomInfo.altLoc==(this->getBuilder())->getFirstAltLoc() || atomInfo.altLoc=="A" || atomInfo.altLoc==" ")
    {
        //YES: Creates a new atom with the given data
        bool isAtomCreated = (this->getBuilder())->createAtom(atomInfo);

        this->getBuilder()->checkResidue(atomInfo);


        return (isAtomCreated);


    }

    //Otherwise
    else
    {
        //Saves the atomic informations in a separate map
        bool isAltLocAdded = (this->getBuilder())->addAltLoc(atomInfo);

        return (isAltLocAdded);
    }

}


bool PDBAltLocState::handleModelRecord()
{
    //It sends a warning
    std::cerr <<"WARNING! PDBParser - MODEL record reached with alternate locations"<<std::endl;

    //It adds a frame
    (this->getBuilder())->getMolecularSystem()->addFrame("model");

    //It generates a new ModelsStatus
    PDBMolecularBuilderStateSP newModel(new PDBModelsState());
    bool isStatusSetted = (this->getBuilder())->setState(newModel);

    return (isStatusSetted);
}
