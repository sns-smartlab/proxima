#ifndef PROXIMA_MOLECULARBUILDERSTATE_H
#define PROXIMA_MOLECULARBUILDERSTATE_H

#include "../utilities/WarningsOff.h"
#include <memory>
#include "../utilities/WarningsOn.h"

/**
 * @author Federico Lazzari
 */
namespace Proxima
{

//forward declaration
class PDBMolecularBuilder;
class PDBModelsState;
class PDBFirstModelState;
class PDBAltLocState;
struct RecordData;

/**
 * @brief The MolecularBuilder class is a support class
 *        usefull for the proper functioning of the PDB Parser.
 *
 *        It is an abstract class that represents the current state
 *        in a "state design pattern" for the PDB parser.
 *        The class defines two methods for the reading of the
 *        "MODEL" and "ATOM" records, both of this methods are
 *        overrided by the concrete classes AltLocState, FirstModelState and ModelsState
 *        which represent the specific possible status situations
 *        that can occur during the parsing of the PDB.
 */
class PDBMolecularBuilderState
{

public:

    PDBMolecularBuilderState();

    //Destructor
    virtual ~PDBMolecularBuilderState() = default;

    /**
     * @brief This function handles the MODEL record.
     *
     * @return true whether the parser can continue parsing lines
     *         false whether the parser must stop parsing lines.
     */
    virtual bool handleModelRecord() = 0;

    /**
     * @brief This function handles the ATOM/HETATM record.
     *
     * @return true whether the parser can continue parsing lines
     *         false whether the parser must stop parsing lines.
     */
    virtual bool handleAtomRecord(RecordData& atomInfo) = 0;

    /**
     * @brief It returns the reference PDBMolecularBuilder object.
     */
    virtual PDBMolecularBuilder* getBuilder();

protected:
    friend class PDBMolecularBuilder;

    virtual bool setBuilder(PDBMolecularBuilder* molBuilder);

private:

    /**
     * @brief The parent MolecularBuilder object
     *        that handles this status.
     */
    PDBMolecularBuilder* molBuilder;

};

/**
 * @brief The shared_ptr type for the class MolecularBuilderState
 */
typedef std::shared_ptr< Proxima::PDBMolecularBuilderState > PDBMolecularBuilderStateSP;
}


#endif // MOLECULARBUILDERSTATE_H
