#ifndef PROXIMA_FIRSTMODELSTATUS_H
#define PROXIMA_FIRSTMODELSTATUS_H

#include "molecularbuilderstate.h"

/**
 * @author Federico Lazzari
 */
namespace Proxima
{

/**
 * @brief The FirstModelStatus class is a support class
 *        usefull for the proper functioning of the PDB Parser.
 *
 *        It represents the state, during the parser of the PDB,
 *        where there isn't any alternate location and there is just
 *        one frame (the first one) declared.
 *
 */
class PDBFirstModelState : public PDBMolecularBuilderState
{
public:

    bool handleAtomRecord(RecordData& atomInfo);

    bool handleModelRecord();

};



}


#endif // FIRSTMODELSTATUS_H
