#include "firstmodelstate.h"
#include "molecularbuilder.h"
#include "altlocstate.h"
#include "modelsstate.h"

using namespace Proxima;

bool PDBFirstModelState::handleAtomRecord(RecordData &atomInfo)
{
    //Is this the first atom?
    if (!(this->getBuilder())->latestAtomGenerated)
    {
        //YES:: Saves the altLoc (as the firstAltLoc) and
        //creates a new Atom with the given data.
        std::string referenceAltLoc = " ";
        if (atomInfo.altLoc!=" ") referenceAltLoc=atomInfo.altLoc;
        (this->getBuilder())->setFirstAltLoc(referenceAltLoc);
        bool isAtomCreated = (this->getBuilder())->createAtom(atomInfo);

        //-------------------------

        //It sets this residue as the first residue encountered
        this->getBuilder()->setFirstResidue(atomInfo);

        return isAtomCreated;

    }

    //Is the altLoc the same as the first one?
    else if(atomInfo.altLoc==(this->getBuilder())->getFirstAltLoc() || atomInfo.altLoc=="A" || atomInfo.altLoc==" ")
    {

        //YES: Creates a new atom with the given data
        bool isAtomCreated = (this->getBuilder())->createAtom(atomInfo);

        //It checks whether is the same residue of the previous atom or a new one
        this->getBuilder()->checkResidue(atomInfo);

        return (isAtomCreated);

    }

    //Otherwise
    else
    {
        //Saves the atomic informations in a separate map.
        bool isAltLocAdded = (this->getBuilder())->addAltLoc(atomInfo);

        //Setting the first altloc as the frame label for the first frame
        std::string firstAltLoc = this->getBuilder()->getFirstAltLoc();
        (this->getBuilder())->getMolecularSystem()->setFrameLabel(0,firstAltLoc);

        //It creates an altLocState.
        PDBMolecularBuilderStateSP newAltLoc(new PDBAltLocState);
        bool isStatusSetted = (this->getBuilder())->setState(newAltLoc);

        return (isAltLocAdded && isStatusSetted);

    }


}


bool PDBFirstModelState::handleModelRecord()
{
    //It adds a frame
    (this->getBuilder())->getMolecularSystem()->addFrame("model");

    //It generates a new ModelsStatus
    PDBMolecularBuilderStateSP newModel(new PDBModelsState());
    bool isStatusSetted = (this->getBuilder())->setState(newModel);

    return (isStatusSetted);
}


