#ifndef PROXIMA_MOLECULARBUILDER_H
#define PROXIMA_MOLECULARBUILDER_H

#include "../molecularsystem.h"
#include "molecularbuilderstate.h"
#include "../parser.h"
#include "../aminoresidue.h"
#include "../nucleotideresidue.h"
#include "residues/residuebuilderstate.h"
#include "residues/aminoresiduebuilderstate.h"
#include "residues/nucleotideresiduebuilderstate.h"
#include "residues/genericresiduebuilderstate.h"

/**
 * @author Federico Lazzari
 */
namespace Proxima
{

/**
 * @brief The AlternateAtomData struct contains all those
 *        informations that could change between one alternate
 *        location to another.
 */
struct RecordData
{
    RecordData();

    /**
     * Copy constructor
     */
    RecordData(const RecordData& other);

    /**
     * operator ==
     */
    bool isEqual(const RecordData& other) const;

    /**
     * Serial number of the atom
     */
    long long serial;

    /**
     * Name of the atom
     */
    std::string name;

    /**
     * The atomic number of the atom
     */
    int atomicNumber;

    /**
     * Position of the atom
     */
    std::vector<double> position;

    /**
     * The charge of the atom.
     */
    double charge;

    /**
     * Temperature factor.
     */
    double temperature;

    /**
     * Occupancy factor.
     */
    double occupancy;

    /**
     * Alternate Location
     */
    std::string altLoc;

    bool isResidueValid;

    /**
     * The name of the residue
     */
    std::string residueName;

    /**
     * The sequence number of the residue
     */
    long long residueSequenceNumber;

    /**
     * The insertion code of the residue
     */
    char insertionCode;

    /**
     * The chainID of the fragment
     */
    char chainID;

};

/**
 * @brief The ResidueData struct handles the most relevant
 *        informations about the current residue or previous ones.
 */
struct PDBResidueData
{
    /**
     * Constructor
     */
    PDBResidueData();

    /**
     * Copy constructor
     */
    PDBResidueData(const PDBResidueData& other);

    /**
     * operator ==
     */
    bool isEqual(const PDBResidueData& other) const;

    /** A map associating each atom's role within the
    current residue with its serial number.**/
    std::multimap<std::string,long long> atoms;

    /**
     * The name of the residue.
     */
    std::string residueName;

    bool isResidueValid;

    /**
     * The sequence number of the residue.
     */
    long long residueSequenceNumber;

    /**
     * The insertion code of the residue.
     */
    char insertionCode;

    /**
     * The chainID of the fragment.
     */
    char chainID;

};


/**
 * @brief The MolecularBuilder class is a support class
 *        usefull for the proper functioning of the PDB Parser.
 *
 *        It handles the current state in a "state design pattern"
 *        for the PDB parser. The class defines two methods for
 *        the reading of the "MODEL" and "ATOM" records and updates
 *        its status accordingly.
 */
class PDBMolecularBuilder
{

public:
    /**
     * @brief The PDBMolecularBuilder constructor,
     *
     *        It automatically sets its state as
     *        FirstModelState.
     */
    PDBMolecularBuilder(std::string molSysName, double total_charge=0);

    //Destructor
    virtual ~PDBMolecularBuilder() = default;

    /**
     * @brief This function calls the current status to
     *        handle the MODEL record.
     *
     * @return true whether the parser can continue parsing lines
     *         false whether the parser must stop parsing lines.
     */
    virtual bool handleModelRecord();

    /**
     * @brief This function calls the current status to
     *        handle the ATOM/HETATM record.
     *
     * @return true whether the parser can continue parsing lines
     *         false whether the parser must stop parsing lines.
     */
    virtual bool handleAtomRecord(RecordData& atomInfo);

    /**
     * @brief This function is called by the parser once every
     *        line of the PDB file has been read.
     *
     *        It checks whether alternate locations have been found
     *        and in that case it adds new frames with their relative
     *        alternate location label and updates the new properties
     *        of the atoms for that alternate location.
     *
     * @return True whether the addition of the atom have been succesfull,
     *         false otherwise.
     */
    virtual bool addAltLocAtoms();

    /**
     * @brief getMolecularSystem
     * @return The molecular system of the following molecular builder
     */
    virtual MolecularSystemSP getMolecularSystem() const;

protected:
    friend class PDBMolecularBuilderState;
    friend class PDBAltLocState;
    friend class PDBFirstModelState;
    friend class PDBModelsState;
    friend class Parser;

    /**
     * @brief It sets a new state for the parser.
     * @param state The new state of the parser
     */
    virtual bool setState(PDBMolecularBuilderStateSP state);

    /**
     * @brief It reads the ATOM/HETATM records and generate
     *        a structure containing all the informations
     *        relative to the Atom.
     *
     * @param  record          The string of the PDB file to be parsed
     * @param  numLine         The number of the line within the PDB file
     *                         (It is necessary in order to print valuable warnings)
     * @param  out_atomInfo    The structure containing all the informations
     *                         extrapoleted from the PDB file.
     * @return True whether the parsing has been succesfull,
     *         false otherwise.
     */
    virtual bool parseAtomRecord(const std::string& record, unsigned numLine, RecordData &out_atomInfo);

    /**
     * @brief It creates a new atom with all the informations given
     *        by the RecordData structure.
     *
     * @param atomInfo         The AtomData structure containing all the informations
     *                         necessary in order to create the Atom object.
     * @return  True whether the creation of the Atom has been succesfull,
     *          false otherwise.
     */
    virtual bool createAtom(const RecordData& atomInfo);


    /**
     * @brief It updates all the relevant informations of an
     *        already created atom at a secific frame.
     *
     * @param frameNum      The frame number at which the atomic data
     *                      have to be updated.
     * @param atomInfo      The AtomData structure containing all the
     *                      new informaions to be updated.
     *
     * @return True whether the update of the Atom's informations has been succesfull,
     *         false otherwise.
     */
    virtual bool updateAtom(size_t frameNum, const RecordData& atomInfo);

    /**
     * @brief It adds a new alternate locations updating both encounteredAltLocList
     *        and altLocLabel_to_atomData.
     *
     * @param altLoc        The alternate location std::stringacters to add
     * @param atomData      The AtomData structure containing all the
     *                      new informaions to be updated.
     * @return True whether the addition of the new alternate location has been succesfull,
     *         false otherwise.
     */
    virtual bool addAltLoc(RecordData atomData);

    /**
     * @brief It sets the first alternate location
     * @param firstAltLoc The first alternate location encountered
     * @return True whether the addition of the alternate location has been succesfull,
     *         false otherwise.
     */
    virtual bool setFirstAltLoc(const std::string firstAltLoc);

    /**
     * @brief It handles the first residue ever encountered setting it as the currentResidue.
     */
    virtual bool setFirstResidue(const RecordData& atomInfo);

    /**
     * @brief It handles the current residue informations
     *        obtained by parsing the current line of the PDB.
     *
     *        If the residue is changed, it invokes the
     *        residue builder and creates the new residue.
     *
     *        Otherwise, it just adds the current atom informations
     *        to the current residue.
     */
    virtual bool checkResidue(RecordData& atomInfo);

    /**
     * @brief It returns the first alternate location ever encountered.
     */
    virtual std::string getFirstAltLoc();



protected:
    //FUNCTIONS CALLED BY RESIDUES BUILDERS
    friend class PDBResidueBuilder;
    friend class PDBAminoResidueBuilderState;
    friend class PDBGenericResidueBuilderState;
    friend class PDBResidueBuilderState;
    friend class PDBNucleotideResidueBuilderState;

    /**
     * @brief It returns the current residue.
     */
    virtual PDBResidueData getCurrentResidue();

    /**
     * @brief It returns the latest generated residue.
     */
    virtual PDBResidueData getPreviousResidue();

    /**
     * @brief It return the current fragment.
     */
    virtual FragmentSP getCurrentFragment();

    /**
     * @brief It sets the current fragment.
     */
    virtual void setCurrentFragment(FragmentSP frag);

    /**
     * @brief It sets the current residue state as newState.
     */
    virtual void setCurrentResidueState(PDBResidueBuilderStateSP newState);

    /**
     *        It handles the current residue structure in
     *        order to create a new residue object.
     *
     *        It first detects the type of the residue (e.g. AminoAcid),
     *        then it checks whether it is connected to previous residue
     *        or not.
     *
     *        If it is, it just add the new residue to the same fragment
     *        of the previous one.
     *
     *        If is not, it creates a new fragment and then it adds the
     *        current residue to the new fragment.
     */
    virtual bool handleNewResidue();

    /**
     *        It detects the type of the current residue
     *        e.g. whether is an AminoResiduo or a general residue.*/
    virtual void detectTypeOfResidue();

    /**
     * @brief It sets the current residue.
     */
    virtual void setCurrentResidue(PDBResidueData& resData);

protected:
    /**
     *@brief  It returns true whether the residue data given are compatible
     *        with those of an AminoResidue, false otherwise.
     *@note   It looks within the residue data for the atomic names of the backbone
     */
    virtual bool isAminoAcid(const PDBResidueData &resData);

    /**
     *@brief  It returns true whether the residue data given are compatible
     *        with those of an NucleotideResidue, false otherwise.
     *
     *@note   It looks within the residue data for the atomic names of the
     *        phoshpate and the sugar.
     */
    virtual bool isNucleotide(const PDBResidueData &resData);

private:
    /**
     * @brief The final Molecular System resulting from the parser of the PDB.
     */
    MolecularSystemSP ms;

    //ATOMS
    //---------

    /**
    * @brief The set of all the Alternate Location encountered in the PDB.
    */
   std::set<std::string> encounteredAltLocList;

   /**
    * @brief The first alternate location ever encountered during the parsing of the PDB.
    */
   std::string firstAltLoc;

   /**
    * @brief A multimap associating, for each atom, each alternate location
    *        to the informations about the same atom
    *        in that relative alternate location state.
    */
   std::multimap<std::string,RecordData> altLocLabel_to_atomData;

   /**
    * @brief Type definition for an iterator of a multimap
    */
   typedef std::multimap<std::string,RecordData>::iterator MMAPIterator;

   /**
    * @brief The latest atom generated while parsing the PDB (it is updated once a new atom is generated).
    */
   AtomSP latestAtomGenerated;
   //---------


   //RESIDUE
   //---------
   /**
    * @brief The current fragment (it is updated once a new fragment is found).
    */
   FragmentSP currentFragment;

   /**
    * @brief The latest residue generated (it is updated once a new residue is generated).
    */
   PDBResidueData latestResidueGenerated;

   /**
    * @brief The current residue (it is updated once a new residue is found).
    */
   PDBResidueData currentResidue;

   /**
    * @brief The current residue state object (e.g. AminoResidueState or NucleotideResidueState).
    */
   PDBResidueBuilderStateSP resState;
   //---------

   //CURRENT PARSER STATE
   //---------------------

   /**
    * @brief The current state of the parser while parsing the PDB (e.g. AltLocState, ModelsState, etc.).
    */
   PDBMolecularBuilderStateSP state;

   //---------------------

};


/**
 * @brief The shared_ptr type for the class MolecularBuilder
 */
typedef std::shared_ptr< Proxima::PDBMolecularBuilder > PDBMolecularBuilderSP;

}
#endif // MOLECULARBUILDER_H
