#ifndef PROXIMA_MODELSSTATUS_H
#define PROXIMA_MODELSSTATUS_H

#include "molecularbuilderstate.h"

/**
 * @author Federico Lazzari
 */
namespace Proxima
{

/**
 * @brief The ModelsStatus class is a support class
 *        usefull for the proper functioning of the PDB Parser.
 *
 *        It represents the state, during the parser of the PDB,
 *        where there isn't any alternate location and there are
 *        more frames declared.
 *
 */
class PDBModelsState : public PDBMolecularBuilderState
{
public:

    bool handleAtomRecord(RecordData &atomInfo);

    bool handleModelRecord();

};



}
#endif // MODELSSTATUS_H
