#ifndef PROXIMA_RESIDUE_H
#define PROXIMA_RESIDUE_H

#include "./utilities/WarningsOff.h"
#include <string>
#include <map>
#include "./utilities/WarningsOn.h"

#include "atom.h"


/**
 * @author Federico Lazzari
 */
namespace Proxima
{

//forward declarations
class MolecularSystem;
class Fragment;
class NucleotideResidue;
class AminoResidue;

/**
 * @brief The Residue class represents residues within the molecular system.
 */
class Residue
{

    /**
     * @brief const_atom_iterator
     *        An iterator to a Residue object.
     */
	typedef std::map<long long,int>::const_iterator const_atom_iterator;

private:

friend class NucleotideResidue;
friend class AminoResidue;

    /**
     * @brief The serial number of the residue.
     */
    long long _serial;

    /**
     * @brief The sequence number of the residue.
     */
    long long _sequenceNumber;

    /**
     * @brief The insertion code of the residue.
     */
    char _insertionCode;

    /**
    * @brief The name of the residue.
    */
    std::string _name;

    /**
     * @brief The reference fragment for the current residue.
     */
    Fragment* _parentFragment;

    /**
     * @brief This associates each atom serial to an integer representing its role within the residue.
     */
    std::map<long long,int> _atomsSerials;


public:

    /**
     * @brief Default name of a general residue
    */
    static const std::string DEFAULT_NAME;

    /**
     * @brief Constructor
     *
     * @param serial            The serial of of the residue.
     *
     * @param sequenceNumber    Sequence number of the residue within the
     *                          molecular fragment it belongs to.
     *
     * @param insertionCode     Insertion code of the residue
     *
     * @param name              The name of this residue.
     *                          Usually this name is not unique, since denotes its type.
     *                          If an empty string is passed as parameter,
     *                          Residue::DEFAULT_NAME is used instead.
     */
    Residue(long long serial, long long sequenceNumber, char insertionCode = 0,
                const std::string name=DEFAULT_NAME);


    /**
    * @brief Destructor
    */
    virtual ~Residue() {}

    virtual void clone(std::shared_ptr<Residue> &other);


    /**
     * @brief   Adds an atom to this residue.
     *
     *          The "role" parameter allows to define the specific role
     *          of this atom whithin the residue. The implementation provided
     *          by this class ignores this parameter, but it may be exploited
     *          by Residue's subclasses (e.g. Keeping track of the carbon-alpha
     *          in an amino-acid residue).
     *
     *          If one of the following conditions happens, the insertion fails
     *          and this method returns false:
     *          - The Residue already contains an atom with the same serial number.
     *
     *          - The Atom is not contained in the Molecular System of the Residue,
     *            or the Residue has not been previously added to some Molecular System.
     *
     *          - The atom can't be employed in the specific role.
     *
     * @param serial  The serial number of the atom to add to the residue.
     * @param role  Constant defining the specific role of this atom within the
     *              residue.
     * @return  True if the operation succeeds, false otherwise.
     *
     *
     */
    virtual bool addAtom(long long serial, int role=0);

    /**
     * @brief Implements an "isEqual" operator for residues.
     */
    bool isEqual(const Residue& res) const;

    /**
     * @brief Returns an iterator to the first <serial, role> couple of the residue where
     *          serial is the serial numer of the atom.
     *          role is the role of the atom within the residue.
     */
    const_atom_iterator getAtomBegin() const;

    /**
     * @brief Returns an end iterator to a <serial, role> couple of the residue where
     *          serial is the serial numer of the atom.
     *          role is the role of the atom within the residue.
     */
    const_atom_iterator getAtomEnd() const;

    /**
    * @brief Returns the name of the residue
    */
    virtual std::string getName() const;

    /**
     * @brief Returns true if the atom having the serial number passed as
     *        parameter is contained in the residue, false otherwise.
     */
    virtual bool contains(long long atomSerial) const;

    /**
     * @brief Returns the number of atoms contained in the residue.
     */
    virtual long long getNumAtoms() const;

    /**
     * @brief Returns the Serial of the Residue
     */
    virtual long long getSerial() const;

    /**
     * @brief Returns the sequence number of the Residue.
     */
    virtual long long getSequenceNumber() const;

    /**
     * @brief Returns the Insertion Code of the Residue
     */
    virtual char getInsertionCode() const;

    /**
     * @brief Returns the molecular fragment this residue belongs to.
     *          If no parent fragment is found, this function
     *          returns a nullptr.
     */
    virtual Fragment* getParentFragment() const;

    /**
     * @brief Returns the molecular system this residue belongs to.
     *          If no molecular system is found, this function
     *          returns a nullptr.
     */
    virtual MolecularSystem* getParentMolecularSystem() const;

    /**
     * @brief Overloads the "<" operator by comparing two residues
     *        according to their serial number.
     *        This operator is required by sorting algorithms and ordered
     *        data structures.
     */
    virtual bool operator<(const Residue& other) const;

    /**
     * @brief Overloads the ">" operator by comparing two residues
     *        according to their serial number.
     *        This operator is required by sorting algorithms and ordered
     *        data structures.
     */
    virtual bool operator>(const Residue& other) const;

protected:

    /**
    * Methods accessible only by subclasses and by the Fragment class.
    */
    friend class Fragment;

    /**
     * @brief Sets the molecular fragment this residue belongs to.
     */
    virtual void setParentFragment(Fragment* parentFragment);


};

/**
 * @brief ResidueSP is a smart pointer to a MolecularSystem object.
 */
typedef std::shared_ptr< Proxima::Residue > ResidueSP;


}

#endif // RESIDUE_H
