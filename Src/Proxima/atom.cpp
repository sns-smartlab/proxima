#include <iostream>
#include <memory>

#include "atom.h"

using namespace Proxima;


// Constructor and destructor
Atom::Atom(int atomicNum, const std::string &name, long long serial)
{
	_atomicNum = atomicNum;
	_name = name;
	_serial = serial;

	// Creates a default frame
    this->addFrame();
}

void Atom::clone(std::shared_ptr<Atom> &atm)
{
    this->_atomicNum = atm->_atomicNum;
    this->_name = atm->_name;
    this->_serial = atm->_serial;

    //Copio i frame dell'atomo
    Frame newFrame = atm->_frames[0];
    this->_frames[0] = newFrame;

    for (size_t i=1; i!=atm->getNumFrames(); i++)
    {
        Frame newFrame = atm->_frames[i];
        this->_frames.push_back(newFrame);
    }
}


//Atom::Atom(const Atom &atm)
//{
//    this->_atomicNum = atm._atomicNum;
//    this->_name = atm._name;
//    this->_serial = atm._serial;

//    //Copio i frame dell'atomo
//    for (size_t i=0; i!=atm.getNumFrames(); i++)
//    {
//        Frame newFrame = atm._frames[i];
//        this->_frames.push_back(newFrame);
//    }
//}


// "Get" methods
std::string Atom::getName() const
{
	return _name;
}

long long Atom::getSerial() const
{
	return _serial;
}

int Atom::getAtomicNum() const
{
    return _atomicNum;
}

std::vector<double> Atom::getPosition(size_t frameNum) const
{
    if(frameNum >= _frames.size())
        throw std::invalid_argument("Atom::getPosition - frame number out of range!");

    return _frames[frameNum]._position;
}

Atom::bond_iterator Atom::getBondBegin(size_t frameNum) const
{
	if(frameNum >= _frames.size())
        throw std::invalid_argument("Atom::getBondBegin - frame number out of range!");
    return _frames[frameNum]._bondsList.begin();
}

Atom::hbond_iterator Atom::getHBondBegin(size_t frameNum) const
{
	if(frameNum >= _frames.size())
        throw std::invalid_argument("Atom::getHBondBegin - frame number out of range!");
    return _frames[frameNum]._hbondsList.begin();
}

HBondSP Atom::getHBond(size_t id, size_t frameNum) const
{
    if(frameNum >= _frames.size())
        throw std::invalid_argument("Atom::getHBond - frame number out of range!");

    if(id >= this->getNumBonds(frameNum))
        throw std::invalid_argument("Atom::getHBond - bond index out of range!");

    auto it = std::next(this->_frames[frameNum]._hbondsList.begin(),id);
    return *it;
}

Atom::bond_iterator Atom::getBondEnd(size_t frameNum) const
{
	if(frameNum >= _frames.size())
        throw std::invalid_argument("Atom::getBondEnd - frame number out of range!");
    return _frames[frameNum]._bondsList.end();
}

BondSP Atom::getBond(size_t id, size_t frameNum) const
{
    if(frameNum >= _frames.size())
        throw std::invalid_argument("Atom::getBond - frame number out of range!");

    if(id >= this->getNumBonds(frameNum))
        throw std::invalid_argument("Atom::getBond - bond index out of range!");

    auto it = std::next(this->_frames[frameNum]._bondsList.begin(),id);
    return *it;
}

Atom::hbond_iterator Atom::getHBondEnd(size_t frameNum) const
{
	if(frameNum >= _frames.size())
        throw std::invalid_argument("Atom::getHBondEnd - frame number out of range!");
    return _frames[frameNum]._hbondsList.end();

}

size_t Atom::getNumFrames() const
{
	return _frames.size();
}

double Atom::getOccupancy(size_t frameNum) const
{
    if(frameNum >= _frames.size())
        throw std::invalid_argument("Atom::getOccupancy - frame number out of range!");

    return _frames[frameNum]._occupancy;
}

double Atom::getTemperatureFact(size_t frameNum) const
{
    if(frameNum >= _frames.size())
        throw std::invalid_argument("Atom::getTemperatureFact - frame number out of range!");

    return _frames[frameNum]._temperature;
}

double Atom::getCharge(size_t frameNum) const
{
	if(frameNum >= _frames.size())
        throw std::invalid_argument("Atom::getCharge - frame number out of range!");
    return  _frames[frameNum]._charge;
}

size_t Atom::getNumBonds(size_t frameNum) const
{
	if(frameNum >= _frames.size())
        throw std::invalid_argument("Atom::getNumBonds - frame number out of range!");
    return _frames[frameNum]._bondsList.size();
}

size_t Atom::getNumHBonds(size_t frameNum) const
{
	if(frameNum >= _frames.size())
        throw std::invalid_argument("Atom::getNumHBonds - frame number out of range!");
    return _frames[frameNum]._hbondsList.size();
}

bool Atom::isAtomAromatic(size_t frameNum) const
{
    return this->_frames[frameNum].isAromatic;
}

BondSP Atom::findBond(size_t frameNum, long long otherAtomSerial) const
{
	if(frameNum >= _frames.size())
		throw std::invalid_argument("Atom::findBond - frame number out of range!");

    for(const BondSP& bond : _frames[frameNum]._bondsList)
	{
		if( (bond->getMinor()==otherAtomSerial) || (bond->getHigher()==otherAtomSerial) )
            return bond;
	}

	return NULL;
}

HBondSP Atom::findHBond(size_t frameNum, long long atom1, long long atom2) const
{
	if(frameNum >= _frames.size())
		throw std::invalid_argument("Atom::findHBond - frame number out of range!");

    for(const HBondSP& hbond : _frames[frameNum]._hbondsList)
	{
		if( ( (hbond->getDonor() == atom1)    ||
			  (hbond->getAcceptor() == atom1) ||
			  (hbond->getHydrogen() == atom1)  )
			  &&
			( (hbond->getDonor() == atom2)    ||
			  (hbond->getAcceptor() == atom2) ||
			  (hbond->getHydrogen() == atom2)  )
			  &&
			  (atom1 != atom2)
		  )

            return hbond;
	}

	return NULL;
}




// "Set" methods
void Atom::setCharge(size_t frameNum, double charge)
{
	if(frameNum >= _frames.size())
        throw std::invalid_argument("Atom::setCharge - frame number out of range!");
    _frames[frameNum]._charge=charge;
}

void Atom::setOccupancy(size_t frameNum, double occupancy)
{
	if(frameNum >= _frames.size())
        throw std::invalid_argument("Atom::setOccupancy - frame number out of range!");
    _frames[frameNum]._occupancy=occupancy;
}

void Atom::setTemperatureFact(size_t frameNum, double temperature)
{
	if(frameNum >= _frames.size())
        throw std::invalid_argument("Atom::setTemperatureFact - frame number out of range!");
    _frames[frameNum]._temperature=temperature;
}

bool Atom::isEqual(const Atom& other) const
{
	if(this->getSerial()   == other.getSerial()    &&
	   this->getAtomicNum()== other.getAtomicNum() &&
	   this->getName()     == other.getName()) return true;
	else return false;
}





// Only the MolecularSystem can call the following methods

void Atom::addFrame()
{
	if(_frames.empty()) _frames.push_back(Atom::Frame());
    else _frames.push_back(_frames.at(_frames.size()-1));
}

void Atom::setPlanarity(bool value, size_t frame)
{
    this->_frames[frame].isAromatic=value;
}

void Atom::addBond(size_t frameNum, BondSP bond)
{
	if(frameNum >= _frames.size())
		throw std::invalid_argument("Atom::addBond - frame number out of range!");
	if ((bond->getMinor()!= this->getSerial()) && (bond->getHigher() != this->getSerial()))
		throw std::invalid_argument("Atom::addBond - Atom is not part of this bond!");

    _frames[frameNum]._bondsList.push_back(bond);
}

void Atom::addHBond(size_t frameNum, HBondSP hydrogenBond)
{
	if(frameNum >= _frames.size())
		throw std::invalid_argument("Atom::addHBond - frame number out of range!");
	if ((hydrogenBond->getDonor() != this->getSerial()) && (hydrogenBond->getAcceptor() != this->getSerial()) && (hydrogenBond->getHydrogen() != this->getSerial()))
		throw std::invalid_argument("Atom::addHBond - Atom is not part of this bond!");

    _frames[frameNum]._hbondsList.push_back(hydrogenBond);

}

void Atom::removeBond(size_t frameNum, BondSP bond)
{
	if(frameNum >= _frames.size())
		throw std::invalid_argument("Atom::removeBond - frame number out of range!");

    _frames[frameNum]._bondsList.remove(bond);
}

void Atom::removeHBond(size_t frameNum, HBondSP hydrogenBond)
{
	if(frameNum >= _frames.size())
		throw std::invalid_argument("Atom::removeHBond - frame number out of range!");

    _frames[frameNum]._hbondsList.remove(hydrogenBond);
}

void Atom::addHybridisation(size_t frameNum, const Hybridisation &hybridisation)
{
    if(frameNum >= _frames.size())
        throw std::invalid_argument("Atom::addHybridisation - frame number out of range!");

    _frames[frameNum]._hybridisation = hybridisation;
}

void Atom::setHybridisation(size_t frameNum, const Hybridisation &hybridisation)
{
    if(frameNum >= _frames.size())
        throw std::invalid_argument("Atom::addHybridisation - frame number out of range!");

    Hybridisation h;
    if (!(this->_frames[frameNum]._hybridisation.isEqual(h)))
        return;

    _frames[frameNum]._hybridisation = hybridisation;
}

Hybridisation Atom::getHybridisation(size_t frameNum) const
{
    if(frameNum >= _frames.size())
        throw std::invalid_argument("Atom::getHybridisation(size_t frameNum) - frame number out of range!");

    return _frames[frameNum]._hybridisation;
}

void Atom::setPosition(size_t frameNum, const std::vector<double> &newPos)
{
    if(frameNum >= _frames.size())
        throw std::invalid_argument("Atom::setPosition - frame number out of range!");

    _frames[frameNum]._position = newPos;
}






//Atom::Atom(Atom&& other)
//{
//	_atomicNum = other._atomicNum;
//	other._atomicNum = 0;

//	_serial = other._serial;
//	other._serial = 0;

//	_name = std::move(other._name);

//	_framesList = std::move(other._framesList);
//}

//Atom&Atom::operator=(Atom&& other)
//{
//	if (this != &other)
//	{
//		/* Reset */
//		_atomicNum = 0;
//		_framesList.clear();
//		_name.clear();
//		_serial=0;

//		/* Move */
//		_atomicNum = other._atomicNum;
//		other._atomicNum = 0;

//		_serial = other._serial;
//		other._serial = 0;

//		_name = std::move(other._name);

//		_framesList = std::move(other._framesList);

//	}
//	return *this;
//}
