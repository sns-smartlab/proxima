#include "residue.h"
#include "molecularsystem.h"
#include "fragment.h"



using namespace Proxima;

const std::string Residue::DEFAULT_NAME= " ";

Residue::Residue(long long serial, long long sequenceNumber, char insertionCode,
                 const std::string name)
{
    _serial = serial;
    _sequenceNumber = sequenceNumber;
    _insertionCode = insertionCode;
    _name = name;

    // setParentFragment can change this
    _parentFragment = nullptr;
}

void Residue::clone(std::shared_ptr<Residue> &other)
{
    this->_serial = other->_serial;
    this->_sequenceNumber = other->_sequenceNumber;
    this->_insertionCode = other->_insertionCode;
    this->_name = other->_name;
    this->_atomsSerials = other->_atomsSerials;
}


bool Residue::addAtom(long long serial, int role)
{
    MolecularSystem *molsys = this->getParentMolecularSystem();
    if (molsys==nullptr)
    {
        std::cout<<"Warning! Residue::addAtom - First add the residue to a Fragment"<<std::endl;
        return false;
    }

    if (!molsys->containsAtom(serial))
    {
        std::cout<<"Warning! Residue::addAtom - First add the atom to the molecular system"<<std::endl;
        return false;
    }

	if (contains(serial)) return false;

	_atomsSerials.insert({serial,role});
    return true;
}

bool Residue::isEqual(const Residue &res) const
{
    if(this->_atomsSerials   == res._atomsSerials    &&
       this->_name == res._name                      &&
       this->_insertionCode   == res._insertionCode  &&
       this->_serial  == res._serial) return true;
    else return false;
}

Residue::const_atom_iterator Residue::getAtomBegin() const
{
    return _atomsSerials.begin();
}

Residue::const_atom_iterator Residue::getAtomEnd() const
{
    return _atomsSerials.end();
}

std::string Residue::getName() const
{
    return _name;
}

bool Residue::contains(long long atomSerial) const
{
    if (_atomsSerials.find(atomSerial)!=_atomsSerials.end())
        return true;
    return false;
}

long long Residue::getNumAtoms() const
{
    return long(_atomsSerials.size());
}

long long Residue::getSerial() const
{
    return _serial;
}

long long Residue::getSequenceNumber() const
{
    return _sequenceNumber;
}

char Residue::getInsertionCode() const
{
    return _insertionCode;
}

Fragment *Residue::getParentFragment() const
{
    return _parentFragment;
}

bool Residue::operator<(const Residue &other) const
{
    if(this->_serial < other.getSerial()) return true;
    else return false;
    //if(this->_insertionCode < other.getInsertionCode()) return true;
    //if(this->_insertionCode > other.getInsertionCode()) return false;
    //return false;
}

bool Residue::operator>(const Residue &other) const
{
    if(this->_serial > other.getSerial()) return true;
    else return false;
}

MolecularSystem *Residue::getParentMolecularSystem() const
{
    if (this->_parentFragment==nullptr) return nullptr;
    return _parentFragment->getParentMolecularSystem();
}

void Residue::setParentFragment(Fragment *parentFragment)
{
    _parentFragment = parentFragment;
}
