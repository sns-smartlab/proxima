#ifndef PROXIMA_FORCEFIELDPARAMETERS_H
#define PROXIMA_FORCEFIELDPARAMETERS_H

#include <map>

namespace Proxima {

class ForceFieldParameters
{
public:
    ForceFieldParameters();
    virtual ~ForceFieldParameters()=default;

    virtual double getHarmonicForceConstant(int atom1, int atom2, double dist);

    virtual double getCubicForceConstant(int atom1, int atom2, double dist);

    virtual double getQuarticForceConstant(int atom1, int atom2, double dist);

protected:

    virtual double getA(int atom1, int atom2, int fc);
    virtual double getB(int atom1, int atom2, int fc);


private:
    std::map<std::pair<int,int>,std::pair<double,double>> f2;
    std::map<std::pair<int,int>,std::pair<double,double>> f3;
    std::map<std::pair<int,int>,std::pair<double,double>> f4;
};


}


#endif // FORCEFIELDPARAMETERS_H
