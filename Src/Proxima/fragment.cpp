#include "fragment.h"
#include <typeinfo>
#include <iostream>
using namespace Proxima;


const std::string Fragment::DEFAULT_NAME = " ";

Fragment::Fragment(char chainID, const std::string &name)
{
    this->_parentMolSys = NULL;
    this->_name = name;
    this->_chainID = chainID;
}

void Fragment::clone(std::shared_ptr<Fragment> &other)
{
    this->_name = other->_name;
    this->_chainID = other->_chainID;

    //COPIO TUTTI I RESIDUI
    for (auto it = other->_residues.begin(); it != other->_residues.end(); it++)
    {
        ResidueSP second = it->second;
        ResidueSP newResidue(new Residue(0,0,0,""));
        newResidue->clone(second);
        this->addGenericResidue(newResidue);
    }

    //COPIO TUTTI I NUCLEOTIDI
    for (auto it = other->_nucleotideResidues.begin(); it != other->_nucleotideResidues.end(); it++)
    {
        NucleotideResidueSP second = it->second;
        NucleotideResidueSP newResidue(new NucleotideResidue(0,0,0,""));
        newResidue->clone(second);
        this->addNucleotideResidue(newResidue);
    }

    //COPIO TUTTI GLI AMINO RESIDUI
    for (auto it = other->_aminoResidues.begin(); it != other->_aminoResidues.end(); it++)
    {
        AminoResidueSP second = it->second;
        AminoResidueSP newResidue(new AminoResidue(0,0,0,""));
        newResidue->clone(second);
        this->addAminoResidue(newResidue);
    }
}


bool Fragment::isEqual(const Fragment &other) const
{
    if(this->getName()   == other.getName()    &&
       this->getChainID() == other.getChainID() &&
       this->getNumberOfAtoms()     == other.getNumberOfAtoms()) return true;
    else return false;
}

std::string Fragment::getName() const
{
    return this->_name;
}

char Fragment::getChainID() const
{
    return this->_chainID;
}

bool Fragment::containsResidue(long long sequenceNumber, char insertionCode) const
{
    std::pair<long long,char> test(sequenceNumber,insertionCode);

    if ((_residues.find(test)!=_residues.end())                         ||
        (_aminoResidues.find(test)!=_aminoResidues.end())               ||
        (_nucleotideResidues.find(test) != _nucleotideResidues.end())) return true;

    return false;
}

ResidueSP Fragment::getGenericResidue(long long sequenceNumber, char insertionCode) const
{
    std::pair<long long,char> test(sequenceNumber,insertionCode);
    ResidueSP it=_residues.at(test);

    if (it!=nullptr) return it;

    return ResidueSP();
}

AminoResidueSP Fragment::getAminoResidue(long long sequenceNumber, char insertionCode) const
{
    std::pair<long long,char> test(sequenceNumber,insertionCode);
    try {
        AminoResidueSP it=_aminoResidues.at(test);
        return it;
    } catch (...) {
        return AminoResidueSP();
    }
}

NucleotideResidueSP Fragment::getNucleotideResidue(long long sequenceNumber, char insertionCode) const
{
    std::pair<long long,char> test(sequenceNumber,insertionCode);
    NucleotideResidueSP it = _nucleotideResidues.at(test);

    if (it!=nullptr) return it;

    return NucleotideResidueSP();
}

bool Fragment::addGenericResidue(ResidueSP residue)
{
    if(residue==nullptr) return false;

    if(this->getParentMolecularSystem()==nullptr)
    {
        std::cout<<"Warning! Fragment::addResidue - First add the Fragment to a Molecular System"<<std::endl;
        return false;
    }

    if (this->containsResidue(residue->getSequenceNumber(), residue->getInsertionCode()))
		return false;

    //The residue is a component of another fragment
    if (residue->getParentFragment()!=nullptr) return false;

    std::pair<long long, char> key(residue->getSequenceNumber(),residue->getInsertionCode());

    std::pair<std::pair<long long, char>,ResidueSP> couple(key,residue);

    this->_residues.insert(couple);

    residue->setParentFragment(this);

    return true;
}

bool Fragment::addAminoResidue(AminoResidueSP residue)
{

    if(residue==nullptr) return false;

    if(this->getParentMolecularSystem()==nullptr)
    {
        std::cout<<"Warning! Fragment::addAminoResidue - First add the Fragment to a Molecular System"<<std::endl;
        return false;
    }

    if (this->containsResidue(residue->getSequenceNumber(), residue->getInsertionCode()))
        return false;

    //The residue is a component of another fragment
    if (residue->getParentFragment()!=nullptr) return false;

    std::pair<long long, char> key(residue->getSequenceNumber(),residue->getInsertionCode());

    std::pair<std::pair<long long, char>,AminoResidueSP> couple(key,residue);

    this->_aminoResidues.insert(couple);

    residue->setParentFragment(this);

    return true;
}

bool Fragment::addNucleotideResidue(NucleotideResidueSP residue)
{

    if(residue==nullptr) return false;

    if(this->getParentMolecularSystem()==nullptr)
    {
        std::cout<<"Warning! Fragment::addNucleotideResidue - First add the Fragment to a Molecular System"<<std::endl;
        return false;
    }

    if (this->containsResidue(residue->getSequenceNumber(), residue->getInsertionCode()))
        return false;

    //The residue is a component of another fragment
    if (residue->getParentFragment()!=nullptr) return false;

    std::pair<long long, char> key(residue->getSequenceNumber(),residue->getInsertionCode());

    std::pair<std::pair<long long, char>,NucleotideResidueSP> couple(key,residue);

    this->_nucleotideResidues.insert(couple);

    residue->setParentFragment(this);

    return true;
}

int Fragment::getNumberOfResidues() const
{
    int residues = int(this->_residues.size());
    int aminoResidues = int(this->_aminoResidues.size());
    int nucleotideResidues = int(this->_nucleotideResidues.size());
    return (residues+aminoResidues+nucleotideResidues);
}

int Fragment::getNumberOfGenericResidues() const
{
    return int(this->_residues.size());
}

int Fragment::getNumberOfAminoResidues() const
{
   return int(this->_aminoResidues.size());
}

int Fragment::getNumberOfNucleotideResidues() const
{
   return int(this->_nucleotideResidues.size());
}

long long Fragment::getNumberOfAtoms() const
{
    long long atoms=0;

    for (auto it=this->_residues.begin(); it!=this->_residues.end(); it++)
    {
        ResidueSP residue = it->second;
        atoms = atoms + residue->getNumAtoms();
    }

    for (auto it=this->_aminoResidues.begin(); it!=this->_aminoResidues.end(); it++)
    {
        AminoResidueSP residue = it->second;
        atoms = atoms + residue->getNumAtoms();
    }

    for (auto it=this->_nucleotideResidues.begin(); it!=this->_nucleotideResidues.end(); it++)
    {
        NucleotideResidueSP residue = it->second;
        atoms = atoms + residue->getNumAtoms();
    }


    return atoms;
}

Fragment::const_genRes_iter Fragment::getGenericResBegin() const
{
    return this->_residues.begin();
}

Fragment::const_genRes_iter Fragment::getGenericResEnd() const
{
    return this->_residues.end();
}

Fragment::const_aminoRes_iter Fragment::getAminoResBegin() const
{
    return this->_aminoResidues.begin();
}

Fragment::const_aminoRes_iter Fragment::getAminoResEnd() const
{
    return this->_aminoResidues.end();
}

Fragment::const_nucleotideRes_iter Fragment::getNucleotideResBegin() const
{
    return this->_nucleotideResidues.begin();
}

Fragment::const_nucleotideRes_iter Fragment::getNucleotideResEnd() const
{
    return this->_nucleotideResidues.end();
}

MolecularSystem *Fragment::getParentMolecularSystem() const
{
    return this->_parentMolSys;
}

//TODO: Inserire un controllo che quando ho già cominciato ad aggiungere residui non posso piu cambiare il sistema molecolare?
void Fragment::setParentMolecularSystem(MolecularSystem *parentMolSys)
{
    this->_parentMolSys = parentMolSys;
}
