# Core C++

Contains the C++ source code of Proxima.

##Qt
We suggest to use the QtCreator suit (https://www.qt.io/product) for compiling the code.

* First execute the _setup.py_ script from shell:
```
python setup.py
```
The script will ask you for the absolute path of the Eigen linear algebra library (http://eigen.tuxfamily.org) that tis required by Proxima.

* Then you can open the ProximaPrj.pro file with QtCreator and compile the library


