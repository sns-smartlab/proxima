import sys

print("Absolute path of the eigen folder: ")
eigenPath = sys.stdin.readline()

outFile = open("Proxima/Proxima.pri", "w")

outFile.write("### SOURCE CODE ###\n# Eigen (http://eigen.tuxfamily.org)\n")
outFile.write("INCLUDEPATH += " + str(eigenPath)+"\n")
outFile.write("DEPENDPATH  += " + str(eigenPath)+"\n")
outFile.write("\n")
outFile.write("QMAKE_CXXFLAGS += -fopenmp\nQMAKE_LFLAGS += -fopenmp\nQMAKE_CXXFLAGS_DEBUG += -O0\nQMAKE_CXXFLAGS_RELEASE += -O2\nSOURCES += \ \n $$PWD/bond.cpp \ \n $$PWD/hbond.cpp \ \n $$PWD/atom.cpp \ \n $$PWD/molecularsystem.cpp \ \n $$PWD/parser.cpp \ \n $$PWD/atomicproperties.cpp \ \n $$PWD/atomsgrid.cpp \ \n $$PWD/hbinteraction.cpp \ \n $$PWD/utilities/utilities.cpp \ \n $$PWD/utilities/floating_point.cpp \ \n $$PWD/aminoresidue.cpp \ \n $$PWD/fragment.cpp \ \n $$PWD/residue.cpp \ \n $$PWD/nucleotideresidue.cpp \ \n $$PWD/parser/molecularbuilder.cpp \ \n $$PWD/parser/molecularbuilderstate.cpp \ \n $$PWD/parser/altlocstate.cpp \ \n $$PWD/parser/firstmodelstate.cpp \ \n $$PWD/parser/modelsstate.cpp \ \n $$PWD/parser/residues/aminoresiduebuilderstate.cpp \ \n $$PWD/parser/residues/genericresiduebuilderstate.cpp \ \n $$PWD/parser/residues/nucleotideresiduebuilderstate.cpp \ \n $$PWD/parser/residues/residuebuilderstate.cpp \ \n $$PWD/sssrgenerator.cpp \ \n $$PWD/ringgenerator.cpp \ \n $$PWD/gasteigerparameters.cpp \ \n $$PWD/utilities/cubegrid.cpp \ \n $$PWD/boparameters.cpp \nHEADERS += \ \n $$PWD/bond.h \ \n $$PWD/hbond.h \ \n $$PWD/atom.h \ \n $$PWD/molecularsystem.h \ \n $$PWD/parser.h \ \n $$PWD/atomicproperties.h \ \n $$PWD/atomsgrid.h \ \n $$PWD/hbinteraction.h \ \n $$PWD/utilities/utilities.h \ \n $$PWD/utilities/floating_point.h \ \n $$PWD/aminoresidue.h \ \n $$PWD/nucleotideresidue.h \ \n $$PWD/fragment.h \ \n $$PWD/residue.h \ \n $$PWD/parser/molecularbuilder.h \ \n $$PWD/parser/molecularbuilderstate.h \ \n $$PWD/parser/altlocstate.h \ \n $$PWD/parser/firstmodelstate.h \ \n $$PWD/parser/modelsstate.h \ \n $$PWD/parser/residues/aminoresiduebuilderstate.h \ \n $$PWD/parser/residues/genericresiduebuilderstate.h \ \n $$PWD/parser/residues/nucleotideresiduebuilderstate.h \ \n $$PWD/parser/residues/residuebuilderstate.h \ \n $$PWD/sssrgenerator.h \ \n $$PWD/ringgenerator.h \ \n $$PWD/gasteigerparameters.h \ \n $$PWD/utilities/cubegrid.h \ \n $$PWD/boparameters.h")

outFile.close()
